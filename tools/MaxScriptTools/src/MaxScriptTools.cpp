#include <iostream>

#include "SceneParser.h"

void BuildSceneInstancing(const std::string & basePath, const std::string & filePath);
void CreateInstancedMaterials(const std::string & folderWhereMaterialsAreAt);

int main(int argc, char *argv[])
{
	// Debugging.
	//std::string xd = std::string("C:/Users/Gabi/Documents/Visual Studio 2017/Projects/Tabernet/bin/data/scenes/ra.json");
	//SceneParser().start("C:/Users/Gabi/Documents/Visual Studio 2017/Projects/Tabernet", xd);

	// For now we only have one argument for our tool. This is to parse our scene.json
	// and get all the prefabs in it.
	if (argc < 3) {
		std::cout << "You didn't provide enough arguments.\n";
		return -1;
	}

	std::string command = argv[1];

	// We want to build our scene, i.e parse the json file to prepare all
	// other necessary jsons.
	if (command == "-b")
		BuildSceneInstancing(argv[2], argv[3]);
	else if (command == "-d")
		CreateInstancedMaterials(argv[2]);
}


typedef std::string PrefabPath;

void BuildSceneInstancing(const std::string & basePath, const std::string & filePath) {
	std::cout << "Building scene instancing.\n";
	std::cout << basePath << "\n";
	std::cout << filePath << "\n";

	SceneParser().start(basePath, filePath);
}

void CreateInstancedMaterials(const std::string & folderWhereMaterialsAreAt) {
	std::cout << "Creating instancing materials.\n";
	std::cout << "Path to search in: " << folderWhereMaterialsAreAt << "\n";

	SceneParser().parseMaterials(folderWhereMaterialsAreAt);
}

