#pragma once
#include <regex>

#define	INT_REGEX						std::string("[0-9]+")
#define	FLOAT_REGEX					std::string("[0-9]*[\\.][0-9]+") + "|" + INT_REGEX
#define	BOOL_REGEX					std::string("[tT][rR][uU][eE]|[fF][aA][lL][sS][eE]")
#define VALUE_REGEX					FLOAT_REGEX + "|" + BOOL_REGEX
#define	VARIABLE_REGEX			std::string("[a-zA-Z][a-zA-Z0-9]*")
#define	OPERATOR_REGEX			std::string("==|<=|>=|>|<|!=")
#define FUNCTION_NAME_REGEX std::string("[a-zA-Z][a-zA-Z0-9]*")
//#define	FUNCTION_REGEX			FUNCTION_NAME_REGEX + "[(].*[)]"
#define NEGATION_REGEX			std::string("[!]?")
#define	SPACE_REGEX					std::string("[ ]*")
#define REGEX_GROUP(regex)	"(" + regex + ")"			