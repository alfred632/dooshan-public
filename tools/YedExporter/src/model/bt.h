#pragma once
#include "export_resource.h"
#include <unordered_set>

#define INT_VAR_TYPE				"int"
#define FLOAT_VAR_TYPE			"float"
#define BOOL_VAR_TYPE				"bool"

#define SEQUENCE_NODE_TYPE	"sequence"
#define PRIORITY_NODE_TYPE	"priority"
#define RANDOM_NODE_TYPE		"random"
#define DECORATOR_NODE_TYPE	"decorator"
#define ACTION_NODE_TYPE		"action"

class CBehaviourTree {
public:
	// data model
	struct TNode {
		std::string id;
		std::string name;
		std::string type;
		double xPos;
		json params;
	};
	struct TTransition {
		TNode * source;
		TNode * target;
		std::string sourceName;
		std::string targetName;
		json params;
	};
	struct TVariable {
		std::string name;
		std::string type;
		json value;
	};

	std::string name;
	TNode rootNode;

	// hashset to make sure no node is repeated
	std::unordered_set<std::string> namesMap;

	std::map<std::string, TNode> nodes;
	std::list<TTransition> transitions;
	std::map<std::string, TVariable> variables;

	//TState* getStateById(const std::string& id);
};

class CBTGroup : public IExportResource  {
public:
	bool loadXML(const std::string& filename, const std::string& outputPath, const std::string& outExtension) override;
	bool saveJSON(const std::string & directory, const std::string & extension) override;
private:
	std::map<std::string, std::string> _nodeColorTypes{
		{"#CCFFCC", SEQUENCE_NODE_TYPE},
		{"#FF9999", PRIORITY_NODE_TYPE},
		{"#00CCFF", RANDOM_NODE_TYPE},
		{"#FFFF99", DECORATOR_NODE_TYPE},
		{"#FFCC00", ACTION_NODE_TYPE}
	};
	std::map<std::string, std::string> _variableColorTypes{
		{"#CCFFCC", INT_VAR_TYPE},
		{"#FF9999", FLOAT_VAR_TYPE},
		{"#00CCFF", BOOL_VAR_TYPE}
	};

	std::vector<CBehaviourTree> _btGroups;

	bool processNode(CBehaviourTree::TNode & outNode, tinyxml2::XMLElement * XMLnode, CBehaviourTree & bt);
	bool processTransition(CBehaviourTree::TTransition & outTransition, tinyxml2::XMLElement * XMLnode, CBehaviourTree & bt);
	bool processVariable(CBehaviourTree::TVariable & outVariable, tinyxml2::XMLElement * XMLnode);

};
