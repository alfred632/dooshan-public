#include "mcv_platform.h"
#include "comp_arena3.h"
#include "entity/entity_parser.h"
#include "engine.h"

#include "components/common/comp_name.h"
#include "components/common/comp_tags.h"
#include "components/common/comp_health.h"

DECL_OBJ_MANAGER("arena_3", TCompArena3);

void TCompArena3::load(const json& j, TEntityParseContext& ctx) {
	enemiesToSpawn = j.value("enemies_to_spawn", enemiesToSpawn);
	timeBetweenSpawns = j.value("time_between_spawns", timeBetweenSpawns);
	maxNumEnemiesAlive = j.value("max_enemies_alive", maxNumEnemiesAlive);
	doors.reserve(2);
	enemies.reserve(enemiesToSpawn);

	enemiesToSpawn = 15;

	// for now we hardcode them as many of these things will probably end up scripted.
	int numSpawns = 10;
	spawnNames.reserve(numSpawns);
	spawns.reserve(numSpawns);
	spawnNames.push_back("SpawnArena3");
	for (int i = 1; i < numSpawns + 1; i++)
		spawnNames.push_back("SpawnArena3-" + std::to_string(i));

}

void TCompArena3::debugInMenu() {
	ImGui::DragInt("Enemies to spawn: ", &enemiesToSpawn, 0, 0, 10000);
	ImGui::DragFloat("Time between spawns: ", &timeBetweenSpawns, 0.1f, 0.f, 10000.f);
	ImGui::DragInt("Max Number of Enemies Alive: ", &maxNumEnemiesAlive, 0, 0, 10000);
}

void TCompArena3::registerMsgs() {
	DECL_MSG(TCompArena3, TMsgEntityTriggerEnter, onTriggerEnter);
}

void TCompArena3::update(float dt) {
	if (!active) return;

	// Erase dead enemies.
	checkIfAnyEnemiesDied();

	// On spawn time runs out, spawn.
	if (enemiesToSpawn > 0) {
		currentTime += dt;
		if (currentTime > timeBetweenSpawns && maxNumEnemiesAlive > currentEnemiesAlive) {
			currentTime = 0.0f;
			SpawnEnemies();
		}
	}
	else if (currentEnemiesAlive == 0) // All enemies are spawned and dead, and demon is spawned and dead.
		endArena();
}

void TCompArena3::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	if (active) return;

	CEntity * entity = msg.h_entity;
	TCompTags * tags = entity->getComponent<TCompTags>();
	if (tags && tags->hasTag(Utils::getID("player"))) {
		active = true;
		createDoors();
		fetchSpawns();
		// Spawn some enemies with the demon.
		for (int i = 0; i < 6; i++)
			SpawnEnemies();

		// Don't spawn for now.
		//SpawnDemon();
	}
}

void TCompArena3::onSpawnStatus(const TMsgSpawnStatus & msg) {
	if (msg.occupied)
		removeSpawnFromFreeOnes(msg.spawnEntityHandle);
	else
		addSpawnToFreeOnes(msg.spawnEntityHandle);
}

void TCompArena3::addSpawnToFreeOnes(CHandle spawn) {
	freeSpawns.push_back(spawn);
}

void TCompArena3::removeSpawnFromFreeOnes(CHandle spawn) {
	for (auto spawnIt = freeSpawns.begin(); spawnIt != freeSpawns.end(); spawnIt++) {
		if (spawn == (*spawnIt)) {
			freeSpawns.erase(spawnIt);
			break;
		}
	}
}

void TCompArena3::createDoors() {
	TEntityParseContext ctx;
	parseScene("data/prefabs/arenas/arena3doors.json", ctx);

	for (auto & ent : ctx.entities_loaded)
		doors.push_back(ent);
}

void TCompArena3::fetchSpawns() {
	// Name of the arena to send to spawns so they know their owner.
	TCompName * arenaName = getComponent<TCompName>();
	if (arenaName == nullptr)
		Utils::fatal("Arena doesn't have a name!");
	std::string sArenaName = arenaName->getName();

	/* Now fetch all spawns. */
	for (auto & name : spawnNames) {
		CHandle spawnHand = getEntityByName(name);
		if (!spawnHand.isValid())
			Utils::fatal("Spawn with name %s not valid.\n", name.c_str());
		CEntity * ent = spawnHand;
		TCompSpawn * spawnComp = ent->getComponent<TCompSpawn>();
		if (spawnComp == nullptr)
			Utils::fatal("Spawn with name %s doesn't have spawn component.\n", name.c_str());
		spawnComp->setSpawnOwner(sArenaName);
		spawns.push_back(spawnHand);
		freeSpawns.push_back(spawnHand);
	}
}

TCompSpawn * TCompArena3::pickRandomSpawn() {
	int numFreeSpawns = static_cast<int>(freeSpawns.size());
	TCompSpawn * spawn = nullptr;
	if (numFreeSpawns > 0) {
		int spawnIndx = RNG.i(static_cast<int>(numFreeSpawns));
		CEntity * spawnEnt = freeSpawns[spawnIndx];
		spawn = spawnEnt->getComponent<TCompSpawn>();
	}
	return spawn;
}

void TCompArena3::SpawnEnemies() {
	TCompSpawn * spawn = pickRandomSpawn();
	enemies.push_back(spawn->spawnRandomEnemy());
	enemiesToSpawn--;
	currentEnemiesAlive++;
}

void TCompArena3::SpawnDemon() {
	CEntity * ent = spawns[spawns.size()-1]; // Spawn 10 in the middle of the room.
	TCompSpawn * spawn = ent->getComponent<TCompSpawn>();
	demonHandle = spawn->spawnDemon();
}

void TCompArena3::checkIfAnyEnemiesDied() {
	auto endIt = std::remove_if(enemies.begin(), enemies.end(), [this](const CHandle & enemyHandle) {
		if (enemyHandle.isValid()) {
			if (this->checkIfEnemyDead((enemyHandle))) {
				this->currentEnemiesAlive--;
				return true;
			}
			return false;
		}
		this->currentEnemiesAlive--;
		return true;
	});
	enemies.erase(endIt, enemies.end());
}

bool TCompArena3::checkIfEnemyDead(CEntity * e) {
	TCompHealth * health = e->getComponent<TCompHealth>();
	if (health)
		return health->isDead();
	return false;
}

void TCompArena3::endArena() {
	// Destroy doors.
	for (auto & door : doors)
		door.destroy();

	// Destroy the spawns.
	for (auto & spawn : spawns)
		spawn.destroy();

	CHandle(this).destroy(); // We destroy the trigger.
}