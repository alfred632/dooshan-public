#include "mcv_platform.h"
#include "comp_arena_trap.h"
#include "components\common\comp_tags.h"
#include "entity/entity_parser.h"
#include "components\arena\comp_final_trigger.h"
#include "components\ui\comp_win_ui.h"

DECL_OBJ_MANAGER("final_trigger", TCompFinalTrigger);

void TCompFinalTrigger::load(const json& j, TEntityParseContext& ctx) {}

void TCompFinalTrigger::debugInMenu() {}

void TCompFinalTrigger::registerMsgs() {
	DECL_MSG(TCompFinalTrigger, TMsgEntityTriggerEnter, onTriggerEnter);
}

void TCompFinalTrigger::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	CHandle entityEntered = msg.h_entity;
	CEntity * entity = entityEntered;
	TCompTags * tags = entity->getComponent<TCompTags>();
	if (tags->hasTag(Utils::getID("player"))) {
		// Send message to ui to show win.
		TMsgWinMessageUI msg = { true };
		CEntity * UI = getEntityByName("HUD");
		UI->sendMsg(msg);
	}
}