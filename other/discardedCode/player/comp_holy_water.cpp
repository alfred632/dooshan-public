#include "mcv_platform.h"
#include "comp_holy_water.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "comp_corruption_controller_player.h"

DECL_OBJ_MANAGER("holy_water", TCompHolyWater);

void TCompHolyWater::registerMsgs() {
	DECL_MSG(TCompHolyWater, TMsgFullRestore, onFullyRestored);
	DECL_MSG(TCompHolyWater, TMsgToogleComponent, onToggleComponent);
}

void TCompHolyWater::declareInLua() {
	EngineScripting.declareComponent<TCompHolyWater>
		(
			"TCompHolyWater",
			"getCurrentCharges", &TCompHolyWater::getCurrentCharges,
			"setCurrentCharges", &TCompHolyWater::setCurrentCharges,
			"getMaxCharges", &TCompHolyWater::getMaxCharges,
			"setMaxCharges", &TCompHolyWater::setMaxCharges,
			"use", &TCompHolyWater::use,
			"restoreCharge", sol::overload
				( 
					[](TCompHolyWater * tc) { return tc->restoreCharge(); },
					&TCompHolyWater::restoreCharge
				)
		);
}

TCompCorruptionControllerPlayer* TCompHolyWater::getPlayerCorruptionComponent() {
	if (!corruptionComponent.isValid()) {
		CEntity* player = getEntityByName("Player");
		if (!player) return nullptr;
		else return player->getComponent<TCompCorruptionControllerPlayer>();
	}
	else return corruptionComponent;
}

void TCompHolyWater::update(float dt) {}

void TCompHolyWater::load(const json& j, TEntityParseContext& ctx) {
	_charges = j.value("starting_charges", _maxCharges);
	_maxCharges = j.value("max_charges", _maxCharges);
	_corruptionRestore = j.value("corruption_restore", _corruptionRestore);
}

void TCompHolyWater::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::DragInt("Current charges", &_charges, 1, 0, _maxCharges);
	ImGui::DragInt("Max charges", &_maxCharges, 1, 0, 16);
}

int TCompHolyWater::getCurrentCharges() {
	return _charges;
}

void TCompHolyWater::setCurrentCharges(int charges) {
	_charges = std::min(charges, _maxCharges);
}

int TCompHolyWater::getMaxCharges() {
	return _maxCharges;
}

void TCompHolyWater::setMaxCharges(int charges) {
	_maxCharges = charges;
	_charges = std::min(_charges, _maxCharges);
}

void TCompHolyWater::use() {
	if (_charges > 0) {
		TCompCorruptionControllerPlayer* c_corr = getComponent<TCompCorruptionControllerPlayer>();
		if (!c_corr || c_corr->getCorruptionLevel() == 0.0f)
			return;
		CEntity * player = CHandle(this).getOwner();
		TMsgCorruptionRemoved msg;
		msg.h_sender = this;
		msg.removedCorruption = _corruptionRestore;
		player->sendMsg(msg);
		_charges --;
	}
}

void TCompHolyWater::restoreCharge(int qty) {
	_charges = std::min(_charges + qty, _maxCharges);
}

void TCompHolyWater::onFullyRestored(const TMsgFullRestore & msg) {
	restoreCharge(_maxCharges);
}