#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "components/player/comp_corruption_controller_player.h"
#include "entity/common_msgs.h"

class TCompHolyWater : public TCompBase {

public:

	static void registerMsgs();
	static void declareInLua();

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();

	int getCurrentCharges();
	void setCurrentCharges(int charges);
	int getMaxCharges();
	void setMaxCharges(int charges);
	void use();
	void restoreCharge(int qty = 1);

private:
	int _charges, _maxCharges = 3;
	float _corruptionRestore = 20.0f;
	CHandle corruptionComponent;

	DECL_SIBLING_ACCESS();
	void onFullyRestored(const TMsgFullRestore & msg);

	TCompCorruptionControllerPlayer* getPlayerCorruptionComponent();

};