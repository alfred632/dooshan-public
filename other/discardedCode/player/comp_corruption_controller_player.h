#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompCorruptionControllerPlayer;

typedef void (TCompCorruptionControllerPlayer::*AddPenaltyFuncPointer)();
typedef void (TCompCorruptionControllerPlayer::*RemovePenaltyFuncPointer)();

class TCompCorruptionControllerPlayer : public TCompBase {
	/* Handles. */
	CHandle playerCameraH;
	CHandle healthCompH;
	
	/* Current and max corruption. */
	float currentCorruption = 0.0f;
	float maxCorruption = 100.0f;

	/* Corruption penalties */
	struct CorruptionPenalty {
		std::string corruptionName; // Used for debugging, not really necessary but it helps.
		float corruptionThreshold; // Value for when the penalty starts applying.
		AddPenaltyFuncPointer penaltyFunc; // Pointer to the penalty function that will be called when the value must be applied.
		RemovePenaltyFuncPointer remPenaltyFunc; // Same as above but for removing penalty.
	};
	// In which position we are in the penalties array.
	int currentIndxCorrArr = 0;
	// An array that holds all corruption penalties ordered.
	const static int numOfPenalties = 4;
	std::array<CorruptionPenalty, numOfPenalties+1> thresholdsCorruptionPenalties;

	DECL_SIBLING_ACCESS();

	void checkForPenaltyAddition();
	void checkForPenaltyRemoval();

	// Penalties.
	/* Percission penalty */
	float _precissionFactorSMG = 1.5f;
	float _precissionFactorShotgun = 0.0f;
	void addPrecisionPenalty();
	void removePrecisionPenalty();

	/* Vision penalty. */
	float visionPenaltyStartThreshold;
	float visionPenaltyEndThreshold;
	void addVisionPenalty();
	void removeVisionPenalty();
	
	/* Insanity penalty. */
	void addInsanityPenalty();
	void removeInsanityPenalty();

	/* Max life penalty. */
	float maxLifePenaltyStartThreshold; // Max life starting threshold.
	float maxLifePenaltyEndThreshold; // Max life ending threshold.
	float minMaxLifePercentage = 0.2f; // The % of max life that will remain once the threshold is fully applied.
	void addMaxLifePenalty();
	void removeMaxLifePenalty();

	void changeCorruption(float corruptionDelta);
	
	// Messages.
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onCorruptionAdded(const TMsgCorruptionAdded & msg);
	void onCorruptionRemoved(const TMsgCorruptionRemoved & msg);
	void onFullyRestored(const TMsgFullRestore & msg);

public:
	void init(); // Remove once json is done.

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();
	static void declareInLua();

	float getCorruptionLevel() { return currentCorruption; }

	float getMaxCorruption() { return maxCorruption; }

	void addCorruption(float addedCorruption);
};