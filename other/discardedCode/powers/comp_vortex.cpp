#include "mcv_platform.h"
#include "comp_vortex.h"
#include "components/common/comp_transform.h"
#include "components/controllers/comp_character_controller.h"
#include "components/common/comp_tags.h"
#include "engine.h"
#include "entity/msgs.h"
#include <algorithm>

DECL_OBJ_MANAGER("vortex", TCompVortex);

void TCompVortex::load(const json& j, TEntityParseContext& ctx) {
	_radius = j.value("radius", 2.0f);
	_timeToLive = j.value("time_to_live", 3.0f);
	_force = j.value("force", 2.f);
}

void TCompVortex::update(float dt)
{
	for (auto it = _enemies.begin(); it != _enemies.end(); it++) {
		if ((*it).isValid()) {
			if (checkIfEnemyDead((*it))) {
				_enemies.erase(it);
				break;
			}
		}
		else {
			_enemies.erase(it);
			break;
		}
	}

	_timeToLive -= dt;
	if (_timeToLive < 0) {
		this->getEntity().destroy();
		return;
	}
	moveEntities(dt);
}

void TCompVortex::moveEntities(float dt) {
	for (CEntity* const& e : _enemies) {
		
		CHandle eH = e;
		if (!eH.isValid()) continue;

		TCompCharacterController* c = e->getComponent<TCompCharacterController>();
		if (!c) continue;

		TCompTransform* eT = e->getComponent<TCompTransform>(); //Enemy transform
		if (!eT) continue;

		TCompTransform* vT = getComponent<TCompTransform>(); //Vortex transform
		VEC3 displacement = vT->getPosition() - eT->getPosition();
		float distance = displacement.Distance(vT->getPosition(), eT->getPosition());
		if (distance < 0.01f)
			continue;
		displacement.Normalize();
		displacement *= _force * ((1.0f - Maths::clamp(distance, 0.0f, 2.0f) / 2.0f) + 0.35f);
		c->move(displacement);
	}
}

bool TCompVortex::checkIfEnemyDead(CEntity * e) {
	TCompHealth * health = e->getComponent<TCompHealth>();
	if (health)
		return health->isDead();
	return false;
}

void TCompVortex::onUpdateIndicatorMsg(const TMsgUpdateIndicator& msg) {
	TCompTransform* c_trans = getComponent<TCompTransform>();
	TCompCollider* c_col = getComponent<TCompCollider>();
	c_trans->setPosition(msg.position);
	float yaw = vectorToYaw(msg.front);
	c_trans->setAngles(yaw, 0.f, 0.f);
	c_col->actor->setGlobalPose(toPxTransform(*c_trans));
}

void TCompVortex::onTriggerEnter(const TMsgEntityTriggerEnter& trigger_enter) {
	//Check if enemy
	CEntity* e = trigger_enter.h_entity;
	TCompTags* e_tags = e->getComponent<TCompTags>();

	if (!e_tags)
		return;

	bool isEnemy = e_tags->hasTag(Utils::getID("enemyMelee")) || e_tags->hasTag(Utils::getID("enemyRanged"));
	if (!isEnemy)
		return;

	//else check if new to the vector and needs to be added
	if (std::find(_enemies.begin(), _enemies.end(), trigger_enter.h_entity) != _enemies.end()) {
		Utils::dbg("[TCompVortex::onTriggerEnter] Entity already on the enemies list wtf.\n");
	}
	else {
		_enemies.push_back(trigger_enter.h_entity);
	}
}

void TCompVortex::onTriggerExit(const TMsgEntityTriggerExit& trigger_exit) {
	auto endIt = std::remove_if(_enemies.begin(), _enemies.end(), [trigger_exit](const CHandle& chandle) {
		return chandle.asVoidPtr() == trigger_exit.h_entity.asVoidPtr();
	});
	_enemies.erase(endIt, _enemies.end());
}

void TCompVortex::registerMsgs() {
	DECL_MSG(TCompVortex, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompVortex, TMsgUpdateIndicator, onUpdateIndicatorMsg);
	DECL_MSG(TCompVortex, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompVortex, TMsgEntityTriggerExit, onTriggerExit);
}

void TCompVortex::debugInMenu()
{
	TCompBase::debugInMenu();
}

void TCompVortex::renderDebug()
{
}


