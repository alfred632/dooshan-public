
-- Custom useful functions

function mod(num, quo)
	return num - math.floor(num / quo) * quo
end

function getTableSize(t)
    local count = 0
    for _, __ in pairs(t) do
        count = count + 1
    end
    return count
end


dbg("--------------------- LUA has been started ---------------------")