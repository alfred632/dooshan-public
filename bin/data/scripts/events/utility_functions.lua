-- Utility functions TagComponentfor LUA scripting.

function lerp(a, b, t)
	return a * (1.0 - t) + b * t
end

-- Get value from variables array. If not found returns default value.
function getValueVariablesArray(variables, variableName, defaultValue)
	local val = variables[variableName]
	if val == nil then
		return defaultValue
	end
	return val
end

function entityHasTag(entity, tag)
	local TagComponent = entity:TCompTags()
	if TagComponent == nil then return end
	return TagComponent:hasTag(tag)
end