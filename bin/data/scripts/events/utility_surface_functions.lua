-- Utility functions for LUA scripting. Used for identifying if a surface is of a given type.

function isSurfaceBaseEnemy(typeSurface)
	if typeSurface == "Enemy_Default" then
		return true
	end
	return false;
end

function isSurfaceEnemyImmortal(typeSurface)
	if typeSurface == "Immortal" then
		return true
	end
	return false
end

function isSurfaceSand(typeSurface)
	if typeSurface == "Sand" then
		return true
	end
	return false;
end

function isSurfaceWood(typeSurface)
	if typeSurface == "Wood" then
		return true
	end
	return false
end

function isSurfaceMetal(typeSurface)
	if typeSurface == "Metal" then
		return true
	end
	return false
end