-- FX Functions for the rifle weapon. See General_Shooting to see the default functions used for all weapons.

-- Identical to the one called from spawnOnHitFXDefaultWeapon but when an immortal is hit. Spawns blood instead of sparks.
function spawnOnHitFXRifle(touchingPos, lookAt, typeSurface)
	if isSurfaceBaseEnemy(typeSurface) or isSurfaceEnemyImmortal(typeSurface) then
		SpawnDecalsOnBaseEnemy(touchingPos, lookAt, typeSurface)
	else
		SpawnDecalsOnSolidSurface(touchingPos, lookAt, typeSurface)
	end
end