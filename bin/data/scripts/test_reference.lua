function sandbox()
	dbg("Player health is ", player:TCompHealth():getCurrentHealth())
	playerTransformHandle = player:TCompTransform():asCHandle()
	playerTransform = playerTransformHandle:asTCompTransform()
	playerTransform:lookAt(playerTransform:getPosition(), Vector3.new(), playerTransform:getUp())
	playerPos = playerTransform:getPosition()
	dbg("Player position is ", playerPos.x, ", ", playerPos.y, ", ", playerPos.z)
	vec = Vector3.new(1,2,3)
	vec2 = Vector3.new(1)
	vec3 = vec + vec2
	dbg(tostring(vec))
	dbg(tostring(vec3))
	yaw, pitch, roll = playerTransform:getYawPitchRoll()
	dbg("yaw=", yaw, ", pitch=", pitch, ", roll=", roll)
	LogicManager:setEntityTCompPlayerCharacterControllerActive(player, false)
	player:TCompHolyWater():restoreCharge()
end

function duckTypingTest()
	playerTransform = player:TCompTransform()

	-- to securelly call a method (same applies for attributes)
	if playerTransform.TCompMisHuevos ~= nil then
		dbg("TCompMisHuevos method from Player.TCompTransform returned: ", tostring(playerTransform:misHuevos()))
	end
	if playerTransform.getPosition ~= nil then
		dbg("getPosition method from Player.TCompTransform returned: ", tostring(playerTransform:getPosition()))
	end
	-- doesn't crash!! duck typing FTW!
end

function tpBeneath()
	playerTransform = player:TCompTransform()
	player:TCompPlayerCharacterController():teleport(playerTransform:getPosition() - playerTransform:getUp() * 0.5)
end

function pathfindPls()
	player = getEntity("Player")
	playerTransform = player:TCompTransform()
	agent = getEntity("TestSubject")
	pathfindingAgent = agent:TCompPathfindingAgent()
	pathfindingAgent:requestMove(playerTransform:getPosition())
end

player = getEntity("Player")
--sandbox()
--duckTypingTest()
pathfindPls()

dbg("End of Test script")
