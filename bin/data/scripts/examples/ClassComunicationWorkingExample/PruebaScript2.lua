local iteration = 0

function onUpdate()
	-- Your test code goes here
	iteration = iteration + 1
	dbg("PruebaScript2.")
	
	local instance = ScriptManager.getRegisteredScriptInstance(1)
	local instance2 = ScriptManager.getRegisteredScriptInstance(2)
	local ins = instance:getScriptInstance()
	
	if ins == nil then
		dbg("PruebaScript2 recieved nill")
	else
		dbg("PruebaScript2 quitando dinero a ScriptPruebaBueno")
		instance:getScriptInstance():anotherMethod(10)
		instance2:getScriptInstance():withdraw(10)
	end
		
	dbg("Iteration Num ", iteration)
	dbg("---------------------")
	
end
