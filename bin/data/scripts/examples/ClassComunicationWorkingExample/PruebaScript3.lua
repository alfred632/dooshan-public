Account = {}
Account.__index = Account

function Account:create(balance)
   local acnt = {}             -- our new object
   setmetatable(acnt,Account)  -- make Account handle lookup
   acnt.balance = balance      -- initialize our object
   return acnt
end

function Account:show()
	dbg("Money left ", self.balance) 
end

function Account:withdraw(amount)
	self.balance = self.balance - amount
	dbg("Método de Account")
	dbg("Dinero ", self.balance)
end

-- create and use an Account
acc = Account:create(1000)

function onStart()
	-- Your test code goes here
	dbg("onStart called")
	ScriptManager.publishScriptInstance(2, acc)
end

function onEntityCreated(CHandle)
	-- Your test code goes here
	dbg("onEntityCreated called", CHandle)
end

function onSceneCreated()
	-- Your test code goes here
	dbg("onSceneCreated called")
end

function onUpdate()
	-- Your test code goes here
	--dbg("Esto es PruebaScript3")
	--acc:show()
	--dbg("---------------------")
end

function onTriggerEnter()
	-- Your test code goes here
	dbg("onTriggerEnter called")
end

function onTriggerExit()
	-- Your test code goes here
	dbg("onTriggerExit called")
end

function onEnable()
	-- Your test code goes here
	dbg("onEnable called")
end

function onDisable()
	-- Your test code goes here
	dbg("onDisable called")
end

function onEnd()
	-- Your test code goes here
	dbg("onEnd called PRUEBASCRIPT3")
	ScriptManager.removeScriptInstance(2)
end

