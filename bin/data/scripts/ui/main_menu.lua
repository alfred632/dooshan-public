
function onbt_startClicked()
	ModuleMainMenu:startGame()

	LogicManager:setCenterMouseActive(true)
	LogicManager:showCursor(false)
end

function onbt_exitClicked()
	LogicManager:exitApplication()
end