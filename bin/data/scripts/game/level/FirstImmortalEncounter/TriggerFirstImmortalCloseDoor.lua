local ownHandle
local ScriptHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	ScriptHandle = CHandle:TCompScript():asCHandle()
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	
	local DoorHandle = LogicManager:getEntityByName("door_casadepaso027")
	if not DoorHandle:isValid() then return end
	
	LogicManager:setEventParameter("event:/Music/MusicHorde", "Horde", 0.0)
	LogicManager:playEvent("event:/Music/MusicExplore")
	LogicManager:setEventParameter("event:/Music/MusicExplore", "Explore", 1.0)

	local DoorComp = DoorHandle:TCompDoor()
	DoorComp:CloseDoors()
	
	LogicManager:BindTimedScriptEvent(ScriptHandle, "RemoveDynamic", 5.00)	
end


function RemoveDynamic()
	-- Clear entities in previous volumes if there are any.
	local Entity = LogicManager:getEntityByName("CullingBoxArea")
	if Entity:isValid() then
		Entity:TCompSceneCullingVolume():RemoveDynamicEntitiesInVolume()
	end
	
	-- Remove trigger.
	component:disableNextFrame()
end