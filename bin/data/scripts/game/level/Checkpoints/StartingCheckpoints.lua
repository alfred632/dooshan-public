local ownHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
end


function onCheckpointReached()
end

local entitiesToReset = {
	"TriggerHint01_2", "TriggerPuertaInicioOpen", "TriggerHint03", "TriggerHint04", "TriggerHint05", 
	"ArenaIntroOpenDoor", "ArenaIntroCinematic", "CinematicEntranceSanctuary", 
	"CinematicSanctuaryIntroPresentation"
}
local doorsToClose = {
	"door_casadepaso002", "door_casadepaso023", "wooddoor"
}
local arenaToReset = "ArenaIntro"
function onCheckpointRestorePlayer()
	LogicManager:ClearBindTimedScriptEvents()
	LogicManager:removeEntitiesByTag("pickup")

	arenaEntity = getEntity(arenaToReset)
	arenaEntity:TCompArena():ResetArena()

	LogicManager:disablePoolingEnemies()

	for _, entityToReset in ipairs(entitiesToReset) do
		LogicManager:setEntityActive(entityToReset, false)
		LogicManager:setEntityActive(entityToReset, true)
	end
	for _, doorName in ipairs(doorsToClose) do
		door = getEntity(doorName):TCompDoor()
		door:ResetDoors()
	end
end