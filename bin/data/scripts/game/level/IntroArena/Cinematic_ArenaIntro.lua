local ScriptHandle 

function onStart(variables)

end

function onEntityCreated(CHandle)
	-- Get own handle.
	ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Init cinematic here.
	StartCameras()
end

function StartCameras()
	-- Set camera to use.
	LogicManager:setDefaultCamera("Camera-Intro-Arena")
	
	-- Start black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", true)
	
	-- Set input disabled.
	LogicManager:setActivePlayerInput(false)
	
	-- Start camera. Listen to event on camera end.
	local Camera = LogicManager:getEntityByName("Camera-Intro-Arena")
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	Camera:TCompTransCurveController():start()
	LogicManager:BindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle, "onCinematicEnded")

	LogicManager:playEvent("event:/Ambience/WindTrees")
end

function onCinematicEnded()
	local S1H = LogicManager:getEntityByName("SpawnArenaIntroCinematic")
	spawnRangedSniperRoof(S1H, true)
	LogicManager:BindTimedScriptEvent(ScriptHandle, "EndCinematic", 3.0)
end


function EndCinematic()
	-- End black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", false)
	
	-- Set inactive again.
	local Camera = LogicManager:getEntityByName("Camera-Intro-Arena")
	Camera:TCompTransCurveController():stop()
	
	-- Return player control.
	LogicManager:setActivePlayerInput(true)
	
	-- Activate arena.
	local ArenaIntro = LogicManager:getEntityByName("ArenaIntro")
	ArenaIntro:TCompArena():StartArena()
	
	-- End the cinematic. We return to the previous camera.
	LogicManager:EndCinematic()
end

function onEnd()
	-- Unbind events before destroying.
	local Camera = LogicManager:getEntityByName("Camera-Intro-Arena")
	if Camera == nil or not (Camera:isValid()) then return end
		
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	LogicManager:UnbindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle)

end