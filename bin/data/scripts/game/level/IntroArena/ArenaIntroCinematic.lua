local ownHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	ScriptHandle = CHandle:TCompScript():asCHandle()
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	
	-- Play cinematic pointing to door opening and wave coming.
	LogicManager:StartCinematic("data/prefabs/cinematics/CinematicArenaIntro.json");
	LogicManager:BindTimedScriptEvent(ScriptHandle, "playArenaMusic", 7.0)
	
	-- Remove trigger.
	component:disableNextFrame()
end

function playArenaMusic()
	LogicManager:playEvent("event:/Music/MusicBattle")
	LogicManager:setEventParameter("event:/Music/MusicBattle", "Battle", 1.0)
end