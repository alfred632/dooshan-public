local ownHandle
local ScriptHandle
local EnemySpawned = false
local EnemyToKill

function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	
	-- Get own handle.
	ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Spawn enemy at the beginning of creation.
	spawnFirstMelee()
	
end

function onEnable()
	EnemyToKill = nil
	EnemySpawned = false

	-- Spawn enemy at the beginning of creation.
	LogicManager:BindTimedScriptEvent(ScriptHandle, "spawnFirstMelee", 0.5)

end

function spawnFirstMelee()
	local Spawn = LogicManager:getEntityByName("SpawnEnemyTutorial")
	if not Spawn:isValid() then return end
	EnemyToKill = Spawn:TCompSpawn():SpawnEntityInstant("EnemyMelee", false)
	EnemyToKill:TCompHealth():setCurrentHealth(30)
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end
	if EnemySpawned then return end
	EnemySpawned = true
	LogicManager:alertEnemy(EnemyToKill)
	LogicManager:BindTimedScriptEvent(ScriptHandle, "CheckIfEnemyDead", 0.20)
end

function CheckIfEnemyDead()
	if not(EnemyToKill == nil) and EnemyToKill:isValid() then
		if EnemyToKill:TCompHealth():isDead() then
			local DoorHandle = LogicManager:getEntityByName("door_casadepaso002")
			if not DoorHandle:isValid() then return end
			
			local DoorComp = DoorHandle:TCompDoor()
			DoorComp:OpenDoors()

			-- TriggerHint03.
			local TriggerHint03 = LogicManager:getEntityByName("TriggerHint03")
			if not TriggerHint03:isValid() then return end
			local ReloadHintTrigger = ScriptManager.getRegisteredScriptInstance(TriggerHint03)
			ReloadHintTrigger:getScriptInstance():SpawnEnemies()

			-- Deactivate trigger.
			LogicManager:setEntityActive(ownHandle, false)
			return
		end
	end
	
	LogicManager:BindTimedScriptEvent(ScriptHandle, "CheckIfEnemyDead", 0.20)
end