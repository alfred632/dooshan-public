local S1H, S2H, S3H, S4H, S5H, S6H, S7H, S8H, S9H, S10H, S11H, S12H, S13H, S14H

local ownHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
end

function onSceneCreated()
	-- Fetch spawns.
	S1H = LogicManager:getEntityByName("SpawnWave26")
	S2H = LogicManager:getEntityByName("SpawnWave27")
	S3H = LogicManager:getEntityByName("SpawnWave28")
	S4H = LogicManager:getEntityByName("SpawnWave25")
	S5H = LogicManager:getEntityByName("SpawnWave23")
	S6H = LogicManager:getEntityByName("SpawnWave5")
	S7H = LogicManager:getEntityByName("SpawnWave10")
	S8H = LogicManager:getEntityByName("SpawnWave24")
	S9H = LogicManager:getEntityByName("SpawnWave")
	
	S10H = LogicManager:getEntityByName("SpawnCorridor4")
	S11H = LogicManager:getEntityByName("SpawnCorridor6")
	S12H = LogicManager:getEntityByName("SpawnCorridor7")
	S13H = LogicManager:getEntityByName("SpawnCorridor5")
	S14H = LogicManager:getEntityByName("SpawnCorridor14")
end

function onTriggerEnter(entity)
	if not entityHasTag(entity, "player") then return end

	-- Spawn some enemies.
	spawnMelee(S1H, true)
	spawnMelee(S2H, true)
	spawnMelee(S3H, true)
	spawnMelee(S4H, true)
	spawnMelee(S5H, true)
	spawnMelee(S6H, true)
	spawnMelee(S7H, true)
	--spawnMelee(S8H)
	--spawnMelee(S9H)
	
	spawnRangedSniperRoof(S10H, true)
	spawnRangedSniperRoof(S11H, true)
	spawnRangedSniperRoof(S12H, true)
	--spawnRangedSniperRoof(S13H)
	--spawnRangedSniperRoof(S14H)

	-- Remove trigger.
	component:disableNextFrame()
end