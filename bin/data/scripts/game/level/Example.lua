--[[ You can create an script that is not implemented with a class.
This won't be able to be called by other scripts, but can not be a problem
if you know you don't need it. For example, when creating a script for a cinematic. ]]

--[[ onEvent functions. They should have this specific signature, don't change.
This functions are called by the TCompScript when an event is recieved and they
will get sent to the given object. ]]

-- Recieved when the TCompScript is just loaded. Do anything you may need here.
-- Components are still not created though, the entity not being finished creating.
function onStart()
	
end

-- Recieved when the entity is created. Do anything you may want with
-- the instance and then get sure to publish the script if you want it
-- to be accessed from outside this script. In this case we don't.
function onEntityCreated(CHandle)
	dbg("onEntityCreated. CHandle recieved: ", CHandle)
	
	-- We don't register anything because this script doesn't use a class.
end

-- Do anything with your instance here if you want.
-- This is recieved when the complete scene is created.
function onSceneCreated()

end

-- Cal from here the update of your object if you have one.
function onUpdate(dt)
	-- For example:
	--instance:update(dt)
end

-- This function is called whenever a onTriggerEnter event
-- is recieved by the entity with the given script.
function onTriggerEnter()
	dbg("onTriggerEnter called")
end

-- This function is called whenever a onTriggerExit event
-- is recieved by the entity with the given script.
function onTriggerExit()
	dbg("onTriggerExit called")
end

-- This function is called whenever a onEnable event
-- is recieved by the entity with the given script.
function onEnable()
	dbg("onEnable called")
end

-- This function is called whenever a onDisable event
-- is recieved by the entity with the given script.
function onDisable()
	dbg("onDisable called")
end

-- This function is called whenever a onEnd event
-- is recieved by the entity with the given script.
function onEnd(CHandle)
	dbg("onEnd called")
end