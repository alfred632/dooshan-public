local ScriptHandle 

function onEntityCreated(CHandle)
	-- Get own handle.
	ScriptHandle = CHandle:TCompScript():asCHandle()
	
	-- Init cinematic here.
	StartCameras()
end

function StartCameras()
	-- Set camera to use.
	LogicManager:setDefaultCamera("CameraStartIntro")
	
	-- Start black bars post fx. Applied to camera-output.
	LogicManager:showCinematicBarsFull("Camera-Output", true)
	
	-- Set input disabled.
	LogicManager:setActivePlayerInput(false)
	
	-- Start with a timer.
	LogicManager:BindTimedScriptEvent(ScriptHandle, "StartCurve", 3.0)
	local Camera = LogicManager:getEntityByName("CameraStartIntro")
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	Camera:TCompTransCurveController():start()
	Camera:TCompTransCurveController():update(0.0)
	Camera:TCompTransCurveController():stop()
	LogicManager:playEvent("event:/Music/GameComplete")

end

function StartCurve()
	local Camera = LogicManager:getEntityByName("CameraStartIntro")
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	Camera:TCompTransCurveController():start()
	LogicManager:BindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle, "EndCinematic")
end

function EndCinematic()
	-- End black bars post fx. Applied to camera-output.
	LogicManager:activateCinematicBars("Camera-Output", false)

	-- Set inactive again.
	local Camera = LogicManager:getEntityByName("CameraStartIntro")
	Camera:TCompTransCurveController():stop()
	
	LogicManager:BindTimedScriptEvent(ScriptHandle, "FinalCinematicEnd", 3.0)

	LogicManager:playEvent("event:/Ambience/Pajaritos", Vector3:new(-7, 3, -4.5))
	
	LogicManager:playEvent("event:/Ambience/WindTrees", Vector3:new(-7, 3, -4.5))
end

function FinalCinematicEnd()
	-- Enable hints for player.
	local PlayerH = LogicManager:getEntityByName("Player")
	if PlayerH:isValid()then
	 PlayerH:TCompHints():SetEnabled(true)
	end
	
	-- Return player control.
	LogicManager:setActivePlayerInput(true)
	
	--Enable the hintsand hud widgets
	LogicManager:activateWidget("hud")
	
	-- End the cinematic. We return to the previous camera.
	LogicManager:EndCinematic()
end

function onEnd()
	-- Unbind events before destroying.
	local Camera = LogicManager:getEntityByName("CameraStartIntro")
	if Camera == nil or not (Camera:isValid()) then return end
		
	local CurveHandle = Camera:TCompTransCurveController():asCHandle()
	LogicManager:UnbindScriptToComponentEvent(CurveHandle, "onEndReached", ScriptHandle)
end