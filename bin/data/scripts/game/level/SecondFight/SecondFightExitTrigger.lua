local Activated = false

local SunHandle
local CurrentInterpolationTime = 0.0
local InterpolationTime = 25.0
local InterpolationTimeRotation = InterpolationTime * 0.8

local SunStartPosition = Vector3:new(346, 10, -129)
local SunFinalPosition = Vector3:new(346, 95, -129)

local SunStartYaw
local SunFinalYaw = 0.0
local SunFinalPitch
local SunFinalRoll

local SunStartColor
local SunFinalColor = Vector4:new(0.51, 0.0, 9.1451, 1.0)

local SunStartColorLight
local SunFinalColorLight = Vector4:new(0.4706, 0.22, 3.14, 1.0)

local ownHandle
local ScriptHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
	ScriptHandle = CHandle:TCompScript():asCHandle()
end

function onEnable()
	Activated = true
end

function onTriggerEnter(otherEntity)
	if not entityHasTag(otherEntity, "player") then return end

	local DoorHandle2 = LogicManager:getEntityByName("door_casadepaso033")
	if not DoorHandle2:isValid() then return end
	local DoorComp2 = DoorHandle2:TCompDoor()
	DoorComp2:OpenDoors()

	component:disableNextFrame()

	if Activated then return end
	Activated = true

	-- Start light interpolation.
	if FetchSun() then
		StartLightInterpolation()
	end
end

function FetchSun()
	SunHandle = LogicManager:getEntityByName("Sun")
	if not SunHandle or not SunHandle:isValid() then return false end
	
	SunStartColorLight = SunHandle:TCompLightDir():GetLightColor()
	SunStartColor = SunHandle:TCompRender():getColor()
	local angles = SunHandle:TCompTransform():getYawPitchRoll()
	SunStartYaw = angles.x
	SunFinalPitch = angles.y
	SunFinalRoll = angles.z
	return true
end

function StartLightInterpolation()
	LogicManager:BindTimedScriptEvent(ScriptHandle, "UpdateLighting", 0.05)	
end

function UpdateLighting()
	CurrentInterpolationTime = CurrentInterpolationTime + 0.05
	local InterpolationFactor = CurrentInterpolationTime/InterpolationTime
	local InterpolationFactorRotation = CurrentInterpolationTime/InterpolationTimeRotation
	if InterpolationFactor > 1.0 then InterpolationFactor = 1.0 end
	if InterpolationFactorRotation > 1.0 then InterpolationFactorRotation = 1.0 end

	local NewSunPosition = lerp(SunStartPosition, SunFinalPosition, InterpolationFactor)
	local NewSunYaw = lerp(SunStartYaw, SunFinalYaw, InterpolationFactorRotation)

	SunHandle:TCompTransform():setPosition(NewSunPosition)
	SunHandle:TCompTransform():setAngles(NewSunYaw, SunFinalPitch, SunFinalRoll)
	
	local NewSunColor = lerp(SunStartColor, SunFinalColor, InterpolationFactor)
	SunHandle:TCompRender():setColor(NewSunColor)
	
	local NewSunColorLight = lerp(SunStartColorLight, SunFinalColorLight, InterpolationFactor)
	SunHandle:TCompLightDir():SetLightColor(NewSunColorLight)
	
	local NewFogLerp = lerp(0.0, 1.0, InterpolationFactor)
	LogicManager:setFogLerp("Camera-Output", NewFogLerp)
	
	if InterpolationFactor < 1.0 then
		LogicManager:BindTimedScriptEvent(ScriptHandle, "UpdateLighting", 0.05)
	else
		LogicManager:setEntityActive(entity, false)
	end
end