local ownHandle
function onEntityCreated(CHandle)
	-- Get own handle.
	ownHandle = CHandle
end

local spawnNames = {
	"spawn_15_group1_1", "spawn_15_group1_2",
	"spawn_15_group2_1",
	"spawn_15_group4_1", "spawn_15_group4_2", "spawn_15_group4_3", "spawn_15_group4"
}
function onTriggerEnter(entity)
	if entityHasTag(entity, "player") then
		for _, spawnName in ipairs(spawnNames) do
			spawn = getEntity(spawnName):TCompSpawn()
			enemy = spawn:SpawnEntityInstant(false)
			if (spawnName == "spawn_15_group2_1") then
				LogicManager:alertEnemy(enemy)
			end
		end

		-- Open doors
		local DoorHandle2 = LogicManager:getEntityByName("door_casadepaso032")
		if not DoorHandle2:isValid() then return end
		
		local DoorComp2 = DoorHandle2:TCompDoor()
		DoorComp2:OpenDoors()
		
		component:disableNextFrame()
	end
end