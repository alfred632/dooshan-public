Spawn = {}
Spawn.__index = Spawn

EnemyToSpawn = {}
EnemyToSpawn.__index = EnemyToSpawn

function EnemyToSpawn:new(nEnemyId, nTimeToSpawn)
   local enemyToSpawn = {}             -- our new object
   setmetatable(enemyToSpawn, EnemyToSpawn)  -- make enemyToSpawn lookup
   enemyToSpawn.enemyIDToSpawn = nEnemyId
   enemyToSpawn.timeToSpawn = nTimeToSpawn
   return enemyToSpawn
end

function Spawn:new()
   local spawn = {}             -- our new object
   setmetatable(spawn, Spawn)  -- make Spawn handle lookup
   
   spawn.ownHandle = nil -- Own handle
   spawn.ownScriptHandle = nil -- Script instance handle
   spawn.spawnOwnerName = "" -- Name of the owner of the spawn.
   spawn.spawnOwner = nil -- Instance to the spawn owner.
   spawn.occupied = 0 -- How many entities are in the spawn, if 0, no entities.
   spawn.enemiesToSpawn = {} -- Holds enemies to spawn
   
   -- Variables for spawning enemies.
   self.spawnableEnemies = {"melee", "meleeMini", "ranged", "immortal"} -- What enemies can this spawn spawn. By default all enemies.
   self.timeToSpawnMeleeAfterParticlesStart = 2.0
   self.timeToSpawnMeleeMiniAfterParticlesStart = 2.0
   self.timeToSpawnRangedAfterParticlesStart = 2.0
   self.timeToSpawnImmortalAfterParticlesStart = 2.0
   
   return spawn
end

function Spawn:onTriggerEnter()
	if (not self:isOccupied()) and (self:checkSpawnOwner()) then
		local instance = self.spawnOwner:getScriptInstance()
		if not (instance == nil) then
			instance:spawnIsFree(self.ownScriptHandle, false)
		end
	end

	self.occupied = self.occupied + 1
end

function Spawn:onTriggerExit()
	self.occupied = self.occupied - 1
	
	-- Clamp just in case.
	if self.occupied < 0 then
		self.occupied = 0
	end
	
	if (not self:isOccupied()) and (self:checkSpawnOwner()) then
		local instance = self.spawnOwner:getScriptInstance()
		if not (instance == nil) then
			instance:spawnIsFree(self.ownScriptHandle, true)
		end
	end
end

function Spawn:spawnRandomEnemy()
	if self:isOccupied() then return false end
	
	-- Get a random enemy.
	local randomEnemy = LogicManager:getRandomInt(1, #self.spawnableEnemies);
	local enemyToSpawn = self.spawnableEnemies[randomEnemy]
	
	if enemyToSpawn == "melee" then
		self:spawnMelee()
	elseif enemyToSpawn == "meleeMini" then
		self:spawnMeleeMini()
	elseif enemyToSpawn == "ranged" then
		self:spawnRanged()
	else
		self:spawnImmortal()
	end

	return true
end

function Spawn:spawnMelee()
	-- Spawn particles first.
	LogicManager:SpawnParticle("data/particles/fire_melee_spawn.particles", self.ownHandle:TCompTransform():getPosition());
	
	-- Maybe spawn a sound too?
	
	-- Add the enemy to the vector so it gets spawned after a given time.
	table.insert(self.enemiesToSpawn, EnemyToSpawn:new("melee", self.timeToSpawnMeleeAfterParticlesStart))
end

function Spawn:spawnMeleeMini()
	-- Spawn particles first.
	LogicManager:SpawnParticle("data/particles/fire_melee_spawn.particles", self.ownHandle:TCompTransform():getPosition());
	
	-- Maybe spawn a sound too?
	
	-- Add the enemy to the vector so it gets spawned after a given time.
	table.insert(self.enemiesToSpawn, EnemyToSpawn:new("meleeMini", self.timeToSpawnMeleeMiniAfterParticlesStart))
end


function Spawn:spawnRanged()
	-- Spawn particles first.
	LogicManager:SpawnParticle("data/particles/fire_ranged_spawn.particles", self.ownHandle:TCompTransform():getPosition());
	
	-- Maybe spawn a sound too?
	
	-- Add the enemy to the vector so it gets spawned after a given time.
	table.insert(self.enemiesToSpawn, EnemyToSpawn:new("ranged", self.timeToSpawnRangedAfterParticlesStart))
end

function Spawn:spawnImmortal()
	-- Spawn particles first.
	LogicManager:SpawnParticle("data/particles/fire_immortal_spawn.particles", self.ownHandle:TCompTransform():getPosition());
	
	-- Maybe spawn a sound too?
	
	-- Add the enemy to the vector so it gets spawned after a given time.
	table.insert(self.enemiesToSpawn, EnemyToSpawn:new("immortal", self.timeToSpawnImmortalAfterParticlesStart))
end

-- Spawns the enemy and sends the handle to the owner if it has one.
function Spawn:spawnEnemy(enemyID)
	local handle = nil
	
	if self.spawnOwner == nil then return end
	local instance = self.spawnOwner:getScriptInstance()
	if instance == nil then return end

	-- Add here other enemies. Should call the pooling here instead.
	if enemyID == "melee" then
		handle = LogicManager:createEntity("data/prefabs/enemies/enemyMelee.json", self.ownHandle:TCompTransform():getPosition())
	elseif enemyID == "meleeMini" then
		handle = LogicManager:createEntity("data/prefabs/enemies/enemyMelee.json", self.ownHandle:TCompTransform():getPosition())
	elseif enemyID == "ranged" then
		handle = LogicManager:createEntity("data/prefabs/enemies/enemyMelee.json", self.ownHandle:TCompTransform():getPosition())
	else
		handle = LogicManager:createEntity("data/prefabs/enemies/enemyMelee.json", self.ownHandle:TCompTransform():getPosition())
	end
	
	-- Add the handle to the arena if the spawn has an arena.
	if not (handle == nil) then
		instance:AddSpawnedEnemy(handle)
	end
	
end

function Spawn:checkQueuedEnemiesToSpawn(dt)
	for idx = 1, #self.enemiesToSpawn do
		local enemyToSpawn = self.enemiesToSpawn[idx]
		enemyToSpawn.timeToSpawn = enemyToSpawn.timeToSpawn - dt;
		
		-- Spawn the enemy and remove.
		if(enemyToSpawn.timeToSpawn < 0.0) then
			self:spawnEnemy(enemyToSpawn.enemyIDToSpawn)
			table.remove(self.enemiesToSpawn, idx)
			break
		end
	end
end

function Spawn:isOccupied()
	return self.occupied > 0
end

function Spawn:setSpawnOwner(spawnOwnerName)
	self.spawnOwnerName = spawnOwnerName

	-- No spawn owner.
	if (self.spawnOwnerName == nil) or (self.spawnOwnerName == "") then 
		return
	end
	
	-- Unregister from previous spawn if it had one.
	if not (self.spawnOwner == nil) then
		self.spawnOwner:getScriptInstance():unregisterSpawn(self.ownScriptHandle)
	end
	
	local handleSpawnOwner = LogicManager:getEntityByName(self.spawnOwnerName)
	self.spawnOwner = ScriptManager.getRegisteredScriptInstance(handleSpawnOwner)

	-- Register the spawn so it gets used.
	self.spawnOwner:getScriptInstance():registerSpawn(self.ownScriptHandle)
end

function Spawn:checkSpawnOwner()
	if (self.spawnOwnerName == nil) or (self.spawnOwnerName == "") then
		return false
	else
		local handleSpawnOwner = LogicManager:getEntityByName(self.spawnOwnerName)
		self.spawnOwner = ScriptManager.getRegisteredScriptInstance(handleSpawnOwner)
		if (self.spawnOwner == nil) then
			return false
		end
	end	
	return true
end

function Spawn:unregisterOnEnd()
	-- Unregister from previous spawn if it had one.
	if (self.spawnOwner == nil) then
		return
	end
	
	if (self.spawnOwner:getScriptInstance() == nil) then
		return
	end

	self.spawnOwner:getScriptInstance():unregisterSpawn(self.ownScriptHandle)
end

-- create and use an Spawn
instance = Spawn:new()

function onStart(variables)
	-- Set variables here if there are any.
	instance.spawnOwnerName = variables["spawnOwnerName"]
	
	-- Load all enemies that can be spawned
	local enemiesToSpawnNames = variables["enemiesToSpawnNames"]
	if not(enemiesToSpawnNames == nil) then
		instance.spawnableEnemies = enemiesToSpawnNames
	end
	
end

function onEntityCreated(CHandle, num)
	-- Get own handle.
	instance.ownHandle = CHandle
	
	-- Set the instance so it can be accessed.
	ScriptManager.publishScriptInstance(CHandle, instance)
	
	instance.ownScriptHandle = ScriptManager.getRegisteredScriptInstance(instance.ownHandle)
end

function onSceneCreated()
	instance:setSpawnOwner(instance.spawnOwnerName)
end

function onUpdate(dt)
	instance:checkQueuedEnemiesToSpawn(dt)
end

function onTriggerEnter()
	instance:onTriggerEnter()
end

function onTriggerExit()
	instance:onTriggerExit()
end

function onEnable()
end

function onDisable()
end

function onEnd()
	instance:unregisterOnEnd()
	ScriptManager.removeScriptInstance(instance.ownHandle)
end