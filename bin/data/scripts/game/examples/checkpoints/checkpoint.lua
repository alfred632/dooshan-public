
function onCheckpointReached()
	dbg("First checkpoint reached!")
end

function onCheckpointRestorePlayer()
	dbg("Player restored to first checkpoint")
end