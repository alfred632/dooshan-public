//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"

//--------------------------------------------------------------------------------------
struct ParticleRenderData
{
  float3 Pos         : TEXCOORD2;
  float  Time        : TEXCOORD3;
  uint Id            : TEXCOORD4;
  float4 Velocity    : TEXCOORD5;
};

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
  float4 Pos : SV_POSITION;
  float2 Uv : TEXCOORD0;
  float2 UvNext : TEXCOORD1;
  float  UvBlend : TEXCOORD2;
  float4 Color : COLOR;
};

struct VS_OUTPUT_SPHERICAL
{
  float4 Pos : SV_POSITION;
  float3 ParticlePos : POSITION0;
  float3 ParticlePointPos : POSITION1;
  float2 Uv : TEXCOORD0;
  float  Radius : TEXCOORD1;
  float4 Color : COLOR;
};

struct VS_OUTPUT_MESH
{
  float4 Pos : SV_POSITION;
  float3 N : NORMAL;
  float2 Uv : TEXCOORD0;
  float2 UvNext : TEXCOORD1;
  float  UvBlend : TEXCOORD2;
  float4 Color : COLOR;
};

float getRandomNumber(float seed){
  return frac(sin(seed) * 43758.5453);
}

// Quaternion multiplication
// http://mathworld.wolfram.com/Quaternion.html
float4 qmul(float4 q1, float4 q2)
{
    return float4(
        q2.xyz * q1.w + q1.xyz * q2.w + cross(q1.xyz, q2.xyz),
        q1.w * q2.w - dot(q1.xyz, q2.xyz)
    );
}

// Vector rotation with a quaternion
// http://mathworld.wolfram.com/Quaternion.html
float3 rotate_vector(float3 v, float4 r)
{
    float4 r_c = r * float4(-1, -1, -1, 1);
    return qmul(r, qmul(float4(v, 0), r_c)).xyz;
}

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(

  // Mesh billboard info
  float4 iPos : POSITION,
  float2 Uv: TEXCOORD0,

  // Particle instance data
  ParticleRenderData particle
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;

  float3 localPos = iPos.xyz;
  if (psystem_stretched_billboard) {
    // Stretched billboard render type
    float3 velocity = particle.Velocity.xyz;
    if (psystem_velocity_affects_stretch) {
      float speed = length(velocity);
      localPos.y *= max(min(psystem_stretch * speed - (psystem_stretch - 1), psystem_stretch), 1);
    }
    else
      localPos.y *= psystem_stretch;

    float3 orient = cross(CameraPosition - particle.Pos, velocity);

    // orient billboard according to velocity and particle position relative to camera
    localPos = localPos.x * normalize(orient)
              + localPos.y * normalize(velocity);
  }
  else {
    // Billboard render type
    float angle = 0.0;
    if (psystem_random_rotation)
      angle = frac( sin(particle.Id) * 43758.5453) * PI * 2;   // get random angle
    else
      angle = psystem_rotation_offset;

    // This rotates a vector.
    localPos.x = cos(angle) * iPos.x - sin(angle) * iPos.y;
    localPos.y = sin(angle) * iPos.x + cos(angle) * iPos.y; 

    // orient billboard to camera
    localPos = localPos.x * CameraLeft
              + localPos.y * CameraUp;
  }

  float t = particle.Time;
  float partColSizeRandomVal = getRandomNumber(particle.Id);
  
  /* Particle color. */
  float4 particleColor;

  // Particle has an starting random color.
  if(psystem_random_color){
    float color_random;
    if(psystem_random_dynamic_color)
      color_random = fmod(partColSizeRandomVal + t, 1.0); // Get base particle color.
    else
      color_random = fmod(partColSizeRandomVal, 1.0); // Get base particle color.
    
    // Get alpha for color blending, we use the alphas from the colors.
    // Convert range 0..1 to 0 - 11
    float time_in_color_table = t * ( 12 - 1 );
    float color_idx;
    float color_amount = modf( time_in_color_table, color_idx);
    
    float4 color_min_random = psystem_min_colors_over_time[ color_idx ] * ( 1 - color_amount ) + psystem_min_colors_over_time[ color_idx + 1 ] * color_amount;
    float4 color_max_random = psystem_max_colors_over_time[ color_idx ] * ( 1 - color_amount ) + psystem_max_colors_over_time[ color_idx + 1 ] * color_amount;

    particleColor = color_min_random * ( 1.0 - color_random ) + color_max_random * color_random;
  }
  else{
    // Convert range 0..1 to 0 - 11
    float time_in_color_table = t * ( 12 - 1 );

    // if time_in_color_table = 1.2. It returns color_entry = 1., color_amount = 0.2
    float color_entry;
    float color_amount = modf( time_in_color_table, color_entry);
    particleColor = psystem_min_colors_over_time[ color_entry ] * ( 1 - color_amount )
                          + psystem_min_colors_over_time[ color_entry + 1 ] * ( color_amount );
  }
  output.Color = particleColor;

  /* Particles size. */
  float particle_size;
  
  // Particle has an starting random size.
  if(psystem_random_size){
    // Convert range 0..1 to 0..11
    float time_in_size_table = t * ( 12 - 1 );

    // Get base particle size.
    // if time_in_size_table = 1.2.      size_entry = 1., size_amount = 0.2
    float size_idx;
    float size_amount_between_times = modf(time_in_size_table, size_idx);
    float4 sizes_in_time = psystem_sizes_over_time[ size_idx ] * ( 1 - size_amount_between_times )
                         + psystem_sizes_over_time[ size_idx + 1 ] * ( size_amount_between_times );

    // x and y contain the min and max values our particle can go between.
    float size_amount = modf(partColSizeRandomVal, size_idx);
    particle_size = sizes_in_time.x + sizes_in_time.y * (size_amount);

    // Check if we must change size, we use the alpha of the value for it.
    float size_mod = sizes_in_time.a;
    particle_size = particle_size * size_mod;
  }
  else{
    // Convert range 0..1 to 0..11
    float time_in_size_table = t * ( 12 - 1 );

    // if time_in_size_table = 1.2.      size_entry = 1., size_amount = 0.2
    float size_entry;
    float size_amount = modf( time_in_size_table, size_entry);
    particle_size = psystem_sizes_over_time[ size_entry ].x * ( 1 - size_amount )
                          + psystem_sizes_over_time[ size_entry + 1 ].x * ( size_amount );
  }
  localPos *= particle_size;

  // Translate to particle position
  float3 worldPos = particle.Pos + localPos;
  output.Pos = mul(float4(worldPos,1), ViewProjection);

  /* Get particle UV */
  // Get random uv value from 0.0 to 1.0 if random.
  float particle_uv;
  if(psystem_random_uvs){
    particle_uv = frac(sin(particle.Id) * 43758.5453);
    if(psystem_random_uvs_change_with_time)
      particle_uv = fmod(t * psystem_uvs_speed_change + particle_uv, 1.0);
  }
  else
    particle_uv = fmod(t * psystem_uvs_speed_change, 1.0);

  // Advance animation frame..
  // If time goes between 0..1 => nframes goes between 0..#frames-1 and 0..#frames.y
  float2 nframe = float2(particle_uv * psystem_nframes.y * psystem_nframes.x, particle_uv * psystem_nframes.y );
  float nextParticleUV = fmod(particle_uv + 1.0/(psystem_nframes.y * psystem_nframes.x), 1.0); 
  float2 nextFrame = float2(nextParticleUV * psystem_nframes.y * psystem_nframes.x, nextParticleUV * psystem_nframes.y );

  // Get integer parts
  float2 ixy, remainder;
  remainder = modf( nframe, ixy );

  // If we blend, we get the uv of the next value.
  if(psystem_uvs_blend){
    // Get integer next uv to blend. We apply modulo to not go outside margins.
    // Maybe we could also check if we don't want to go to 0 again. For now
    // I'm not writing it as we don't need it.
    float2 ixyNextFrame;
    modf( nextFrame, ixyNextFrame );

    // Now coords goes from (#x,#y)..(#x+1,#y+1)
    float2 uvs = Uv;
    uvs.xy += ixyNextFrame;

    // Scale to 0..1.
    output.UvNext = uvs * psystem_frame_size;
    output.UvBlend = remainder;
  }

  // Now coords goes from (#x,#y)..(#x+1,#y+1)
  Uv.xy += ixy;

  // Scale to 0..1
  output.Uv = Uv * psystem_frame_size;

  return output;
}

VS_OUTPUT_SPHERICAL VS_SPHERICAL(

  // Mesh billboard info
  float4 iPos : POSITION,
  float2 Uv: TEXCOORD0,

  // Particle instance data
  ParticleRenderData particle
)
{
  VS_OUTPUT_SPHERICAL output = (VS_OUTPUT_SPHERICAL)0;

  float3 localPos = iPos.xyz;

  float angle = 0.0;
  if (psystem_random_rotation)
    angle = frac( sin(particle.Id) * 43758.5453) * PI * 2;   // get random angle
  else
    angle = psystem_rotation_offset;
  localPos.x = cos(angle) * iPos.x - sin(angle) * iPos.y;
  localPos.y = sin(angle) * iPos.x + cos(angle) * iPos.y; 

  // orient billboard to camera
  localPos = localPos.x * CameraLeft
            + localPos.y * CameraUp;

  float t = particle.Time;

  // Convert range 0..1 to 0 - 11
  float time_in_color_table = t * ( 12 - 1 );

  // if time_in_color_table = 1.2. It returns color_entry = 1., color_amount = 0.2
  float color_entry;
  float color_amount = modf( time_in_color_table, color_entry);
  float4 color_over_time = psystem_min_colors_over_time[ color_entry ] * ( 1 - color_amount )
                         + psystem_min_colors_over_time[ color_entry + 1 ] * ( color_amount );

  output.Color = color_over_time;

  // Convert range 0..1 to 0..11
  float time_in_size_table = t * ( 12 - 1 );

  // if time_in_size_table = 1.2.      size_entry = 1., size_amount = 0.2
  float size_entry;
  float size_amount = modf( time_in_size_table, size_entry);
  float size_over_time = psystem_sizes_over_time[ size_entry ].x * ( 1 - size_amount )
                         + psystem_sizes_over_time[ size_entry + 1 ].x * ( size_amount );

  localPos *= size_over_time;

  // Translate to particle position
  float3 worldPos = particle.Pos + localPos;

  output.Pos = mul(float4(worldPos,1), ViewProjection);

  output.ParticlePos = mul(float4(particle.Pos,1), View).xyz;
  output.ParticlePointPos = mul(float4(worldPos,1), View).xyz;
  output.Radius = size_over_time / 2.0;

  // Advance animation frame..
  // If time goes between 0..1 => nframes goes between 0..#frames-1 and 0..#frames.y
  float2 nframe = float2(t * psystem_nframes.y * psystem_nframes.x, t * psystem_nframes.y );

  // Get integer parts
  float2 ixy;
  modf( nframe, ixy );

  // Now coords goes from (#x,#y)..(#x+1,#y+1)
  Uv.xy += ixy;

  // Scale to 0..1
  output.Uv = Uv * psystem_frame_size;
  
  return output;
}

VS_OUTPUT_MESH VS_MESH(

  // Mesh billboard info
  float4 iPos : POSITION,
  float3 N : NORMAL,
  float2 Uv: TEXCOORD0,

  // Particle instance data
  ParticleRenderData particle
)
{
  VS_OUTPUT_MESH output = (VS_OUTPUT_MESH)0;

  float3 localPos = rotate_vector(iPos.xyz, particle.Velocity);

  float t = particle.Time;
  float partColSizeRandomVal = getRandomNumber(particle.Id);
  
  /* Particle color. */
  float4 particleColor;

  // Particle has an starting random color.
  if(psystem_random_color){
    float color_random;
    if(psystem_random_dynamic_color)
      color_random = fmod(partColSizeRandomVal + t, 1.0); // Get base particle color.
    else
      color_random = fmod(partColSizeRandomVal, 1.0); // Get base particle color.
    
    // Get alpha for color blending, we use the alphas from the colors.
    // Convert range 0..1 to 0 - 11
    float time_in_color_table = t * ( 12 - 1 );
    float color_idx;
    float color_amount = modf( time_in_color_table, color_idx);
    
    float4 color_min_random = psystem_min_colors_over_time[ color_idx ] * ( 1 - color_amount ) + psystem_min_colors_over_time[ color_idx + 1 ] * color_amount;
    float4 color_max_random = psystem_max_colors_over_time[ color_idx ] * ( 1 - color_amount ) + psystem_max_colors_over_time[ color_idx + 1 ] * color_amount;

    particleColor = color_min_random * ( 1.0 - color_random ) + color_max_random * color_random;
  }
  else{
    // Convert range 0..1 to 0 - 11
    float time_in_color_table = t * ( 12 - 1 );

    // if time_in_color_table = 1.2. It returns color_entry = 1., color_amount = 0.2
    float color_entry;
    float color_amount = modf( time_in_color_table, color_entry);
    particleColor = psystem_min_colors_over_time[ color_entry ] * ( 1 - color_amount )
                          + psystem_min_colors_over_time[ color_entry + 1 ] * ( color_amount );
  }
  output.Color = particleColor;

  /* Particles size. */
  float particle_size;
  
  // Particle has an starting random size.
  if(psystem_random_size){
    // Convert range 0..1 to 0..11
    float time_in_size_table = t * ( 12 - 1 );

    // Get base particle size.
    // if time_in_size_table = 1.2.      size_entry = 1., size_amount = 0.2
    float size_idx;
    float size_amount_between_times = modf(time_in_size_table, size_idx);
    float4 sizes_in_time = psystem_sizes_over_time[ size_idx ] * ( 1 - size_amount_between_times )
                         + psystem_sizes_over_time[ size_idx + 1 ] * ( size_amount_between_times );

    // x and y contain the min and max values our particle can go between.
    float size_amount = modf(partColSizeRandomVal, size_idx);
    particle_size = sizes_in_time.x + sizes_in_time.y * (size_amount);

    // Check if we must change size, we use the alpha of the value for it.
    float size_mod = sizes_in_time.a;
    particle_size = particle_size * size_mod;
  }
  else{
    // Convert range 0..1 to 0..11
    float time_in_size_table = t * ( 12 - 1 );

    // if time_in_size_table = 1.2.      size_entry = 1., size_amount = 0.2
    float size_entry;
    float size_amount = modf( time_in_size_table, size_entry);
    particle_size = psystem_sizes_over_time[ size_entry ].x * ( 1 - size_amount )
                          + psystem_sizes_over_time[ size_entry + 1 ].x * ( size_amount );
  }
  localPos *= particle_size;

  // Translate to particle position
  float3 worldPos = particle.Pos + localPos;
  output.Pos = mul(float4(worldPos,1), ViewProjection);

  /* Get particle UV */
  // Get random uv value from 0.0 to 1.0 if random.
  float particle_uv;
  if(psystem_random_uvs){
    particle_uv = frac(sin(particle.Id) * 43758.5453);
    if(psystem_random_uvs_change_with_time)
      particle_uv = fmod(t * psystem_uvs_speed_change + particle_uv, 1.0);
  }
  else
    particle_uv = fmod(t * psystem_uvs_speed_change, 1.0);

  // Advance animation frame..
  // If time goes between 0..1 => nframes goes between 0..#frames-1 and 0..#frames.y
  float2 nframe = float2(particle_uv * psystem_nframes.y * psystem_nframes.x, particle_uv * psystem_nframes.y );
  float nextParticleUV = fmod(particle_uv + 1.0/(psystem_nframes.y * psystem_nframes.x), 1.0); 
  float2 nextFrame = float2(nextParticleUV * psystem_nframes.y * psystem_nframes.x, nextParticleUV * psystem_nframes.y );

  // Get integer parts
  float2 ixy, remainder;
  remainder = modf( nframe, ixy );

  // If we blend, we get the uv of the next value.
  if(psystem_uvs_blend){
    // Get integer next uv to blend. We apply modulo to not go outside margins.
    // Maybe we could also check if we don't want to go to 0 again. For now
    // I'm not writing it as we don't need it.
    float2 ixyNextFrame;
    modf( nextFrame, ixyNextFrame );

    // Now coords goes from (#x,#y)..(#x+1,#y+1)
    float2 uvs = Uv;
    uvs.xy += ixyNextFrame;

    // Scale to 0..1.
    output.UvNext = uvs * psystem_frame_size;
    output.UvBlend = remainder;
  }

  // Now coords goes from (#x,#y)..(#x+1,#y+1)
  Uv.xy += ixy;

  // Scale to 0..1
  output.Uv = Uv * psystem_frame_size;

  output.N = rotate_vector(N, particle.Velocity);

  return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
  float4 texture_color = txAlbedo.Sample(samLinear, input.Uv);
  
  // Blend if necessary
  if(psystem_uvs_blend){
    float4 texture_color_to_blend = txAlbedo.Sample(samLinear, input.UvNext);
    texture_color = (1.0 - input.UvBlend) * texture_color + input.UvBlend * texture_color_to_blend;
  }

  return texture_color * input.Color;
}

float4 PS_SPHERICAL(VS_OUTPUT_SPHERICAL input) : SV_Target
{
  float4 texture_color = txAlbedo.Sample(samLinear, input.Uv);

  int3 ss_load_coords = uint3(input.Pos.xy, 0);
  float zlinear = txGLinearDepth.Load(ss_load_coords).x * CameraZFar;

  float alpha = 0.0;
  float dist = length(input.ParticlePos.xy - input.ParticlePointPos.xy);
  float radius = input.Radius;
  if (dist < radius) {
    float depth = sqrt(radius * radius - dist * dist);
    float front = -input.ParticlePos.z - depth;
    float back = -input.ParticlePos.z + depth;
    float totalDepth = min(zlinear, back) - max(front, CameraZNear);
    alpha = clamp(texture_color.a - exp(-2*PI*(1 - dist / radius) * totalDepth), 0, 1);
  }
  texture_color.a = alpha; 
  return texture_color * input.Color;
}

float4 PS_MESH(VS_OUTPUT_MESH input) : SV_Target
{
  float4 texture_color = txAlbedo.Sample(samLinear, input.Uv);
  
  float3 L = float3(0.7071, 0.7071, 0);
  float NdotL = ((dot(input.N, L) + 1.0) / 2.0);

  // Blend if necessary
  if(psystem_uvs_blend){
    float4 texture_color_to_blend = txAlbedo.Sample(samLinear, input.UvNext);
    texture_color = (1.0 - input.UvBlend) * texture_color + input.UvBlend * texture_color_to_blend;
  }

  float4 final_color = texture_color * input.Color;
  final_color = float4(final_color.rgb * NdotL, final_color.a);
  return final_color;
}