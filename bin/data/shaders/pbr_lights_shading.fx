#include "pbr_common.fx" // PBR functions.
#include "gbuffer.inc" // To take information from the gbuffer.

//--------------------------------------------------------------------------------------
//  Shadow sampling functions.
//--------------------------------------------------------------------------------------

// Vogel disk sampling for shadows.
float2 vogelDiskSample(int sampleIndex, int samplesCount, float phi)
{
  float GoldenAngle = 2.4f;
  float r = sqrt((sampleIndex + 0.5f) / (float) samplesCount);
  float theta = sampleIndex * GoldenAngle + phi;
  float sine = sin( theta );
  float cosine = cos( theta );
  return float2(r * cosine, r * sine);
}

// Reads a shadow value from a single texture.
float shadowsTap(float2 homo_coord, float coord_z) {
  return txLightShadow.SampleCmp(samPCF, homo_coord, coord_z, 0).x;
}

// Reads a shadow value from a 2D texture array.
float shadowsTapArray(const int textureIndex, float2 homo_coord, float coord_z) {
  return txLightShadowArray.SampleCmp(samPCF, float3(homo_coord, textureIndex), coord_z, 0).x;
}

float getShadowFactorCSM( int textureIndex, float3 wPos) {
  float4 PositionInLightProjSpace = mul( float4(wPos,1), LightViewProjOffset);
  PositionInLightProjSpace.xyz += CascadeOffsets[textureIndex].xyz;
  PositionInLightProjSpace.xyz *= CascadeScales[textureIndex].xyz;
  
  float3 PositionLightHomoSpace = PositionInLightProjSpace.xyz / PositionInLightProjSpace.w;
  
  if( PositionLightHomoSpace.z < 0 ) return 0.f;

  // Poisson distribution random points around a circle
  const float2 offsets[] = {
    float2(0,0),
    float2(-0.3700152, 0.575369),
    float2(0.5462944, 0.5835142),
    float2(-0.4171277, -0.2965972),
    float2(-0.8671125, 0.4483297),
    float2(0.183309, 0.1595028),
    float2(0.6757001, -0.4031624),
    float2(0.8230421, 0.1482845),
    float2(0.1492012, 0.9389217),
    float2(-0.2219742, -0.7762423),
    float2(-0.9708459, -0.1171268),
    float2(0.2790326, -0.8920202)
  };

  float scale_factor = LightShadowStepDivResolution;
  
  const int nsamples = 12;
  float shadow_factor = 0.0;
  [unroll]
  for( int i=0; i<nsamples; ++i ) {
    float2 coord = offsets[ i ];
    float2 uv = PositionLightHomoSpace.xy + coord * LightShadowStepDivResolution;
    shadow_factor += shadowsTapArray(textureIndex, uv, PositionLightHomoSpace.z).x;
  }
  shadow_factor /= nsamples;

  /* Show texture resolution */
  //const int shadowResolution = 1024;
  //int2 c = int2(uv.xy * shadowResolution);
  //int2 c2 = int2(c.x & 1, c.y & 1);
  //return c2.x != c2.y;

  return shadow_factor;
}

float getShadowFactor( float3 wPos, float3 PositionLightHomoSpace ) {  
  if( PositionLightHomoSpace.z < 0 ) return 0.f;

  // Poisson distribution random points around a circle
  const float2 offsets[] = {
    float2(0,0),
    float2(-0.3700152, 0.575369),
    float2(0.5462944, 0.5835142),
    float2(-0.4171277, -0.2965972),
    float2(-0.8671125, 0.4483297),
    float2(0.183309, 0.1595028),
    float2(0.6757001, -0.4031624),
    float2(0.8230421, 0.1482845),
    float2(0.1492012, 0.9389217),
    float2(-0.2219742, -0.7762423),
    float2(-0.9708459, -0.1171268),
    float2(0.2790326, -0.8920202)
  };

  
  float scale_factor = LightShadowStepDivResolution;
  float angle = hash2( wPos.x + hash2( wPos.z ).x ).x;
  float cos_a = cos( angle ) * scale_factor;
  float sin_a = sin( angle ) * scale_factor;

  const int nsamples = 12;
  float shadow_factor = 0.0;
  [unroll]
  for( int i = 0; i < nsamples; ++i ) {
    float2 coord = offsets[ i ];
    float2 rotated_coord = float2( coord.x * cos_a - coord.y * sin_a,
                    coord.y * cos_a + coord.x * sin_a );

    //float2 uv = PositionLightHomoSpace.xy;
    //float2 uv = PositionLightHomoSpace.xy + rotated_coord;
    float2 uv = PositionLightHomoSpace.xy + coord * LightShadowStepDivResolution;
    shadow_factor += shadowsTap( uv, PositionLightHomoSpace.z).x;
  }

  shadow_factor /= nsamples;

  return shadow_factor;
}

/*
  PCF. Don't like it. Maybe it's wrong.
    
   float shadow_factor = 0.0;

   const int pcfCount = 2;
   const float totalTexels = (pcfCount * 2.0 + 1.0) * (pcfCount * 2.0 + 1.0);
   const float mapSize = 1024;
   float texelSize = 1.0 / mapSize;

   [unroll]
   for(int x = -pcfCount; x <= pcfCount; ++x)
     for(int y = -pcfCount; y <= pcfCount; ++y)
       shadow_factor += shadowsTap( PositionLightHomoSpace.xy
       + float2(x, y) * texelSize, PositionLightHomoSpace.z).x;
  
  shadow_factor /= totalTexels;
*/

// We check if the pixel is outside the light bound, our values go from 0.0 to 1.0 due
// to the offset added to the ViewProj of the light.
float CheckPositionOutsideLightBounds(float3 PositionLightHomoSpace){
  if(PositionLightHomoSpace.x > 1.0 || PositionLightHomoSpace.x < 0.0
  || PositionLightHomoSpace.y > 1.0 || PositionLightHomoSpace.y < 0.0
  || PositionLightHomoSpace.z > 1.0 || PositionLightHomoSpace.z < 0.0)
    return 0.0;
  return 1.0;
}

//--------------------------------------------------------------------------------------
//  Shade functions.
//--------------------------------------------------------------------------------------

// Shading function for both point lights and spot lights.
float4 shadePoint( float4 iPosition, bool use_shadows ) {

  // Fetch GBuffer information about the pixel.
  GBuffer g;
  decodeGBuffer( iPosition.xy, g );
  if(!g.wasDrawnTo) return (0,0,0,0); // Maybe i should remove this in the future when scattering.

  // From wPos to Light
  float3 light_dir_full = LightPosition.xyz - g.wPos;
  float  distance_to_light = length( light_dir_full );
  float3 light_dir = light_dir_full / distance_to_light;

  float  NdL = saturate(dot(g.N, light_dir));
  float  NdV = saturate(dot(g.N, g.view_dir));
  float3 h   = normalize(light_dir + g.view_dir); // half vector
  
  float  NdH = saturate(dot(g.N, h));
  float  VdH = saturate(dot(g.view_dir, h));
  float  LdV = saturate(dot(light_dir, g.view_dir));
  float  a   = max(0.001f, g.roughness * g.roughness);
  float3 cDiff = Diffuse(g.albedo);
  float3 cSpec = Specular(g.specular_color, h, g.view_dir, light_dir, a, NdL, NdV, NdH, VdH, LdV);

  // Light attenuation.
  float att = saturate(distance_to_light/LightRadius );
  att = 1. - att;

  // Calculate the light final intensity.
  float lightFinalIntensity = att * LightIntensity;

  float3 final_color = LightColor.xyz * NdL * (cDiff * (1.0f - cSpec) + cSpec) * lightFinalIntensity;
  return float4( final_color, 1 );
}

// Shading function for both point lights and spot lights.
float4 shadeSpot( float4 iPosition, bool use_shadows ) {

  // Fetch GBuffer information about the pixel.
  GBuffer g;
  decodeGBuffer( iPosition.xy, g );
  if(!g.wasDrawnTo) return (0,0,0,0); // Maybe i should remove this in the future when scattering.

  // Get projected position according to light.
  float4 PositionInLightProjSpace = mul( float4(g.wPos, 1), LightViewProjOffset );
  float3 PositionLightHomoSpace = PositionInLightProjSpace.xyz / PositionInLightProjSpace.w;

  // Shadow factor entre 0 (totalmente en sombra) y 1 (no ocluido).
  float visibility_factor = use_shadows ? getShadowFactor(g.wPos, PositionLightHomoSpace)
   : CheckPositionOutsideLightBounds(PositionLightHomoSpace);

  // From wPos to Light
  float3 light_dir_full = LightPosition.xyz - g.wPos;
  float  distance_to_light = length( light_dir_full );
  float3 light_dir = light_dir_full / distance_to_light;

  float  NdL = saturate(dot(g.N, light_dir));
  float  NdV = saturate(dot(g.N, g.view_dir));
  float3 h   = normalize(light_dir + g.view_dir); // half vector
  
  float  NdH = saturate(dot(g.N, h));
  float  VdH = saturate(dot(g.view_dir, h));
  float  LdV = saturate(dot(light_dir, g.view_dir));
  float  a   = max(0.001f, g.roughness * g.roughness);
  float3 cDiff = Diffuse(g.albedo);
  float3 cSpec = Specular(g.specular_color, h, g.view_dir, light_dir, a, NdL, NdV, NdH, VdH, LdV);

  // Light attenuation.
  float att = saturate( distance_to_light / LightRadius );
  att = 1. - att;

  // Get projection texture. This texture is used to give a shape to the light instead
  // of using the frustum shape.
  float projection = txProjector.Sample(samLinear, PositionLightHomoSpace).r;

  // Calculate the light final intensity.
  float lightFinalIntensity = att * LightIntensity * visibility_factor * projection;

  float3 final_color = LightColor.xyz * NdL * (cDiff * (1.0f - cSpec) + cSpec) * lightFinalIntensity;
  return float4( final_color, 1 );
}

// Shade function for directional lights. As directional lights don't have a position, they represent a light very far away,
// like the sun, the function for shading is slitgly different, treating the light direction as a constant, instead of based
// on the vertex and light position. Furthermore, directional lights use cascade shadow maps for shading.
float4 shadeDirectionalLight( float4 iPosition, bool use_shadows ) {

  // Declare some float3 to store the values from the GBuffer
  GBuffer g;
  decodeGBuffer( iPosition.xy, g );
  if(!g.wasDrawnTo) return (0,0,0,0);
  
  float clipSpaceDepth  = mul(float4(g.wPos, 1.0f), ViewProjection).z; // Position of the pixel in clip space.
  float4 projectionPos = mul(float4(g.wPos, 1.0f), LightViewProjOffset);
  
  float visibility_factor = 1.0; // 0 = shadowed, 1 = not shadowed, ...
  int cascadeIdx = -1;
  float4 CascadeIndicator = float4(0.0f, 0.0f, 0.0f, 0.0f);
  if(use_shadows){
    /* Sample from cascade */
    for (int i = 0 ; i < NUM_CASCADES_SHADOW_MAP ; i++) {
      // Select based on whether or not the pixel is inside the projection
      // used for rendering to the cascade
      float3 cascadePos = projectionPos + CascadeOffsets[i].xyz;
      cascadePos *= CascadeScales[i].xyz;
      cascadePos = abs(cascadePos - 0.5f);
      if(all(cascadePos <= 0.5f)){
        cascadeIdx = i;
        visibility_factor = getShadowFactorCSM( cascadeIdx, g.wPos );
        // Debug.
        if (cascadeIdx == 0)
          CascadeIndicator = float4(255, 0.0, 0.0, 0.0);
        else if (cascadeIdx == 1)
          CascadeIndicator = float4(0.0, 255, 0.0, 0.0);
        else if (cascadeIdx == 2)
          CascadeIndicator = float4(0.0, 0.0, 255, 0.0);
        break;
      }
    }

    /* Blend between cascades */
    if(cascadeIdx != -1){
      
      // Sample the next cascade, and blend between the two results to
      // smooth the transition. Blends between all cascades.
       const float BlendThreshold = 0.2f; // Start blending at 5% of the cascade. (5% not 0.05%!).
       float cascadeSplit = CascadeEndClipSpace[cascadeIdx].z;
       float splitSize = cascadeIdx == 0 ? cascadeSplit : cascadeSplit - CascadeEndClipSpace[cascadeIdx - 1].z;
       float fadeFactor = (cascadeSplit - clipSpaceDepth) / splitSize;
      
       float3 cascadePos = projectionPos + CascadeOffsets[cascadeIdx].xyz;
       cascadePos *= CascadeScales[cascadeIdx].xyz;
       cascadePos = abs(cascadePos * 2.0f - 1.0f);
       float distToEdge = 1.0f - max(max(cascadePos.x, cascadePos.y), cascadePos.z);
       fadeFactor = max(distToEdge, fadeFactor);
       
       [branch]
       if(fadeFactor <= BlendThreshold && cascadeIdx != (NUM_CASCADES_SHADOW_MAP - 1))
       {
         float3 nextSplitVisibility = getShadowFactorCSM( cascadeIdx+1, g.wPos);
         float lerpAmt = smoothstep(0.0f, BlendThreshold, fadeFactor);
         visibility_factor = lerp(nextSplitVisibility, visibility_factor, lerpAmt);
       }
       
       int lastCascadeIdx = NUM_CASCADES_SHADOW_MAP - 1;
       if(cascadeIdx == lastCascadeIdx){
         // Fade last cascade.
         const float fadeThreshold = 0.45f;
         float lastCascade = CascadeEndClipSpace[lastCascadeIdx].z;
         float splitSizeLastCascade = lastCascadeIdx == 0 ? lastCascade : lastCascade - CascadeEndClipSpace[lastCascadeIdx - 1].z;
         float fadeCascade = (lastCascade - clipSpaceDepth) / splitSizeLastCascade;
         if(fadeCascade <= fadeThreshold){
           float fadeFactor = 1.0f - fadeCascade/fadeThreshold;
           visibility_factor = clamp(visibility_factor + fadeFactor, 0.0f, 1.0f);
         }
        }
      }
    }

  // From wPos to Light
  float3 light_dir = -LightFront;

  float  NdL = saturate(dot(g.N, light_dir));
  float  NdV = saturate(dot(g.N, g.view_dir));
  float3 h   = normalize(light_dir + g.view_dir); // half vector
  
  float  NdH = saturate(dot(g.N, h));
  float  VdH = saturate(dot(g.view_dir, h));
  float  LdV = saturate(dot(light_dir, g.view_dir));
  float  a   = max(0.001f, g.roughness * g.roughness);
  float3 cDiff = Diffuse(g.albedo);
  float3 cSpec = Specular(g.specular_color, h, g.view_dir, light_dir, a, NdL, NdV, NdH, VdH, LdV);

  float3 final_color = LightColor.xyz * NdL * (cDiff * (1.0f - cSpec) + cSpec) * LightIntensity * visibility_factor;
  return float4(final_color, 1);
  return float4( CascadeIndicator.xyz * visibility_factor, 1 );
}