#include "screen_quad.fx"

/* Fog equations based on the ones found at:
http://in2gpu.com/2014/07/22/create-fog-shader/
http://www.iquilezles.org/www/articles/fog/fog.htm and
https://halisavakis.com/my-take-on-shaders-firewatch-multi-colored-fog/ */

/* Linear fog */
float3 ApplyLinearFog(float3 fragmentColor, float distanceCameraToWS){
  float fogFactor = clamp((FogEnd - distanceCameraToWS)/(FogEnd-FogStart), 0.0, 1.0);   // 0 = fog, 1 = no fog.
  return lerp(FogColor, fragmentColor, fogFactor);
}

/* Exponential fog */
// Fog density should be small (around 0.02 or 0.05) or the fog will cover a lot of space.
float3 ApplyExponentialFog(float3 fragmentColor, float distanceCameraToWS){
  float fogAmount = 1.0 - exp( -distanceCameraToWS * FogDensity );   // 0 = no fog, 1 = fog.
  return lerp( fragmentColor, FogColor, fogAmount );
}

/* Firewatch like fog. Linear. */
float3 ApplyLinearStylisticFog(float3 fragmentColor, float distanceCameraToWS){
  float fogFactor = 1.0 - clamp((FogEnd - distanceCameraToWS)/(FogEnd-FogStart), 0.0, 1.0);   // 0 = no fog, 1 = fog.
  float4 fogColorFromTex = txGradient.Sample(samClampLinear, float2(fogFactor, 0));
  float4 fogColorFromTex2 = txGradient2.Sample(samClampLinear, float2(fogFactor, 0));
  float4 fogFinalColor = lerp(fogColorFromTex, fogColorFromTex2, FogLerp);

  return lerp( fragmentColor, fogFinalColor.rgb, clamp(fogFactor * fogFinalColor.a * FogIntensity, 0.0, 1.0));
}

/* Firewatch like fog. Exponential. */
float3 ApplyExponentialStylisticFog(float3 fragmentColor, float distanceCameraToWS){
  float fogFactor = 1.0 - exp( -distanceCameraToWS * FogDensity ); // 0 = no fog, 1 = fog.
  float4 fogColorFromTex = txGradient.Sample(samClampLinear, float2(fogFactor, 0));
  float4 fogColorFromTex2 = txGradient2.Sample(samClampLinear, float2(fogFactor, 0));
  float4 fogFinalColor = lerp(fogColorFromTex, fogColorFromTex2, FogLerp);

  return lerp( fragmentColor, fogFinalColor.rgb, clamp(fogFactor * fogFinalColor.a * FogIntensity, 0.0, 1.0));
}

/* Firewatch like fog. Linear. */
float3 ApplyDebugLinear(float distanceCameraToWS){
  float fogFactor = 1.0 - clamp((FogEnd - distanceCameraToWS)/(FogEnd-FogStart), 0.0, 0.9);   // 0 = no fog, 1 = fog.
  return float3(fogFactor, 0.0, 0.0);
}

/* Firewatch like fog. Exponential. */
float3 ApplyDebugExponential(float distanceCameraToWS){
  float fogFactor = 1.0 - exp( -distanceCameraToWS * FogDensity ); // 0 = no fog, 1 = fog.
  return float3(fogFactor, 0.0, 0.0);
}

float4 PS_Apply_Fog(VS_FULL_OUTPUT input) : SV_Target
{
  float4 FragmentColor = txAlbedo.Sample(samLinear, input.UV);
  int3 ss_load_coords = uint3(input.Pos.xy, 0);
  float depth = txGLinearDepth.Load(ss_load_coords).x;
  
  // Don't add fog to the skybox by default.
  if(depth == 1.0) return FragmentColor;

  float3 worldPos = getWorldCoords(input.Pos.xy, depth);
  float3 cameraEyeToWSPos = worldPos - CameraPosition.xyz;

  // Linear.
  //FragmentColor = float4(ApplyLinearFog(FragmentColor, length(cameraEyeToWSPos)), 1.0);
  
  // Exponential.
  //FragmentColor = float4(ApplyExponentialFog(FragmentColor, length(cameraEyeToWSPos)), 1.0);

  // Advanced. Not used.
  //FragmentColor = float4(ApplyExponentialFogAdvanced(FragmentColor.rgb,
  //cameraEyeToWSPos), 1.0);

  // Stylistic linear.
  FragmentColor = float4(ApplyLinearStylisticFog(FragmentColor, length(cameraEyeToWSPos)), 1.0);
  
  // Stylistic exponential.
  //FragmentColor = float4(ApplyExponentialStylisticFog(FragmentColor, length(cameraEyeToWSPos)), 1.0);

  // Debug linear.
  //FragmentColor = float4(ApplyDebugLinear(length(cameraEyeToWSPos)), 1.0);

  // Debug exponential.
  //FragmentColor = float4(ApplyDebugExponential(length(cameraEyeToWSPos)), 1.0);

  return FragmentColor;
}

/* Exponential Fog Advanced */
/*
// Don't like it much. I leave it here just in case. :I
float3 ApplyExponentialFogAdvanced(float3 fragmentColor, float3 cameraEyeToWSPos){
  float distanceCameraToWS = length(cameraEyeToWSPos);
  float3 cameraEyeToWSPosNorm = cameraEyeToWSPos / distanceCameraToWS;
  
  // Get the final color of the fog.
  float sunContribution = max(dot(cameraEyeToWSPosNorm, FogSunDir), 0.0);
  float3 fogFinalColor = lerp(FogColor, FogColorEnd, pow(sunContribution, 8.0));

  // Get the amount of fog.
  float3 extinctionColor = float3(exp(-distanceCameraToWS * FogExtinction.x), exp(-distanceCameraToWS * FogExtinction.y), exp(-distanceCameraToWS * FogExtinction.z) );
  float3 inscatteringColor = float3(exp(-distanceCameraToWS * FogInscattering.x), exp(-distanceCameraToWS * FogInscattering.y), exp(-distanceCameraToWS * FogInscattering.z) );
  return fragmentColor * (1.0 - extinctionColor) + fogFinalColor * inscatteringColor;
}
*/