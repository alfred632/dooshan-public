//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
  float4 Pos : SV_POSITION;
  float2 Uv : TEXCOORD0;
  float4 Color : COLOR;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
  float4 Pos : POSITION,
  float2 Uv: TEXCOORD0,
  float4 Color : COLOR
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;
  output.Pos = mul(Pos, World);

  output.Pos = mul(output.Pos, ViewProjection);
  output.Color = Color * ObjColor;
  
  // Advance animation frame..
  float t = UItimeRatio;
  // If time goes between 0..1 => nframes goes between 0..#frames-1 and 0..#frames.y
  float2 nframe = float2(t * UInframes.y * UInframes.x, t * UInframes.y );

  // Get integer parts
  float2 ixy;
  modf( nframe, ixy );

  // Now coords goes from (#x,#y)..(#x+1,#y+1)
  Uv.xy += ixy;

  // Scale to 0..1
  output.Uv = Uv * UIframeSize;

  return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
  float2 final_uv = lerp(UIminUV, UImaxUV, input.Uv);
  float4 texture_color = txAlbedo.Sample(samLinear, final_uv.xy);
  return texture_color * input.Color * UItint;
}
