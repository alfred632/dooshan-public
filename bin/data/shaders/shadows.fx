#ifndef SHADOWS
#define SHADOWS

#include "common.fx"
#include "pbr_common.fx"

struct VS_OUTPUT_SHADOW
{
  float4 Pos   : SV_POSITION;
  // To generate the shadows I don't need the UV's or the normal
};

struct VS_OUTPUT_ALPHA
{
  float4 Pos   : SV_POSITION;
  float2 Uv    : TEXCOORD0;
};

// Base non instanced shadows.
VS_OUTPUT_SHADOW VS(
  float4 iPos : POSITION
  )
{
  VS_OUTPUT_SHADOW output = (VS_OUTPUT_SHADOW)0;
  float4 world_pos = mul( iPos, World );
  output.Pos = mul(world_pos, ViewProjection );
  return output;
}

// Base instancing without culling.
VS_OUTPUT_SHADOW VS_instanced(
  float4 iPos : POSITION,
  VS_INSTANCE_WORLD instance_data     // Stream 1
  )
{
  VS_OUTPUT_SHADOW output = (VS_OUTPUT_SHADOW)0;
  float4x4 instanceWorld = getWorldOfInstance(instance_data);
  float4 world_pos = mul( iPos, instanceWorld );
  output.Pos = mul(world_pos, ViewProjection );
  return output;
}

// For skinned shadows.
VS_OUTPUT_SHADOW VS_skin(
  float4 iPos : POSITION,
  VS_SKINNING skinning
  )
{
  float4x4 SkinMatrix = getSkinMtx( skinning );

  VS_OUTPUT_SHADOW output = (VS_OUTPUT_SHADOW)0;
  // Skinned pos
  float4 world_pos = mul(iPos, SkinMatrix);
  output.Pos = mul(world_pos, ViewProjection );
  return output;
}

// For shadows of materials with an alpha layer like grass or trees.
VS_OUTPUT_ALPHA VS_Alpha(
  float4 iPos : POSITION,
  float3 N : NORMAL,
  float2 uv   : TEXCOORD0
  )
{
  VS_OUTPUT_ALPHA output = (VS_OUTPUT_ALPHA)0;
  float4 world_pos = mul( iPos, World );
  output.Pos = mul(world_pos, ViewProjection );
  output.Uv = uv;
  return output;
}

// For shadows of materials with an alpha layer like grass or trees instanced
// without GPU culling.
VS_OUTPUT_ALPHA VS_instanced_Alpha(
  float4 iPos : POSITION,
  float3 N : NORMAL,
  float2 uv   : TEXCOORD0,
  VS_INSTANCE_WORLD instance_data     // Stream 1
  )
{
  VS_OUTPUT_ALPHA output = (VS_OUTPUT_ALPHA)0;
  float4x4 instanceWorld = getWorldOfInstance(instance_data);
  float4 world_pos = mul( iPos, instanceWorld );
  output.Pos = mul(world_pos, ViewProjection );
  output.Uv = uv;
  return output;
}

// Used for shadows of materials with an alpha like tree leaves.
void PS_Alpha_Shadows(VS_OUTPUT_ALPHA input){
  float4 albedo = txAlbedo.Sample(samLinear, input.Uv);
  
  // Cut under a given threshold. This allows for transparency.
  if( albedo.a <= 0.2 )
    clip(-1);
}

#endif