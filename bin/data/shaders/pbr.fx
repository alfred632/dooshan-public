#ifndef PBR
#define PBR

#include "common.fx" // Common function used by this shaders.
#include "pbr_common.fx" // PBR functions.
#include "gbuffer.inc" // To take information from the gbuffer.
#include "pbr_lights_shading.fx" // Shading lights functions. Including shadows functions.

// Default vs.
VS_OUTPUT VS(
  VS_INPUT input
)
{
  // Use world from the constants uniform
  return runObjVS( input, World );
}

// Vertex Shader for Skin, using standard vertex + skin info.
VS_OUTPUT VS_skin(
  VS_INPUT input,
  VS_SKINNING skinning
)
{
  float4x4 SkinMatrix = getSkinMtx( skinning );
  return runObjVS( input, SkinMatrix );
}

// VS for instances.
VS_OUTPUT VS_instanced (
  VS_INPUT input,
  VS_INSTANCE_WORLD instance_data     // Stream 1
)
{
  // Use world from the instance
  float4x4 instanceWorld = getWorldOfInstance(instance_data);
  return runObjVS( input, instanceWorld );
}

//--------------------------------------------------------------------------------------
// Pixel Shader declarations.
//--------------------------------------------------------------------------------------

void PS_Common(
  VS_OUTPUT input,
  out float4 o_albedo,
  out float4 o_normal,
  out float1 o_depth,
  out float4 o_self_illumination,
  out float1 o_acc_depth,
  bool use_alpha_test)
  {
    // Albedo.
    float4 albedo_color = txAlbedo.Sample(samLinear, input.Uv);
    
    if( use_alpha_test && albedo_color.a <= 0.2 )
      clip(-1);
    
    o_albedo.rgb = albedo_color.rgb;
    o_albedo.a = txMetallic.Sample(samLinear, input.Uv).r * scalar_metallic;

    // Normal mapping
    float4 N_tangent_space = txNormal.Sample(samLinear, input.Uv);  // Between 0..1
    N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1
    //N_tangent_space.z *= 0.7;
    //N_tangent_space.xyz = normalize(N_tangent_space.xyz);
    float3x3 TBN = computeTBN( input.N, input.T ); 
    float3 N = mul( N_tangent_space.xyz, TBN );   // Normal from NormalMap

    // Save roughness in the alpha coord of the N render target
    float roughness = txRoughness.Sample(samLinear, input.Uv).r * scalar_roughness;
    o_normal = encodeNormal(N, roughness);

    float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
    float  linear_depth = dot( cam2obj, CameraFront ) / CameraZFar;

    o_depth = linear_depth;
    o_acc_depth = linear_depth;

    // Self illumination. We could use the emissive and store the AO
    // in the alpha channel for further optimization. We are leavint
    // it like this for now.
    float3 emissive_color = txEmissive.Sample(samLinear, input.Uv).rgb * scalar_emissive;
    float ao = txAOMap.Sample(samLinear, input.Uv).r * scalar_ao;
    o_self_illumination = float4(emissive_color, ao);
  }

// Pixel shader used for filling the gBuffer. The first variable fill the first RT and so on...
void PS( 
  VS_OUTPUT input,
  out float4 o_albedo : SV_Target0,
  out float4 o_normal : SV_Target1,
  out float1 o_depth  : SV_Target2,
  out float4 o_self_illumination  : SV_Target3,
  out float1 o_acc_depth  : SV_Target4
)
{
  PS_Common(input, o_albedo, o_normal, o_depth, o_self_illumination, o_acc_depth, false);
}

// Pixel shader used for filling the gBuffer. The first variable fill the first RT and so on...
// This version is used for rendering trees, grass, and things with an alpha.
void PS_Alpha( 
  VS_OUTPUT input,
  out float4 o_albedo : SV_Target0,
  out float4 o_normal : SV_Target1,
  out float1 o_depth  : SV_Target2,
  out float4 o_self_illumination  : SV_Target3,
  out float1 o_acc_depth  : SV_Target4
)
{
  PS_Common(input, o_albedo, o_normal, o_depth, o_self_illumination, o_acc_depth, true);
}

//--------------------------------------------------------------------------------------
void PS_gbuffer_mix(
  VS_OUTPUT input
, out float4 o_albedo : SV_Target0
, out float4 o_normal : SV_Target1
, out float1 o_depth  : SV_Target2
, out float4 o_self_illumination  : SV_Target3
, out float1 o_acc_depth  : SV_Target4
) {
  // This is different -----------------------------------------
  float2 iTexR = input.Uv * mix_scale_r;
  float2 iTexG = input.Uv * mix_scale_g;
  float2 iTexB = input.Uv * mix_scale_b;
  float2 iTex1 = input.Uv;

  float4 weight_texture_boost = txMixBlendWeights.Sample(samLinear, iTex1); 

  float4 albedoR = txAlbedo.Sample(samLinear, iTexR);
  float4 albedoG = txAlbedo1.Sample(samLinear, iTexG);
  float4 albedoB = txAlbedo2.Sample(samLinear, iTexB);

  // Use the alpha of the albedo as heights + texture blending extra weights + material ctes extra weights (imgui)
  float w1, w2, w3;
  computeBlendWeights( albedoR.a + mix_boost_r + weight_texture_boost.r
                     , albedoG.a + mix_boost_g + weight_texture_boost.g
                     , albedoB.a + mix_boost_b + weight_texture_boost.b
                     , w1, w2, w3 );

  // Use the weight to 'blend' the albedo colors
  float4 albedo = albedoR * w1 + albedoG * w2 + albedoB * w3;
  o_albedo.xyz = albedo.xyz;

  // Mix the normal
  float3 normalR = txNormal.Sample(samLinear, iTexR).xyz * 2.0 - 1.0;
  float3 normalG = txNormal1.Sample(samLinear, iTexG).xyz * 2.0 - 1.0;
  float3 normalB = txNormal2.Sample(samLinear, iTexB).xyz * 2.0 - 1.0;
  float3 normal_color = normalR * w1 + normalG * w2 + normalB * w3; 
  float3x3 TBN = computeTBN( input.N, input.T );

  // Normal map comes in the range 0..1. Recover it in the range -1..1
  float3 wN = mul( normal_color, TBN );
  float3 N = normalize( wN );

  // Missing: Do the same with the metallic & roughness channels
  // ...

  // Possible plain blending without heights
  //o_albedo.xyz = lerp( albedoB.xyz, albedoG.xyz, weight_texture_boost.y );
  //o_albedo.xyz = lerp( o_albedo.xyz, albedoR.xyz, weight_texture_boost.x );

  //o_albedo.xyz = float3( iTex1.xy, 0 );   // Show the texture coords1
  //o_albedo.xyz = weight_texture_boost.xyz;  // Show the extra weight textures
  o_albedo.a = txMetallic.Sample(samLinear, iTexR).r;

  // This is the same -----------------------------------------
  // Save roughness in the alpha coord of the N render target
  float roughness = txRoughness.Sample(samLinear, iTexR).r;
  o_normal = encodeNormal( N, roughness );

  // Compute the Z in linear space, and normalize it in the range 0...1
  // In the range z=0 to z=zFar of the camera (not zNear)
  float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
  float  linear_depth = dot( cam2obj, CameraFront ) / CameraZFar;
  o_depth = linear_depth;
  o_acc_depth = linear_depth;

  // For now 0.0f as the terrain probably won't have any texture of self illumination
  // nor ambient occlusion.
  o_self_illumination = float4(0.0f, 0.0f, 0.0f, 1.0f);
}

// Pixel shader for calculating the ambient light of the scene.
float4 PS_Ambient(
  in float4 iPosition : SV_Position,
  in float2 iUV : TEXCOORD0
) : SV_Target
{
  // Declare some float3 to store the values from the GBuffer.
  GBuffer g;
  decodeGBuffer( iPosition.xy, g );
  if(!g.wasDrawnTo) return (0,0,0,0);
  
  // if roughness = 0 -> I want to use the miplevel 0, the all-detailed image
  // if roughness = 1 -> I will use the most blurred image, the 8-th mipmap, If image was 256x256 => 1x1
  float mipIndex = g.roughness * g.roughness * 8.0f;
  float3 env = txEnvironmentMap.SampleLevel(samLinear, g.reflected_dir, mipIndex).xyz;
  float3 env_2 = txEnvironmentMap2.SampleLevel(samLinear, g.reflected_dir, mipIndex).xyz;
  env = lerp(env, env_2, GlobalSkyBoxLerp);
  env = pow(abs(env), 2.2f); // Convert the color to linear also.
  //return float4( env, 1 );

  // The irrandiance, is read using the N direction.
  // Here we are sampling using the cubemap-miplevel 4, and the already blurred txIrradiance texture
  // and mixing it in base.
  float3 irradiance_mipmaps = txEnvironmentMap.SampleLevel(samLinear, g.N, 4).xyz;
  float3 irradiance_mipmaps_2 = txEnvironmentMap2.SampleLevel(samLinear, g.N, 4).xyz;
  irradiance_mipmaps = lerp(irradiance_mipmaps, irradiance_mipmaps_2, GlobalSkyBoxLerp);

  float3 irradiance_texture = txIrradianceMap.Sample(samLinear, g.N).xyz;
  float3 irradiance_texture_2 = txIrradianceMap2.Sample(samLinear, g.N).xyz;
  irradiance_texture = lerp(irradiance_texture, irradiance_texture_2, GlobalSkyBoxLerp);

  float3 irradiance = irradiance_texture * 0.8 + irradiance_mipmaps * ( 0.2 );
  
  // How much the environment we see
  float3 env_fresnel = Specular_F_Roughness(g.specular_color, 1. - g.roughness * g.roughness, g.N, g.view_dir);
  //return float4(env_fresnel, 1 );

  // Get the AO factor, the product of the AO map and the AO postprocessing.
  float ao = txAO.Sample( samLinear, iUV).x * g.self_illuminationAndAO.a;


  float4 final_color = float4(env_fresnel * 0.05 * env * GlobalReflectionIntensity + 
                              g.albedo.xyz * irradiance * GlobalAmbientLightIntensity, 1.0f);
  final_color.rgb = final_color.rgb * GlobalAmbientBoost * ao + g.self_illuminationAndAO.rgb;
  return final_color;
}

float4 PS_Ambient_Decals(
  in float4 iPosition : SV_Position,
  in float2 iUV : TEXCOORD0
) : SV_Target
{
  // Declare some float3 to store the values from the GBuffer.
  GBuffer g;
  decodeGBuffer( iPosition.xy, g );

  int3 ss_load_coords = uint3(iPosition.xy, 0);

  // if roughness = 0 -> I want to use the miplevel 0, the all-detailed image
  // if roughness = 1 -> I will use the most blurred image, the 8-th mipmap, If image was 256x256 => 1x1
  float mipIndex = g.roughness * g.roughness * 8.0f;
  float3 env = txEnvironmentMap.SampleLevel(samLinear, g.reflected_dir, mipIndex).xyz;
  float3 env_2 = txEnvironmentMap2.SampleLevel(samLinear, g.reflected_dir, mipIndex).xyz;
  env = lerp(env, env_2, GlobalSkyBoxLerp);
  env = pow(abs(env), 2.2f); // Convert the color to linear also.

  // The irrandiance, is read using the N direction.
  // Here we are sampling using the cubemap-miplevel 4, and the already blurred txIrradiance texture
  // and mixing it in base.
  float3 irradiance_mipmaps = txEnvironmentMap.SampleLevel(samLinear, g.N, 4).xyz;
  float3 irradiance_mipmaps_2 = txEnvironmentMap2.SampleLevel(samLinear, g.N, 4).xyz;
  irradiance_mipmaps = lerp(irradiance_mipmaps, irradiance_mipmaps_2, GlobalSkyBoxLerp);

  float3 irradiance_texture = txIrradianceMap.Sample(samLinear, g.N).xyz;
  float3 irradiance_texture_2 = txIrradianceMap2.Sample(samLinear, g.N).xyz;
  irradiance_texture = lerp(irradiance_texture, irradiance_texture_2, GlobalSkyBoxLerp);
  
  float3 irradiance = irradiance_texture * 0.8 + irradiance_mipmaps * ( 0.2 );

  // How much the environment we see
  float3 env_fresnel = Specular_F_Roughness(g.specular_color, 1. - g.roughness * g.roughness, g.N, g.view_dir);

  // Get the AO factor, the product of the AO map and the AO postprocessing.
  float ao = txAO.Sample( samLinear, iUV).x * g.self_illuminationAndAO.a;

  float4 final_color = float4(env_fresnel * 0.25 /* * env * GlobalReflectionIntensity */ + 
                              g.albedo.xyz * irradiance * GlobalAmbientLightIntensity, txGAlbedo.Load(ss_load_coords).a);
  final_color.rgb = final_color.rgb * GlobalAmbientBoost * ao + g.self_illuminationAndAO.rgb;
  return final_color;
}

// -------------------------------------------------
// This shader combines the output of the deferred before postFX are applied.
float4 PS_GBuffer_Resolve( 
  float4 iPosition   : SV_POSITION,
  float2 iUV         : TEXCOORD0
  ) : SV_Target
{
  int3 ss_load_coords = uint3(iPosition.xy, 0);

  float4 acc_light = txAccLights.Load(ss_load_coords);
  float4 albedo_color = txGAlbedo.Load(ss_load_coords);
  float  linear_depth = txGLinearDepth.Sample(samLinear, iUV).x;
  //return float4( 0.5, 1, 0, 1);
  return acc_light;
}

// -------------------------------------------------
// Combines two textures into one.
float4 PS_Combine(
  in float4 iPosition : SV_Position,
  in float2 iUV : TEXCOORD0
) : SV_Target
{
  // Declare some float3 to store the values from the GBuffer.
  GBuffer g;
  decodeGBuffer( iPosition.xy, g );
  if(g.wasDrawnTo) return float4(txAlbedo.Sample(samLinear, iUV).rgb, 1);
  return float4(0, 0, 0, 0);
}

// VS of the skybox.
void VS_skybox( 
  in float4 iPosition  : POSITION,
  in float4 iColor     : COLOR0,
  out float4 oPosition : SV_Position 
)
{
  // Convert the range 0..1 from iPosition to -1..1 to match the homo space
  oPosition = float4(iPosition.x * 2 - 1., 1 - iPosition.y * 2, 1, 1);
}

// PS skybox.
float4 PS_skybox( in float4 iPosition : SV_Position ) : SV_Target
{
  float3 view_dir = mul( float4( iPosition.xy, 1, 1 ), CameraScreenToWorld ).xyz;
  float4 skybox_color = txEnvironmentMap.Sample(samLinear, view_dir);
  float4 skybox_color_2 = txEnvironmentMap2.Sample(samLinear, view_dir);
  float4 finalSkyboxColor = lerp(skybox_color, skybox_color_2, GlobalSkyBoxLerp);
  return float4(finalSkyboxColor.xyz, 1.0) * GlobalAmbientBoost;
}

// VS Passthrough for the lights geometries minus directional lights.
void VS_pass(
  in float4 iPos : POSITION,
  out float4 oPos : SV_POSITION
)
{
  float4 world_pos = mul(iPos, World);
  oPos = mul(world_pos, ViewProjection);
}

// Passthrough for directional lights geometry (a quad painting the whole screen).
void VS_LightDir_Pass(
  in float4 iPos   : POSITION,
  out float4 oPos : SV_POSITION
  )
{
  oPos = float4(iPos.x * 2 - 1., 1 - iPos.y * 2, 1, 1);
}


//--------------------------------------------------------------------------------------
//  PBR lighting functions.
//--------------------------------------------------------------------------------------

// Shade for point lights. Right now they don't have shadows, so no shadows are computed.
// Only light shading is done.
float4 PS_point_lights(
  in float4 iPosition : SV_Position
) : SV_Target
{
  return shadePoint( iPosition, false );
}

// Shade for spot lights.
float4 PS_spot_lights(
  in float4 iPosition : SV_Position
) : SV_Target
{
  // Add this line if you want to use a projection texture. Adapt it to a new shader.
  return shadeSpot( iPosition, LightHasShadows );
}

// Shade for directional lights.
float4 PS_dir_lights(
  in float4 iPosition : SV_Position
) : SV_Target
{
  return shadeDirectionalLight( iPosition, LightHasShadows );
}

#endif