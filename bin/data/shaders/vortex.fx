//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "pbr.fx"
#include "noise.fx"

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
  float4 Pos : POSITION,
  float3 N : NORMAL,
  float2 Uv: TEXCOORD0,
  float4 T : NORMAL1
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;
  output.Pos = mul(Pos, World);
  
  if (Uv.x > 0.03 && Uv.x < 0.97){
    float offsetx = 0.0;
    Unity_SimpleNoise_float(Uv + GlobalWorldTime * 8.0, 1.0, offsetx);
    offsetx = 0.5 - offsetx;
    output.Pos.x = output.Pos.x + offsetx;
    float offsety = 0.0;
    Unity_SimpleNoise_float(Uv + GlobalWorldTime, 1.0, offsety);
    output.Pos.y = output.Pos.y + offsety / 3.0;
  }
  else {
    float offsetx = 0.0;
    Unity_SimpleNoise_float(float2(0.0, 0.0) + GlobalWorldTime * 8.0, 1.0, offsetx);
    offsetx = 0.5 - offsetx;
    output.Pos.x = output.Pos.x + offsetx;
    float offsety = 0.0;
    Unity_SimpleNoise_float(float2(0.0, 0.0) + GlobalWorldTime, 1.0, offsety);
    output.Pos.y = output.Pos.y + offsety / 3.0;
  }
  
  
  output.WorldPos = output.Pos.xyz;
  output.Pos = mul(output.Pos, ViewProjection);
  output.N = mul(N, (float3x3)World);
  output.T = float4( mul(T.xyz, (float3x3)World), T.w);
  output.Uv = Uv;
  return output;
}
//--------------------------------------------------------------------------------------
// Pixel Shader: Discard pixels with the noise grow over time
//--------------------------------------------------------------------------------------
float4 PS_VORTEX( 
  VS_OUTPUT input
) : SV_Target0
{
  float4 o_albedo = txAlbedo.Sample(samLinear, input.Uv);
  float2 ourUv = input.Uv;
 
  float  dissolve = 0.4;              // 0..1 

  float  radialShearStrength = 10;
  float2 radialShearCenter = float2(0.5, 0.5);
  float2 radialShearSpeed = float2(0.0, 0.0);//float2(0.0, -0.5);
  float  radialShearNoiseScale = 20;

  float  twirlAmount = 4;
  float2 twirlCenter = float2(0.5, 0.0);    // 0..1
  float2 twirlSpeed = float2(0.0, 0.0);//float2(0.5, 0.2);
  float  twirlNoiseScale = 1;

  float2 radialShearSpeedTime = radialShearSpeed * float2(GlobalWorldTime, GlobalWorldTime);
  float2 twirlSpeedTime = twirlSpeed * GlobalWorldTime;

  float  radialShearNoise = 1.0;
  radialShearNoise = txNoise.Sample(samLinear, float2(ourUv.x + GlobalWorldTime / 2.0, ourUv.y + fmod(GlobalWorldTime / 4, 5)));

  float  finalNoise = radialShearNoise;
  o_albedo *= finalNoise;
  if (o_albedo.w < dissolve)
    o_albedo.w = 0.0;
  o_albedo.x = o_albedo.x * 0.5;

  if (o_albedo.w >= 0.4 && o_albedo.w <= 0.45)
    o_albedo.xyzw = float4(0.5, 0 , 0.4, 1);
  else
    o_albedo.xyz = o_albedo.xyz * .5;

  return o_albedo;
}