#ifndef PBR_SPECIAL_EFFECTS
#define PBR_SPECIAL_EFFECTS

#include "pbr.fx" // PBR functions.
#include "noise.fx"

//--------------------------------------------------------------------------------------
// Pixel Shader declarations.
//--------------------------------------------------------------------------------------


void PS_Fresnel( 
  VS_OUTPUT input,
  out float4 o_albedo : SV_Target0,
  out float4 o_normal : SV_Target1,
  out float1 o_depth  : SV_Target2,
  out float4 o_self_illumination  : SV_Target3,
  out float1 o_acc_depth  : SV_Target4
)
{
  PS_Common(input, o_albedo, o_normal, o_depth, o_self_illumination, o_acc_depth, false);
  float3 N = decodeNormal(o_normal.xyz);
  float fresnel = saturate(1 - abs(dot(N, CameraFront)));
  fresnel = pow(fresnel, FresnelPBRExponent);
  float4 fresnelColor = fresnel * FresnelPBRColor * FresnelPBRIntensity;
  o_self_illumination += fresnelColor;
  o_self_illumination.a = 0.3;
}

void PS_MeleeBuffed( 
  VS_OUTPUT input,
  out float4 o_albedo : SV_Target0,
  out float4 o_normal : SV_Target1,
  out float1 o_depth  : SV_Target2,
  out float4 o_self_illumination  : SV_Target3,
  out float1 o_acc_depth  : SV_Target4
)
{
  PS_Common(input, o_albedo, o_normal, o_depth, o_self_illumination, o_acc_depth, false);

  float noiseScale = 1;
  float noiseSpeed = 5;
  float noiseStrength = 10;
  float2 noiseCenter = float2(0.5, 0.5);
  float3 effectRgb = float3(1,0,0);

  float twirlAmount = 10;
  float2 twirlCenter = float2(0.5,0.5);
  float twirlSpeed = 2;
  float twirlScale = 1;

  float noisePower = 4;
 
  /* Alternative 1: radial noise effect */
  /* Radial Shear */
  float radialOffset = GlobalWorldTime * noiseSpeed;
  float2 radialUV;
  Unity_RadialShear_float(input.Uv, noiseCenter, noiseStrength, float2(radialOffset, radialOffset), radialUV);
  /* Noise with radial shear */
  float RadialShearNoise;
  Unity_SimpleNoise_float(radialUV, noiseScale, RadialShearNoise);
  /****************************************************************/

  /* Twirl */
  float twirlOffset = GlobalWorldTime * twirlSpeed;
  float2 TwirlUV;
  Unity_Twirl_float(input.Uv, twirlCenter, twirlAmount, twirlOffset, TwirlUV);
  /* Noise with twirl */
  float TwirlNoise;
  Unity_SimpleNoise_float(TwirlUV, twirlScale, TwirlNoise);
  /****************************************************************/

  float combinedNoise = RadialShearNoise * TwirlNoise;
  float combinedNoisePowered = pow(combinedNoise, noisePower);

  float4 color = float4(o_albedo.xyz * effectRgb, combinedNoisePowered);
  //o_albedo = color * combinedNoisePowered;


  /* Alternative 2: playing with uvs, creates a stripe shaped effect*/
  float r = o_albedo.x  * (sin(input.Uv.x * 100.0 + GlobalWorldTime * 7) + 1.0)/2.0;
  float g = o_albedo.y * (sin(-input.Uv.x * 10.0 + GlobalWorldTime) + 1.0)/2.0;
  float b = o_albedo.z * (sin(-input.Uv.x * 20.0 + GlobalWorldTime * 10.0) + 1.0)/2.0;

  /* Current effect: ping pong color */
  effectRgb = effectRgb * cos(GlobalWorldTime * 5) + 1.0;

  o_albedo = float4(o_albedo * effectRgb, 1) ;
  

}
#endif