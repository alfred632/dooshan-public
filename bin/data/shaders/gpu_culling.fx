#ifndef GPU_CULLING
#define GPU_CULLING

#include "common.fx"
#include "pbr_common.fx"
#include "shadows.fx"

// --------------------------------------------------------------
// instances input data: AABB + WORLD
struct TInstance {
  float3   aabb_center;
  uint     prefab_idx;
  float3   aabb_half;
  uint     dummy2;
  float4x4 world;
  float4   objColor;
};

struct TCulledInstance {
  float4x4 world;
  float4   objColor;
};

struct TPrefab {
  uint id;
  uint  lod_prefab;
  float lod_threshold;
  uint num_objs;
  uint num_render_type_ids;
  uint total_num_objs;
  uint render_type_ids[6];
};

// --------------------------------------------------------------
// Holds the 6 planes to perform the culling
cbuffer TCullingPlanes : register(b11) {
  float4 planes[6];
  float3 culling_camera_pos;
  float  culling_dummy;
};

// --------------------------------------------------------------
cbuffer TCtesInstancing : register(b12) {
  uint  total_num_objs;
  uint  instance_base;
  uint2 instancing_padding;
};

// --------------------------------------------------------------
bool isVisible( TInstance instance ) {
  [unroll]
  for( int i=0; i<6; ++i ) {
    const float4 plane = planes[i];
    const float r = dot( abs( plane.xyz ), instance.aabb_half );
    const float c = dot( plane.xyz, instance.aabb_center ) + plane.w;
    if( c < -r ) return false;
  }
  return true;
}

// --------------------------------------------------------------
[numthreads(64, 1, 1)]
void cs_cull_instances(
  uint id : SV_DispatchThreadID     // Unique id as uint
  ,   StructuredBuffer<TInstance>       instances        : register(t0)
  ,   StructuredBuffer<TPrefab>         prefabs          : register(t1)
  ,   RWStructuredBuffer<TCulledInstance> culled_instances : register(u0)
  ,   RWByteAddressBuffer                 draw_datas       : register(u1)
) 
{
  if( id >= total_num_objs ) return;
  
  // Get the instance.
  TInstance instance = instances[ id ];
  
  // If it's not visible, ignore it.
  if( !isVisible( instance ) ) return;

  // If it's visible, add it as visible.
  TCulledInstance culled_instance;
  culled_instance.world = instance.world;
  culled_instance.objColor = instance.objColor;

  // Sum of all the bytes for each object in TDrawData.
  // This includes DrawIndexedInstancedArgs and the bytes
  // for the base and the two dummy vars.
  const uint bytes_per_draw_data_id = 32;

  // Each prefab defines which draw call types must be added
  TPrefab prefab = prefabs[ instance.prefab_idx ];

  // LOD -> Change prefab type based on distance
  if( prefab.lod_prefab >= 0 ){
    float  distance_to_camera = length( culling_camera_pos - instance.aabb_center );
    if( distance_to_camera > prefab.lod_threshold)
      prefab = prefabs[prefab.lod_prefab];
  }

  // Now, get the prefab and for each render type in it, increase it.
  for( int i = 0; i < prefab.num_render_type_ids; ++i ) {
    uint render_type_id = prefab.render_type_ids[i];
    uint offset_render_type = render_type_id * bytes_per_draw_data_id;
    uint base = draw_datas.Load( offset_render_type + 20 );  // Read TDrawData[render_type_id].base

    // Atomically read+add(+1)+write. Result is recv in myIndex
    uint myIndex;
    draw_datas.InterlockedAdd( offset_render_type + 4, 1, myIndex );   // +1 to .instanceCount

    // Store the instance in the requested position
    culled_instances[base + myIndex] = culled_instance;
  }

}

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
  in VS_INPUT input,
  in uint InstanceID : SV_InstanceID,
  StructuredBuffer<TCulledInstance> culled_instances : register(t0))
{
  TCulledInstance culled_instance = culled_instances[ instance_base + InstanceID ];
  float4x4 newWorld = culled_instance.world;
  return runObjVS( input, newWorld );
}

VS_OUTPUT_SHADOW VS_SHADOWS(
  float4 iPos : POSITION,
  in uint InstanceID : SV_InstanceID,
  StructuredBuffer<TCulledInstance> culled_instances : register(t0))
{
  TCulledInstance culled_instance = culled_instances[ instance_base + InstanceID ];
  float4x4 newWorld = culled_instance.world;

  VS_OUTPUT_SHADOW output = (VS_OUTPUT_SHADOW)0;
  float4 world_pos = mul( iPos, newWorld );
  output.Pos = mul(world_pos, ViewProjection );
  return output;
}

VS_OUTPUT_ALPHA VS_SHADOWS_ALPHA(
  float4 iPos : POSITION,
  float3 N : NORMAL,
  float2 uv   : TEXCOORD0,
  in uint InstanceID : SV_InstanceID,
  StructuredBuffer<TCulledInstance> culled_instances : register(t0))
{
  TCulledInstance culled_instance = culled_instances[ instance_base + InstanceID ];
  float4x4 newWorld = culled_instance.world;

  VS_OUTPUT_ALPHA output = (VS_OUTPUT_ALPHA)0;
  output.Pos = mul(iPos, newWorld);
  output.Pos = mul(output.Pos, ViewProjection );
  output.Uv = uv;
  return output;
}

#endif