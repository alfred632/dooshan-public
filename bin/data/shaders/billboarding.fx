#include "common.fx"
#include "gbuffer.inc"
#include "noise.fx"

// Billboards.
// Here we implement different billboards.
// Some interesting links that we read from (we only used one of them for the
// cheating version).
// https://www.geeks3d.com/20140807/billboarding-vertex-shader-glsl/
// or
// https://en.wikibooks.org/wiki/Cg_Programming/Unity/Billboards
// or
// http://www.lighthouse3d.com/opengl/billboarding/index.php?billCheat1
// First takes the model view matrix and simply removes the rotation
// gr8 but costly imo.
// Second converts origin point from object space to view space and then adds
// the vertices.
// Third link is quite complicated but interesting to read at least.
// Final method is the easiest and fastest imo.

struct VS_FULL_OUTPUT {
  float4 Pos   : SV_POSITION;
  float2 UV    : TEXCOORD0;
};

// We recieve the iPos and the base Uvs of the mesh.
// Spherical billboard that always rotates to face the camera.
VS_FULL_OUTPUT VS_SPHERICAL(
  float4 iPos : POSITION,
  float2 Uv: TEXCOORD0
)
{
  float3 worldPosFromMatrix = float3(World[3][0], World[3][1], World[3][2]);

  float3 localPos = iPos.xyz;

  // Orient billboard to face camera. We use the camera left to
  // get the up vector. If we would use the front, it would rotate
  // as the cross product changes angle based on side.
  float3 camToObj = worldPosFromMatrix - CameraPosition;
  float3 upVector = normalize(cross(CameraLeft, camToObj));
  float3 leftVector = normalize(cross(upVector, camToObj));
  localPos = (localPos.x * leftVector) * BillboardingScale.x
  + (localPos.y * upVector) * BillboardingScale.y;

  // Move to the world pos.
  float3 vertexWorldPos = localPos + worldPosFromMatrix;

  // Now get this local vector and move it.
  VS_FULL_OUTPUT output = (VS_FULL_OUTPUT)0;
  output.Pos = mul(float4(vertexWorldPos,1), ViewProjection);
  output.UV = Uv;

  return output;
}

// We recieve the iPos and the base Uvs of the mesh.
// Gives the impression of a spherical billboard but doesn't rotate
// to point to the camera, points to the plane of the camera.
VS_FULL_OUTPUT VS_SPHERICAL_CHEATED(
  float4 iPos : POSITION,
  float2 Uv: TEXCOORD0
)
{
  float3 worldPosFromMatrix = float3(World[3][0], World[3][1], World[3][2]);

  float3 localPos = iPos.xyz;

  // Orient billboard to camera plane.
  localPos = (localPos.x * CameraLeft) * BillboardingScale.x
  + (localPos.y * CameraUp) * BillboardingScale.y;

  // Move to the world pos.
  float3 vertexWorldPos = localPos + worldPosFromMatrix;

  // Now get this local vector and move it.
  VS_FULL_OUTPUT output = (VS_FULL_OUTPUT)0;
  output.Pos = mul(float4(vertexWorldPos,1), ViewProjection);
  output.UV = Uv;

  return output;
}

// We receive the iPos and the base Uvs of the mesh.
// This are actually cheting spherical billboards as they don't
// face the camera but the viewing plane (they look in the viewing plane
// instead of pointing to the camera). I leave it here just if somebody is interested.
VS_FULL_OUTPUT VS_SPHERICAL_CHEATED_SECOND_VERSION(
  float4 iPos : POSITION,
  float2 Uv: TEXCOORD0
)
{  
  // Start by converting the origin pos from object space to world space.
  float4 originInWorldPos = mul(float4(0,0,0,1), World);
  // Now convert the origin pos that is in world space to view space.
  float4 originInViewProjectionSpace = mul(originInWorldPos, View);
  
  // As view space is a rotated version of world space with the xy plane parallel
  // to the view (i.e, now we have a point that is rotated facing the camera).
  // Therefore, we can now add the points from the vertices directly and the effect will work.
  VS_FULL_OUTPUT output = (VS_FULL_OUTPUT)0;
  output.Pos = originInViewProjectionSpace + float4(iPos.x, iPos.y, 0.0, 0.0);
  output.Pos = originInViewProjectionSpace * float4(BillboardingScale.x, BillboardingScale.x, 0.0, 0.0);
  output.Pos = mul(output.Pos, Projection);
  output.UV = Uv;

  return output;
}

// We recieve the iPos and the base Uvs of the mesh.
// Cylindrical billboard that always rotates to face the camera.
VS_FULL_OUTPUT VS_CYLINDRICAL(
  float4 iPos : POSITION,
  float2 Uv: TEXCOORD0
)
{
  float3 localPos = iPos.xyz;

  // Extract world position of the object from world matrix.
  float3 worldPosFromMatrix = float3(World[3][0], World[3][1], World[3][2]);

  // We need the up vector of the object (not the camera!).
  // We extract it from the matrix. We normalize it just in case.
  float3 upVectorObj = normalize(float3(World[1][0], World[1][1], World[1][2]));

  // Orient billboard to face camera in the horizontala axis and to orient on y axis
  // based on billboard entity rotation.
  float3 camtoObj = worldPosFromMatrix - CameraPosition;
  float3 leftVector = normalize(cross(upVectorObj, camtoObj));
  localPos = (localPos.x * leftVector) * BillboardingScale.x
  + (localPos.y * upVectorObj) * BillboardingScale.y;

  // Move to the world pos.
  float3 vertexWorldPos = localPos + worldPosFromMatrix;

  // Now get this oriented and scaled point and convert it to projection space.
  VS_FULL_OUTPUT output = (VS_FULL_OUTPUT)0;
  output.Pos = mul(float4(vertexWorldPos,1), ViewProjection);
  output.UV = Uv;

  return output;
}

// We recieve the iPos and the base Uvs of the mesh.
// Gives the impression of a cylindrical billboard but doesn't rotate
// to point to the camera. It faces the view plane only.
VS_FULL_OUTPUT VS_CYLINDRICAL_CHEATED(
  float4 iPos : POSITION,
  float2 Uv: TEXCOORD0
)
{
  // We need the up vector of the object. We extract it from the matrix.
  // We normalize it just in case.
  float3 upVectorObj = normalize(float3(World[1][0], World[1][1], World[1][2]));

  float3 localPos = iPos.xyz;

  // Orient billboard to camera plane.
  localPos = (localPos.x * CameraLeft) * BillboardingScale.x
  + (localPos.y * upVectorObj) * BillboardingScale.y;

  // Move to the world pos.
  float3 worldPosFromMatrix = float3(World[3][0], World[3][1], World[3][2]);
  float3 vertexWorldPos = localPos + worldPosFromMatrix;

  // Now get this oriented and scaled point and convert it to projection space.
  VS_FULL_OUTPUT output = (VS_FULL_OUTPUT)0;
  output.Pos = mul(float4(vertexWorldPos,1), ViewProjection);
  output.UV = Uv;

  return output;
}

// Render billboard. Note that we are not even sending information to a depth texture.
// This shader is only being used right now to render the light for godrays so I don't care.
float4 PS_Billboard(
  VS_FULL_OUTPUT input
  ) : SV_Target
{
  float4 albedo = txAlbedo.Sample(samLinear, input.UV);
  if(albedo.a < 0.05)
    clip(-1);

  return albedo * ObjColor;
}