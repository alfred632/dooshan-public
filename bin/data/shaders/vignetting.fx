#include "screen_quad.fx"

float4 PS_VIGNETTING(VS_FULL_OUTPUT input) : SV_TARGET
{
  float2 uv = input.UV.xy;
  float2 coord = (uv.xy - 0.5);
  float rf = sqrt(dot(coord, coord)) * VignettingFallOff;
  float rf2_1 = rf * rf + 1.0;
  float e = 1.0 / (rf2_1 * rf2_1);
  
  float4 inputColor = txAlbedo.Sample( samLinear, input.UV );
  return float4(inputColor.xyz * e, 1.0);
}