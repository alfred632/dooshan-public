#ifndef PBR_COMMON
#define PBR_COMMON

//--------------------------------------------------------------------------------------
// Vertex Shaders Declarations
//--------------------------------------------------------------------------------------

// Standard vertex for objects (without skinning)
struct VS_INPUT
{
  float4 Pos : POSITION;
  float3 N : NORMAL;
  float2 Uv: TEXCOORD0;
  float4 T : NORMAL1;
};

struct VS_OUTPUT
{
  float4 Pos      : SV_POSITION;
  float3 N        : NORMAL;
  float2 Uv       : TEXCOORD0;
  float3 WorldPos : TEXCOORD1;
  float4 T        : NORMAL1;
};

// This is used by normal rendering, skin rendering and instancing rendering
VS_OUTPUT runObjVS(VS_INPUT input, float4x4 world ) {
  VS_OUTPUT output = (VS_OUTPUT)0;
  output.Pos = mul(input.Pos, world);
  output.WorldPos = output.Pos.xyz;
  output.Pos = mul(output.Pos, ViewProjection);
  output.N = mul(input.N, (float3x3)world);
  output.T = float4( mul(input.T.xyz, (float3x3)world), input.T.w);
  output.Uv = input.Uv;
  return output;
}

//--------------------------------------------------------------------------------------
// PBR Equations used by PBR shaders.
//--------------------------------------------------------------------------------------

// Returns the diffuse color of an albedo texture pixel.
float3 Diffuse(float3 pAlbedo) {
    return pAlbedo/PI;
}

// Gloss = 1 - rough * rough
float3 Specular_F_Roughness(float3 specularColor, float gloss, float3 h, float3 v) {
  // Schlick using roughness to attenuate fresnel.
  return (specularColor + (max(gloss, specularColor) - specularColor) * pow((1 - saturate(dot(v, h))), 5));
}

float NormalDistribution_GGX(float a, float NdH)
{
  // Isotropic ggx.
  float a2 = a*a;
  float NdH2 = NdH * NdH;
  
  float denominator = NdH2 * (a2 - 1.0f) + 1.0f;
  denominator *= denominator;
  denominator *= PI;
  
  return a2 / denominator;
}

float Geometric_Smith_Schlick_GGX(float a, float NdV, float NdL)
{
  // Smith schlick-GGX.
  float k = a * 0.5f;
  float GV = NdV / (NdV * (1 - k) + k);
  float GL = NdL / (NdL * (1 - k) + k);
  return GV * GL;
}

float Specular_D(float a, float NdH)
{
  return NormalDistribution_GGX(a, NdH);
}

float Specular_G(float a, float NdV, float NdL, float NdH, float VdH, float LdV) 
{
  return Geometric_Smith_Schlick_GGX( a, NdV, NdL );
}

float3 Fresnel_Schlick(float3 specularColor, float3 h, float3 v)
{
  return (specularColor + (1.0f - specularColor) * pow((1.0f - saturate(dot(v, h))), 5));
}

float3 Specular_F(float3 specularColor, float3 h, float3 v)
{
  return Fresnel_Schlick(specularColor, h, v);
}

float3 Specular(float3 specularColor, float3 h, float3 v, float3 l, float a, float NdL, float NdV, float NdH, float VdH, float LdV)
{
  return ((Specular_D(a, NdH) * Specular_G(a, NdV, NdL, NdH, VdH, LdV)) * Specular_F(specularColor, v, h) ) / (4.0f * NdL * NdV + 0.0001f);
}

//--------------------------------------------------------------------------------------
// Extra Equations.
//--------------------------------------------------------------------------------------

// Returns a hash number.
float2 hash2(float n) {
  return frac(sin(float2(n, n + 1.0)) * float2(43758.5453123, 22578.1459123));
}

// Returns random
float rand( float x ) { return frac( sin(x) * 43758.5453 ); }

#endif