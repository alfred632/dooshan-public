//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "pbr.fx"
#include "noise.fx"

//--------------------------------------------------------------------------------------
// Pixel Shader: Discard pixels with the noise grow over time
//--------------------------------------------------------------------------------------
void PS_VANISH_DR( 
  VS_OUTPUT input,
  out float4 o_albedo : SV_Target0,
  out float4 o_normal : SV_Target1,
  out float1 o_depth  : SV_Target2,
  out float4 o_self_illumination  : SV_Target3,
  out float1 o_acc_depth  : SV_Target4
)
{
  PS_Common(input, o_albedo, o_normal, o_depth, o_self_illumination, o_acc_depth, false);

  float3 noiseValue = txNoise.Sample(samLinear, input.Uv * VanishNoiseScale).xyz; // 0..1
  float timeRatio = saturate(VanishCurrentTime/VanishTotalTime); // From 1.0 to 0.0. Current time decreases.
  float noisePixel = timeRatio - noiseValue.x;

  // If the arg is negative, the pixel shader execution is aborted.
  clip(noisePixel);

  float result = warpingExposed(input.WorldPos, GlobalWorldTime * 2, 0.8, 1.0, 3.0, 0.4, 6);
  if( noisePixel < VanishInnerBorderSize && noisePixel > 0.01 )
    o_albedo = float4(VanishInnerBorderColor/(result),1);

  // Make a second border
  if( noisePixel < VanishOutterBorderSize )
    o_albedo = float4(VanishOutterBorderColor/(result),1);

  // Get alpha lower by time
  float alpha = saturate( 1 + noisePixel * 2 );

  float newIntensity = saturate(VanishAddedIntensity) * (1 - timeRatio) + 1;
  o_albedo = float4(o_albedo.xyz * newIntensity, 1);
}