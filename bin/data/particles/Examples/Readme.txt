Particles parameters:
	texture → The texture the particles from the emitter will use
		"texture" : "data/textures/particles/fire.dds"

	count → The number of particles that will be created when every emmit is due.
		"count" : [0, 0] 					//It doesn't create particles
		"count" : [1, 1] 					//Only creates 1 particle per emit
		"count" : [1, 3]					//Creates between 1 and 3 particles per emit

	maxCount → The maximum number of particles that can be alive at the same time from the same emitter
		"max_count" : 10					//When there are 10 particles from the emitter, it will not spawn any more

	interval → The interval between each emit pulse when particles will be created from the emitter.
		"interval" : [0.0, 0.0] 			//It will be spawning X (from count) particles every frame
		"interval" : [0.5, 2.0] 			//The next emit will be between 0.5 and 2.0 seconds

	duration → The particles duration.
		"duration" : [0, 0] 				//The particles will not die
		"duration" : [2.5, 3.0] 			//The particles will have a lifetime between 2.5 and 3.0 seconds

	emitter_duration → The emitter duration
		"emitter_duration" : 3 				//The emitter will be enabled 3 seconds and then it will disable itself

	gravity_factor → The gravity scale factor (this * 9.8)
		"gravity_factor" : [0, 0] 			//The particle will not be afected by gravity
		"gravity_factor" : [0.5, 1.5]		//The particles will have a gravity of Random(0.5 .. 1.5)*9.8 when they are created
		"gravity_factor" : [-2.0, 0]		//The particles will have a gravity of Random(-2.0 .. 0)*9.8 when they are created (They will have a negative gravity making them go up)

	direction → The direction the particles will have when created
		"direction" : "0 1 0" 				//They will go up
		"direction" : "1 0 0" 				//They will go to the right
		"direction" : "0 0 1" 				//They will go to the front
		"direction" : "0 -1 0" 				//They will go down
		Values go from -1 to 1

	cone_angle → The aperture of the cone the emitter will have based on the direction.
		"cone_angle" : 0 					//Straight line
		"cone_angle" : 180 					//Max value, this makes the emitter have a sphere emitting shape

	speed → The initial speed
		"speed" : [0.0, 0.0] 				//Doesn't have speed
		"speed" : [-5.0, 8.0]				//Random between -5 and 8

	color → The initial color of the particles
		"color" : "1.0 1.0 0.0 1.0" 		//RGBA, R+G+A
		If the texture has a color already then set this to 1.0 1.0 1.0 1.0


Overlifetime parameters
	(The first parameter in the arrays is the % of the lifetime duration)

	colors → The colors over lifetime
		"colors" : [
		  	[0.0, "1.0 0.0 0.0 1.0"],
		  	[0.5, "0.0 1.0 0.0 1.0"],
  			[1.0, "0.0 0.0 1.0 1.0"]
  		],
  		In this example when the particle is created (0.0) will use the first row color, red.
  		Then at 0.5 of its lifetime will be green, and finally at 1.0 will be blue.

  	sizes → The sizes over lifetime
  		"sizes" : [
  			[0.0, 1.0],
  			[0.5, 3.0],
  			[1.0, 1.0]
  		],
  		Scale 1.0 when created. 0.5 of its lifetime will be 3 times bigger, and when it dies will be back at its original size.