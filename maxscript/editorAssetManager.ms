-- This file holds all the references to the models that can be added as prefabs in a scene.
-- Whenever a new prefab is created, add it here in the lines below, then press CTRL + E to compile the script again.
-- Values will be actualized and the prefab will appear in its corresponding section.

-- Before starting, get sure to have compiled mcv_startup.ms by opening it and pressing CTRL + E.
global mcv_3DsMaxAssets_path = mcv_scripts_path + "/maxscript/assets/"

global Editor3DSMaxAssetsManager

struct Editor3DSMaxAssetsManager(
	-- Path to root asset directory where the folders with assets are (i.e folders where .max files are).
	pathToRootAssetDirectory = mcv_3DsMaxAssets_path + "/*",

	-- Asset dictionaries are ordered by key representing type of asset (city, interior, decoration, interaction, ...) and the value is
	-- another dictionary holding a key representing the asset name and the path of the .max file.
	assetdictionaries = Map(),
	
	-- Load asset into the given category.
	fn loadAssetIntoCategory asset_path asset_category = (
		-- Get the asset dictionary for the given asset category.
		local assetDictionary = assetdictionaries.getItemValue asset_category
		
		-- Get the asset name (i.e the name of the .max file) from the full path.
		local asset_name = getFilenameFile asset_path
		
		-- Get sure it's a max.
		local asset_type = getFilenameType asset_path
		if asset_type != ".max" then
			return true
		
		-- If two assets have the same name and are loaded into the same category this error will throw, this is very unlikely to happen
		-- but we load by default all assets in the root assets directory into the root category, if another root category would appear,
		-- this could happen. We leave this here for completion.
		local assetWasFound = assetDictionary.hasValue asset_name
		if assetWasFound then(
			local message = "Two assets have the same name ( " + asset_name + " ) in the dictionary, this should not happen. Do you have the same file in different folders?"
			messagebox message
			return false
		)
		
		-- Finally, add the item into the dictionary.
		assetDictionary.addItem asset_name asset_path
	),
	
	-- Fetches all the assets from this directory and root ones.
	fn fetchAssetsFromDirectories current_path category_for_the_assets isRoot =
	(
		local loadedSuccessfully = true
		
		-- Check we are parsing a category, otherwise, don't change the asset_category
		-- Remove * at the end.
		local fileToCheck = substring current_path 1 (current_path.count-1)
		fileToCheck += "prefab_object.max_metadata"
		local isACategory = not doesFileExist fileToCheck

		-- Get the asset category.
		local asset_category = category_for_the_assets
		if isACategory and not isRoot then(
			local filenamePath = getFilenamePath current_path
			local basePath = "\\maxscript\\assets\\"
			local start_base_dir = findString filenamePath basePath
			local startAssetDirectory = start_base_dir + basePath.count
			asset_category = substring filenamePath startAssetDirectory filenamePath.count
			-- The -2 removes the // at the end of the category.
			asset_category = substring asset_category 1 (asset_category.count - 2)
		)
		
		if isACategory or isRoot then(
			format "Parsing category: % \n" asset_category
		)
		
		-- Get the dictionary for this type of assets, if not found we create it.
		local itemFound = assetdictionaries.getItemValue asset_category
		if itemFound == undefined then(
			local newMap = Map()
			assetdictionaries.addItem asset_category newMap
		)
		
		-- Get all files in the current directory into an asset dictionary. Each directory will have its own asset category.
		-- For now we don't support child categories so a category contain others categories.
		-- Probably we won't as we don't deem it necessary for now.

		-- For all files in the folder, we load them into their category.
		local files = getFiles current_path
		for filePath in files do(
			if (loadAssetIntoCategory filePath asset_category) == false then
				loadedSuccessfully = false
		)
		
		if isACategory then(
			-- Finally, get all directories in the current path and keep on moving downwards.
			local directories = GetDirectories (current_path)
			for directory in directories do(
				local directoryToFetch = directory + "\\*"
				if (fetchAssetsFromDirectories directoryToFetch asset_category false) == false then
					loadedSuccessfully = false
			)
		)
		
		return loadedSuccessfully
	),
	
	-- Call this function to load assets starting from the root asset directory.
	fn loadAssetsIntoEditor = (
		assetdictionaries = Map()
		
		format "Loading assets into editor.\n"
		-- We start with the root folder, as category for it we will call it "General".
		if (fetchAssetsFromDirectories pathToRootAssetDirectory "General" true) == true then(
			format "Loading finished correctly.\n"
		)
		else(
			assetdictionaries = Map()
			format "Something went wrong while loading assets.\n"
		)
		
	),
	
	-- Returns all the assets categories.
	fn getAllAssetCategories = (
		return assetdictionaries.keys
	),
	
	-- Returns the map with all the assets for the given category. Key is name of the object. Value is .max filepath.
	-- To show them, loop through the keys, each value is ordered respective to its key, so the index of the
	-- key is the index of the value.
	fn getAssetsMapForTheGivenCategory assetCategory = (
		local assetCategory = assetdictionaries.getItemValue assetCategory
		return assetCategory
	),
	
	fn getAllAssetsNamesForTheGivenCategory assetCategory = (
		local assetCategory = assetdictionaries.getItemValue assetCategory
		return assetCategory.keys
	),
	
	fn getAllAssetsNamesPathsForTheGivenCategory assetCategory = (
		local assetCategory = assetdictionaries.getItemValue assetCategory
		return assetCategory.values
	),
	
	-- Given the name of an asset and the category, returns the asset path.
	fn getAssetPath assetCategory assetName = (
		local assetMap = getAssetsMapForTheGivenCategory assetCategory
		if assetMap == undefined then
			return undefined

		return assetMap.getItemValue assetName
	)
)


global EditorAssetsManager = Editor3DSMaxAssetsManager()
EditorAssetsManager.loadAssetsIntoEditor()