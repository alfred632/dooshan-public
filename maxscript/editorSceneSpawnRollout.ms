if SpawnEditor != undefined then
	try(destroyDialog SpawnEditor)catch()

rollout SpawnEditor "Spawn Editor"
(	
	/* Layout */
	label titleSpawn "Spawn selector"
	
	-- List of enemy types.
	listbox enemy_list "Enemy types:" items:#("EnemyMelee", "EnemyRanged", "EnemyRangedBuffed", "EnemyRangedSniperGround", "EnemyRangedSniperRoof", "EnemyImmortal")
	local selectedEnemiesToSpawn = #()

	label spacing1 ""
	listbox enemiesToSpawn "Enemies to spawns:" items:#("No enemies") readOnly:true
	label spacing2 ""
	
	button btn_create_spawn "Create spawn"
	label spacing3 ""
	button btn_add_enemy_type "Add Enemy Type"
	label spacing4 ""
	button btn_remove_enemy_type "Remove Enemies"
	label spacing5 ""
	
	-- Close collider rollout.
	button btn_close_spawn_rollout "Close"
	
		-- Close the rollout.
	on btn_create_spawn pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Create Spawn Time: %\n\n" timeOfExecution
	
		-- Pass culling parameters as values for the user properties that can be easily read once in the json.
		local enemyTypes = "["
		for enemyType in selectedEnemiesToSpawn do
			enemyTypes += enemyType + " "
		enemyTypes = substituteString enemyTypes " " ", "
		enemyTypes = substring enemyTypes 1  (enemyTypes.count-2)
		enemyTypes = enemyTypes + "]"
		
		local selectionD = getCurrentSelection()
		SceneEditor.setUserDefinedPropertyForObjs selectionD "prefab" "spawn.json" false
		SceneEditor.setUserDefinedPropertyForObjs selectionD "spawn" enemyTypes false
		
		-- Updates the user defined properties for the selected object if we have one showing in the editor.
		SceneEditor.SceneEditorRollout.updateUserDefinedPropertiesForSelectedObject()
		destroyDialog SpawnEditor
	)
	
	on btn_add_enemy_type pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Add Enemy To Spawn Time: %\n\n" timeOfExecution
		appendIfUnique  selectedEnemiesToSpawn enemy_list.selected
		enemiesToSpawn.items = selectedEnemiesToSpawn
	)
		
	on btn_remove_enemy_type pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		selectedEnemiesToSpawn = #()
		format "------ Removed enemies list Time: %\n\n" timeOfExecution
		enemiesToSpawn.items = selectedEnemiesToSpawn
	)
	
	-- Close the rollout.
	on btn_close_spawn_rollout pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Closed spawn rollout   Time: %\n\n" timeOfExecution
		destroyDialog SpawnEditor
	)
	
	-- Called when the rollout opens.
	on SpawnEditor open do()
)