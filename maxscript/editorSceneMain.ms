clearListener()

-- Closes the window if it already exists and we are executing the script again.
if SceneEditorRollout != undefined then
	try(closeRolloutFloater SceneEditorRollout) catch()

if PrefabRollout != undefined then
	try(destroyDialog PrefabRollout)catch()

if SpawnEditor != undefined then
	try(destroyDialog SpawnEditor)catch()

if ColliderRollout != undefined then
	try(destroyDialog ColliderRollout)catch()

if CullingVolumeRollout != undefined then
	try(destroyDialog CullingVolumeRollout)catch()

-- Make global for callbacks on selected obj.
global SceneEditor
global SceneEditorRollout	

utility SceneEditor "Editor"
(
	-- Variable for setting the window as floating, by default set to false.
	local setAsFloater = false

	/* Forward declarations. */
	
	-- Open editor as dialog or rollout. Also open other dialogs.
	local startSceneEditorDialog
	local startSceneEditorRollout
	local openDialog
	local closeDialog

	/* Object creation functions */
	local checkIfObjAlreadyHasProperties
	local clearUserDefinedProperties
	local checkUserDefinedProperties
	local setUserDefinedPropertyForObjs
	local setUserDefinedPropertyForObject
	local clearUserDefinedPropertiesForSelectedObjs
	
	fn isPrefab obj = (
		return obj != undefined and getUserProp obj "prefab"  != undefined and obj.parent == null
	)

	-- Creates an asset.
	fn reloadPrefab objToReload = (
		local prefabText = getUserProp objToReload "prefab"
		local prefabTextArray = filterString prefabText "./"
		local arraySize = prefabTextArray.count
		local selectedCategory = ""
		local selectedPrefab = prefabTextArray[arraySize - 1]

		-- Build the category string path
		for i = 1 to (arraySize - 2) do (
			selectedCategory = selectedCategory + prefabTextArray[i] + "/" 
		)
		selectedCategory = substring selectedCategory 1 (selectedCategory.count - 1) --Remove the last '/'

		--format "[ReloadPrefab] category % prefab %\n" selectedCategory selectedPrefab
		local assetPath = EditorAssetsManager.getAssetPath selectedCategory selectedPrefab
		local prefabRelativePath = EditorPrefabsManager.getPrefabPath selectedCategory selectedPrefab
		if prefabRelativePath == undefined then(
			--local message = "Prefab with name " + selectedPrefab +  " was not found inside the prefab folder. Please, create the prefab inside the folder before adding it. To do so, use the Export As Prefab button in the MCV exporter."
			--messagebox message
			return false
		)
		--format "AssetPath: %\n" assetPath

		-- For the object, add the file		
		-- Don't know if there is a faster way, this will have to do :p.
		mergeMAXFile assetPath #select #AutoRenameDups #renameMtlDups 

		PrefabRollout.convertMaterialsFromRelativePathToUserAbsolutePath assetPath
		
		--format "Object loaded and merged\n"
		
		-- Position objects in the selection.
		for obj in selection do(
			if obj.parent != undefined then continue -- Ignore children, they get positioned automatically.
			local scaleObj = obj.scale
			obj.transform = objToReload.transform
			obj.scale = scaleObj
		)
		
		--format "Object loaded and placed\n"

		SceneEditor.setUserDefinedPropertyForObject selection[1] "prefab" prefabRelativePath true
		for idx = 2 to selection.count do(
			SceneEditor.setUserDefinedPropertyForObject selection[idx] "ignore" true false
		)

		SceneEditor.SceneEditorRollout.updateUserDefinedPropertiesForSelectedObject()

		-- Delete all selected objects, also their children as they don't have sense if we are placing thsi prefab.
		if isValidNode objToReload then
			delete objToReload.children
		delete objToReload

		format "\n"
	)

	-- Get hour and minutes as string
	fn getTimePressedButton = (
		local timeExecution = getUniversalTime()
		local hour = (mod (timeExecution[5]+2) 24 as integer) as string
		local minutes = timeExecution[6] as string
		return hour + ":" + minutes
	)
	
	-- Sets the user properties for the objs passed as parameter.
	fn setUserDefinedPropertyForObjs objs key val clearProperties = (
		if objs == undefined or objs.count < 1 then(
			messagebox "No object was selected!"
			return false
		)
		
		for obj in objs do (
			-- Clear user defined properties if already present on the object.
			checkUserDefinedProperties obj clearProperties
			format "Setting % as % = %.\n\n" obj.name key val
			setUserProp obj key val
		)
	)
	
	-- Sets the user properties for the object passed as parameter.
	fn setUserDefinedPropertyForObject obj key val clearProperties = (
		if obj == undefined then(
			messagebox "No object was selected!"
			return false
		)
			
		-- Clear user defined properties if already present on the object.
		checkUserDefinedProperties obj clearProperties
		format "Setting % as % = %.\n\n" obj.name key val
		setUserProp obj key val
	)

	-- This is the rollout with the buttons for editing the scene.
	rollout SceneEditorRollout "Scene and Assets Editor"
	(
		-- Width of the rollout when opened as floating.
		local dialogWidth = 500
		
		-- Used for spacing.
		label spacing ""
		
		-- Make prefab.
		button btn_make_prefab "Set selection as prefab" tooltip:"Spawn a prefab into the scene."
		
		-- Mark object as trigger.
		button btn_trigger "Mark selection as trigger" tooltip:"Marks the object as a trigger."
		
		-- Mark object as collider.
		button btn_collider "Mark selection as collider" tooltip:"Marks the object as a collider."
		
		-- Mark object as spawn.
		button btn_spawn "Mark selection as spawn" tooltip:"Marks the object as a spawn."
		
		-- Mark object as camera.
		button btn_camera "Mark selection as camera" tooltip:"Marks the object as a camera."
		
		-- Mark object as camera_auxiliar.
		button btn_camera_auxiliar "Mark selection as camera auxiliar" tooltip:"Marks the object as a camera auxiliar."
		
		-- Add JSON extra data.
		button btn_json "Add JSON data" tooltip:"Add Json data."
		
		-- Mark object as instanced.
		button btn_instancing "Set selection instancing off/on" tooltip:"Marks the object as instanced or not. By default objects are instanced."
		
		-- Mark object as casting shadows.
		button btn_cast_shadows "Set selection casting shadows off/on" tooltip:"Marks the object for casting shadows or not. By default objects cast shadows."
		
		-- Mark object as having transperencies.
		button btn_transparencies_texture "Set selection for alpha texture on/off" tooltip:"Objects like grass or trees should be marked with this button as their albedo textures uses the alpha channel for transparencies. By default objects have no transparencies."
		
		-- Mark object as skeleton.
		button btn_skeleton "Mark selection as skeleton" tooltip:"Marks the object as a skeleton."
		
		-- Mark object as culling volume.
		button btn_make_culling_volume "Mark selection as culling volume." tooltip:"Marks the object as a culling volume."
		
		-- Set default clearing object properties.
		button btn_clear_properties "Clear selection object properties."

		-- Set button to reload all the prefabs on the scene.
		button btn_reload_prefabs "Reload all the scene prefabs."

		groupBox grouping_actions_box "Scene/Assets Editor Actions" pos:[0,0] width:162 height:400
		
		-- Text box that shows the user properties of the object.
		edittext prefix_txt "User properties for selected obj:" fieldWidth:470 height:600 labelOnTop:true
		local currentlySelectedObj = undefined
		
		button btn_make_floating_window "Set window floating"
		
		button btn_rebind_assets "Rebind assets and materials." tooltip:"Use this button to rebind all material textures to your directory path."
		
		-- Updates the defined properties of the currently selected object so they display ok.
		-- I didn't found a callback for input so i will use this function instead.
		fn updateUserDefinedPropertiesForSelectedObject = (
			if currentlySelectedObj != undefined then(
				prefix_txt.text = getUserPropBuffer currentlySelectedObj
			)
		)
		
		-- Same as the upper one but the obj is recieved as parameter.
		fn updateUserDefinedPropertiesForObject obj = (
			prefix_txt.text = getUserPropBuffer obj
		)
		
		-- Set as prefab button.
		on btn_make_prefab pressed do(
			try(destroyDialog PrefabRollout)catch()
			createDialog PrefabRollout
		)
		
		-- Set as trigger button.		
		on btn_trigger pressed do(
			local timeOfExecution = getTimePressedButton()
			format "------ Make Trigger Time: %\n\n" timeOfExecution
			local selectionD = getCurrentSelection()
			setUserDefinedPropertyForObjs selectionD "trigger" "trigger" false
			setUserDefinedPropertyForObjs selectionD "tags" "[trigger]" false
			-- Updates the user defined properties for the selected object if we have one showing.
			updateUserDefinedPropertiesForSelectedObject()
		)
		
		-- Set as spawn.
		on btn_spawn pressed do(
			try(destroyDialog SpawnEditor)catch()
			createDialog SpawnEditor
		)
		
		on btn_camera pressed do(
			local timeOfExecution = getTimePressedButton()
			format "------ Make camera Time: %\n\n" timeOfExecution
			local selectionD = getCurrentSelection()
			setUserDefinedPropertyForObjs selectionD "camera" "camera" false
			setUserDefinedPropertyForObjs selectionD "tags" "[camera]" false
			-- Updates the user defined properties for the selected object if we have one showing.
			updateUserDefinedPropertiesForSelectedObject()
		)
		
		on btn_camera_auxiliar pressed do(
			local timeOfExecution = getTimePressedButton()
			format "------ Make camera Time: %\n\n" timeOfExecution
			local selectionD = getCurrentSelection()
			setUserDefinedPropertyForObjs selectionD "camera_auxiliar" "camera_auxiliar" false
			setUserDefinedPropertyForObjs selectionD "tags" "[camera_auxiliar]" false
			-- Updates the user defined properties for the selected object if we have one showing.
			updateUserDefinedPropertiesForSelectedObject()
		)
		
		-- Set JSON data.
		on btn_json pressed do(
			try(destroyDialog JSONEditor)catch()
			createDialog JSONEditor width:600
		)
		
		-- Set as skeleton.
		on btn_skeleton pressed do(
			local timeOfExecution = getTimePressedButton()
			format "------ Marked as Skeleton Time: %\n\n" timeOfExecution
			local selectionD = getCurrentSelection()
			setUserDefinedPropertyForObjs selectionD "skeleton" "skeleton" false
			-- Updates the user defined properties for the selected object if we have one showing.
			updateUserDefinedPropertiesForSelectedObject()
		)
		
		-- Set as collider button.
		on btn_collider pressed do(
			try(destroyDialog ColliderRollout)catch()
			createDialog ColliderRollout
		)

		-- Mark for instancing.
		on btn_instancing pressed do(
			local objs = getCurrentSelection()
			
			if objs == undefined or objs.count < 1 then(
				messagebox "No object was selected!"
				return false
			)
		
			for obj in objs do (
				-- Clear user defined properties if already present on the object.
				checkUserDefinedProperties obj false
				local val = getUserProp obj "instancing"
				if val == undefined then
					val = false
				else
					val = not val
				format "Setting % as % = %.\n\n" "instancing" "instancing" val
				setUserProp obj "instancing" val
			)
			
			-- Updates the user defined properties for the selected object if we have one showing.
			updateUserDefinedPropertiesForSelectedObject()
		)
		
		-- Mark for casting shadows.
		on btn_cast_shadows pressed do(
			local objs = getCurrentSelection()
			
			if objs == undefined or objs.count < 1 then(
				messagebox "No object was selected!"
				return false
			)
		
			for obj in objs do (
				-- Clear user defined properties if already present on the object.
				checkUserDefinedProperties obj false
				local val = getUserProp obj "cast_shadows"
				if val == undefined then
					val = false
				else
					val = not val
				format "Setting % as % = %.\n\n" "cast_shadows" "cast_shadows" val
				setUserProp obj "cast_shadows" val
			)
			
			-- Updates the user defined properties for the selected object if we have one showing.
			updateUserDefinedPropertiesForSelectedObject()
		)
		
		on btn_transparencies_texture pressed do(
			local objs = getCurrentSelection()
			
			if objs == undefined or objs.count < 1 then(
				messagebox "No object was selected!"
				return false
			)
		
			for obj in objs do (
				-- Clear user defined properties if already present on the object.
				checkUserDefinedProperties obj false
				local val = getUserProp obj "has_alpha_texture_for_transparency"
				if val == undefined then
					val = true
				else
					val = not val
				format "Setting % as % = %.\n\n" "has_alpha_texture_for_transparency" "has_alpha_texture_for_transparency" val
				setUserProp obj "has_alpha_texture_for_transparency" val
			)
			
			-- Updates the user defined properties for the selected object if we have one showing.
			updateUserDefinedPropertiesForSelectedObject()
		)
		
		-- Set as culling volume.
		on btn_make_culling_volume pressed do(
			try(destroyDialog CullingVolumeRollout)catch()
			createDialog CullingVolumeRollout width:250
		)
		
		-- Clear properties.
		on btn_clear_properties pressed do(
			local timeOfExecution = getTimePressedButton()
			format "------ Clear Object Properties   Time: %\n\n" timeOfExecution
			SceneEditor.clearUserDefinedPropertiesForSelectedObjs()
			-- Updates the user defined properties for the selected object if we have one showing.
			updateUserDefinedPropertiesForSelectedObject()
		)

		-- Reload all the prefabs from the scene
		on btn_reload_prefabs pressed do(
			local timeOfExecution = getTimePressedButton()
			format "------ Reload scene prefabs   Time: %\n\n" timeOfExecution
			try (
				gc()

				--Select all
				max select all

                -- Loop all the items getting the user properties if they have a prefab
                local nitems = 0
                local scenePrefabs = GetCurrentSelection()
                for obj in scenePrefabs do (
                    if not isValidNode obj then continue
                    if not isPrefab obj then continue --If not prefab continue
                    reloadPrefab obj
                )
				MessageBox "Scene prefabs reloaded OK"
			) catch (
				MessageBox ("Error reloading scene prefabs:\n" + getCurrentException())
			)
		)
		
		/* Clear properties checkbox */
		
		-- Checkbox clear properties.
		on clear_properties_by_default changed state_checkbox do(
			local timeOfExecution = getTimePressedButton()
			format "------ Changed Clear Properties by default to: %   Time: %\n\n" state_checkbox timeOfExecution
		)
		
		-- Press this button to get all materials and change their paths.
		on btn_rebind_assets pressed do(
			for matClass in material.classes do(
				for m in (getclassinstances matClass processAllAnimatables:true processChildren:true) do(
					if classof m == Standardmaterial then (
						if m.diffusemap != undefined then(
							if hasProperty m.diffusemap "filename" then(
								local start = findString m.diffusemap.filename "\maxscript\assets\\"
								if start != undefined then(
									local subPath = substring m.diffusemap.filename start m.diffusemap.filename.count
									m.diffusemap.filename = mcv_scripts_path + subPath
								)
						    )
						)
						
						if m.bumpMap != undefined then(
							local start = findString m.bumpMap.normal_map.filename "\maxscript\assets\\"
							if start != undefined then(
								local subPath = substring m.bumpMap.normal_map.filename start m.bumpMap.normal_map.filename.count
								m.bumpMap.normal_map.filename = mcv_scripts_path + subPath
							)
						)
						
						if m.selfIllumMap != undefined then(
							local start = findString m.selfIllumMap.filename "\maxscript\assets\\"
							if start != undefined then(
								local subPath = substring m.selfIllumMap.filename start m.selfIllumMap.filename.count
								m.selfIllumMap.filename = mcv_scripts_path + subPath
							)
						)
						
						if m.displacementMap != undefined then(
							local start = findString m.displacementMap.filename "\maxscript\assets\\"
							if start != undefined then(
								local subPath = substring m.displacementMap.filename start m.displacementMap.filename.count
								m.displacementMap.filename = mcv_scripts_path + subPath
							)
						)
						
						format "Rebinded standard material: %\n" m.name
					)
					else if classof m == Physical_Material then(
						
						if m.base_color_map != undefined then(
							local start = findString m.base_color_map.filename "\maxscript\assets\\"
							if start != undefined then(
								local subPath = substring m.base_color_map.filename start m.base_color_map.filename.count
								m.base_color_map.filename = mcv_scripts_path + subPath
							)
						)
						
						if m.bump_map != undefined then(
							local start = findString m.bump_map.filename "\maxscript\assets\\"
							if start != undefined then(
								local subPath = substring m.bump_map.filename start m.bump_map.filename.count
								m.bump_map.filename = mcv_scripts_path + subPath
							)
						)
						
						if m.metalness_map != undefined then(
							local start = findString m.metalness_map.filename "\maxscript\assets\\"
							if start != undefined then(
								local subPath = substring m.metalness_map.filename start m.metalness_map.filename.count
								m.metalness_map.filename = mcv_scripts_path + subPath
							)
						)
						
						if m.roughness_map != undefined then(
							local start = findString m.roughness_map.filename "\maxscript\assets\\"
							if start != undefined then(
								local subPath = substring m.roughness_map.filename start m.roughness_map.filename.count
								m.roughness_map.filename = mcv_scripts_path + subPath
							)
						)
						
						if m.emission_map != undefined then(
							local start = findString m.emission_map.filename "\maxscript\assets\\"
							if start != undefined then(
								local subPath = substring m.emission_map.filename start m.emission_map.filename.count
								m.emission_map.filename = mcv_scripts_path + subPath
							)
						)
						
						if m.emission_map != undefined then(
							local start = findString m.emission_map.filename "\maxscript\assets\\"
							if start != undefined then(
								local subPath = substring m.emission_map.filename start m.emission_map.filename.count
								m.emission_map.filename = mcv_scripts_path + subPath
							)
						)
						
						if m.displacement_map != undefined then(
							local start = findString m.displacement_map.filename "\maxscript\assets\\"
							if start != undefined then(
								local subPath = substring m.displacement_map.filename start m.displacement_map.filename.count
								m.displacement_map.filename = mcv_scripts_path + subPath
							)
						)
						
						format "Rebinded physical material: %\n" m.name
					)
					
				)
			)
		)
		
		/* User Properties Text Box */
		
		-- If user clicks outside the box, the user properties of the selected object will be updated.
		on prefix_txt changed modified_user_properties do(
			-- If text is not null, update object.
			if currentlySelectedObj != undefined and modified_user_properties != undefined then(
				format "Updated properties for obj %\n" currentlySelectedObj
				prefix_txt.text = modified_user_properties
				setUserPropBuffer currentlySelectedObj prefix_txt.text
			)
		)
		
		-- Called on change selection callback.
		fn updatePrefixTextOnSelectObj = (
			local selectedObjects = getCurrentSelection()
			local numObjectSelected = selectedObjects.count
			currentlySelectedObj = undefined
			if numObjectSelected < 1 then
				prefix_txt.text = "No user properties shown due to no object selected.\n"
			else if numObjectSelected > 1 then
				prefix_txt.text = "No user properties shown due to more than one object selected.\n"
			else(
				currentlySelectedObj = selectedObjects[1]
				prefix_txt.text = getUserPropBuffer currentlySelectedObj
			)
		)
		
		on btn_make_floating_window pressed do(
			SceneEditor.setAsFloater = not SceneEditor.setAsFloater
			-- We are cloasing a floating window.
			if not SceneEditor.setAsFloater then(
				local timeOfExecution = getTimePressedButton()
				format "------ Closing floating window   Time: %\n\n" timeOfExecution
				
				-- Close floating and open normal rollout in utilities.
				closeDialog SceneEditorRollout
				startSceneEditorRollout()
			)
			else(
				local timeOfExecution = getTimePressedButton()
				format "------ Opening floating window   Time: %\n\n" timeOfExecution

				-- Close normal and open floating dialog.
				removeRollout SceneEditorRollout
				startSceneEditorDialog()
			)
		)
	)
	
	on SceneEditor open do
	(
		format "Starting scene editor.\n"
		
		-- Create rollout.
		if setAsFloater then
			startSceneEditorDialog()
		else
			startSceneEditorRollout()
		
		-- Called when a selection of objects in the scene is done or when mouse is over scene.
		callbacks.addScript #selectionSetChanged "SceneEditor.SceneEditorRollout.updatePrefixTextOnSelectObj()" id:#updatedUserPropertiesText
		registerRedrawViewsCallback SceneEditor.SceneEditorRollout.updatePrefixTextOnSelectObj
	)
	
	on SceneEditor close do
	(
		format "Closing scene editor.\n"
		-- Close as floating window too.
		if setAsFloater then
			closeDialog SceneEditorRollout
		else
			removeRollout SceneEditorRollout
		
		/* Remove any dialogs if open */
		try(destroyDialog PrefabRollout)catch()
		try(destroyDialog ColliderRollout)catch()
		try(destroyDialog CullingVolumeRollout)catch()
		
		callbacks.RemoveScripts #selectionSetChanged id:#updatedUserPropertiesText
		unRegisterRedrawViewsCallback  SceneEditor.SceneEditorRollout.updatePrefixTextOnSelectObj
	)
	
	/* Initialization functions */
	
	-- Start the editor as a dialog.
	fn startSceneEditorDialog = (
		openDialog SceneEditorRollout SceneEditorRollout.dialogWidth
		
		-- Update user properties text to show values.
		SceneEditorRollout.updatePrefixTextOnSelectObj()
		
		-- Set button text.
		SceneEditorRollout.btn_make_floating_window.text = "Attach to window utilities."
		SceneEditorRollout.grouping_actions_box.width = 500
	)
	
	-- Start the editor as a rollout.
	fn startSceneEditorRollout = (
		addRollout SceneEditorRollout
		
		-- Update user properties text to show values.
		SceneEditorRollout.updatePrefixTextOnSelectObj()
		
		-- Set button text.
		SceneEditorRollout.btn_make_floating_window.text = "Set window floating."
		SceneEditorRollout.grouping_actions_box.width = 164
	)
	
	-- Opens a dialog
	fn openDialog dialog_type width_dialog = (
		createDialog dialog_type width:width_dialog style:#(#style_titlebar, #style_border, #style_sysmenu, #style_minimizebox)
	)
	
	-- Closes a dialog
	fn closeDialog dialog_type = (
		destroyDialog dialog_type
	)
	
	/* Object creation */
	
	-- Checks if the object already has properties set.
	fn checkIfObjAlreadyHasProperties obj = (
		local buffer = getUserPropBuffer obj
		if buffer != "" then(
			return true
		)
		return false
	)
	
	-- Removes all user defined properties from an object.
	fn clearUserDefinedProperties obj = (
		setUserPropBuffer obj ""
	)
	
	-- Checks if the object has user defined properties. Removes them if the second param is true.
	fn checkUserDefinedProperties obj clearProperties = (
		if checkIfObjAlreadyHasProperties obj then(
			if clearProperties then(
				local buffer = getUserPropBuffer obj
				format "[ATTENTION!]: Object with name % already has properties set. They will be removed.\n" obj.name
				format "Properties are: %" buffer
				clearUserDefinedProperties obj
			)
		)
	)
	
	-- Clear user defined properties for selection
	fn clearUserDefinedPropertiesForSelectedObjs = (
		local selectedObjects = getCurrentSelection()
		for obj in selectedObjects do (
			-- Clear user defined properties if already present on the object.
			if checkIfObjAlreadyHasProperties obj then(
				clearUserDefinedProperties obj
				format "Cleared user defined properties for object %\n\n" obj.name
			)
			else
				format "User defined properties for this object are empty %\n\n" obj.name
		)
	)
)