clearListener()

fn isNavmesh obj = (
	return obj != undefined and getUserProp obj "collider"  != undefined and obj.parent != null
)

----------------------------------------------------------
struct TNavmeshExporter (

	project_path = mcv_scripts_path + "/bin/",
	base_path = "data/",
	current_scene_name,
	scenes_path = base_path + "scenes/",
	mesh_path = base_path + "meshes/",
	mats_path = base_path + "materials/",
	navmesh_path = mesh_path + "navmesh/raw/",
	
	-- ----------------------------------------------------------
	fn selectChildrenNavmesh obj = (
		
		for child in obj.children do (
			if isNavmesh child then selectmore child
		)

	),
	
	fn exportSelectedNavmeshes = (
		local navmesh_file = project_path + navmesh_path + current_scene_name + ".obj"
		format "Exporting navmesh %\n " navmesh_file
		exportfile navmesh_file #noPrompt selectedOnly:true
	),
	
	fn exportNavmesh = (
		max select none
		current_scene_name = getFilenameFile maxFileName
		
		local nitems = 0
		for obj in $* do (
			if isNavmesh obj then continue
			selectChildrenNavmesh obj
		)
		
		--exportSelectedNavmeshes()
	)
	
)
	
--navExporter = TNavmeshExporter()
--navExporter.exportNavmesh()
