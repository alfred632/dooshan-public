-- Path to assets icons.
global mcv_3DsMaxAssetsIcons_path = mcv_scripts_path + "/maxscript/assets_editor_icons/"

if PrefabRollout != undefined then
	try(destroyDialog PrefabRollout)catch()

rollout PrefabRollout "Prefab Rollout"
(	
	/* Layout */
	label titlePrefab "Prefab selector"
	
	-- List of prefab categories
	dropdownlist prefab_categories_list "Prefab categories" items:#("None")
	
	-- List of prefabs for the given category
	listbox prefab_list "Prefabs:" items:#("None")
	
	-- Show image for the selected object.	
	local noIconImageName = "editor_base_icons/default"
	imgtag img_tag "Icon of the asset" bitmap:undefined width:128 height:128 align:#center
	
	-- Set prefab.
	label spacing1 ""
	button btn_make_selected_prefab "Set as selected prefab"
	label spacing2 ""
	
	-- Reload prefabs.
	button btn_reload_assets "Reload prefabs" tooltip:"If you have exported a prefab and don't see it, press this button."
	label spacing3 ""
	
	-- Close prefab rollout.
	button btn_close_prefab_rollout "Close"
	
	-- Shows the icon of an asset. Pass the name as parameter.
	fn showAssetIcon nameOfTheAsset = (
		local icon_as_bitmap = undefined
		if not nameOfTheAsset == undefined then(
			local nameOfIcon = nameOfTheAsset + ".png"
			local iconPath = mcv_3DsMaxAssetsIcons_path + nameOfIcon
			icon_as_bitmap = openBitmap iconPath	
		)
		
		if icon_as_bitmap == undefined then(
			local randomNum = random 1 4 as string
			local noIconPath = mcv_3DsMaxAssetsIcons_path + noIconImageName + randomNum + ".png"
			icon_as_bitmap = openBitmap noIconPath
		)
		img_tag.bitmap = icon_as_bitmap
	)
	
	-- Convert Material from relative path to user path.
	fn getMaterialRelativePathToUserPath assetPathWithoutFilename mat = (
		if classof mat == StandardMaterial then(
			for map in mat.maps do(
				if map == undefined then
					continue
				map.filename = assetPathWithoutFilename + map.filename
			)
		)
		else if classof mat == Physical_Material then(
			if mat.base_color_map != undefined then
				mat.base_color_map.filename = assetPathWithoutFilename + mat.base_color_map.filename
			
			if mat.bump_map != undefined then
				mat.bump_map.filename = assetPathWithoutFilename + mat.bump_map.filename
			
			if mat.metalness_map != undefined then
				mat.metalness_map.filename = assetPathWithoutFilename + mat.metalness_map.filename
			
			if mat.roughness_map != undefined then
				mat.roughness_map.filename = assetPathWithoutFilename + mat.roughness_map.filename
			
			if mat.emission_map != undefined then
				mat.emission_map.filename = assetPathWithoutFilename + mat.emission_map.filename
			
			if mat.displacement_map != undefined then
				mat.displacement_map.filename = assetPathWithoutFilename + mat.displacement_map.filename
			
			if mat.cutout_map != undefined then
				mat.cutout_map.filename = assetPathWithoutFilename + mat.cutout_map.filename
		)
	)
	
	-- Convert relative path of the material in the .max file to user path.
	fn convertMaterialsFromRelativePathToUserAbsolutePath assetPath = (
		local assetPathWithoutFilename = getFilenamePath assetPath
		
		local mergedObjects = getCurrentSelection()
		for merged in mergedObjects do(
			-- Remap objects so their textures point where they have to.
			local mat = merged.material
			
			if classof mat == MultiMaterial then (
				local multi_mat = mat
				local objAsMesh = merged
				if classof objAsMesh == Editable_mesh then(
					local materials_of_mesh = getMaterialsUsedByMesh objAsMesh
					for mat_idx = 1 to materials_of_mesh.count do (
						if materials_of_mesh[ mat_idx ] == undefined then continue
						local mat_of_mesh = multi_mat[ mat_idx ]
						getMaterialRelativePathToUserPath assetPathWithoutFilename mat_of_mesh
					)
				)
			)
			else
				getMaterialRelativePathToUserPath assetPathWithoutFilename mat		
		)
	)
	
	-- Creates an asset.
	fn createAsset = (
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Setting assets for selection   Time: %\n\n" timeOfExecution
		
		local selectedCategory = prefab_categories_list.selected
		local selectedPrefab = prefab_list.selected
		
		local assetPath = EditorAssetsManager.getAssetPath selectedCategory selectedPrefab
		local selectedObjects = getCurrentSelection()
		if selectedObjects.count < 1 then(
			messagebox "You don't have any objects selected."
			return false
		)
		
		local prefabRelativePath = EditorPrefabsManager.getPrefabPath selectedCategory selectedPrefab
		if prefabRelativePath == undefined then(
			local message = "Prefab with name " + selectedPrefab +  " was not found inside the prefab folder. Please, create the prefab inside the folder before adding it. To do so, use the Export As Prefab button in the MCV exporter."
			messagebox message
			return false
		)

		-- For all objects selected, add the file.
		local selectedObjects = getCurrentSelection()
		for selectedObj in selectedObjects do(
			if selectedObj.parent != undefined and findItem selectedObjects selectedObj.parent then(
				continue
			)
			
			-- Don't know if there is a faster way, this will have to do :p.
			mergeMAXFile assetPath #select #AutoRenameDups #useMergedMtlDups
			if selection.count < 1 then(
				messagebox "No object was found in the merge max file!"
				return false
			)
			
			convertMaterialsFromRelativePathToUserAbsolutePath assetPath
			
			-- Position objects in the selection.
			local posSelectedObjToPosAt = selectedObj.transform.pos
			for obj in selection do(
				if obj.parent != undefined then continue -- Ignore children, they get positioned automatically.
				local scaleObj = obj.scale
				obj.transform = selectedObj.transform
				obj.scale = scaleObj
			)
			
			SceneEditor.setUserDefinedPropertyForObject selection[1] "prefab" prefabRelativePath true
			for idx = 2 to selection.count do(
				SceneEditor.setUserDefinedPropertyForObject selection[idx] "ignore" true false
			)
		)
		
		SceneEditor.SceneEditorRollout.updateUserDefinedPropertiesForSelectedObject()
		
		-- Delete all selected objects, also their children as they don't have sense if we are placing thsi prefab.
		for obj in selectedObjects do(
			if isValidNode obj then
				delete obj.children
		)
		delete selectedObjects
		
		format "\n"
	)
	
	/* Button. */
	
	-- List of prefab categories.
	on prefab_categories_list selected selectedCategory do(
		-- Set all assets for the given category in the prefab list.
		-- We use the assets manager as we only want to export assets.
		if prefab_categories_list.selected != undefined then(
			local items = EditorPrefabsManager.getAllPrefabsNamesForTheGivenCategory prefab_categories_list.selected
			if items == undefined then
				items = #()
			prefab_list.items = items
		)
		-- Show asset of icon on changed category.	
		if prefab_list.selected != undefined then
			showAssetIcon prefab_list.selected
	)
	
	-- List of prefabs.
	on prefab_list selected selectedPrefab do(
		-- Show icon of selected prefab.
		if prefab_list.selected != undefined then
			showAssetIcon prefab_list.selected
	)
	
	-- Create a prefab button.
	on btn_make_selected_prefab pressed do(
		createAsset()
	)
	
	-- Reload prefabs.
	on btn_reload_assets pressed do(
		EditorAssetsManager.loadAssetsIntoEditor()
		EditorPrefabsManager.loadPrefabsIntoEditor()
		MessageBox "Reloaded successfully."
	)
	
	-- Close the rollout.
	on btn_close_prefab_rollout pressed do(
		local timeOfExecution = SceneEditor.getTimePressedButton()
		format "------ Closed prefab rollout   Time: %\n\n" timeOfExecution
		destroyDialog PrefabRollout
	)
	
	-- Called when the rollout opens.
	on PrefabRollout open do(
		-- Get all assets categories. We use the assets manager as we want to only spawn things that have max scripts.
		prefab_categories_list.items = EditorAssetsManager.getAllAssetCategories()
		
		if prefab_categories_list.items.count < 1 then(
			MessageBox "No asset categories where found in maxscript/assets, export prefabs before opening this window."
			destroyDialog PrefabRollout
		)
		
		-- Set all assets for the given category in the prefab list.
		local items = EditorPrefabsManager.getAllPrefabsNamesForTheGivenCategory prefab_categories_list.items[1]
		if items == undefined then
			items = #()

		prefab_list.items = items 
		
		-- Show icon of selected prefab.
		showAssetIcon prefab_list.selected
	)
)