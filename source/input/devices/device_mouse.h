#pragma once

#include "input/device.h"
#include "input/input_enums.h"

class CDeviceMouse : public Input::IDevice
{
public:
	CDeviceMouse(const std::string & name) : Input::IDevice(name){}
	
	void update(Input::TMouseData & data) override;
	void processWindowMsg(UINT message, WPARAM wParam, LPARAM lParam);

	bool setLock(bool nLock) { locked = nLock; }
	void setActive(bool active) { _active = active; }

private:
	bool _buttons[Input::BT_MOUSE_COUNT] = {};
	int _posX = 0, _posY = 0;
	int _rawDeltaX = 0, _rawDeltaY = 0;
	int _wheelDeltaSteps = 0;
	bool locked = false;

	bool _active = true;
};
