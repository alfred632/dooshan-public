#pragma once

#include "mcv_platform.h"
#include "input/devices/device_keyboard.h"
#include "input/interfaces/keyboard.h"
#include "input/input_enums.h"
#include "engine.h"

void CDeviceKeyboard::update(Input::TKeyboardData & data)
{
	if (!_active)
		return;
	for (int i = 0; i < Input::BT_KEYBOARD_COUNT; ++i) {
		data.keys[i] = Utils::isPressed(i);
	}
}

void CDeviceKeyboard::processWindowMsg(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (!_active)
		return;

	switch (message)
	{
	//Raw keyboard
	case WM_INPUT:
	{
		UINT dwSize = 48;
		static BYTE lpb[48];

		GetRawInputData((HRAWINPUT)lParam, RID_INPUT,
			lpb, &dwSize, sizeof(RAWINPUTHEADER));

		RAWINPUT* raw = (RAWINPUT*)lpb;

		if (raw->header.dwType == RIM_TYPEKEYBOARD)
		{
			kData->keys[raw->data.keyboard.VKey] = !raw->data.keyboard.Flags; //We need to negate the flag
			EngineInput.setControllerActive(false);
		}
		break;
	}
	default:
		break;
	}
}