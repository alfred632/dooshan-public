#pragma once

#include "mcv_platform.h"
#include "input/button.h"
#include "input/input_enums.h"
#include "windows/app.h"

namespace Input
{
	struct TMouseData
	{
		bool buttons[BT_MOUSE_COUNT];
		VEC2 position;
		VEC2 rawDelta;
		float wheelDelta = 0.f;
		float sensitivity = 1.0f;
	};

	class CMouse
	{
		
	public:
		CMouse() {

		}

		void update(TMouseData& data, float dt)
		{
			PROFILE_FUNCTION("CMouse");
			_sensitivity = data.sensitivity;
			_prevPosition = _currPosition;
			_currPosition = data.position;
			_deltaPosition = _currPosition - _prevPosition;
			_rawDeltaPosition = data.rawDelta;
			for (int i = 0; i < BT_MOUSE_COUNT; ++i)
			{
				float newValue = data.buttons[i] ? 1.f : 0.f;
				_buttons[i].update(newValue, dt);
			}
			if (_isCenterMouseModeActive && CApplication::instance().getFocus()) {
				if (waitingNextWinPosMsgEnum == JUST_LOCKED) {
					_deltaPosition = VEC2(0, 0);
					_prevWinPositionMsg = data.position;
					waitingNextWinPosMsgEnum = WAITING;
				}
				else if (waitingNextWinPosMsgEnum == WAITING) {
					if (_currPosition.x != _prevWinPositionMsg.x ||		//We recieved new mouse coords
						_currPosition.y != _prevWinPositionMsg.y)
						waitingNextWinPosMsgEnum = RECIEVED;
					else
						_deltaPosition = VEC2(0, 0);
				}

				VEC2 windowCenter = (CApplication::instance().getWindowSize() * 0.5f);
				POINT p{ LONG(windowCenter.x), LONG(windowCenter.y) };
				ClientToScreen(CApplication::instance().getWindowHandler(), &p);

				SetCursorPos(p.x, p.y);
				_currPosition = windowCenter;
			}

			_wheelDelta = data.wheelDelta;
		}

		void setCenterMouseMode(bool isActive) {
			_isCenterMouseModeActive = isActive;
			if (isActive) {
				waitingNextWinPosMsgEnum = JUST_LOCKED;
				// We initialize displayCount to 0 because the mouse is showed if the displayCount is 0 or greater.
				int displayCount = 0;
				do {
					displayCount = ShowCursor(false);
				} while (displayCount >= 0);
			} 
			else {
				// We initialize displayCount to -1 because the mouse is not showed if the displayCount is -1 or lower.
				int displayCount = -1;
				do {
					displayCount = ShowCursor(true);
				} while (displayCount < 0);
			}
		}

		VEC2 getDelta() const
		{
			return _deltaPosition * .5f * _sensitivity;
		}

		VEC2 getRawDelta() const
		{
			return _rawDeltaPosition * .5f * _sensitivity;
		}

		Vector2 getPosition() {
			POINT p;
			GetCursorPos(&p);
			ScreenToClient(Application.getWindowHandler(), &p);
			return Vector2(p.x, p.y);
		}

		TButton _buttons[BT_MOUSE_COUNT];
		VEC2 _currPosition;
		VEC2 _prevPosition;
		VEC2 _prevWinPositionMsg;
		VEC2 _deltaPosition = VEC2(0, 0);
		VEC2 _rawDeltaPosition = VEC2(0, 0);
		float _sensitivity = 1.0f;
		enum WaitingNextWinPosMsg { JUST_LOCKED, WAITING, RECIEVED }; //Waiting next window mouse position mesage
		WaitingNextWinPosMsg waitingNextWinPosMsgEnum = RECIEVED;
		bool _isCenterMouseModeActive;
		float _wheelDelta = 0.f;
		bool _locked;
	};
}

