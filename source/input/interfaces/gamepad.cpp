#include "mcv_platform.h"
#include "gamepad.h"
#include "engine.h"
#include "profiling/profiling.h"

void Input::CGamepad::update(const TGamepadData & data, float delta) {
	PROFILE_FUNCTION("CGamepad");
	_connected = data.connected;
	if (!_initialized) {
		EngineInput.setControllerActive(_connected);
		_initialized = true;
	}
	for (int i = 0; i < BT_GAMEPAD_COUNT; ++i) {
		if (data.buttons[i] != 0.0f)
			EngineInput.setControllerActive(true);
		_buttons[i].update(data.buttons[i], delta);
	}
}
