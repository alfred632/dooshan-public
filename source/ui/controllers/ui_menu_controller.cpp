#pragma once

#include "mcv_platform.h"
#include "ui/widgets/ui_button.h"
#include "ui/controllers/ui_menu_controller.h"
#include "engine.h"
#include "input/input.h"

namespace UI
{

  void CMenuController::update(float dt)
  {
    if (EngineInput["menu_next"].justPressed())
    {
      setCurrentOption(++_currentOption);
    }
    if (EngineInput["menu_prev"].justPressed())
    {
      setCurrentOption(--_currentOption);
    }
    if (EngineInput["menu_confirm"].justPressed())
    {
      _options[_currentOption].callback();
    }
  }

  void CMenuController::registerOption(CButton* button, Callback callback)
  {
    _options.emplace_back(TOption{button, callback});
  }

  void CMenuController::setCurrentOption(int idx)
  {
    _currentOption = Maths::clamp(idx, 0, static_cast<int>(_options.size()) - 1);
    
    for (auto option : _options)
    {
      option.button->setCurrentState("enabled");
    }

    if (_currentOption >= 0 && _currentOption < _options.size())
    {
      _options.at(_currentOption).button->setCurrentState("selected");
    }
  }
}
