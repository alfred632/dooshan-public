#pragma once

#include "modules/module.h"
#include "..\\tools\DirectXTK-jun2019\Inc\SpriteBatch.h"
#include "..\\tools\DirectXTK-jun2019\Inc\SpriteFont.h"

namespace UI
{
  class CWidget;
  class CController;
  class CImage;
  class CButton;
  class CProgress;
  class CTextFont;

  extern std::unique_ptr<DirectX::SpriteBatch> spriteBatch;
  extern std::map<std::string, std::unique_ptr<DirectX::SpriteFont>> spriteFonts;
  extern std::string defaultFont;

  struct TFloatDamage {
	  std::string textDamage;
	  std::string fontName;
	  VEC4 textColor;
	  VEC4 outlineColor;
	  bool isCritical;
	  VEC3 targetWorldPosition;
	  VEC2 offset;
	  float scale;
	  std::string animationName;
	  float aliveTime;
  };

  class CModuleUI : public IModule
  {
  public:
    CModuleUI(const std::string& name);

    bool start() override;
    void update(float dt) override;
    void render();

    void renderInMenu() override;
	void renderDebug() override {};

    void registerWidget(CWidget* widget);
	CTextFont* createFloatingDamageWidget(TFloatDamage settings);
    void activateWidget(const std::string& name);
    void deactivateWidget(const std::string& name);
	void deleteWidget(const std::string& name) {}
	void deleteWidget(CWidget* widget);
	void setChildWidgetVisibleByTime(const std::string childName, const std::string parentName, bool status, float time);

    void registerAlias(CWidget* widget);
    void registerController(CController* controller);
	void unregisterController(CController* controller);

	void initFonts();
	void changeDefaultFont(std::string newDefaultFontName);

    CWidget* getWidgetByName(const std::string& name);
    CWidget* getWidgetByAlias(const std::string& alias);
	bool findWidgetByName(CWidget& widget, const std::string name, CWidget* parent = nullptr);
	CImage* getImageWidgetByName(std::string parentName, std::string widgetName);
	CButton* getButtonWidgetByName(std::string parentName, std::string widgetName);
	CTextFont* getFontWidgetByName(std::string parentName, std::string widgetName);
	CProgress* getProgressWidgetByName(std::string parentName, std::string widgetName);

  private:
    std::map<std::string_view, CWidget*> _registeredWidgets;
    std::map<std::string_view, CWidget*> _registeredAlias;
    std::vector<CWidget*> _activeWidgets;
    std::vector<CController*> _activeControllers;
	std::vector<CWidget*> _toRemoveWidgets;
	int fTextIdx = 0;

  };
}
