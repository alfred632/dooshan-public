#pragma once

#include "mcv_platform.h"
#include "ui/ui_parser.h"
#include "engine.h"
#include "ui/module_ui.h"
#include "ui/ui_widget.h"
#include "ui/widgets/ui_image.h"
#include "ui/widgets/ui_text.h"
#include "ui/widgets/ui_text_font.h"
#include "ui/widgets/ui_button.h"
#include "ui/widgets/ui_button_container.h"
#include "ui/widgets/ui_progress.h"
#include "render/textures/texture.h"
#include "ui/effects/ui_fx_animate_uv.h"
#include "ui/effects/ui_fx_scale.h"
#include "effects/ui_fx_movement.h"
#include <fstream>
#include <iomanip>
#include "render/render.h"


namespace UI
{
  void CParser::loadFile(const json& jsonWidgetsListFile)
  {
    
    for (const json& jDataFile : jsonWidgetsListFile)
    {
      loadWidget(jDataFile);
    }
  }

  void CParser::saveFile(CWidget* widget) {
	  json widget_json;

	  widget_json["name"] = widget->_name;
	  widget_json["alias"] = widget->_alias;

	  std::vector<json> wChildren;
	  for (auto& child : widget->getChildren()) {
		  if (CImage* image = dynamic_cast<CImage*>(child)) {
			  wChildren.push_back(UIParser.parseWidgetChild(*image));
		  }
		  else if (CProgress* progress = dynamic_cast<CProgress*>(child)) {
			  wChildren.push_back(UIParser.parseWidgetChild(*progress));
		  }
		  else if (CTextFont * font = dynamic_cast<CTextFont*>(child)) {
			  wChildren.push_back(UIParser.parseWidgetChild(*font));
		  }
		  else if (CButton * button = dynamic_cast<CButton*>(child)) {
			  wChildren.push_back(UIParser.parseWidgetChild(*button));
		  }
		  else if (CWidget* w = dynamic_cast<CWidget*>(child)) {
			  wChildren.push_back(UIParser.parseWidgetChild(*w));
		  }
		  else
			  Utils::dbg("[CParser::saveFile] Error saving file");
	  }

	  widget_json["children"] = wChildren;

	  std::string testpath = "data/ui/widgets/" + widget->getName() + ".json";
	  std::ofstream file(testpath.c_str());
	  file << std::setw(4) << widget_json << std::endl;

  }

  void CParser::loadWidget(const std::string& widgetFile)
  {
    json jData = Utils::loadJson(widgetFile);
    
    CWidget* widget = parseWidget(jData, nullptr);

    widget->updateTransform();

    EngineUI.registerWidget(widget);
  }

  CWidget* CParser::parseWidget(const json& jData, CWidget* parent)
  {
    const std::string& name = jData["name"];
    const std::string alias = jData.value<std::string>("alias", "");
    const std::string type = jData.value<std::string>("type", "widget");

    CWidget* widget = nullptr;

    if (type == "image")									widget = parseImage(jData);
		else if (type == "text")							widget = parseText(jData);
		else if (type == "text_font")					widget = parseTextFont(jData);
    else if (type == "button")						widget = parseButton(jData);
		else if (type == "button_container")	widget = parseButtonContainer(jData);
    else if (type == "progress")					widget = parseProgress(jData);
    else																	widget = parseWidget(jData);

    widget->_name = name;
    widget->_alias = alias;
    widget->setParent(parent);

    // parse effects
    if (jData.count("effects") > 0)
    {
      for (auto& jEffectData : jData["effects"])
      {
        CEffect* fx = parseEffect(jEffectData);
        if (fx)
        {
          fx->_owner = widget;
          widget->_effects.push_back(fx);
        }
      }
    }

    // parse children widgets
    if (jData.count("children") > 0)
    {
      for (auto& jChildrenData : jData["children"])
      {
        parseWidget(jChildrenData, widget);

      }
    }

    if (!alias.empty())
    {
      EngineUI.registerAlias(widget);
    }

    return widget;
  }
  
  CWidget* CParser::parseWidget(const json& jData)
  {
    CWidget* widget = new CWidget;

    parseParams(widget->_params, jData);

    return widget;
  }

  CWidget* CParser::parseImage(const json& jData)
  {
    CImage* image = new CImage;

    parseParams(image->_params, jData);
    parseParams(image->_imageParams, jData);
		image->_isMasked = jData.value("masked", image->_isMasked);
	
	for (const std::string &anims : image->_imageParams.animationPaths) {
		image->loadAnimations(Utils::loadJson(anims));
	}

    return image;
  }

  CWidget* CParser::parseText(const json& jData)
  {
    CText* text = new CText;

    parseParams(text->_params, jData);
    parseParams(text->_textParams, jData);

    return text;
  }

  CWidget* CParser::parseTextFont(const json& jData)
  {
	  CTextFont* textFont = new CTextFont;

	  parseParams(textFont->_textParams, jData);

	  return textFont;
  }

  CWidget* CParser::parseButton(const json& jData)
  {
    CButton* button = new CButton;

    parseParams(button->_params, jData);

		//Vector2 bbPos = button->_params.position;
		Vector2 bbPos = Vector2::Zero;
		Vector2 bbSize = loadVector2(jData, "size") * button->_params.scale;
		
		// Take into account the pivots
		bbPos.x = -button->_params.pivot.x * bbSize.x;
		bbPos.y = -button->_params.pivot.y * bbSize.y;

		button->_boundingBox = TBoundingBox(bbPos, bbSize);
		button->_pressedSoundEvent = jData.value("pressed_sound_event", button->_pressedSoundEvent);

    assert(jData.count("states") > 0);
    for (auto& jState : jData["states"])
    {
      CButton::TState st;

			if (jState.count("bg") > 0)
				parseParams(st.bgParams, jState["bg"]);

			if (jState.count("text_font") > 0)
				parseParams(st.textParams, jState["text_font"]);
			st.textParams.position += bbSize / 2.0f;

			if (jState.count("fg") > 0)
				parseParams(st.fgParams, jState["fg"]);

      const std::string& stateName = jState["name"];

      button->_states[stateName] = st;
    }

    return button;
  }

	CWidget * CParser::parseButtonContainer(const json & jData) {
		CButtonContainer* btnContainer = new CButtonContainer;

		parseParams(btnContainer->_params, jData);

		btnContainer->_loops = jData.value("loops", btnContainer->_loops);
		btnContainer->_prevButtonMapping = jData.value("prev_button", btnContainer->_prevButtonMapping);
		btnContainer->_nextButtonMapping = jData.value("next_button", btnContainer->_nextButtonMapping);
		btnContainer->_switchSoundEvent = jData.value("switch_sound_event", btnContainer->_switchSoundEvent);

		return btnContainer;
	}

  CWidget* CParser::parseProgress(const json& jData)
  {
    CProgress* progress = new CProgress;

    parseParams(progress->_params, jData);
    parseParams(progress->_imageParams, jData);
    parseParams(progress->_progressParams, jData);

    return progress;
  }

  void CParser::parseParams(TParams& params, const json& jData)
  {
	/*Adjust position and scale to resolution*/
	float ratioX = Render.getWidth() / 1920.0f;
	float ratioY = Render.getHeight() / 1080.0f;

    params.pivot = loadVector2(jData, "pivot", params.pivot);
    params.position = loadVector2(jData, "position", params.position);
	/*params.position.x *= ratioX;
	params.position.y *= ratioY;*/
    params.scale = loadVector2(jData, "scale", params.scale);
	params.scale.x *= ratioX;
	params.scale.y *= ratioY;
    params.rotation = jData.value<float>("rotation", params.rotation);
    params.visible = jData.value<bool>("visible", params.visible);
  }

  void CParser::parseParams(TImageParams& params, const json& jData)
  {
	/*Adjust position and scale to resolution*/
	float ratioX = Render.getWidth() / 1920.0f;
	float ratioY = Render.getHeight() / 1080.0f;

    params.size = loadVector2(jData, "size", params.size);
	params.size.x *= ratioX;
	params.size.y *= ratioY;
    params.texture = jData.count("texture") > 0 ? EngineResources.getResource(jData["texture"])->as<CTexture>() : params.texture;
    params.mask = jData.count("mask") > 0 ? EngineResources.getResource(jData["mask"])->as<CTexture>() : params.mask;
		params.additive = jData.value<bool>("additive", params.additive);
    params.color = loadVector4(jData, "color", params.color * 255) / 255;
    params.minUV = loadVector2(jData, "minUV", params.minUV);
    params.maxUV = loadVector2(jData, "maxUV", params.maxUV);
	params.minUV.x *= ratioX;
	params.minUV.y *= ratioY;
    params.frameSize = loadVector2(jData, "frame_size", params.frameSize);
    params.nFrames = loadVector2(jData, "count_frames", params.nFrames);
    params.frameDuration = jData.value<float>("frame_duration", params.frameDuration);
    
    if (jData.count("animations") > 0) {
      for (auto& animation : jData["animations"]) {
        params.animationPaths.push_back(animation["path"]);
      }
    }

    params.isAnimationLooped = jData.value<bool>("is_animation_looped", params.isAnimationLooped);
  }

  void CParser::parseParams(TTextParams& params, const json& jData)
  {
    params.text = jData.value<std::string>("text", params.text);
    params.texture = jData.count("font_texture") > 0 ? EngineResources.getResource(jData["font_texture"])->as<CTexture>() : params.texture;
    params.size = loadVector2(jData, "font_size", params.size);
  }

  void CParser::parseParams(TTextFontParams& params, const json& jData)
  {
	  /*Adjust position and scale to resolution*/
	  float ratioX = Render.getWidth() / 1920.0f;
	  float ratioY = Render.getHeight() / 1080.0f;

	  std::string text = jData.value<std::string>("text", std::string(""));
	  params.text = std::wstring(text.begin(), text.end());
	  params.fontName = jData.value<std::string>("font_name", params.fontName);
	  params.position = loadVector2(jData, "position", params.position);
	  params.position.x *= ratioX;
	  params.position.y *= ratioY;
	  params.scale = loadVector2(jData, "scale", params.scale);
	  params.rotation = DEG2RAD(jData.value<float>("rotation", RAD2DEG(params.rotation)));

	  Vector4 tColor = loadVector4(jData, "text_color", Vector4(1.0f, 1.0f, 1.0f, 1.0f));
	  params.textColor.f[0] = tColor.x;
	  params.textColor.f[1] = tColor.y;
	  params.textColor.f[2] = tColor.z;
	  params.textColor.f[3] = tColor.w;

	  Vector4 eColor = loadVector4(jData, "effect_color", Vector4(0.0f, 0.0f, 0.0f, 1.0f));
	  params.effectColor.f[0] = eColor.x;
	  params.effectColor.f[1] = eColor.y;
	  params.effectColor.f[2] = eColor.z;
	  params.effectColor.f[3] = eColor.w;

	  params.hasOutline = jData.value<bool>("has_outline", params.hasOutline);
	  params.hasDropShadow = jData.value<bool>("has_drop_shadow", params.hasDropShadow);
  }

  void CParser::parseParams(TProgressParams& params, const json& jData)
  {
    params.ratio = jData.value<float>("ratio", params.ratio);
  }

  CEffect* CParser::parseEffect(const json& jData)
  {
    const std::string type = jData.value<std::string>("type", "");

    CEffect* fx = nullptr;

	if (type == "animate_uv")      fx = parseFXAnimateUV(jData);
	else if (type == "scale")      fx = parseFXScale(jData);
	else if (type == "movement")   fx = parseFXMovement(jData);
    
    assert(fx);

    return fx;
  }

  CEffect* CParser::parseFXAnimateUV(const json& jData)
  {
    CFXAnimateUV* fx = new CFXAnimateUV;

    fx->_speed = loadVector2(jData, "speed", fx->_speed);

    return fx;
  }

  CEffect* CParser::parseFXScale(const json& jData)
  {
    CFXScale* fx = new CFXScale;

    fx->_scale = loadVector2(jData, "scale", fx->_scale);
    fx->_duration = jData.value<float>("duration", fx->_duration);
    
    const std::string mode = jData.value<std::string>("mode", "single");
    if (mode == "single")         fx->_mode = CFXScale::EMode::Single;
    else if (mode == "loop")      fx->_mode = CFXScale::EMode::Loop;
    else if(mode == "ping_pong")  fx->_mode = CFXScale::EMode::PingPong;

    fx->_interpolator = parseInterpolator(jData.value<std::string>("interpolator", ""));

    return fx;
  }
   
  CEffect* CParser::parseFXMovement(const json& jData)
  {
	  CFXMovement* fx = new CFXMovement;
	

	  return fx;
  }

  Interpolator::IInterpolator* CParser::parseInterpolator(const std::string& type)
  {
    static Interpolator::TLinearInterpolator linear;
    static Interpolator::TQuadInOutInterpolator quadInt;
    static Interpolator::TBackOutInterpolator backInt;
    static Interpolator::TBounceOutInterpolator bounceInt;

    if (type == "quad")         return &quadInt;
    else if (type == "back")    return &backInt;
    else if (type == "bounce")  return &bounceInt;
    else                        return &linear;
  }

  json CParser::parseWidgetChild(CImage& child) {
	  json j;
	  j["name"] = child._name;
	  j["scale"] = vec2ToString(child._params.scale);

	  TImageParams* childParams = &child._imageParams;
	  j["type"] = "image";
	  j["texture"] = childParams->texture->getName();
	  j["position"] = vec2ToString(child._params.position);
	  
	  j["size"] = vec2ToString(childParams->size);
	  j["additive"] = childParams->additive;
	  j["color"] = vec4ToString(childParams->color);
	  j["minUV"] = vec2ToString(childParams->minUV);
	  j["maxUV"] = vec2ToString(childParams->maxUV);
	  j["frame_size"] = vec2ToString(childParams->frameSize);
	  j["count_frames"] = vec2ToString(childParams->nFrames);
	  j["frame_time"] = childParams->frameDuration;
	  
	  return j;
  }

  json CParser::parseWidgetChild(CProgress& child) {
	  json j;
	  /*j["name"] = child._name;
	  j["scale"] = vec2ToString(child._params.scale);

	  TImageParams* childParams = &child._imageParams;
	  j["type"] = "progress";
	  j["texture"] = childParams->texture->getName();
	  j["size"] = vec2ToString(childParams->size);
	  j["position"] = vec2ToString(child._params.position);

	  j["ratio"] = child._progressParams.ratio;*/
	  

	  return j;
  }

  json CParser::parseWidgetChild(CTextFont& child) {
	  json j;
	 /* j["name"] = child._name;
	  j["scale"] = vec2ToString(child._params.scale);

	  TImageParams* childParams = child.getParams();
	  j["type"] = "image";
	  j["texture"] = child.childParams->texture()->getName();
	  j["size"] = vec2ToString(child.childParams->size);
	  j["position"] = vec2ToString(child._params.position);*/

	  return j;
  }

  json CParser::parseWidgetChild(CButton& child)
  {
	  json j;
	  return j;
  }

  json CParser::parseWidgetChild(CWidget& child)
  {
	  json j;
	  j["name"] = child._name;
	  j["alias"] = child._alias;
	  j["position"] = vec2ToString(child._params.position);

	  std::vector<json> children;
	  for (auto& c : child.getChildren()) {
		  if (CImage * image = dynamic_cast<CImage*>(c)) {
			  children.push_back(UIParser.parseWidgetChild(*image));
		  }
		  else if (CProgress * progress = dynamic_cast<CProgress*>(c)) {
			  children.push_back(UIParser.parseWidgetChild(*progress));
		  }
		  else if (CTextFont * font = dynamic_cast<CTextFont*>(c)) {
			  children.push_back(UIParser.parseWidgetChild(*font));
		  }
		  else if (CButton * button = dynamic_cast<CButton*>(c)) {
			  children.push_back(UIParser.parseWidgetChild(*button));
		  }
		  else if (CWidget * w = dynamic_cast<CWidget*>(c)) {
			  children.push_back(UIParser.parseWidgetChild(*w));
		  }
		  else
			  Utils::dbg("[CParser::saveFile] Error saving file");
	  }

	  j["children"] = children;

	  return j;
  }

}
