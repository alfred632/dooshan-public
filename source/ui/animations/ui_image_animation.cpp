#pragma once

#include "mcv_platform.h"
#include "ui_image_animation.h"
#include "geometry/geometry.h"

namespace UI
{
	void CImageAnimation::addSpecialNode(NodeSpecial new_special) {
		specialNodes.push_back(new_special);
	}

	void CImageAnimation::displaySpecialPopup(NodeSpecial* node, int id){
		ImGui::Text("Image");
		ImGui::Text("");
		ImGui::Text("Node: %d", id);
		if (!node->special.empty())
			ImGui::Text("SRC path: %s", node->special);
			//ImGui::Image((void*)node.special->getShaderResourceView(), ImVec2(256, 256));
		ImGui::Text("Time: %.2f", node->time);
	}

	void CImageAnimation::displayMediaControls() {
		ImGui::PushID(this);
		ImGui::BeginGroup();
		if (ImGui::Button("Play")) {
			startAnimation(looping);
		}
		ImGui::SameLine();
		if (ImGui::Button("Pause")) {
			pauseAnimation();
		}
		ImGui::SameLine();
		if (ImGui::Button("Stop")) {
			stopAnimation();
		}
		ImGui::SameLine();
		ImGui::Text("Loop: ");
		ImGui::SameLine();
		ImGui::Checkbox("##RepLopp", &looping);
		ImGui::Text("Time: %f", current_time);
		ImGui::Text("MaxTime: ");
		ImGui::SameLine();
		ImGui::DragFloat("##maxfloat", &max_time, 0.01f, current_time, 20.f, "%.2f");
		ImGui::EndGroup();
		ImGui::PopID();
	}

	void CImageAnimation::displaySpecialNodeContextMenu(std::string node_content){}

	void CImageAnimation::update(float dt)
	{
		if (stopNextFrame) 
			stopAnimation();	

		if (running) {
			current_time += dt;
			if (current_time >= max_time) {
				if (looping)
					current_time = 0.0f;
				else {
					current_time = max_time;
					stopNextFrame = true;
				}
			}
		}
	}

	void CImageAnimation::setAnimationTime(float time)
	{
		if (time <= max_time)
			current_time = time;
		else
			Utils::error("WARNING - [CImageAnimation::setAnimationTime] New time is greater than max time\n");
	}

}
