#pragma once

#include "mcv_platform.h"
#include "ui/ui_widget.h"
#include "ui/ui_effect.h"
#include "imgui/imgui_source/imgui.h"
#include "effects/ui_fx_movement.h"
#include "imgui/imgui_source/imgui_internal.h"
#include "widgets/ui_button.h"
#include "widgets/ui_text.h"
#include "widgets/ui_progress.h"
#include "widgets/ui_image.h"
#include "ui_parser.h"

#include "render/render_utils.h"

namespace UI
{
	void CWidget::start()
	{
		for (auto fx : _effects)
		{
			fx->start();
		}

		for (auto& child : _children)
		{
			child->start();
		}

		init();
  }

  void CWidget::stop()
  {
    for (auto fx : _effects)
    {
      fx->stop();
    }

    for (auto& child : _children)
    {
      child->stop();
    }
  }

  void CWidget::update(float dt)
  {
	  if (_aliveClock.isFinished())
		  _params.visible = false;
	  
	updateTransform(); 
    for (auto fx : _effects)
    {
      fx->update(dt);
    }

    for (auto& child : _children)
    {
      child->update(dt);
    }
  }

  void CWidget::doRender()
  {
	if (!_params.visible)
	{
		return;
	}

	render();
	bool renderedTFWidget = needsToResetRenderConfig();

	for (auto& child : _children)
	{
		if (!renderedTFWidget && child->needsToResetRenderConfig())
			renderedTFWidget = true;
		else if (renderedTFWidget) {
			activateAllSamplers();
			renderedTFWidget = false;
		}
		child->doRender();
	}

	if (renderedTFWidget)
		activateAllSamplers();
  }

  void CWidget::updateTransform()
  {
    computeAbsolute();

    for (auto& child : _children)
    {
      child->updateTransform();
    }
  }

  void CWidget::computePivot()
  {
    _pivot = MAT44::Identity * MAT44::CreateTranslation(-_params.pivot.x, -_params.pivot.y, 0.f);
  }

  void CWidget::computeLocal()
  {
    computePivot();

    MAT44 tr = MAT44::CreateTranslation(_params.position.x, _params.position.y, 0.f);
    MAT44 sc = MAT44::CreateScale(_params.scale.x, _params.scale.y, 0.f);
    MAT44 rot = MAT44::CreateRotationZ(_params.rotation);
    
    _local = rot * sc * tr;
  }

  void CWidget::computeAbsolute()
  {
    computeLocal();

    _absolute = _parent ? _local * _parent->_absolute : _local;
  }

  void CWidget::setParent(CWidget* parent)
  {
    removeFromParent();

    if (!parent)
    {
      return;
    }

    _parent = parent;
    _parent->_children.push_back(this);
  }

  void CWidget::removeFromParent()
  {
    if (!_parent)
    {
      return;
    }

    auto it = std::find(_parent->_children.begin(), _parent->_children.end(), this);
    assert(it != _parent->_children.end());
    _parent->_children.erase(it);
    _parent = nullptr;
  }

  void CWidget::addEffect(std::string effectType){

	  if (effectType.compare("movement") == 0) {
		  Utils::dbg("added effect movement");
		  CFXMovement* effect = new CFXMovement;
		  effect->_owner = this;
		  _effects.push_back(effect);
	  }
	  else if (effectType.compare("uv") == 0) {
		  Utils::dbg("added effect uv");
	  }
	  
  }

  void CWidget::addChild(std::string name, std::string type)
  {

	  if (type == "Button") {
		  CButton* child = new CButton;
		  child->_name = name;
		  child->setParent(this);
		  _children.push_back(child);
	  }
	  else if (type == "Image") {
		  CImage* child = new CImage;
		  child->_name = name;
		  child->setParent(this);
		  _children.push_back(child);
	  }
	  else if (type == "ProgressBar") {
		  CProgress* child = new CProgress;
		  child->_name = name;
		  child->setParent(this);
		  _children.push_back(child);
	  }
	  else if (type == "Text") {
		  CText* child = new CText;
		  child->_name = name;
		  child->setParent(this);
		  _children.push_back(child);
	  }

	  
  }

  void CWidget::renderInMenu() {

	  ImGuiWindowFlags flags = 0;//ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoScrollbar;
	  
	  if (ImGui::TreeNode(getName().c_str())) {

		  ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
		  ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.13f, 0.14f, 0.168f, 1.0f));
		  ImGui::BeginChild("Image Widget", ImVec2(0, 0), true, flags);

		  ImGui::Text(std::string(_name + "/" + _alias).c_str());
		  ImGui::Separator();
		  ImGui::Text("Params:");
		  ImGui::Text("");
		  ImGui::Text("");
		  ImGui::Text("Pivot: ");
		  ImGui::SameLine();
		  ImGui::PushItemWidth(100);
		  ImGui::DragFloat2("##Pivot", &_params.pivot.x, 0.01f, 0.0f, 2000.0f);
		  ImGui::PopItemWidth();
		  ImGui::SameLine();
		  ImGui::Text("Position: ");
		  ImGui::SameLine();
		  ImGui::PushItemWidth(100);
		  ImGui::DragFloat2("##Position", &_params.position.x, 1.0f, -100.0f, 2000.0f);
		  ImGui::PopItemWidth();
		  ImGui::SameLine();
		  ImGui::Text("Scale: ");
		  ImGui::SameLine();
		  ImGui::PushItemWidth(100);
		  ImGui::DragFloat2("##Scale", &_params.scale.x, 0.01f, -100.0f, 100.0f);
		  ImGui::PopItemWidth();
		  ImGui::Text("Rotation: ");
		  ImGui::SameLine();
		  ImGui::PushItemWidth(200);
		  ImGui::DragFloat("##Rotation", &_params.rotation, 0.01f, -1, 1.0f);
		  ImGui::PopItemWidth();
		  ImGui::SameLine();
		  ImGui::Text("Is Visible? ");
		  ImGui::SameLine();
		  ImGui::Checkbox("##Visible", &_params.visible);
		  
		  ImGui::Separator();

		  for (auto& child : getChildren()) {
			  child->renderInMenu();

			  if (ImGui::Button("Delete")) {
				  removeFromParent();
			  }

		  }

		ImGui::EndChild();
		ImGui::PopStyleColor();
		ImGui::PopStyleVar();
		ImGui::TreePop();
	  }
  }


  void CWidget::startClock(float time, bool killWidget)
  {
	  _aliveClock = CClock(time);
	  _aliveClock.start();
	  _killAfterClockFinish = true;
  }

}
