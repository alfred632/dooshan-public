#pragma once

#include "ui/ui_widget.h"
#include "ui/animations/ui_text_animation.h"

namespace UI
{
	class CTextFont : public CWidget
	{
	public:

		CTextFont() {
			origTransform = _textParams.position;
			origScale = _textParams.scale;
			origColor = _textParams.textColor;
			_resetRenderConfig = true;
		}
		void loadAnimations(json animData);
		void render() override;
		void update(float dt) override;
		void renderInMenu() override;
		void playAnimation(std::string name, bool loop);
		void stopAnimation(std::string name);
		TTextFontParams* getTextParams() { return &_textParams; }
		void setTextParams(TTextFontParams params);

	private:
		VEC2 origTransform, origScale;
		VEC4 origColor;

		VEC2 lastTransform, lastScale;
		VEC4 lastColor;

		TTextFontParams _textParams;
		std::vector<CTextAnimation*> animations;
		CTextAnimation* currentAnimation;
		friend class CParser;
		char editorText[30] = "";
	};

	
}
