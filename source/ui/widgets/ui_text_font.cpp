#pragma once

#include "mcv_platform.h"
#include "ui/widgets/ui_text_font.h"
#include "ui/ui_utils.h"

namespace UI
{
	void CTextFont::loadAnimations(json animData)
	{
		const std::string	name;
		bool				isLooping;
		float				animationTime;
		CTextAnimation* new_anim = new CTextAnimation();

		const std::string jName = animData.value("name", name);
		const bool jIsLooping = animData.value("IsLooping", isLooping);
		const float jAnimTime = animData.value("animation_time", animationTime);

		new_anim->configure(jName, jAnimTime, jIsLooping);

		if (animData.count("transform_nodes") > 0) {
			for (const auto& node : animData["transform_nodes"]) {
				new_anim->addTransformNode(loadVector2(node, "value"), node["time"]);
			}
		}

		if (animData.count("scale_nodes") > 0) {
			for (const auto& node : animData["scale_nodes"]) {
				new_anim->addScaleNode(loadVector2(node, "value"), node["time"]);
			}
		}

		if (animData.count("color_nodes") > 0) {
			for (const auto& node : animData["color_nodes"]) {
				new_anim->addColorNode(loadVector4(node, "value"), node["time"]);
			}
		}

		//Interpolators
		new_anim->setTransformInterpolator(animData.value("transform_interpolator", "cubic_in"));
		new_anim->setScaleInterpolator(animData.value("scale_interpolator", "cubic_in"));
		new_anim->setColorInterpolator(animData.value("color_interpolator", "cubic_in"));
		new_anim->setSpecialInterpolator(animData.value("special_interpolator", "cubic_in"));

		animations.push_back(new_anim);
	}

	void CTextFont::render()
	{
		VEC2 position;
		if (_parent) {
			position = _parent->getParams()->position + _textParams.position;
		}
		else {
			position = _textParams.position;
		}

		if (_textParams.hasOutline)
			renderFontTextWithOutline(_textParams.text, position, _textParams.textColor, _textParams.effectColor, _textParams.scale, _textParams.rotation, _textParams.fontName);
		else if (_textParams.hasDropShadow)
			renderFontTextWithDropShadow(_textParams.text, position, _textParams.textColor, _textParams.effectColor, _textParams.scale, _textParams.rotation, _textParams.fontName);
		else
			renderFontText(_textParams.text, position, _textParams.textColor, _textParams.scale, _textParams.rotation, _textParams.fontName);
	}

	void CTextFont::update(float dt)
	{
		for (auto& anim : animations) {
			anim->update(dt);
			if (anim->isRunning()) {
				if (anim->getNumTransformNodes() > 0) {
					lastTransform = origTransform + anim->getTransformKeyframe(origTransform).vec2;
					_textParams.position = lastTransform;
				}
				if (anim->getNumScaleNodes() > 0) {
					lastScale = anim->getScaleKeyframe(origScale).vec2;
					_textParams.scale = lastScale;
				}
				if (anim->getNumColorNodes() > 0) {
					lastColor = anim->getColorKeyframe(origColor).vec4;
					_textParams.textColor.f[0] = lastColor.x;
					_textParams.textColor.f[1] = lastColor.y;
					_textParams.textColor.f[2] = lastColor.z;
					_textParams.textColor.f[3] = lastColor.w;
				}
			}
		}

		_textParams.currentTime += dt;
	}

	void CTextFont::renderInMenu()
	{

		if (ImGui::TreeNode(getName().c_str())) {

			ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
			ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.13f, 0.14f, 0.168f, 1.0f));
			ImGui::BeginChild("Text Widget", ImVec2(0, 0), true, 0);

			ImGui::Text(std::string(_name + "/" + _alias).c_str());
			ImGui::Separator();
			ImGui::Text("Params:");
			ImGui::Text("");
			ImGui::Text("");
			ImGui::Text("Is Visible? ");
			ImGui::SameLine();
			ImGui::Checkbox("##Visible", &_params.visible);

			MENUTEXT("");


			ImGui::Text("Text Params: ");
			ImGui::SameLine();
			std::wcstombs(editorText, _textParams.text.c_str(), _textParams.text.size());
			ImGui::InputText("##TextIT", editorText, 30);
			std::string stdNewText(editorText);
			_textParams.text = std::wstring(stdNewText.begin(), stdNewText.end());

			ImGui::Text("Position: ");
			ImGui::SameLine();
			ImGui::DragFloat2("##PositionDF", &_textParams.position.x, 0.1f, -100.0f, 2000.f);

			ImGui::Text("Font Name: %s", _textParams.fontName);

			ImGui::Text("Scale: ");
			ImGui::SameLine();
			ImGui::DragFloat2("##ScaleDF", &_textParams.scale.x, 0.01f, 0.0f, 2000.f);

			ImGui::Text("Rotation: ");
			ImGui::SameLine();
			ImGui::DragFloat("##RotationDF", &_textParams.rotation, 0.01f, -360.0f, 360.f);

			ImGui::Text("Color: ");
			ImGui::SameLine();
			ImGui::DragFloat4("##ColorDF", &_textParams.textColor.f[0], 0.01f, 0.0f, 1.f);

			ImGui::Text("Effect Color: ");
			ImGui::SameLine();
			ImGui::DragFloat4("##EffectColorDF", &_textParams.effectColor.f[0], 0.01f, 0.0f, 1.f);

			ImGui::Text("Outline : ");
			ImGui::SameLine();
			ImGui::Checkbox("##OutlineCB", &_textParams.hasOutline);

			ImGui::Text("Drop Shadow : ");
			ImGui::SameLine();
			ImGui::Checkbox("##ShadowCB", &_textParams.hasDropShadow);

				/* Animation Editor */
			if (animations.size() > 0) {
				static bool p_open;
				static int selected = 0;
				ImGui::SetNextWindowSize(ImVec2(700, 300), ImGuiCond_FirstUseEver);
				if (ImGui::Begin("Animation Editor", &p_open))
				{
					// left
					std::string leftId = "left pane" + _name;
					ImGui::BeginChild(leftId.c_str(), ImVec2(150, 0), true);
					for (int i = 0; i < animations.size(); i++)
					{
						char label[128];
						sprintf(label, animations[i]->getName().c_str(), i);
						if (ImGui::Selectable(label, selected == i))
							selected = i;
					}
					ImGui::EndChild();
					ImGui::SameLine();
					// right
					ImGui::BeginGroup();
					std::string leftViewId = "item view" + _name;
					ImGui::BeginChild(leftViewId.c_str(), ImVec2(0, -ImGui::GetFrameHeightWithSpacing())); // Leave room for 1 line below us
					ImGui::Text(animations[selected]->getName().c_str(), selected);
					ImGui::Separator();
					std::string childId = "Child1" + _name;
					ImGui::BeginChild(childId.c_str(), ImVec2(ImGui::GetWindowContentRegionWidth(), 0), true);
					animations[selected]->renderTimeline();
					ImGui::EndChild();
					ImGui::EndChild();
					ImGui::EndGroup();


					ImGui::End();
				}

				// Media controls
				std::string mediaId = "left media control" + _name;
				bool mediaWndOpen = true;
				if (ImGui::Begin(mediaId.c_str(), &mediaWndOpen, ImVec2(150, 50))) {
					animations[selected]->displayMediaControls();
					ImGui::End();
				}
			}

			ImGui::EndChild();
			ImGui::PopStyleColor();
			ImGui::PopStyleVar();
			ImGui::TreePop();
		}
	}


	void CTextFont::playAnimation(std::string name, bool loop)
	{
		for (auto& anim : animations) {
			if (anim->getName() == name) {
				currentAnimation = anim;
				currentAnimation->startAnimation(loop);
			}
		}

		assert(currentAnimation != nullptr && "Animation %s not found", name);

	}

	void CTextFont::stopAnimation(std::string name)
	{
		currentAnimation->stopAnimation();

		_textParams.position = origTransform;
		_textParams.scale = origScale;
		_textParams.textColor.f[0] = origColor.x;
		_textParams.textColor.f[1] = origColor.y;
		_textParams.textColor.f[2] = origColor.z;
		_textParams.textColor.f[3] = origColor.w;
	}

	

	void CTextFont::setTextParams(TTextFontParams params)
	{
		_textParams = params;
		origTransform = params.position;
		origScale = params.scale;
		origColor = params.textColor;
	}

}
