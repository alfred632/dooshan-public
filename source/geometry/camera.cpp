#include "mcv_platform.h"
#include "camera.h"

#include <limits> // Used for FLOATMAX FLOAT MIN


#include "geometry/Miniball.hpp"

float CCamera::cascade_start_percentage = 0.0f;
float CCamera::cascade_max_percentage = 0.10f; // 0.1 * zFar, which in our case yields 100 meters of shadows.
float CCamera::perc_c1 = 0.1f;
float CCamera::perc_c2 = 0.22f;
float CCamera::perc_c3 = 1.0f;

float CCamera::cascade_ends[NUM_CASCADES_SHADOW_MAP];

CCamera::CCamera() {
	projection = Matrix::CreatePerspectiveFieldOfView(fov_radians, aspect_ratio, z_near, z_far);
	lookAt(VEC3(1, 1, 1), VEC3(0, 0, 0));
	cascade_fov = fov_radians;
}

CCamera::CCamera(const VEC3 & new_eye, const VEC3 & new_target,
	const VEC3 & new_up_aux, float nFov_radians, float nAspect_ratio,
	float nZ_near, float nZ_far)
	: eye(new_eye), target(new_target), up(new_up_aux),
	fov_radians(nFov_radians), cascade_fov(nFov_radians), aspect_ratio(nAspect_ratio),
	z_near(nZ_near), z_far(nZ_far)
{
	projection = Matrix::CreatePerspectiveFieldOfView(fov_radians, aspect_ratio, z_near, z_far);
	lookAt(eye, target, up_aux);
}

void CCamera::updateViewProjection() {
	view_projection = view * projection;
}

void CCamera::updateProjection() {
	projection = Matrix::CreatePerspectiveFieldOfView(fov_radians, aspect_ratio, z_near, z_far);
	updateViewProjection();
	setCascadeFrustaZ();
}

void CCamera::lookAt(VEC3 new_eye, VEC3 new_target, VEC3 new_up_aux) {
	view = Matrix::CreateLookAt(new_eye, new_target, new_up_aux);
	
	eye = new_eye;
	target = new_target;
	up_aux = new_up_aux;
	
	// Regenerate 3 main axis
	front = (target - eye);
	front.Normalize();
	left = new_up_aux.Cross(front);
	left.Normalize();
	up = front.Cross(left);
	
	updateViewProjection();

	// CSM variables must be recalculated next frame.
	setCSMVariablesDirty();
}

void CCamera::setProjectionParams(float new_fov_radians, float new_z_near, float new_z_far) {
	//aspect_ratio = Render.getAspectRatio();
	is_ortho = false;
	fov_radians = new_fov_radians;
	z_near = new_z_near;
	z_far = new_z_far;
	updateProjection();

	// CSM variables must be recalculated next frame.
	setCSMVariablesDirty();
}

void CCamera::setOrthoParams(bool is_centered, float new_left, float new_width, float new_top, float new_height, float new_z_near, float new_z_far) {
	is_ortho = true;
	ortho_centered = is_centered;
	ortho_width = new_width;
	ortho_height = new_height;
	ortho_left = new_left;
	ortho_top = new_top;

	aspect_ratio = fabsf(ortho_width / ortho_height);
	z_far = new_z_far;
	z_near = new_z_near;

	if (is_centered)
		projection = MAT44::CreateOrthographic(ortho_width, ortho_height, z_near, z_far);
	else
		projection = MAT44::CreateOrthographicOffCenter(ortho_left, ortho_width, ortho_height, ortho_top, z_near, z_far);

	viewport.x0 = 0;
	viewport.y0 = 0;
	viewport.width = (int)ortho_width;
	viewport.height = (int)ortho_height;
	aspect_ratio = (float)ortho_width / (float)ortho_height;

	// Position and target and unset!
	//eye = VEC3(ortho_width * 0.5f, ortho_height * 0.5f, z_near);
	//front.x = 0;
	//front.y = 0;
	//front.z = 1;
	//left.x = 1;
	//left.y = 0;
	//left.z = 0;
	//up.x = 0;
	//up.y = 1;
	//up.z = 0;
	//view = MAT44::Identity;
	updateViewProjection();

	setCSMVariablesDirty();
}

void CCamera::setUIProjection(bool is_centered, float new_left, float new_width, float new_top, float new_height, float new_z_near, float new_z_far) {

	// Position and target and unset!
	eye = VEC3(new_width * 0.5f, new_height * 0.5f, new_z_near);
	front.x = 0;
	front.y = 0;
	front.z = 1;
	left.x = 1;
	left.y = 0;
	left.z = 0;
	up.x = 0;
	up.y = 1;
	up.z = 0;
	view = MAT44::Identity;

	setOrthoParams(is_centered, new_left, new_width, new_top, new_height, new_z_near, new_z_far);
}

void CCamera::setAspectRatio(float new_aspect_ratio) {
	aspect_ratio = new_aspect_ratio;
	updateProjection();

	// CSM variables must be recalculated next frame.
	setCSMVariablesDirty();
}

void CCamera::setViewport(int x0, int y0, int width, int height) {
  // save params
  viewport.x0 = x0;
  viewport.y0 = y0;
  viewport.width = width;
  viewport.height = height;

  aspect_ratio = (float)width / (float)height;

  if (!is_ortho)
    setProjectionParams(fov_radians, z_near, z_far);
}

void CCamera::activateViewport() {
	// Setup the viewport
	D3D11_VIEWPORT vp;
	ZeroMemory(&vp, sizeof(vp));
	vp.Width = viewport.width;
	vp.Height = viewport.height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = viewport.x0;
	vp.TopLeftY = viewport.y0;
	Render.setViewport(vp);
}

bool CCamera::getScreenCoordsOfWorldCoord(VEC3 world_pos, VEC3* result) const {

  // It's also dividing by w  -> [-1..1]
  VEC3 pos_in_homo_space = VEC3::Transform(world_pos, getViewProjection());

  // Convert to 0..1 and then to viewport coordinates
  VEC3 pos_in_screen_space(
    viewport.x0 + (pos_in_homo_space.x + 1.0f) * 0.5f * viewport.width,
    viewport.y0 + (1.0f - pos_in_homo_space.y) * 0.5f * viewport.height,
    pos_in_homo_space.z
  );

  assert(result);
  *result = pos_in_screen_space;

  // Return true if the coord is inside the frustum
  return pos_in_homo_space.x >= -1.0f && pos_in_homo_space.x <= 1.0f
    && pos_in_homo_space.y >= -1.0f && pos_in_homo_space.y <= 1.0f
    && pos_in_homo_space.z >= 0.0f && pos_in_homo_space.z <= 1.0f
    ;
}

void CCamera::setCascadeFrustaZ() {
	cascade_ends[0] = cascade_start_percentage + perc_c1 * cascade_max_percentage;
	cascade_ends[1] = cascade_start_percentage + perc_c2 * cascade_max_percentage;
	cascade_ends[2] = cascade_start_percentage + perc_c3 * cascade_max_percentage;

	// calculate cascade distances from camera.
	for (unsigned int i = 0; i < NUM_CASCADES_SHADOW_MAP; ++i) {
		float prevSplitDist = i == 0 ? 0.0 : cascade_ends[i - 1];
		float splitDist = cascade_ends[i];
		// Get the z point in projective space.
		const float clipDist = z_far - z_near;
		cascade_ends_clip_space[i] = z_near + splitDist * clipDist;
	}
}

// Returns the 8 corners of the complete frustum used in CSM.
const Vector3 * CCamera::calculateCSMFrustumCorners() {
	if (updated_complete_frustum_corners) return cascade_complete_frustum_corners;

	float fov = fov_radians;

	// Set the projection of the camera to the one with the cascade fov.
	// We compute our shadows with a defined cascade fov to avoid shimmering
	// when changing fovs.
	if (!is_ortho) setProjectionParams(cascade_fov, z_near, z_far);

	// Get the 8 points of the view frustum in world space
	Vector3 frustumCorners[8] =
	{
			Vector3(-1.0f,  1.0f, 0.0f),
			Vector3(1.0f,  1.0f, 0.0f),
			Vector3(1.0f, -1.0f, 0.0f),
			Vector3(-1.0f, -1.0f, 0.0f),
			Vector3(-1.0f,  1.0f, 1.0f),
			Vector3(1.0f,  1.0f, 1.0f),
			Vector3(1.0f, -1.0f, 1.0f),
			Vector3(-1.0f, -1.0f, 1.0f),
	};

	Matrix invViewProj = getViewProjection().Invert();
	for (unsigned int i = 0; i < 8; ++i)
		cascade_complete_frustum_corners[i] = Vector3::Transform(frustumCorners[i], invViewProj);

	// Set the projection of the camera to the normal fov.
	if (!is_ortho)
		setProjectionParams(fov, z_near, z_far);

	updated_complete_frustum_corners = true;

	return cascade_complete_frustum_corners;
}

// Returns the corners (8 * NumOfCascades) for each of the frustas of the CSM.
// We don't call this method in the CSM but it can be useful for debugging.
const Vector3 * CCamera::calculateCSMFrustasCorners()
{
	//setAspectRatio(1.77777779);
	//z_near = 0.250000000;
	//z_far = 250.000000;
	//fov_radians = 0.589048624;
	//cascade_fov = fov_radians;
	//eye = Vector3(40.0000000, 5.00000000, 5.00000000);
	//lookAt(eye, eye + front, up_aux);
	//setProjectionParams(fov_radians, z_near, z_far);

	if (updated_frustas_corners) return cascade_frustas_corners;

	// Set the projection of the camera to the one with the cascade fov.
	// We compute our shadows with a defined cascade fov to avoid shimmering
	// when changing fovs.
	float fov = fov_radians;
	if (!is_ortho)
		setProjectionParams(cascade_fov, z_near, z_far);

	for (unsigned int i = 0; i < NUM_CASCADES_SHADOW_MAP; ++i) {
		float prevSplitDist = i == 0 ? 0.0 : cascade_ends[i - 1];
		float splitDist = cascade_ends[i];
		calculateFrustumCorners(&cascade_frustas_corners[i * 8], prevSplitDist, splitDist);
	}

	// Set the projection of the camera to the normal fov.
	if (!is_ortho)
		setProjectionParams(fov, z_near, z_far);

	updated_frustas_corners = true;

	return cascade_frustas_corners;
}

const float * CCamera::calculateCSMBSRadiusAndCenter(Vector3 * bsCenters) {
	// We have to calculate this two times. There seems to be a bug with the camera (I think)
	// that makes the radius between points of the frusta change based on the angle of it, which shouldn't happen.
	// I didn't found a way to fix it so I did this instead. May try changing in the future.
	
	if(!updated_sphere_radius)
		calculateBSRadius();
	
	calculateBSCenter(bsCenters);

	return sphere_radius_frustas;
}

void CCamera::calculateBSRadius() {
	Vector3 nEye = eye;
	Vector3 nTarget = target;
	Vector3 nUp = up;
	float fov = fov_radians;

	// We set a fake look at matrix. This is due to the corners method returning different radius depending on how we move the camera.
	// Seems like a precision issue, we might try finding how to fix it in the future. With low fovs the ceiling makes it disappears
	// but with our fov it doesn't work. We use this method instead to calculate the radius and store it as constant.
	lookAt(Vector3(0, 0, 0), Vector3(0, 0, 1), Vector3::Up);
	// Also set a fake projection matrix with the fov we will use for the cascade which is static compared to the one from the camera.
	if (!is_ortho)
		setProjectionParams(cascade_fov, z_near, z_far);

	Vector3 cascade_frustas_corners_sphere[NUM_FRUSTUM_CORNERS];
	for (unsigned int cascadeIdx = 0; cascadeIdx < NUM_CASCADES_SHADOW_MAP; cascadeIdx++) {
		float prevSplitDist = cascadeIdx == 0 ? 0.0 : cascade_ends[cascadeIdx - 1];
		float splitDist = cascade_ends[cascadeIdx];

		// Get the corners of the frusta.
		calculateFrustumCorners(&cascade_frustas_corners_sphere[cascadeIdx * 8], prevSplitDist, splitDist);

		// Pass points as list to get the radius and center.
		std::list<std::vector<float> > lp;
		for (int i = 0; i < 8; ++i) {
			std::vector<float> p(3);
			for (int j = 0; j < 3; ++j)
				p[j] = *((&cascade_frustas_corners_sphere[cascadeIdx * 8 + i].x) + j);
			lp.push_back(p);
		}

		// define the types of iterators through the points and their coordinates
		// ----------------------------------------------------------------------
		typedef std::list<std::vector<float> >::const_iterator PointIterator;
		typedef std::vector<float>::const_iterator CoordIterator;

		// create an instance of Miniball to calculate the center and radius.
		typedef Miniball::Miniball <Miniball::CoordAccessor<PointIterator, CoordIterator> > MB;
		MB mb(3, lp.begin(), lp.end());
		sphere_radius_frustas[cascadeIdx] = std::sqrt(mb.squared_radius());
	}

	// Return to the correct view matrix.
	lookAt(nEye, nTarget, nUp);
	// Also set a fake projection matrix.
	if (!is_ortho)
		setProjectionParams(fov, z_near, z_far);

	updated_sphere_radius = true;
}

void CCamera::calculateBSCenter(Vector3 * bsCenters) {
	Vector3 cascade_frustas_corners_sphere[NUM_FRUSTUM_CORNERS];
	for (unsigned int cascadeIdx = 0; cascadeIdx < NUM_CASCADES_SHADOW_MAP; cascadeIdx++) {
		float prevSplitDist = cascadeIdx == 0 ? 0.0 : cascade_ends[cascadeIdx - 1];
		float splitDist = cascade_ends[cascadeIdx];

		// Get the corners of the frusta.
		calculateFrustumCorners(&cascade_frustas_corners_sphere[cascadeIdx * 8], prevSplitDist, splitDist);

		// Pass points as list to get the radius and center.
		std::list<std::vector<float> > lp;
		for (int i = 0; i < 8; ++i) {
			std::vector<float> p(3);
			for (int j = 0; j < 3; ++j)
				p[j] = *((&cascade_frustas_corners_sphere[cascadeIdx * 8 + i].x) + j);
			lp.push_back(p);
		}

		// define the types of iterators through the points and their coordinates
		// ----------------------------------------------------------------------
		typedef std::list<std::vector<float> >::const_iterator PointIterator;
		typedef std::vector<float>::const_iterator CoordIterator;

		// create an instance of Miniball to calculate the center and radius.
		typedef Miniball::Miniball <Miniball::CoordAccessor<PointIterator, CoordIterator> > MB;
		MB mb(3, lp.begin(), lp.end());

		// center
		Vector3 c;
		const float * cent = mb.center();
		c.x = cent[0];
		c.y = cent[1];
		c.z = cent[2];
		bsCenters[cascadeIdx] = c;
	}
}

void CCamera::calculateFrustumCorners(Vector3 * results, float z_near, float z_far) {
	// Get the 8 points of the view frustum in world space
	Vector3 frustumCornersWS[8] =
	{
			Vector3(-1.0f,  1.0f, 0.0f),
			Vector3(1.0f,  1.0f, 0.0f),
			Vector3(1.0f, -1.0f, 0.0f),
			Vector3(-1.0f, -1.0f, 0.0f),
			Vector3(-1.0f,  1.0f, 1.0f),
			Vector3(1.0f,  1.0f, 1.0f),
			Vector3(1.0f, -1.0f, 1.0f),
			Vector3(-1.0f, -1.0f, 1.0f),
	};

	Matrix invViewProj = view_projection.Invert();
	for (unsigned int j = 0; j < 8; ++j)
		results[j] = Vector3::Transform(frustumCornersWS[j], invViewProj);

	// Get the corners of the current cascade slice of the view frustum
	for (unsigned int j = 0; j < 4; ++j)
	{
		Vector3 cornerRay = results[j + 4] - results[j];
		Vector3 nearCornerRay = cornerRay * z_near;
		Vector3 farCornerRay = cornerRay * z_far;
		results[j + 4] = results[j] + farCornerRay;
		results[j] = results[j] + nearCornerRay;
	}
}

void CCamera::setCSMVariablesDirty()
{
	updated_complete_frustum_corners = updated_frustas_corners = updated_sphere_radius = false;
}

const float * CCamera::calculateCSMFrustaBSRadius() {
	if (updated_sphere_radius) return sphere_radius_frustas;

	Vector3 nEye = eye;
	Vector3 nTarget = target;
	Vector3 nUp = up;
	float fov = fov_radians;

	// We set a fake look at matrix. This is due to the corners method returning different radius depending on how we move the camera.
	// Seems like a precision issue, we might try finding how to fix it in the future. With low fovs the ceiling makes it disappears
	// but with our fov it doesn't work. We use this method instead to calculate the radius and store it as constant.
	lookAt(Vector3(0, 0, 0), Vector3(0, 0, 1), Vector3::Up);
	// Also set a fake projection matrix with the fov we will use for the cascade which is static compared to the one from the camera.
	if (!is_ortho)
		setProjectionParams(cascade_fov, z_near, z_far);

	Vector3 cascade_frustas_corners_sphere[NUM_FRUSTUM_CORNERS];
	for (unsigned int cascadeIdx = 0; cascadeIdx < NUM_CASCADES_SHADOW_MAP; cascadeIdx++) {
		float prevSplitDist = cascadeIdx == 0 ? 0.0 : cascade_ends[cascadeIdx - 1];
		float splitDist = cascade_ends[cascadeIdx];
		calculateFrustumCorners(&cascade_frustas_corners_sphere[cascadeIdx * 8], prevSplitDist, splitDist);

		// Calculate the centroid of the view frustum slice
		Vector3 frustumCenter;
		for (unsigned int j = 0; j < 8; ++j)
			frustumCenter = frustumCenter + cascade_frustas_corners_sphere[cascadeIdx * 8 + j];
		frustumCenter *= 1.0f / 8.0f;

		// Calculate the radius of a bounding sphere surrounding the frustum corners. This bounding sphere
		// will become the AABB of our projection. While we lose some resolution due to the bigger coverage of an sphere
		// the frustums of the camera will remain stable when it moves and rotates and our shadows won't shimmer.
		// The method can be found in the book ShaderX6 or the following websites:
		float sphereRadius = 0.0f;
		for (unsigned int j = 0; j < 8; ++j)
		{
			float dist = (cascade_frustas_corners_sphere[cascadeIdx * 8 + j] - frustumCenter).Length();
			sphereRadius = std::max(sphereRadius, dist);
		}
		sphere_radius_frustas[cascadeIdx] = std::ceil(sphereRadius * 16.0f) / 16.0f;
	}

	// Return to the correct view matrix.
	lookAt(nEye, nTarget, nUp);
	// Also set a fake projection matrix.
	if (!is_ortho)
		setProjectionParams(fov, z_near, z_far);

	updated_sphere_radius = true;

	return sphere_radius_frustas;
}