#pragma once

#include "geometry.h"

VEC3  yawToVector(float yaw);
float vectorToYaw(const VEC3 & front);
VEC3  yawPitchToVector(float yaw, float pitch);
void vectorToYawPitch(const VEC3 & front, float & yaw, float & pitch);
