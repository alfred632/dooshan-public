#include "mcv_platform.h"
#include "app.h"
#include "render/render.h"
#include "windows/app.h"
#include "render/primitives.h"
#include "render/module_render.h"
#include "engine.h"

void CApplication::executeApp() {
	// Update
	float elapsed = time_since_last_render.elapsedAndReset();
	Time.set(Time.current + elapsed);
	CEngine::instance().executeFrame();
}

// the entry point for any Windows program
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow) {

	PROFILE_SET_NFRAMES(3);
  	PROFILE_FRAME_BEGINS();

	Remotery* rmt;
	rmt_CreateGlobalInstance(&rmt);

	json cfg = Utils::loadJson("data/config.json");
	bool fullscreen = cfg.value("fullscreen", false);
	int windowsWidth = 1920, windowsHeight = 1080;
	int renderWidth = 1920, renderHeight = 1080;

	if (fullscreen) {
		if (cfg.count("fullscreen-app-windows") > 0) {
			const json & cfg_app_windows = cfg["fullscreen-app-windows"];
			windowsWidth = cfg_app_windows.value("width", windowsWidth);
			windowsHeight = cfg_app_windows.value("height", windowsHeight);
		}
		if (cfg.count("fullscreen-render") > 0) {
			const json & cfg_render = cfg["fullscreen-render"];
			renderWidth = cfg_render.value("width", renderWidth);
			renderHeight = cfg_render.value("height", renderHeight);
		}
	}
	else {
		if (cfg.count("app-windows") > 0) {
			const json & cfg_app_windows = cfg["app-windows"];
			windowsWidth = cfg_app_windows.value("width", windowsWidth);
			windowsHeight = cfg_app_windows.value("height", windowsHeight);
		}
		if (cfg.count("render") > 0) {
			const json & cfg_render = cfg["render"];
			renderWidth = cfg_render.value("width", renderWidth);
			renderHeight = cfg_render.value("height", renderHeight);
		}
	}
	
	if (cfg.count("max_anisotropy") > 0)
		setMaxAnisotropy(cfg["max_anisotropy"]);

	// Start application.
	CApplication & app = CApplication::instance();
	if (!app.init(hInstance, nCmdShow, "Dooshan", "Dooshan", windowsWidth, windowsHeight, fullscreen))
		return false;

	// Start render.
	if (!Render.init(app.getWindowHandler(), renderWidth, renderHeight))
		return false;

	// Start engine.
	CEngine::instance().init();

	app.runMainLoop();

	// Before finishing, free all memory.

	/* Constant buffers. */
	ctes_object.destroy();
	ctes_camera.destroy();

	/* Render. */
	Render.close();

	/* Remotery. */
	rmt_DestroyGlobalInstance(rmt);

	/* Engine. */
	CEngine::instance().close();

	/* App. */
	app.close(hInstance, "Dooshan");

	return true;
}