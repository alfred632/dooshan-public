#pragma once

#include "modules/module.h"
#include "entity/entity.h"


#include "geometry/curve.h"

/* Module editor manages all the information necessary to load and use the editor for placing prefabs in the scene.
It's render in menu methods uses ImGui to provide an in-game editor.
Right now the editor provides functionality for:

* Placing assets.
* Editing particles. 
*/

class CModuleEditor : public IModule
{
public:
	typedef std::string CategoryName;
	typedef std::string PrefabName;
	typedef std::string PrefabPath;
	typedef std::vector<std::pair<PrefabName, PrefabPath>> PrefabVector;

private:
	std::map < CategoryName, PrefabVector > prefabs_by_category; // Holds a vector of prefabs by category.
	std::vector<std::string> categories_list_vector; // Holds all categories in the map above in a vector, used for ImGui.


	struct SpawnedAsset {
		CHandle handleGroupEntity;
		std::string prefabPath;
	};
	std::vector< SpawnedAsset > spawnedAssets; // Holds all spawned assets, used to know that must be exported in a file.

	/* Prefab editor. */

	// Load functions.
	void fetchAllPrefabDirectories(std::vector<std::string> & save_directories); // Fetches all directories inside the data/prefabs/.
	void fetchAllPrefabCategories(std::vector<std::string> & directories); // Fetches all categories from the data/prefabs. Folders and subfolders are used as categories.
	void fetchAllPrefabsInsideCategories(); // Getches all prefabs inside a particular category, stores the data in the std::map.

	// Used for adding a prefab category.
	bool addPrefabCategory(CategoryName & category);

	// Renders the scene editor. 
	void renderSceneEditor();

	// Spawns the selected asset.
	void spawnSelectedAsset(const std::string & assetPath, const CTransform & transform, const char * entityNameBuffer);

	// Render assets to export windows.
	void renderAssetsToExportWindow();

	// Export entities to file.
	void exportEntitiesToFile(const std::string & exportedFileName);

	/* Curve editor. */
	int currentKnotEditing = 0;
	CTransform knotToPlace = CTransform();
	CTransformCurve editorCurve;

	// Renders the curve editor
	void renderCurveEditor();

	void renderCreateCurve();

	void saveCurveToJson(const std::string & curveName);

public:
	CModuleEditor(const std::string & name);

	bool start() override;

	void update(float dt) override {}
	
	void renderDebug() override;
	
	void renderInMenu() override;
};