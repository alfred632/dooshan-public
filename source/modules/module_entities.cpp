#include "mcv_platform.h"
#include "module_entities.h"
#include "entity/entity.h"
#include "components/common/comp_tags.h"
#include "render/render_manager.h"

#include "imgui/imgui_manager.h"
#include "logic/logic_manager.h"

void CModuleEntities::loadListOfManagers( const json & j, std::vector< CHandleManager* > & managers) {
	managers.clear();
	
	// For each entry in j["update"] add entry to om_to_update
	std::vector< std::string > names = j;
	for (auto& n : names) {
		auto om = CHandleManager::getByName(n.c_str());
		assert(om || Utils::fatal("Can't find a manager of components of type %s to update. Check file components.json\n", n.c_str()));
		managers.push_back(om);
	}
}

void CModuleEntities::addComponentToRenderDebug(const std::string & componentName) {
	auto om = CHandleManager::getByName(componentName.c_str());
	if (!om) return;
	om_to_render_debug.push_back(om);
}

void CModuleEntities::removeComponentFromRenderDebug(const std::string & componentName) {
	for (auto & component = om_to_render_debug.begin(); component != om_to_render_debug.end(); component++)
	{
		if ((*component)->getName() == componentName) {
			om_to_render_debug.erase(component);
			break;
		}
	}
}

bool CModuleEntities::isComponentInRenderDebug(const std::string & componentName) {
	for (auto & component = om_to_render_debug.begin(); component != om_to_render_debug.end(); component++)
		if ((*component)->getName() == componentName)
			return true;
	return false;
}

bool CModuleEntities::start()
{
	json j = Utils::loadJson("data/components.json");
  
	// Initialize the ObjManager preregistered in their constructors
	// with the amount of components defined in the data/components.json
	std::map< std::string, int > comp_sizes = j["sizes"];;
	int default_size = comp_sizes["default"];

	// Reorder the init manager based on the json
	// The bigger the number in the init_order section, the lower comp_type id you get
	std::map< std::string, int > init_order = j["init_order"];;
	std::sort( CHandleManager::predefined_managers,
		CHandleManager::predefined_managers + CHandleManager::npredefined_managers,
		[&init_order](CHandleManager* m1, CHandleManager* m2) {
			int priority_m1 = init_order[m1->getName()];
			int priority_m2 = init_order[m2->getName()];
			return priority_m1 > priority_m2;
	});
	// Important that the entity is the first one for the chain destruction of components
	assert(strcmp(CHandleManager::predefined_managers[0]->getName(), "entity") == 0);

	// Now with the sorted array
	for (size_t i = 0; i < CHandleManager::npredefined_managers; ++i) {
		auto om = CHandleManager::predefined_managers[i];
		auto it = comp_sizes.find(om->getName());
		int sz = (it == comp_sizes.end()) ? default_size : it->second;
		Utils::dbg("Initializing obj manager %s with %d\n", om->getName(), sz);
		om->init(sz, false);
	}

	loadListOfManagers(j["update"], om_to_update);
	loadListOfManagers(j["render_debug"], om_to_render_debug);

	std::vector< std::string > names = j["multithread"];
	for (auto& n : names) {
		auto om = CHandleManager::getByName(n.c_str());
		assert(om || Utils::fatal("Can't find a manager of components of type %s to update. Check file components.json\n", n.c_str()));
		om->multithreaded = true;
	}

	return true;
}

void CModuleEntities::update(float delta) {

  for (auto om : om_to_update) {
    PROFILE_FUNCTION(om->getName());
    om->updateAll(Time.delta);
  }
  checkHandlesTime(delta);
  CHandleManager::destroyAllPendingObjects();
}

void CModuleEntities::stop() {
	// Destroy all entities, should destroy all components in chain
  auto hm = getObjectManager<CEntity>();
  hm->forEach([](CEntity * e) {
    CHandle h(e);
    h.destroy();
  });
  CHandleManager::destroyAllPendingObjects();
}

void CModuleEntities::renderInMenu()
{
// We don't use this function directly, we call the other Menu
// functions from the imgui_manager.
}

void CModuleEntities::renderEntitiesMenu(){
	ImGui::PushID("Entities-Tab");

	if (ImGui::BeginTabBar("Entities - Sections", ImGuiTabBarFlags_None))
	{
		if (ImGui::BeginTabItem("Show Entities")) {
			static bool flat = false;
			ImGui::Checkbox("Show all entities: ", &flat);

			static ImGuiTextFilter entityName;
			entityName.Draw("Search for entities:");
			auto om = getObjectManager<CEntity>();
			om->forEach([](CEntity* e) {
				CHandle h_e(e);
				if (!flat && h_e.getOwner().isValid())
					return;
				if (entityName.IsActive() && !entityName.PassFilter(e->getName()))
					return;
				ImGui::PushID(e);
				e->debugInMenu();
				ImGui::PopID();
			});
			ImGui::EndTabItem();
		}

		if (ImGui::BeginTabItem("Show Entities by Tag"))
		{
			CTagsManager::get().renderInMenu();
			ImGui::EndTabItem();
		}

		if (ImGui::BeginTabItem("Logic Manager")) {
			LogicManager::Instance().renderInMenu();
			ImGui::EndTabItem();
		}

		ImGui::EndTabBar();
	}
	ImGui::PopID();
}

void CModuleEntities::renderComponentsMenu(){
	ImGui::PushID("Entities-Tab");
	if (ImGui::CollapsingHeader("All Components...")) {
		for (uint32_t i = 1; i < CHandleManager::getNumDefinedTypes(); ++i) {
			auto hm = CHandleManager::getByType(i);

			// Get the name of the component, instances, and allocated space.
			char buf[80];
			memset(buf, 0.0, sizeof(char) * 80);
			const std::string & comp_name = hm->getName();
			sprintf(buf, "%s [%d/%d (%dKb)]###OM%d", hm->getName(), (int)hm->size(), (int)hm->capacity(), (int)(hm->getAllocatedMemorySize() >> 10), hm->getType());
			
			// Draw the render debug.
			if (ImGui::TreeNode(buf)) {
				// Show information to activate or deactivate the component.
				ImGui::Spacing(0, 5);
				bool activeComponent = isComponentInRenderDebug(comp_name);
				if (ImGui::Checkbox("Activate component", &activeComponent))
					activeComponent ? addComponentToRenderDebug(comp_name) : removeComponentFromRenderDebug(comp_name);;
				ImGui::Spacing(0, 5);
				ImGui::Separator();
				ImGui::Spacing(0, 5);

				// Draw all the entities.
				hm->debugInMenuAll();

				ImGui::TreePop();
			}
		}
	}
	ImGui::PopID();
}

void CModuleEntities::renderDebug() {
	renderDebugOfComponents();
}

void CModuleEntities::renderDebugOfComponents() {
	CGpuScope gpu_scope("renderDebugOfComponents");
	PROFILE_FUNCTION("renderDebugOfComponents");
	for (auto om : om_to_render_debug) {
		PROFILE_FUNCTION(om->getName());
		om->renderDebugAll();
	}
}

void CModuleEntities::setCHandleTimeToLive(CHandle handle, float time) {
	entities_to_delete_by_time.push_back(std::pair<CHandle, float>(handle, time));
}

void CModuleEntities::checkHandlesTime(float dt) {
	std::vector<std::pair<CHandle, float>>::iterator it;
	for (it = entities_to_delete_by_time.begin(); it != entities_to_delete_by_time.end(); it++) {
		it->second -= dt;
		if (it->second <= 0.0f) {
			CHandle h = it->first;
			if(h.isValid())
				h.destroy();
			it = entities_to_delete_by_time.erase(it); //Delete the current handle from the map and increment the iterator
			if (it == entities_to_delete_by_time.end()) break;
		}
	}
}