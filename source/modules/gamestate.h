#pragma once
#include "mcv_platform.h"
#include "module.h"

/* A gamestate is a class formed by a vector to module pointers
and a name for its identification. */

class CGamestate : public VModules
{
public:
	std::string _name;
};

using VGamestates = std::vector<CGamestate>;
