#pragma once

#include "modules/module.h"
#include "entity/entity.h"

/* This module updates all components on the update call. */
class CModuleEntities : public IModule
{
	std::vector< CHandleManager * > om_to_update;
	std::vector< CHandleManager * > om_to_render_debug;
	std::vector<std::pair<CHandle, float>> entities_to_delete_by_time;
	void loadListOfManagers(const json & j, std::vector< CHandleManager* > &managers);
	void renderDebugOfComponents();
	
	// Checks the CHandles map in order to see if someone has run out of time and needs to be deleted
	void checkHandlesTime(float dt);

public:
	CModuleEntities(const std::string& aname) : IModule(aname) { }
  
	// This function reads a file named components.json. It gets all the componentManagers,
	// reorders them based on their priority, initialized them to a size (doesn't create them),
	// as they are already created as globals.
	bool start() override;

	// Calls the destroy method for all entities. Each entity will call in its constructor
	// the destroy method for all of its components. Components are removed at the end of the frame.
	void stop() override;
  
	// Updates all the components that should be updated.
	void update(float delta) override;

	void renderDebug() override;
	// For debug rendering.
	void renderInMenu() override;
	void renderEntitiesMenu();
	void renderComponentsMenu();

	// Add components to render debug. Used by ImGui mostly.
	void addComponentToRenderDebug(const std::string & componentName);
	void removeComponentFromRenderDebug(const std::string & componentName);
	bool isComponentInRenderDebug(const std::string & componentName);

	// Set how much time to live the CHandle has, useful to delete entities in a fixed time in seconds
	void setCHandleTimeToLive(CHandle handle, float time);
};

CHandle getEntityByName(const std::string& name);
