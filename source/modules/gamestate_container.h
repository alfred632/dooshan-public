#pragma once

#include <vector>
#include <map>
#include "gamestate.h"

/* CGamestateContainer class works as a container holding all the gamestates and allowing for fast addition and search. */
class CGamestateContainer {
private:
	std::map<std::string, int> _searchGamestate; // Allows for fast search of modules, store its name as a key and its vector position as value.

public:
	VGamestates _gamestates; // Holds gamestates.

	void addGamestate(CGamestate & gamestate); // Adds gamestate to the container.
	CGamestate * findGameState(const std::string & gamestateName); // Returns gamestate pointer if present.
	void clear(); // Removes all gamestates freeing their memory and removes the pointrs in the container.
};