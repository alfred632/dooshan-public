#pragma once

#include "modules/module.h"

class CModuleGameplay : public IModule
{
public:
	CModuleGameplay(const std::string & name);	
	
	bool start() override;
	void stop() override;	
	void update(float dt) override;
	void renderDebug() override {}

private:
	bool _pauseUIActive = false;
	bool _pauseUIKeyboard = true;

	bool _firstFrame = true;

	CClock _startDelayCinematic = CClock(3.0f);

	void togglePauseUI(bool active);
	void togglePauseUIControlsImage(bool keyboard);
	void checkInputModeChanged();
	void closePauseUI();
	void openPauseUI();
	void onMainMenu();

	void declareInLua();
	void cleanStatusEffects();
};