#pragma once
#include "modules/module.h"
#include "SOL2/sol.hpp"
#include <vector>
#include <map>

#define LUA_NIL sol::lua_nil

// Auxiliary method that allows LUA code to get any declared component for an entity.
template <typename TObj>
TObj * getComponentFromEntity(CHandle h_entity) {
	if (h_entity.isValid())
		return static_cast<CEntity *>(h_entity)->getComponent<TObj>();
	else
		return nullptr;
}
// Auxiliary method that allows LUA code to get a CHandle for a specific TComp
template <typename TObj>
CHandle getTCompAsCHandle(TObj * tc) {
	return CHandle(tc);
}

typedef std::string EventID;
typedef std::string ScriptFunctionToCall;
struct EventData;

/* Event handle for a component. */
struct ComponentEventHandle {
	CHandle ComponentHandle;
	EventID EventIDToCall;

	ComponentEventHandle(){}
	ComponentEventHandle(CHandle & nCompHandle, const EventID & nEventIDToCall);

	void CallEventForScript(CHandle CompHandleOfScriptToBindToEvent);
	void CallEventForAllScripts();
};

/* Struct holding a component handle and all events it can have. */
struct ComponentEventData {
	CHandle CompHandleFiringEvent; // Handle to component that has the given event.
	std::map<EventID, EventData> EventsForComponents; // Events the given component can fire.
};

struct EventData {
	EventID EventId;
	std::map<CHandle, ScriptFunctionToCall> ScriptsBindedToEvent; // Handle of the script component and function to call.
};

struct ScriptEvent{
	CHandle ScriptComponentHandle;
	ScriptFunctionToCall FunctionToCall;
	
	ScriptEvent() {}
	ScriptEvent(CHandle nScriptCompHandle, ScriptFunctionToCall nFunctionToCall) : ScriptComponentHandle(nScriptCompHandle), FunctionToCall(nFunctionToCall) {}
};

struct TimedScriptEvent {
	ScriptEvent scriptEvent;
	float TimeToCallEvent;

	TimedScriptEvent() {}
	TimedScriptEvent(ScriptEvent ScriptedEvent, float TimeForEvent) : scriptEvent(ScriptedEvent), TimeToCallEvent(TimeForEvent) {}
};

class CModuleScripting : public IModule {

	sol::state _lua; // Gives access to the LUA interface creating and managing the creation of the interface to Lua.
	sol::environment _testEnv; // Environment used by test scripts.
	sol::environment _uiEnv; // Environment used by UI calls.

	std::map<CHandle, ComponentEventData> ComponentEvents; // Handle of component and component data.
	std::vector<TimedScriptEvent> TimedEvents; // Time before calling and script to call when ended.

	void declareBasics(); // Declare basic structures from our engine so they can be used by LUA scripts.
	void declareGeneralFunctions(); // Load general functions to be called by scripts or code. Like events or test functions.
	void dealWithError(const sol::error & e); // called whenever a exception happens when loading a file or executing code from a string.

public:
	CModuleScripting(const std::string& aname) : IModule(aname) {}

	bool start() override;
	void update(float delta) override;
	void renderDebug() override { };
	void renderInMenu();

	// Here we call onEntitiesLoaded() in LUA so you can do anything once all entities are loaded.
	void onEntitiesLoaded();

	// declares a component type in LuaEntity so it can be accessed within LUA
	template <typename TObj, typename... Args>
	void declareComponent(const char * compName, Args && ... args) {
		// allows access to all components from entity (obviously the CHandle must be a CEntity)
		_lua["CHandle"][compName] = &getComponentFromEntity<TObj>;
		// allows for a CHandle to be converted to a specific TComp, handle:asTCompTransform() for example
		_lua["CHandle"][std::string("as") + compName] = &CHandle::operator TObj *;
		// logic manager methods that require a component type
		_lua["LogicManagerClass"][std::string("setEntity") + compName + "Active"] = &LogicManager::template SetEntityComponentActive<TObj>;
		_lua["LogicManagerClass"][std::string("setTagEntities") + compName + "Active"] = &LogicManager:: template SetTagEntitiesComponentActive<TObj>;
		// actual declaration for TObj
		_lua.new_usertype<TObj>(compName, "new", sol::no_constructor, std::forward<Args>(args)...);
		// allows for a TComp to be converted to a CHandle
		_lua[compName]["asCHandle"] = &getTCompAsCHandle<TObj>;
	}

	// Bind a global instance of the entity in lua
	void bindEntityToLua(const std::string & name, CHandle entity);

	/* Check obj (function, variable, ...) exists in LUA. */

	// Check if a variable or method exists in LUA global environment.
	bool existsInLua(const char * objName);
	bool existsInLua(const std::string & objName);
	// Check if a variable or method exists in LUA specific environment
	bool existsInLuaEnv(const std::string & objName, sol::environment & env);

	/* Execute scripts. */

	// Loads and executes script in global env.
	bool doFile(const std::string & path);
	// Loads and executes scripts in specific env.
	bool doFile(const std::string & path, sol::environment & env);
	// Loads and executes code passed as string in global env.
	bool doString(const std::string & code);
	// Loads and executes code passed as string in specific env.
	bool doString(const std::string & code, sol::environment & env);

	/* Event scripting. */
	ComponentEventHandle RegisterNewComponentEventForScripts(CHandle CompHandleFiringEvent, const std::string & EventID);
	bool UnregisterNewComponentEventForScripts(CHandle CompHandleFiringEvent, const std::string & EventID);
	bool BindScriptToComponentEvent(CHandle CompHandleFiringEvent, const std::string & EventID, CHandle CompHandleOfScriptToBindToEvent, const std::string & FunctionToFireOnEventCall);
	bool UnbindScriptToComponentEvent(CHandle CompHandleFiringEvent, const std::string & EventID, CHandle CompHandleOfScriptToBindToEvent);
	void BindTimedScriptEvent(CHandle CompHandleOfScriptToBindToEvent, const std::string & FunctionToFireOnEventCall, float TimeBeforeCalling);
	void ClearTimedScriptEvents();
	void CallEventAllScript(CHandle CompHandleFiringEvent, const std::string & EventID);
	void CallEventAllScript(const ComponentEventHandle & Handle);
	void CallEvent(CHandle CompHandleFiringEvent, const std::string & EventID, CHandle CompHandleOfScriptToBindToEvent);
	void CallEvent(const ComponentEventHandle & Handle, CHandle CompHandleOfScriptToBindToEvent);

	/* Call functions from scripts. */

	// Calls a function from LUA with that name and provided arguments in the global environment.
	// Function should exist for this to be called.
	// Returns true if it was executed correctly, false ow.
	template <typename... Args>
	bool call(const std::string & elem, Args && ... args) {
		if (existsInLua(elem)) {
			return callLuaObj(_lua[elem], std::forward<Args>(args)...);
		}
		return false;
	}

	// Same as before but method gets executed in a specific LUA environment.
	// Returns true if it was executed correctly, false ow.
	template <typename... Args>
	bool envCall(const std::string & elem, sol::environment & env, Args && ... args) {
		if (existsInLuaEnv(elem, env)) {
			return callLuaObj(env[elem], std::forward<Args>(args)...);
		}
		return false;
	}

	// Calls the LUA object, It only works for functions.
	// Returns true if it was executed correctly, false ow.
	template <typename LuaFuncType, typename... Args>
	bool callLuaObj(LuaFuncType elem, Args && ... args) {
		if (elem.get_type() == sol::type::function) {
			try {
				elem(std::forward<Args>(args)...);
			}
			catch (const sol::error & e) {
				dealWithError(e);
				return false;
			}
		}
		else
			return false;

		return true;
	}


	/* Getters. */

	// Actual LUA state and context, you have access to all SOL2 possibilities with this
	sol::state & getLuaState() { return _lua; }
	sol::environment& getUIEnvironment() { return _uiEnv; }

	friend class TCompScript;
};