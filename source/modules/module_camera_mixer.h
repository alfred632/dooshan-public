#pragma once

#include "modules/module.h"
#include "components/common/comp_transform.h"

class TCompTransform;

class CModuleCameraMixer : public IModule
{
public:
	CModuleCameraMixer(const std::string& aname) : IModule(aname) { }
	bool start() override;
	void stop() override;
	void update(float delta) override;
	void renderDebug() override;
	void renderInMenu() override;

	void blendCamera(CHandle camera, float blendTime, Interpolator::IInterpolator* interpolation = nullptr);
	
	void setDefaultCamera(CHandle camera) {
		_defaultCamera = camera;
		assert(_defaultCamera.isValid());
	}
  
	void setOutputCamera(CHandle camera) { 
		_outputCamera = camera;
		CEntity * ent = _outputCamera;
		_outputCameraTransformH = ent->getComponent<TCompTransform>();
		assert(_outputCamera.isValid() && _outputCameraTransformH.isValid());
	}

	VHandles getCamerasEntitiesMixing() {
		VHandles camerasMixing;
		camerasMixing.push_back(_defaultCamera);
		camerasMixing.push_back(_outputCamera);
		for(auto & cam : _mixedCameras)
			camerasMixing.push_back(cam.camera);
		return camerasMixing;
	}

	CHandle getMixerDefaultCamera() { return _defaultCamera; }
	CHandle getMixerOuputCamera() { return _outputCamera; }

	CTransform* getMixerOutputCameraTransform();

private:
	void blendCameras(const CCamera* camera1, const CCamera* camera2, float ratio, CCamera& output);

	struct TMixedCamera
	{
	CHandle camera;
	float blendTime = 0.f;
	float targetWeight = 1.f; // to be controlled from outside
	float blendedWeight = 0.f;
	float appliedWeight = 0.f;
	Interpolator::IInterpolator* interpolator = nullptr;
	};

	std::vector<TMixedCamera> _mixedCameras;
	CHandle _defaultCamera;
	CHandle _outputCamera;
	CHandle _outputCameraTransformH;
};
