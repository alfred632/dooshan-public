#pragma once

#include "modules/module.h"
#include "components/common/comp_collider.h"
#include "components/common/comp_ragdoll.h"
#include "components/common/comp_hitboxes.h"
#include "particles/particle_emitter.h"

#define VEC3_TO_PXVEC3(vector3) physx::PxVec3(vector3.x, vector3.y, vector3.z)
#define VEC3_TO_PXEXTVEC3(vector3) physx::PxExtendedVec3(vector3.x, vector3.y, vector3.z)
#define PXVEC3_TO_VEC3(pxvec3) VEC3(pxvec3.x, pxvec3.y, pxvec3.z)
#define PXEXTVEC3_TO_VEC3(pxextvec3) VEC3((float)pxextvec3.x, (float)pxextvec3.y, (float)pxextvec3.z)

#define QUAT_TO_PXQUAT(quaternion) physx::PxQuat(quaternion.x, quaternion.y, quaternion.z, quaternion.w)
#define PXQUAT_TO_QUAT(pxquat) QUAT(pxquat.x, pxquat.y, pxquat.z,pxquat.w)

extern CTransform toTransform(physx::PxTransform pxtrans);
extern physx::PxTransform toPxTransform(CTransform mcvtrans);

extern CTransform toTransform(physx::PxTransform pxtrans);
extern physx::PxTransform toPxTransform(CTransform mcvtrans);

class CModulePhysics : public IModule
{
public:

	enum FilterGroup {
		Wall = 1 << 0,
		Floor = 1 << 1,
		Player = 1 << 2,
		Enemy = 1 << 3,
		Trigger = 1 << 4,
		Ragdoll = 1 << 5,
		EnemyHitboxes = 1 << 6,
		PickUps = 1 << 7,
		Bullets = 1 << 8,
		Scenario = Wall | Floor,
		Characters = Player | Enemy,
		Particles = 1 << 9,
		DynamicScenario = 1 << 10,
		LowPoly = 1 << 11,
		HighPoly = 1 << 12,
		DynamicScenarioNoCollision = 1 << 13,
		ScenarioLow = Scenario | LowPoly,
		ScenarioHigh = Scenario | HighPoly,
		All = -1
	};
	
  CModulePhysics(const std::string& aname) : IModule(aname) { }
  bool start() override;
  void stop() override;
  void update(float delta) override;
  void renderDebug() override;
  void renderInMenu() override;

  void setupFiltering(physx::PxShape* shape, physx::PxU32 filterGroup, physx::PxU32 filterMask);
  void setupFiltering(physx::PxParticleSystem* shape, physx::PxU32 filterGroup, physx::PxU32 filterMask);
  void setupFilteringOnAllShapesOfActor(physx::PxRigidActor* actor, physx::PxU32 filterGroup, physx::PxU32 filterMask);
  void createActor(TCompCollider& compCollider);
  void createArticulations(TCompRagdoll& comp_ragdoll);
  void createHitboxes(TCompHitboxes& compHitbox);
  physx::PxParticleSystem* createParticleSystem(physx::PxU32 maxParticles, const particles::TEmitter& emitter);
	void releaseParticleSystem(physx::PxParticleSystem* ps);
	void restartScene();

  bool isActive() const;
  physx::PxScene * getScene() const;
  physx::PxPhysics * getPhysics();
  physx::PxMaterial * getMaterial();
  FilterGroup getFilterByName(const std::string& name);

	PxU32 loadFilter(const json& jConfig, const std::string& name, PxU32 defaultFilter = FilterGroup::All);

private:
	physx::PxRigidActor* createController(TCompCollider& comp_collider);
	bool readShape(physx::PxRigidActor* actor, const json& jcfg);
	void readMaterials();

public:

	/* This class implements custom callbacks for events of PhysX simulation. */
  class CustomSimulationEventCallback : public physx::PxSimulationEventCallback
  {
    // Implements PxSimulationEventCallback
    virtual void							onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 nbPairs);
    virtual void							onTrigger(physx::PxTriggerPair* pairs, physx::PxU32 count);
    virtual void							onConstraintBreak(physx::PxConstraintInfo*, physx::PxU32) {}
    virtual void							onWake(physx::PxActor**, physx::PxU32) {}
    virtual void							onSleep(physx::PxActor**, physx::PxU32) {}
    virtual void							onAdvance(const physx::PxRigidBody*const*, const physx::PxTransform*, const physx::PxU32) {}
  };
  CustomSimulationEventCallback customSimulationEventCallback;

  physx::PxControllerManager * gControllerManager;
  
  struct TJoint {
    struct TObj {
      std::string          name;
      CHandle              h_collider;
      CTransform           transform;
      physx::PxRigidActor* actor = nullptr;
    };
    std::string joint_type;
    TObj obj0;
    TObj obj1;
    void create();
    physx::PxJoint*         px_joint = nullptr;
  };

  typedef std::vector< TJoint > VJoints;
  VJoints joints;
  void debugActor(const char* title, physx::PxRigidActor* actor);
  void debugInMenuJoints();
  void debugInMenuJoint(TJoint& c);
  void debugInMenuJointObj(TJoint& j, physx::PxJointActorIndex::Enum idx);
};
