#pragma once

#include "modules/module.h"
#include "render/shaders/technique.h"

#include "geometry/transform.h"
#include "utils/json_resource.h"

class CMesh;
class CTexture;

class CModuleDani : public IModule
{
public:
	struct TEntity {
		int         id = 0;
		std::string name;
		CTransform  transform;
		Vector4 color = Vector4(1, 1, 0, 1);
		
		float	speed = 1.0f;
		float	rotation_speed = 1.0f;

		const CMesh *    mesh = nullptr;
		const CTexture * texture = nullptr;

		static int player_id;// By default 0 will be the player ID.
		static int counter;
		Matrix getWorld() const;
		TEntity();
		void update(float elapsed);
		void renderInMenu();
	};

	static Vector3 ref_point;
	static Vector4 ref_color;
	static float entityDefaultScale;

	/* Shitty varaibles for entity creation in IMGUI */
	Vector3 nEntityPos;
	Vector3 nEntityRotation;
	float nEntityScale = 0.6f;
	Vector4 nEntityColor;


	Vector3 cameraEye;
	Vector3 cameraTarget;
	Vector3 cameraUp;


private:
	std::vector< TEntity > entities;
	const CTechnique * entitiesTechnique;
	const CJson * json_res = nullptr;

	void renderObjs();
	void updateObjs(float dt);
	void renderDebugObjs();

public:
	CModuleDani(const std::string & name);

	bool start() override;
	void stop() override;
	void update(float dt);
	void render();
	void renderInMenu() override;
};