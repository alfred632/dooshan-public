#include "mcv_platform.h"
#include "module_physics.h"
#include "entity/entity.h"
#include "handle/handle.h"
#include "components/common/comp_transform.h"
#include "components/controllers/comp_character_controller.h"
#include "render/meshes/collision_mesh.h"
#include "engine.h"
#include "utils/data_saver.h"
#include "skeleton/comp_skeleton.h"
#include "skeleton/game_core_skeleton.h"
#include "cal3d/cal3d.h"
#include "skeleton/cal3d2engine.h"
#include "components/common/comp_name.h"

#include "PxPhysicsAPI.h"

#pragma comment(lib, "PhysX3_x64.lib")
#pragma comment(lib, "PhysX3Common_x64.lib")
#pragma comment(lib, "PhysX3Extensions.lib")
#pragma comment(lib, "PxFoundation_x64.lib")
#pragma comment(lib, "PxPvdSDK_x64.lib")
#pragma comment(lib, "PhysX3CharacterKinematic_x64.lib")
#pragma comment(lib, "PhysX3Cooking_x64.lib")

using namespace physx;

// -----------------------------------------------------
CTransform toTransform(PxTransform pxtrans) {
	CTransform trans;
	trans.setPosition(PXVEC3_TO_VEC3(pxtrans.p));
	trans.setRotation(PXQUAT_TO_QUAT(pxtrans.q));
	return trans;
}

// -----------------------------------------------------
PxTransform toPxTransform(CTransform mcvtrans) {
	PxTransform trans;
	trans.p = VEC3_TO_PXVEC3(mcvtrans.getPosition());
	trans.q = QUAT_TO_PXQUAT(mcvtrans.getRotation());
	return trans;
}

// -----------------------------------------------------
PxDefaultAllocator      gAllocator;
PxDefaultErrorCallback  gErrorCallback;
PxFoundation*           gFoundation = nullptr;
PxPhysics*              gPhysics = nullptr;
PxControllerManager*    gControllerManager = nullptr;
PxDefaultCpuDispatcher* gDispatcher = nullptr;
PxScene*                gScene = nullptr;
PxMaterial*             gMaterial = nullptr;
PxPvd*                  gPvd = nullptr;

std::unordered_map<std::string, PxMaterial*> gMaterials;

// -----------------------------------------------------
static PxGeometryType::Enum readGeometryType(const json& j) {
	PxGeometryType::Enum geometryType = PxGeometryType::eINVALID;
	if (!j.count("shape"))
		return geometryType;

	std::string jgeometryType = j["shape"];
	if (jgeometryType == "sphere") {
		geometryType = PxGeometryType::eSPHERE;
	}
	else if (jgeometryType == "box") {
		geometryType = PxGeometryType::eBOX;
	}
	else if (jgeometryType == "plane"){
		geometryType = PxGeometryType::ePLANE;
	}
	else if (jgeometryType == "convex") {
		geometryType = PxGeometryType::eCONVEXMESH;
	}
	else if (jgeometryType == "capsule") {
		geometryType = PxGeometryType::eCAPSULE;
	}
	else if (jgeometryType == "trimesh") {
		geometryType = PxGeometryType::eTRIANGLEMESH;
	}
	else {
		Utils::dbg("Unsupported shape type in collider component %s", jgeometryType.c_str());
	}
  return geometryType;
}


PxFilterFlags CustomFilterShader(
	PxFilterObjectAttributes attributes0, PxFilterData filterData0,
	PxFilterObjectAttributes attributes1, PxFilterData filterData1,
	PxPairFlags& pairFlags, const void* constantBlock, PxU32 constantBlockSize
)
{
	if ((filterData0.word0 & filterData1.word1) && (filterData1.word0 & filterData0.word1))
	{
		if (PxFilterObjectIsTrigger(attributes0) || PxFilterObjectIsTrigger(attributes1))
		{
			pairFlags = PxPairFlag::eTRIGGER_DEFAULT;
		}
		else {
			pairFlags = PxPairFlag::eCONTACT_DEFAULT | PxPairFlag::eNOTIFY_TOUCH_FOUND;
		}
		return PxFilterFlag::eDEFAULT;
	}
	return PxFilterFlag::eSUPPRESS;
}

bool CModulePhysics::start()
{
	// Start the PhysX engine, create the foundation, pvd and physics simulation.
	gFoundation = PxCreateFoundation(PX_FOUNDATION_VERSION, gAllocator, gErrorCallback);
	gPvd = PxCreatePvd(*gFoundation);
	PxPvdTransport* transport = PxDefaultPvdSocketTransportCreate("127.0.0.1", 5425, 10);
	gPvd->connect(*transport, PxPvdInstrumentationFlag::eALL);
	gPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *gFoundation, PxTolerancesScale(), true, gPvd);
	PxInitExtensions(*gPhysics, gPvd);

	// Create the physx scene description and start it  physx scene.
	restartScene();

	gMaterial = gPhysics->createMaterial(0.5f, 0.5f, 0.6f);
	readMaterials();
	if (gMaterials.count("default") > 0) {
		gMaterial->release();
		gMaterial = gMaterials["default"];
	}
	else
		gMaterials["default"] = gMaterial;

	return true;
}

void CModulePhysics::stop() {
	gScene->release();
	gScene = nullptr;
	gDispatcher->release();
	gPhysics->release();
	PxPvdTransport* transport = gPvd->getTransport();
	gPvd->release();
	transport->release();
	gFoundation->release();
}

void CModulePhysics::update(float delta) {
	// Simulate scenario in order to update values on PhysX side
	{
		PROFILE_FUNCTION("Simulate");
		gScene->simulate(delta);
	}
	{
		PROFILE_FUNCTION("fetch");
		gScene->fetchResults(true);
	}
	gControllerManager->computeInteractions(delta);

	// Iterate all PxActors to update position on render space with PhysX values
	PxU32 nbOut = 0;
	PxActor ** activeActors = gScene->getActiveActors(nbOut);
	for (unsigned int i = 0; i < nbOut; ++i)
	{
		PROFILE_FUNCTION("Actor");
		PxRigidActor * rigidActor = ((PxRigidActor *)activeActors[i]);
		assert(rigidActor);
		
		CHandle h_collider;
		h_collider.fromVoidPtr(rigidActor->userData);
		TCompCollider * c_collider = h_collider;
		if (c_collider != nullptr)
		{
			TCompTransform * c = c_collider->getComponent<TCompTransform>();
			if (!c) continue;

			// Update the position based on the controller if it has one.
			if (c_collider->controller) {
				PxExtendedVec3 pxpos_ext = c_collider->controller->getFootPosition();
				c->setPosition(VEC3((float)pxpos_ext.x, (float)pxpos_ext.y, (float)pxpos_ext.z));
			}
			else {
				PxTransform PxTrans = rigidActor->getGlobalPose();
				Vector3 scale = c->getScale();
				c->set(toTransform(PxTrans));
				c->setScale(scale);
			}
		}
	}
}

void CModulePhysics::renderDebug() {
	// Show all the joints
	// for (auto& c : joints) {
	//   PxRigidActor* actors[2];
	//   c.px_joint->getActors(actors[0], actors[1]);
	//   for (int i = 0; i < 2; ++i) {
	//     if (!actors[i])
	//       continue;
	//     CTransform local_t = toTransform(c.px_joint->getLocalPose( i == 0 ? PxJointActorIndex::eACTOR0 : PxJointActorIndex::eACTOR1));
	//     CTransform actor_t = toTransform(actors[i]->getGlobalPose());
	//     MAT44 world = local_t.asMatrix() * actor_t.asMatrix();
	//     drawAxis(world);
	//   }
	// }
}

void CModulePhysics::renderInMenu()
{
	debugInMenuJoints();
}

void CModulePhysics::setupFiltering(PxShape * shape, PxU32 filterGroup, PxU32 filterMask)
{
	PxFilterData filterData;
	filterData.word0 = filterGroup; // word0 = own ID
	filterData.word1 = filterMask;	// word1 = ID mask to filter pairs that trigger a contact callback;
	shape->setSimulationFilterData(filterData);
	shape->setQueryFilterData(filterData);
	shape->getSimulationFilterData();
}

void CModulePhysics::setupFilteringOnAllShapesOfActor(PxRigidActor* actor, PxU32 filterGroup, PxU32 filterMask)
{
  const PxU32 numShapes = actor->getNbShapes();
  std::vector<PxShape*> shapes;
  shapes.resize(numShapes);
  actor->getShapes(&shapes[0], numShapes);
  for (PxU32 i = 0; i < numShapes; i++)
    setupFiltering(shapes[i], filterGroup, filterMask);
}

void CModulePhysics::setupFiltering(physx::PxParticleSystem * shape, physx::PxU32 filterGroup, physx::PxU32 filterMask) {
	PxFilterData filterData;
	filterData.word0 = filterGroup; // word0 = own ID
	filterData.word1 = filterMask;	// word1 = ID mask to filter pairs that trigger a contact callback;
	shape->setSimulationFilterData(filterData);
	shape->getSimulationFilterData();
}

void CModulePhysics::createActor(TCompCollider & comp_collider)
{
	const json & jconfig = comp_collider.jconfig;
	
	// Entity start transform
	TCompTransform * c = comp_collider.getComponent<TCompTransform>();
	PxTransform transform = toPxTransform(*c);
	PxRigidActor * actor = nullptr;

	bool is_controller = jconfig.count("controller") > 0;

	// Controller or physics based?
	if (is_controller)
		actor = createController(comp_collider);
	else {
		// Dynamic or static actor?
		bool isDynamic = jconfig.value("dynamic", false);
		if (isDynamic)
		{
			PxRigidDynamic * dynamicActor = gPhysics->createRigidDynamic(transform);
			PxReal density = jconfig.value("density", 1.f);
			PxReal mass = jconfig.value("mass", 0.f);
			PxRigidBodyExt::updateMassAndInertia(*dynamicActor, density);
			dynamicActor->setMass(mass);
			bool gravityActive = jconfig.value("gravity_active", true);
			Vector3 intertia = loadVector3(jconfig, "inertia", Vector3::One * mass);
			dynamicActor->setMassSpaceInertiaTensor(VEC3_TO_PXVEC3(intertia));

			actor = dynamicActor;

			// setup lock flags so we can lock an axis for translation & rotation
			if (jconfig.count("lock_flags") > 0) {
				PxRigidDynamicLockFlags lockFlags = {};
				auto& jLockFlags = jconfig["lock_flags"];
				std::string rawFlags;
				if (jLockFlags.count("linear") > 0) {
					// translation lock flags
					rawFlags = jLockFlags["linear"];
					// to lower case so we can recognize 'x', 'y', 'z'
					std::transform(rawFlags.begin(), rawFlags.end(), rawFlags.begin(),
												 [](unsigned char c) { return std::tolower(c); });
					// axis
					if (rawFlags.find('x') != std::string::npos)
						lockFlags = lockFlags | PxRigidDynamicLockFlag::eLOCK_LINEAR_X;
					if (rawFlags.find('y') != std::string::npos)
						lockFlags = lockFlags | PxRigidDynamicLockFlag::eLOCK_LINEAR_Y;
					if (rawFlags.find('z') != std::string::npos)
						lockFlags = lockFlags | PxRigidDynamicLockFlag::eLOCK_LINEAR_Z;
				}
				if (jLockFlags.count("angular") > 0) {
					// rotation lock flags
					rawFlags = jLockFlags["angular"];
					// to lower case so we can recognize 'x', 'y', 'z'
					std::transform(rawFlags.begin(), rawFlags.end(), rawFlags.begin(),
												 [](unsigned char c) { return std::tolower(c); });
					// axis
					if (rawFlags.find('x') != std::string::npos)
						lockFlags = lockFlags | PxRigidDynamicLockFlag::eLOCK_ANGULAR_X;
					if (rawFlags.find('y') != std::string::npos)
						lockFlags = lockFlags | PxRigidDynamicLockFlag::eLOCK_ANGULAR_Y;
					if (rawFlags.find('z') != std::string::npos)
						lockFlags = lockFlags | PxRigidDynamicLockFlag::eLOCK_ANGULAR_Z;
				}

				dynamicActor->setRigidDynamicLockFlags(lockFlags);
			}
			
			if (jconfig.value("kinematic", false))
				dynamicActor->setRigidBodyFlag(PxRigidBodyFlag::eKINEMATIC, true);
			else
				dynamicActor->setRigidBodyFlag(PxRigidBodyFlag::eKINEMATIC, false);
		}
		else
		{
			PxRigidStatic* static_actor = gPhysics->createRigidStatic(transform);
			actor = static_actor;
		}
	}

	// The capsule was already loaded as part of the controller
	if (!is_controller) {
		// if shapes exists, try to read as an array of shapes
		if (jconfig.count("shapes")) {
			const json& jshapes = jconfig["shapes"];
			for (const json& jshape : jshapes)
				readShape(actor, jshape);
		}
		// Try to read shape directly (general case...)
		else {
			readShape(actor, jconfig);
		}
	}

	// PhysX is complaining 'adding the actor twice...'
	if (!is_controller)
		gScene->addActor(*actor);

	comp_collider.actor = actor;
	CHandle h_collider(&comp_collider);
	comp_collider.actor->userData = h_collider.asVoidPtr();

	// Joints
	if (jconfig.count("joints")) {
		for (const json& j : jconfig["joints"]) {
			TJoint joint;
			joint.joint_type = j.value("type", "spherical");
			joint.obj0.name = j["obj0"].value("name", "");
			joint.obj0.transform.load(j["obj0"]["transform"]);
			joint.obj1.name = j["obj1"].value("name", "");
			joint.obj1.transform.load(j["obj1"]["transform"]);
			joint.create();
			joints.push_back(joint);
		}
	}
}

PxParticleSystem * CModulePhysics::createParticleSystem(physx::PxU32 maxParticles, const particles::TEmitter& emitter) {
	bool perParticleRestOffset = false;

	// create particle system in PhysX SDK
	PxParticleSystem* ps = gPhysics->createParticleSystem(maxParticles, perParticleRestOffset);

	ps->setParticleReadDataFlag(PxParticleReadDataFlag::eVELOCITY_BUFFER, true);
	//ps->setParticleBaseFlag(PxParticleBaseFlag::eGPU, true);
	ps->setParticleBaseFlag(PxParticleBaseFlag::eCOLLISION_WITH_DYNAMIC_ACTORS, emitter.dynamic_collision);
	ps->setParticleBaseFlag(PxParticleBaseFlag::eCOLLISION_TWOWAY, emitter.two_way_interaction);
	
	ps->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, !emitter.default_gravity);
	if (!emitter.default_gravity)
		ps->setExternalAcceleration(VEC3_TO_PXVEC3(emitter.physx_gravity));
	ps->setDamping(emitter.damping);
	ps->setParticleMass(emitter.mass);
	ps->setMaxMotionDistance(emitter.max_distance_per_frame);

	ps->setRestOffset(emitter.rest_offset);
	//ps->setParticleBaseFlag(PxParticleBaseFlag::ePER_PARTICLE_REST_OFFSET, ???);
	ps->setContactOffset(emitter.contact_offset);
	ps->setRestitution(emitter.restitution);
	ps->setDynamicFriction(emitter.dynamic_friction);
	ps->setStaticFriction(emitter.static_friction);
	setupFiltering(ps, FilterGroup::Particles, emitter.physXMask);

	// add particle system to scene, in case creation was successful
	if (ps)
		gScene->addActor(*ps);

	return ps;
}

void CModulePhysics::releaseParticleSystem(PxParticleSystem * ps) {
	if (gScene) {
		gScene->removeActor(*ps);
		ps->releaseParticles();
		ps->release();
	}
}

void CModulePhysics::restartScene() {
	// Delete actual scene
	if (gScene)
		gScene->release();

	// Start a new one
	PxSceneDesc sceneDesc(gPhysics->getTolerancesScale());
	sceneDesc.gravity = PxVec3(0.0f, -9.81f, 0.0f);
	gDispatcher = PxDefaultCpuDispatcherCreate(2);
	sceneDesc.cpuDispatcher = gDispatcher;
	sceneDesc.filterShader = CustomFilterShader;
	gScene = gPhysics->createScene(sceneDesc);
	gScene->setFlag(PxSceneFlag::eENABLE_ACTIVE_ACTORS, true);
	gScene->setVisualizationParameter(PxVisualizationParameter::eJOINT_LOCAL_FRAMES, 1.0f);
	gScene->setVisualizationParameter(PxVisualizationParameter::eJOINT_LIMITS, 1.0f);

	// Physx pvd flags.
	PxPvdSceneClient* pvdClient = gScene->getScenePvdClient();
	if (pvdClient) {
		pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONSTRAINTS, true);
		pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONTACTS, true);
		pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_SCENEQUERIES, true);
	}

	gControllerManager = PxCreateControllerManager(*gScene);
	gScene->setSimulationEventCallback(&customSimulationEventCallback);
}

void CModulePhysics::createArticulations(TCompRagdoll& comp_ragdoll) {
	if (comp_ragdoll.articulation)
		return;

	//Get components
	CHandle h_comp_ragdoll(&comp_ragdoll);
	CEntity* e = h_comp_ragdoll.getOwner();

	TCompSkeleton* skel = e->getComponent<TCompSkeleton>();
	TCompTransform* comp_transform = e->getComponent<TCompTransform>();
	CHandle h_comp_collider = e->getComponent<TCompCollider>();
	CTransform* entity_trans = (CTransform*)comp_transform;

	//Get core skeleton
	auto core_skel = (CGameCoreSkeleton*)skel->getModel()->getCoreModel();

	//Create the articulation base
	comp_ragdoll.articulation = gPhysics->createArticulation();

	comp_ragdoll.articulation->setMaxProjectionIterations(4);	//Default is 4
	comp_ragdoll.articulation->setSolverIterationCounts(4, 1);	//Default is 4 and 1
	comp_ragdoll.articulation->setSeparationTolerance(0.1f);	//Default is 0.1f

	for (auto& ragdoll_bone_core : core_skel->ragdoll_core.json_skeleton_shapes_core) {
		//Get the child bone and parent cal bones
		auto childCalCoreBone = core_skel->getCoreSkeleton()->getCoreBone(ragdoll_bone_core.bone);
		auto parentCalCoreBone = core_skel->getCoreSkeleton()->getCoreBone(ragdoll_bone_core.parent_bone);
		assert(childCalCoreBone);

		//Get the child transform for the link creation, and the local frames of the joint
		CTransform childTrans;
		childTrans.setPosition(Cal2DX(childCalCoreBone->getTranslationAbsolute()));
		childTrans.setRotation(Cal2DX(childCalCoreBone->getRotationAbsolute()));
		PxTransform px_childTransform = toPxTransform(childTrans);

		//Get the parent information because we are the child
		auto& parent_bone_core = ragdoll_bone_core.parent_core;

		//Create the geometry from the child information
		PxMaterial* ragdollsMaterial = gMaterial;
		if (gMaterials.count("ragdoll") > 0)
			ragdollsMaterial = gMaterials["ragdoll"];
		PxShape* capsuleShape = gPhysics->createShape(PxCapsuleGeometry(ragdoll_bone_core.radius, ragdoll_bone_core.height / 2.0f), *ragdollsMaterial);
		PxTransform relativePose(PxQuat(PxHalfPi, PxVec3(0, 0, 1)));
		capsuleShape->setLocalPose(relativePose);
		setupFiltering(capsuleShape, CModulePhysics::FilterGroup::Ragdoll, CModulePhysics::FilterGroup::Scenario);

		PxArticulationLink* link = NULL;
		PxArticulationLink* parentLink = NULL;
		PxArticulationJoint* joint = NULL;
		//Create articulation link, first articulation parent will be null
		if (!parent_bone_core) {
			link = comp_ragdoll.articulation->createLink(NULL, px_childTransform);
			link->attachShape(*capsuleShape);
			PxRigidBodyExt::setMassAndUpdateInertia(*link, 1.0f);
		}
		else { //Others articulations with parents
			//Get the parent transform for the localFrame0 of the joint, already defined in the json
			VEC3 rot = ragdoll_bone_core.articulation.joint_parent_local_rotation;
			CTransform localFrame0_trans;
			localFrame0_trans.setPosition(ragdoll_bone_core.articulation.joint_parent_local_position);
			localFrame0_trans.setRotation(QUAT::CreateFromYawPitchRoll(rot.x, rot.y, rot.z));
			PxTransform localFrame0 = toPxTransform(localFrame0_trans);

			//To calculate the localFrame 1 first we need the localFrame0 but in the world, not relative, so we need to get the parent world transform
			CTransform parentTrans;
			parentTrans.setPosition(Cal2DX(parentCalCoreBone->getTranslationAbsolute()));
			parentTrans.setRotation(Cal2DX(parentCalCoreBone->getRotationAbsolute()));
			parentTrans.fromMatrix(localFrame0_trans.asMatrix() * parentTrans.asMatrix());

			//Calculate the localFrame1 (child local frame)
			CTransform localFrame1_trans;
			localFrame1_trans.fromMatrix(parentTrans.asMatrix() * childTrans.asMatrix().Invert());
			PxTransform localFrame1 = toPxTransform(localFrame1_trans);

			//Search for parent link
			for (int i = 0; i < comp_ragdoll.ragdoll.num_bones; ++i) {
				auto& ragdoll_bone = comp_ragdoll.ragdoll.bones[i];
				if (ragdoll_bone_core.parent_bone == ragdoll_bone.core->bone) {
					parentLink = ragdoll_bone.link;
				}
			}

			//Create the link (actor) in the actual position (child) and attach the capsule shape
			link = comp_ragdoll.articulation->createLink(parentLink, px_childTransform);
			link->attachShape(*capsuleShape);
			PxRigidBodyExt::setMassAndUpdateInertia(*link, 1.0f);

			//Joint information
			TJsonArticulationJoint* articulation = &ragdoll_bone_core.articulation;
			joint = link->getInboundJoint();
			joint->setParentPose(localFrame0);
			joint->setChildPose(localFrame1);
			joint->setTwistLimit(articulation->twist_limit.x, articulation->twist_limit.y);
			joint->setTwistLimitEnabled(articulation->twist_limit_enabled);
			joint->setSwingLimit(articulation->swing_limit.x, articulation->swing_limit.y);
			joint->setSwingLimitEnabled(articulation->swing_limit_enabled);
			joint->setDamping(32);
			joint->setTangentialDamping(32);
		}

		//Store the physx information
		TSkeletonShapes::TSkeletonBoneShape& ragdoll_bone = comp_ragdoll.ragdoll.bones[comp_ragdoll.ragdoll.num_bones];
		ragdoll_bone.link = link;
		ragdoll_bone.parent_joint = joint;
		ragdoll_bone.core = &ragdoll_bone_core;
		if (!parent_bone_core)
			ragdoll_bone.isRoot = true;

		ragdoll_bone.idx = core_skel->getCoreSkeleton()->getCoreBoneId(ragdoll_bone_core.bone);
		ragdoll_bone.core->instance_idx = comp_ragdoll.ragdoll.num_bones;
		++comp_ragdoll.ragdoll.num_bones;

		//We store the collider handle of the entity in order to receive the callbacks from physics and to not break the engine
		link->userData = h_comp_collider.asVoidPtr();

		for (int i = 0; i < comp_ragdoll.ragdoll.num_bones; ++i) {
			auto& ragdoll_bone = comp_ragdoll.ragdoll.bones[i];
			if (ragdoll_bone.core->parent_core) {
				ragdoll_bone.parent_idx = ragdoll_bone.core->parent_core->instance_idx;
				auto& parent_ragdoll_bone = comp_ragdoll.ragdoll.bones[ragdoll_bone.parent_idx];

				parent_ragdoll_bone.children_idxs[parent_ragdoll_bone.num_children] = i;
				++parent_ragdoll_bone.num_children;
			}
		}
	}

	comp_ragdoll.ragdoll.created = true;
}

void CModulePhysics::createHitboxes(TCompHitboxes& compHitbox) {
	if (compHitbox.hitboxes.created)
		return;
	CHandle h_compHitbox(&compHitbox);
	CEntity* e = h_compHitbox.getOwner();

	TCompSkeleton* skel = e->getComponent<TCompSkeleton>();
	TCompTransform* comp_transform = e->getComponent<TCompTransform>();
	CTransform* entity_trans = (CTransform*)comp_transform;

	PxShape* shape = nullptr;

	auto core_skel = (CGameCoreSkeleton*)skel->getModel()->getCoreModel();

	for (auto& hitboxes_bone_core : core_skel->hitboxes_core.json_skeleton_shapes_core) {
		auto cal_core_bone = core_skel->getCoreSkeleton()->getCoreBone(hitboxes_bone_core.bone);
		assert(cal_core_bone);

		//First we calculate the final actor position by multiplying the position offset by the entity rotation
		CTransform trans;
		VEC3 bone_pos = Cal2DX(cal_core_bone->getTranslationAbsolute());
		VEC3 hitbox_offset = DirectX::XMVector3Rotate(hitboxes_bone_core.position_offset, comp_transform->getRotation());
		trans.setPosition(bone_pos + hitbox_offset);
		//Then the rotation offset is multiplied by the bone absolute rotation
		QUAT rotation_offset = QUAT::CreateFromYawPitchRoll(hitboxes_bone_core.rotation_offset.x, hitboxes_bone_core.rotation_offset.y, hitboxes_bone_core.rotation_offset.z);
		trans.setRotation(rotation_offset * Cal2DX(cal_core_bone->getRotationAbsolute()));

		PxTransform px_transform = toPxTransform(trans);

		TSkeletonShapes::TSkeletonBoneShape& hitboxes_bone = compHitbox.hitboxes.bones[compHitbox.hitboxes.num_bones];

		// If has parent calculate position and height using the bone and his parent, if not use only cal_core_bone
		bool usingParent = hitboxes_bone_core.parent_bone != "";
		if (usingParent) {
			auto cal_parent_bone = core_skel->getCoreSkeleton()->getCoreBone(hitboxes_bone_core.parent_bone);
			VEC3 parent_pos = Cal2DX(cal_parent_bone->getTranslationAbsolute());
			if (hitboxes_bone_core.height == 0.0f) {
				hitboxes_bone_core.height = VEC3::Distance(parent_pos, bone_pos);
				hitboxes_bone.custom_height = false;
			}
		}

		//Create the actor
		PxRigidActor* actor = nullptr;
		PxRigidDynamic* kinematicActor = gPhysics->createRigidDynamic(px_transform);
		kinematicActor->setRigidBodyFlag(PxRigidBodyFlag::eKINEMATIC, true);
		PxRigidBodyExt::updateMassAndInertia(*kinematicActor, 1.f);
		actor = kinematicActor;
		compHitbox.actors.push_back(actor);

		//Create the shape with a relative pose in order to be standing upright by default
		shape = gPhysics->createShape(PxCapsuleGeometry(hitboxes_bone_core.radius, hitboxes_bone_core.height / 2.0f), *gMaterial);

		shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
		shape->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);
		PxTransform relativePose(PxQuat(PxHalfPi, PxVec3(0, 0, 1)));
		shape->setLocalPose(relativePose);

		//We set the EnemyHitboxes as our layer, and set it as the query filter data only, don't need the simulation
		PxFilterData filterData;
		filterData.word0 = FilterGroup::EnemyHitboxes; // word0 = own ID
		shape->setQueryFilterData(filterData);

		actor->attachShape(*shape);
		shape->release();

		PxRigidDynamic* body = (PxRigidDynamic*)actor;
		assert(body);

		hitboxes_bone.actor = body;
		hitboxes_bone.core = &hitboxes_bone_core;

		hitboxes_bone.has_parent = usingParent;

		hitboxes_bone.idx = core_skel->getCoreSkeleton()->getCoreBoneId(hitboxes_bone_core.bone);
		hitboxes_bone.parent_idx = core_skel->getCoreSkeleton()->getCoreBoneId(hitboxes_bone_core.parent_bone);
		hitboxes_bone.core->instance_idx = compHitbox.hitboxes.num_bones;
		++compHitbox.hitboxes.num_bones;

		body->userData = h_compHitbox.asVoidPtr();
		gScene->addActor(*body);
	}

	compHitbox.hitboxes.created = true;

}

bool CModulePhysics::isActive() const {
  return gScene != nullptr;
}

PxScene* CModulePhysics::getScene() const {
  return gScene;
}

PxPhysics* CModulePhysics::getPhysics() {
	return gPhysics;
}

PxMaterial* CModulePhysics::getMaterial() {
	return gMaterial;
}

PxRigidActor* CModulePhysics::createController(TCompCollider& comp_collider) {
	const json& jconfig = comp_collider.jconfig;

	PX_ASSERT(desc.mType == PxControllerShapeType::eCAPSULE);

	PxCapsuleControllerDesc capsuleDesc;
	capsuleDesc.height = jconfig.value("height", TCompCharacterController::DEFAULT_HEIGHT);
	capsuleDesc.radius = jconfig.value("radius", TCompCharacterController::DEFAULT_RADIUS);
	capsuleDesc.climbingMode = PxCapsuleClimbingMode::eCONSTRAINED;
	capsuleDesc.material = gMaterial;

	PxCapsuleController* ctrl = static_cast<PxCapsuleController*>(gControllerManager->createController(capsuleDesc));
	PX_ASSERT(ctrl);
	TCompTransform* c = comp_collider.getComponent<TCompTransform>();
	VEC3 pos = c->getPosition();
	ctrl->setFootPosition(PxExtendedVec3(pos.x, pos.y, pos.z));
	PxRigidActor* actor = ctrl->getActor();
	comp_collider.controller = ctrl;

	setupFilteringOnAllShapesOfActor(actor, loadFilter(jconfig, "group"), loadFilter(jconfig, "mask"));

	return actor;
}

// Reads a single shape for jcfg and adds it to actor
bool CModulePhysics::readShape(PxRigidActor* actor, const json& jcfg) {
	// Shapes....
	PxGeometryType::Enum geometryType = readGeometryType(jcfg);
	if (geometryType == PxGeometryType::eINVALID)
		return false;

	PxShape* shape = nullptr;
	PxMaterial* material = nullptr;
	if (jcfg.count("material") > 0) {
		std::string materialName = jcfg["material"];
		if (gMaterials.count(materialName) > 0)
			material = gMaterials[jcfg["material"]];
		else
			material = gMaterial;
	}
	else
		material = gMaterial;

	if (geometryType == PxGeometryType::eBOX)
	{
		VEC3 jhalfExtent = loadVector3(jcfg, "half_size");
		PxVec3 halfExtent = VEC3_TO_PXVEC3(jhalfExtent);
		shape = gPhysics->createShape(PxBoxGeometry(halfExtent), *material);
	}
	else if (geometryType == PxGeometryType::ePLANE)
	{
		VEC3 jplaneNormal = loadVector3(jcfg, "normal");
		float jplaneDistance = jcfg.value("distance", 0.f);
		PxVec3 planeNormal = VEC3_TO_PXVEC3(jplaneNormal);
		PxReal planeDistance = jplaneDistance;
		PxPlane plane(planeNormal, planeDistance);
		PxTransform offset = PxTransformFromPlaneEquation(plane);
		shape = gPhysics->createShape(PxPlaneGeometry(), *material);
		shape->setLocalPose(offset);
	}
	else if (geometryType == PxGeometryType::eSPHERE)
	{
		PxReal radius = jcfg.value("radius", 1.f);;
		shape = gPhysics->createShape(PxSphereGeometry(radius), *material);
	}
	else if (geometryType == PxGeometryType::eCAPSULE)
	{
		if (jcfg.count("controller") == 0)
			Utils::fatal("Capsules not implemented yet");
	}
	else if (geometryType == PxGeometryType::eCONVEXMESH)
	{
		
		std::string col_mesh_name = jcfg.value("collision_mesh", "");
		const CCollisionMesh* mesh = EngineResources.getResource(col_mesh_name)->as<CCollisionMesh>();
		Utils::dbg("Collision mesh has %d vtxs\n", mesh->header.num_vertex);

		PxDefaultMemoryOutputStream writeBuffer;
		PxU8* cooked_data = nullptr;
		uint32_t cooked_size = 0;

		if (mesh->cooked_data.empty()) {
			// Generate the collision mesh from our exported mesh
			assert(strcmp(mesh->header.vertex_type_name, "Pos") == 0);

			PxConvexMeshDesc meshDesc;
			meshDesc.points.count = mesh->header.num_vertex;
			meshDesc.points.stride = sizeof(PxVec3);
			meshDesc.points.data = mesh->vertices.data();

			// Swap faces for physics
			//meshDesc.flags |= PxConvexFlag::eFLIPNORMALS;

			meshDesc.indices.count = mesh->header.num_indices;
			meshDesc.indices.stride = mesh->header.bytes_per_index;
			meshDesc.indices.data = mesh->indices.data();
			meshDesc.flags = PxConvexFlag::eCOMPUTE_CONVEX | PxConvexFlag::e16_BIT_INDICES;

			// We could save this cooking process
			PxTolerancesScale scale;
			PxCooking *cooking = PxCreateCooking(PX_PHYSICS_VERSION, gPhysics->getFoundation(), PxCookingParams(scale));

			PxConvexMeshCookingResult::Enum result;
			bool status = cooking->cookConvexMesh(meshDesc, writeBuffer, &result);
			assert(status);

			// Append the cooked buffer after the collision mesh render format
			CFileDataSaver fds(mesh->getName().c_str(), "ab");
			cooked_data = writeBuffer.getData();
			cooked_size = writeBuffer.getSize();
			fds.writeBytes(cooked_data, cooked_size);
		}
		else {
			// Append the cooked buffer after the collision mesh render format
			cooked_data = (PxU8*)mesh->cooked_data.data();
			cooked_size = (uint32_t)mesh->cooked_data.size();
		}

		assert(cooked_data);
		assert(cooked_size > 0);
		PxDefaultMemoryInputData readBuffer = PxDefaultMemoryInputData(cooked_data, cooked_size);

		// writeBuffer could be saved to avoid the cooking in the next execution.
		PxConvexMesh* conv_mesh = gPhysics->createConvexMesh(readBuffer);

			shape = gPhysics->createShape(PxConvexMeshGeometry(conv_mesh), *material);
		assert(shape);

		// Create and register a mesh for rendering the collision mesh
		CMesh* render_mesh = mesh->createRenderMesh();
		char res_name[64];
		sprintf(res_name, "Physics_%p", render_mesh);
		render_mesh->setNameAndType(res_name, getResourceTypeFor<CMesh>());
		EngineResources.registerResource(render_mesh);

		// Bind the render mesh as userData of the SHAPE, not the ACTOR.
		shape->userData = render_mesh;
	}
	else if (geometryType == PxGeometryType::eTRIANGLEMESH)
	{
		std::string col_mesh_name = jcfg.value("collision_mesh", "");
		const CCollisionMesh* mesh = EngineResources.getResource(col_mesh_name)->as<CCollisionMesh>();
		Utils::dbg("Collision mesh has %d vtxs\n", mesh->header.num_vertex);

		PxDefaultMemoryOutputStream writeBuffer;
		PxU8* cooked_data = nullptr;
		uint32_t cooked_size = 0;

		if (mesh->cooked_data.empty()) {
			// Generate the collision mesh from our exported mesh
			assert(strcmp(mesh->header.vertex_type_name, "Pos") == 0);

			PxTriangleMeshDesc meshDesc;
			meshDesc.points.count = mesh->header.num_vertex;
			meshDesc.points.stride = sizeof(PxVec3);
			meshDesc.points.data = mesh->vertices.data();

			// Swap faces for physics
			meshDesc.flags |= PxMeshFlag::eFLIPNORMALS;

			// Indices
			meshDesc.triangles.count = mesh->header.num_indices / 3;
			meshDesc.triangles.stride = 3 * mesh->header.bytes_per_index;
			meshDesc.triangles.data = mesh->indices.data();
			if (mesh->header.bytes_per_index == 2)
			meshDesc.flags |= PxMeshFlag::e16_BIT_INDICES;

			// We could save this cooking process
			PxTolerancesScale scale;
			PxCooking *cooking = PxCreateCooking(PX_PHYSICS_VERSION, gPhysics->getFoundation(), PxCookingParams(scale));

			PxTriangleMeshCookingResult::Enum result;
			bool status = cooking->cookTriangleMesh(meshDesc, writeBuffer, &result);
			assert(status);

			// Append the cooked buffer after the collision mesh render format
			CFileDataSaver fds(mesh->getName().c_str(), "ab");
			cooked_data = writeBuffer.getData();
			cooked_size = writeBuffer.getSize();
			fds.writeBytes(cooked_data, cooked_size);
		}
		else {
			// Append the cooked buffer after the collision mesh render format
			cooked_data = (PxU8*) mesh->cooked_data.data();
			cooked_size = (uint32_t)mesh->cooked_data.size();
		}

		assert(cooked_data);
		assert(cooked_size > 0);
		PxDefaultMemoryInputData readBuffer = PxDefaultMemoryInputData(cooked_data, cooked_size);
    
		// writeBuffer could be saved to avoid the cooking in the next execution.
		PxTriangleMesh* tri_mesh = gPhysics->createTriangleMesh(readBuffer);
		shape = gPhysics->createShape(PxTriangleMeshGeometry(tri_mesh), *material);
		assert(shape);

		// Create and register a mesh for rendering the collision mesh
		CMesh* render_mesh = mesh->createRenderMesh();
		char res_name[64];
		sprintf(res_name, "Physics_%p", render_mesh);
		render_mesh->setNameAndType(res_name, getResourceTypeFor<CMesh>());
		EngineResources.registerResource(render_mesh);

		// Bind the render mesh as userData of the SHAPE, not the ACTOR.
		shape->userData = render_mesh;
	}

	if (jcfg.value("trigger", false))
	{
		shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
		shape->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);
	}

	// Shape offset
	if (jcfg.count("offset")) {
		CTransform trans;
		trans.load(jcfg["offset"]);
		shape->setLocalPose(toPxTransform(trans));
	}

	assert(shape);

	setupFiltering(
		shape,
		loadFilter(jcfg, "group"),
		loadFilter(jcfg, "mask"));

	actor->attachShape(*shape);
	shape->release();
	return true;
}

void CModulePhysics::readMaterials() {
	json jCfg = Utils::loadJson("data/modules/physics.json");
	auto& jMaterialsCfg = jCfg["materials"];
	std::string name;
	for (auto& jMaterialEntry : jMaterialsCfg) {
		PxMaterial* material;
		name = jMaterialEntry["name"];
		auto& jMaterial = jMaterialEntry["material"];
		float staticFriction = jMaterial.value("static_friction", gMaterial->getStaticFriction());
		float dynamicFriction = jMaterial.value("dynamic_friction", gMaterial->getDynamicFriction());
		float restitution = jMaterial.value("restitution", gMaterial->getRestitution());
		material = gPhysics->createMaterial(staticFriction, dynamicFriction, restitution);

		gMaterials[name] = material;
	}
}

CModulePhysics::FilterGroup CModulePhysics::getFilterByName(const std::string & name)
{
  if (_strcmpi("player", name.c_str()) == 0)
    return CModulePhysics::FilterGroup::Player;
  else if (_strcmpi("enemy", name.c_str()) == 0)
    return CModulePhysics::FilterGroup::Enemy;
  else if (_strcmpi("characters", name.c_str()) == 0)
    return CModulePhysics::FilterGroup::Characters;
  else if (_strcmpi("wall", name.c_str()) == 0)
    return CModulePhysics::FilterGroup::Wall;
  else if (_strcmpi("floor", name.c_str()) == 0)
    return CModulePhysics::FilterGroup::Floor;
  else if (_strcmpi("scenario", name.c_str()) == 0)
    return CModulePhysics::FilterGroup::Scenario;
  else if (_strcmpi("trigger", name.c_str()) == 0)
		return CModulePhysics::FilterGroup::Trigger;
  else if (_strcmpi("ragdoll", name.c_str()) == 0)
		return CModulePhysics::FilterGroup::Ragdoll;
  else if (_strcmpi("DynamicScenarioNoCollision", name.c_str()) == 0)
	  return CModulePhysics::FilterGroup::DynamicScenarioNoCollision;
	else if (_strcmpi("DynamicScenario", name.c_str()) == 0)
		return CModulePhysics::FilterGroup::DynamicScenario;
  else if (_strcmpi("enemyHitboxes", name.c_str()) == 0)
	  return CModulePhysics::FilterGroup::EnemyHitboxes;
  else if (_strcmpi("pickUps", name.c_str()) == 0)
	  return CModulePhysics::FilterGroup::PickUps;
  else if (_strcmpi("bullets", name.c_str()) == 0)
	  return CModulePhysics::FilterGroup::Bullets;
	else if (_strcmpi("particles", name.c_str()) == 0)
		return CModulePhysics::FilterGroup::Particles;
	else if (_strcmpi("low_poly", name.c_str()) == 0)
		return CModulePhysics::FilterGroup::LowPoly;
	else if (_strcmpi("high_poly", name.c_str()) == 0)
		return CModulePhysics::FilterGroup::HighPoly;
	else if (_strcmpi("scenario_low", name.c_str()) == 0)
		return CModulePhysics::FilterGroup::ScenarioLow;
	else if (_strcmpi("scenario_high", name.c_str()) == 0)
		return CModulePhysics::FilterGroup::ScenarioHigh;

  return CModulePhysics::FilterGroup::All;
}

PxU32 CModulePhysics::loadFilter(const json& jConfig, const std::string& name, PxU32 defaultFilter) {
	PxU32 r = 0;
	if (jConfig.count(name) == 1) {
		if (jConfig[name].is_array()) {
			for (std::string str : jConfig[name])
				r |= getFilterByName(str);
		}
		else
			r = getFilterByName(jConfig[name]);
	}
	else
		return defaultFilter;

	return r;
}

void CModulePhysics::CustomSimulationEventCallback::onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 nbPairs) {
	PROFILE_FUNCTION("onCollision");
	for (PxU32 i = 0; i < nbPairs; i++)
	{
		// ignore pairs when shapes have been deleted
		if (pairs[i].flags & (PxContactPairFlag::eREMOVED_SHAPE_0 | PxContactPairFlag::eREMOVED_SHAPE_1))
			continue;

		CHandle h_comp_colldier;
		h_comp_colldier.fromVoidPtr(pairHeader.actors[0]->userData);

		CHandle h_comp_other;
		h_comp_other.fromVoidPtr(pairHeader.actors[1]->userData);

		CEntity* e_collider = h_comp_colldier.getOwner();
		CEntity* e_other = h_comp_other.getOwner();

		if (!e_collider || !e_other)
			continue;

		if (pairs[i].events == PxPairFlag::eNOTIFY_TOUCH_FOUND)
		{
			TMsgEntityCollisionEnter msg;

			// Notify the actor 1
			msg.h_entity = h_comp_other.getOwner();
			e_collider->sendMsg(msg);

			// Notify the actor 0
			msg.h_entity = h_comp_colldier.getOwner();
			e_other->sendMsg(msg);

			// Call lua event
			CEntity* collisionEntity = msg.h_entity;
			
			if (!collisionEntity)
				return;

			TCompName* entityNameComp = collisionEntity->getComponent<TCompName>();
			if (entityNameComp) {
				const std::string eventName = std::string("onCollisionEnter") + entityNameComp->getName();
				EngineScripting.call(eventName, h_comp_colldier.getOwner(), h_comp_other.getOwner());
			}
		}
		else if (pairs[i].events == PxPairFlag::eNOTIFY_TOUCH_PERSISTS)
		{
			TMsgEntityCollisionStay msg;

			// Notify the actor 1
			msg.h_entity = h_comp_other.getOwner();
			e_collider->sendMsg(msg);

			// Notify the actor 0
			msg.h_entity = h_comp_colldier.getOwner();
			e_other->sendMsg(msg);

			// Call lua event
			CEntity* collisionEntity = msg.h_entity;

			if (!collisionEntity)
				return;

			TCompName* entityNameComp = collisionEntity->getComponent<TCompName>();
			if (entityNameComp) {
				const std::string eventName = std::string("onCollisionStay") + entityNameComp->getName();
				EngineScripting.call(eventName, h_comp_colldier.getOwner(), h_comp_other.getOwner());
			}
		}
		else if (pairs[i].events == PxPairFlag::eNOTIFY_TOUCH_LOST)
		{
			TMsgEntityCollisionExit msg;

			// Notify the actor 1
			msg.h_entity = h_comp_other.getOwner();
			e_collider->sendMsg(msg);

			// Notify the actor 0
			msg.h_entity = h_comp_colldier.getOwner();
			e_other->sendMsg(msg);

			// Call lua event
			CEntity* collisionEntity = msg.h_entity;

			if (!collisionEntity)
				return;

			TCompName* entityNameComp = collisionEntity->getComponent<TCompName>();
			if (entityNameComp) {
				const std::string eventName = std::string("onCollisionExit") + entityNameComp->getName();
				EngineScripting.call(eventName, h_comp_colldier.getOwner(), h_comp_other.getOwner());
			}
		}
	}
}

void CModulePhysics::CustomSimulationEventCallback::onTrigger(PxTriggerPair* pairs, PxU32 count)
{
  PROFILE_FUNCTION("onTrigger");
  for (PxU32 i = 0; i < count; i++)
  {
    // ignore pairs when shapes have been deleted
    if (pairs[i].flags & (PxTriggerPairFlag::eREMOVED_SHAPE_TRIGGER | PxTriggerPairFlag::eREMOVED_SHAPE_OTHER))
      continue;

    CHandle h_comp_trigger;
    h_comp_trigger.fromVoidPtr(pairs[i].triggerActor->userData);
		CHandle h_comp_other;
		h_comp_other.fromVoidPtr(pairs[i].otherActor->userData);

		CEntity* e_trigger = h_comp_trigger.getOwner();
		CEntity* e_other = h_comp_other.getOwner();
		if (!e_trigger || !e_other)
			continue;

		if (pairs[i].status == PxPairFlag::eNOTIFY_TOUCH_FOUND) {
			TMsgEntityTriggerEnter msg;

			// Notify the trigger someone entered
			msg.h_entity = h_comp_other.getOwner();
			e_trigger->sendMsg(msg);

			// Notify that someone he entered in a trigger
			msg.h_entity = h_comp_trigger.getOwner();
			e_other->sendMsg(msg);

			// Call lua event
			CEntity * triggerEntity = msg.h_entity;
			if (triggerEntity) {
				TCompName * entityNameComp = triggerEntity->getComponent<TCompName>();
				if (entityNameComp) {
					const std::string eventName = std::string("onEnter") + entityNameComp->getName();
					EngineScripting.call(eventName, h_comp_other.getOwner(), h_comp_trigger.getOwner());
				}
			}
		}
		else if (pairs[i].status == PxPairFlag::eNOTIFY_TOUCH_LOST) {
			TMsgEntityTriggerExit msg;

			// Notify the trigger someone exit
			msg.h_entity = h_comp_other.getOwner();
			e_trigger->sendMsg(msg);

			// Notify that someone he exit a trigger
			msg.h_entity = h_comp_trigger.getOwner();
			e_other->sendMsg(msg);

			// Call lua event
			CEntity * triggerEntity = msg.h_entity;
			TCompName * entityNameComp = triggerEntity->getComponent<TCompName>();
			if (entityNameComp) {
				const std::string eventName = std::string("onExit") + entityNameComp->getName();
				EngineScripting.call(eventName, h_comp_other.getOwner(), h_comp_trigger.getOwner());
			}
		}
  }
}

void CModulePhysics::debugInMenuJoints() {
  if (ImGui::TreeNode("Joints")) {
    for (auto& c : joints)
      debugInMenuJoint(c);
    ImGui::TreePop();
  }
}

void CModulePhysics::debugActor(const char* title, PxRigidActor* actor) {
  if (actor == nullptr) {
    ImGui::Text("%s NULL", title);
    return;
  }
  ImGui::Text("%s", title); ImGui::SameLine();
  CHandle h_collider;
  h_collider.fromVoidPtr(actor->userData);
  CEntity* e = h_collider.getOwner();
  if (e)
    e->debugInMenu();
  else
    ImGui::Text("NULL");
}

void CModulePhysics::TJoint::create() {
  PxRigidActor* actor0 = nullptr;
  PxRigidActor* actor1 = nullptr;
  
  TCompCollider* c0 = obj0.h_collider;
  if (!c0) {
    CEntity* e = getEntityByName(obj0.name);
    if (e)
      obj0.h_collider = e->getComponent<TCompCollider>();
    c0 = obj0.h_collider;
  }
  if (c0)
    actor0 = c0->actor;

  TCompCollider* c1 = obj1.h_collider;
  if (!c1) {
    CEntity* e = getEntityByName(obj1.name);
    if (e)
      obj1.h_collider = e->getComponent<TCompCollider>();
    c1 = obj1.h_collider;
  } 
  if (c1) {
    actor1 = c1->actor;
  }
  
  PxTransform frame0 = toPxTransform(obj0.transform);
  PxTransform frame1 = toPxTransform(obj1.transform);
  if(joint_type == "spherical")
    px_joint = PxSphericalJointCreate(*gPhysics, actor0, frame0, actor1, frame1);
  else if (joint_type == "fixed")
    px_joint = PxFixedJointCreate(*gPhysics, actor0, frame0, actor1, frame1);
  else if (joint_type == "distance")
    px_joint = PxDistanceJointCreate(*gPhysics, actor0, frame0, actor1, frame1);
	else if (joint_type == "revolute")
		px_joint = PxRevoluteJointCreate(*gPhysics, actor0, frame0, actor1, frame1);
  else 
    Utils::fatal("Invalid joint type %s\n", joint_type.c_str());
}

void CModulePhysics::debugInMenuJointObj(TJoint& j, physx::PxJointActorIndex::Enum idx) {
  std::string title = (idx == PxJointActorIndex::eACTOR0) ? "Obj0 " : "Obj1 ";
  TJoint::TObj& obj = (idx == PxJointActorIndex::eACTOR0) ? j.obj0 : j.obj1;
  title += obj.name;
  if (ImGui::TreeNode(title.c_str())) {
    // Ask physx
    PxTransform pxtrans = j.px_joint->getLocalPose(idx);
    // Save it locally
    obj.transform = toTransform(pxtrans);

    // Update phyxs trans if changed from imgui
    if (obj.transform.renderInMenu()) {
      pxtrans = toPxTransform(obj.transform);
      j.px_joint->setLocalPose(idx, pxtrans);
    }
    ImGui::TreePop();
  }
}

void CModulePhysics::debugInMenuJoint(TJoint& j) {
  if (!j.px_joint) {
    ImGui::LabelText("Joint Type", "Invalid Joint");
    return;
  }

  auto flags = j.px_joint->getConstraintFlags();
  if (flags.isSet(PxConstraintFlag::eBROKEN))
    ImGui::Text("Is Broken!");

  ImGui::LabelText("Joint Type", "%s", j.px_joint->getConcreteTypeName());
  debugInMenuJointObj(j, PxJointActorIndex::eACTOR0);
  debugInMenuJointObj(j, PxJointActorIndex::eACTOR1);
  if (j.joint_type == "spherical") {
    PxSphericalJoint* custom = (PxSphericalJoint*)j.px_joint;
    //spherical->setLimitCone();
  }
  else if (j.joint_type == "fixed") {
    PxFixedJoint* custom = (PxFixedJoint*)j.px_joint;
  }
  else if (j.joint_type == "distance")
  {
    PxDistanceJoint* custom = (PxDistanceJoint*)j.px_joint;
    float distance = custom->getDistance();
    ImGui::LabelText("Distance", "%f", distance);
    // ..
  }
  if( ImGui::SmallButton( "Break"))
    j.px_joint->setBreakForce(0.0f, 0.0f);
  ImGui::Separator();
}
