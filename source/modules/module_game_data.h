#pragma once
#include "modules/module.h"

/* Use this module to load all configuration classes you may need for our game.
Include them here and call it on start so their information gets loaded. */
class CModuleGameData : public IModule
{
public:
	CModuleGameData(const std::string & name);
	bool start() override;
	void update(float dt) override {}
	void renderDebug() override{}
	void renderInMenu() override;
};