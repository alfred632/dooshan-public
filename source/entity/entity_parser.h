#pragma once

// This will store the current loading process of a single file
struct TEntityParseContext {

  // To be used when one file dispatches the loading of other scenes
  // or prefabs
  TEntityParseContext* parent = nullptr;

  // True only when we are parsing a prefab source
  bool                 parsing_prefab = false;

  // In case our prefab creates other prefabs which creates...
  int                  recursion_level = 0;

  // If nonBlocking load we will save how many entities have been loaded between updates from the same file
  int                  entitiesLoadedCount = 0;

  // The current filename being loaded
  std::string          filename;

  // An array of the handles currenty registered 
  VHandles             entities_loaded;

  // In case one entity has requested parsing this file
  CHandle              entity_starting_the_parse;

  // Current entity being parsed. Makes sense only when parsing components of that entity
  CHandle              current_entity;

  // Absolute transform where all instances should be placed relative to
  // Make sense for the comp_transform
  CTransform           root_transform;

	// Arguments to search and replace in prefabs if forwarding of arguments is allowed
	json								 prefab_args;

  // When creating instances that shouldn't be instanced. Used when registering prefabs for
  // instancing. This way we don't add them.
  bool is_registering_for_instancing = false;

  // Find an entity in the list of entities parsed in this file
  // or search in the parent contexts, or search in the global dict
  CHandle findEntityByName(const std::string& name) const;

  TEntityParseContext() = default;
  TEntityParseContext(TEntityParseContext& another, const CTransform& delta_transform);
};

bool parseJson(const json & j_scene, TEntityParseContext& ctx, int depth = 0, float timeLimit = 0.0f);
bool parseScene(const std::string& filename, TEntityParseContext& ctx, int depth = 0, float timeLimit = 0.0f);

