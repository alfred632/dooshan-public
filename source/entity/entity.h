#pragma once

#include "handle/handle.h"
#include "components/common/comp_base.h"
#include "base_callback.h"

struct TMsgToogleEntity;
struct TMsgToogleComponent;

class CEntity : public TCompBase {
	CHandle comps[CHandle::max_types]; // Array of components.

public:
	~CEntity();

	/* Return a component handle. */
	CHandle getComponent(uint32_t comp_type) const {
		assert(comp_type < CHandle::max_types);
		return comps[comp_type];
  }
	
	template< typename TComp >
	CHandle getComponent() const {
		auto om = getObjectManager<TComp>();
		assert(om);
		return comps[om->getType()];
	}

	void debugInMenu();
	void renderDebug();

	void setComponent(uint32_t comp_type, CHandle new_comp);
	void setComponent(CHandle new_comp);
	void load(const json& j, TEntityParseContext & ctx);

	static void registerMsgs();

	const char* getName() const;

	template< class TMsg >
	void sendMsg(const TMsg& msg) {
		auto range = all_registered_msgs.equal_range(TMsg::getMsgID());
		while (range.first != range.second) {
			const auto& slot = range.first->second;

			// Si YO como entidad tengo ese component activo...
			CHandle h_comp = comps[slot.comp_type];
			if (h_comp.isValid())
			slot.callback->sendMsg(h_comp, &msg);
			++range.first;
		}
	}

	template<class TComp, class TMsg>
	void sendMsgToComp(const TMsg & msg) {
		// Get the component type.
		uint32_t componentType = getObjectManager<TComp>()->getType();

		// Get the component handle. If not valid, ignore.
		CHandle comp = comps[componentType];
		if (!comp.isValid()) return;

		// Now, get all components registered to the msg. Send it to the component
		// that is indicated.
		auto rangeCompsSubsToMsg = all_registered_msgs.equal_range(TMsg::getMsgID());
		while (rangeCompsSubsToMsg.first != rangeCompsSubsToMsg.second) {
			const auto & slot = rangeCompsSubsToMsg.first->second;
			if (slot.comp_type == componentType) {
				slot.callback->sendMsg(comp, &msg);
				return;
			}
			++rangeCompsSubsToMsg.first;
		}
	}

private:
	void onToggleEntity(const TMsgToogleEntity & msg);
};

template<> void CEntity::sendMsg(const TMsgToogleEntity & msg);
template<> void CEntity::sendMsg(const TMsgToogleComponent & msg);

// Forward declaring 
template<> CObjectManager< CEntity > * getObjectManager<CEntity>();

extern CHandle getEntityByName(const std::string & name);
