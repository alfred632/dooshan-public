#pragma once

#include "mcv_platform.h"
#include "resources/resource.h"
#include "utils/track.h"
#include <set>

class CMaterial;

namespace particles
{
	// Represents a range, if getRandom is called and is a range, returns a value between minValue and maxValue.
	struct TRange
	{
		float minValue = 0.f;
		float maxValue = 0.f;
		bool isRange = false;
		float getRandom() const;
	};

	typedef std::string EmitterColor;
	static std::set<EmitterColor> colorModes = { "normal", "random_color", "random_dynamic_color" };

	typedef std::string EmitterSize;
	static std::set<EmitterSize> sizeModes = { "normal", "random_size_and_modifier", "random_size_dynamic_and_modifier" };

	typedef std::string EmitterUVs;
	static std::set<EmitterUVs> uvsModes = { "normal", "random_uv", "random_uv_dynamic" };

	typedef std::string LightTypes;
	static std::set<LightTypes> lightModes = { "point_light" };


	// Emitters hold all the information about how to spawn particles, their colors, sizes, ...
	struct TEmitter : public IResource
	{
		// Textures.
		const CMaterial * material = nullptr;

		std::string particleType = "billboard";
		json jConfig;

		// Particle generation
		TRange count; // How many new particles per emit
		unsigned maxCount = 1; // Maximum particles alive in a particle emitter
		TRange interval;
		VEC3 direction = VEC3::Up; // Direction of the emitter.
		float coneAngle = 0.0f; // Cone angle for emitting particles. Specified in degrees for ease of use.
		TRange speed; // Speed at which particles move.
		TRange gravityFactor; // Controls if particle move upwards or downwards when emitted.
		TRange duration;
		float stretch = 1.0f;
		TRange rotationSensitivity = { 7.0f, 10.0f, true };

		// Particle start location
		bool localSpace = false;
		bool start3DBox = false;
		VEC3 boxSize = VEC3(0.0f, 0.0f, 0.0f);
		bool start3DSphere = false;
		float sphereRadius = 0.0f;
		bool start3DCircle = false;
		TRange circleRadius;

		// Particle spawning methods.
		EmitterColor colorMethod = "normal";
		EmitterSize sizeMethod = "normal";
		EmitterUVs uvMethod = "normal";

		// Particle life parameters
		TTrack<VEC4> colors_min;
		TTrack<VEC4> colors_max;
		TTrack<VEC3> sizes;

		// Animated particles
		VEC2 frameSize = VEC2::One; // Size of each frame. By default x: 1.0, y: 1.0 as only one image is expected.
		int initialFrame = 0; // Starting frame. By default 0.
		int frameCount = 1; // Number of frames the particle has.
		bool blendBetweenTexturesInUv = true; // Whether we blend when moving between uvs.
		float uv_offset_movement_during_lifetime = 1.0f; // How much offset will a particle move during it's lifetime.

		// Particle gravity to point
		VEC3 gravityPoint = VEC3::Zero;
		bool localGravityPoint = true;
		TRange gravityPointFactor;

		// Lights.
		LightTypes lightType = "point_light";
		Vector4 light_base_color = Vector4(1, 1, 1, 1);
		float light_base_intensity = 1.0f;
		float light_base_radius = 1.0f;

		bool spawn_lights = false;
		float particles_to_light_ratio = 0.0f;
		bool use_particles_color_for_light = true; // Multiplies the light color by the particles color.
		bool size_affects_range = true;
		bool alpha_affects_intensity = true;
		float range_multiplier = 1.0f;
		float intensity_multiplier = 1.0f;
		int maximum_lights = 20.0;

		// Active emitter time
		float emitterDuration = 0.0f; // How long the emitter lasts before stopping.
		float stopNewEmitsAfter = 0.0f; // Stops emitting after x time.
		bool deleteEmitterOwner = false; // If true, the entity with the emitter gets removed.

		CCteBuffer<TCtesParticles> * ctes = nullptr;

		static const uint32_t max_colors = 12;
		static const uint32_t max_sizes = 12;
		void sampleColorsOverTime();
		void sampleSizesOverTime();

		// How to render this emitter...
		bool is_stretched_billboard = false;
		bool velocity_affects_stretch = false;
		bool random_rotation = true;
		float rotation_offset = 0.0f;
		static const uint32_t invalid_id = ~0;   // All ones
		uint32_t         render_group = invalid_id;
		uint32_t         particles_type_idx = invalid_id;
		bool isRegistered() const { return render_group != invalid_id; }

		// PhysX
		bool is_physx_based = false;
		bool two_way_interaction = true;
		float mass = 0.0f;
		float damping = 1.0f;
		float rest_offset = 1.0f;
		float contact_offset = rest_offset * 2;
		float restitution = 1.0f;
		float dynamic_friction = 0.0f;
		float static_friction = 0.0f;
		bool dynamic_collision = true;
		bool default_gravity = true;
		Vector3 physx_gravity = Vector3(0.0f, -9.81f, 0.0f);
		bool remove_on_contact = false;
		float max_distance_per_frame;
		uint32_t physXMask;

		// !!! Does not hold current particle count for emitter, it's an internal counter for assigning an ID 
		unsigned int particleCount = 0;

		void renderInMenu() override; 	
    	//void onFileChanged(const std::string& filename) override;
	};
}
