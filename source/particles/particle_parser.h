#pragma once

#include "mcv_platform.h"
#include "particles/particle_emitter.h"

namespace particles
{
	class CParser
	{
		EmitterColor emitterColorConfigStringToEnum(const std::string & configName);
		EmitterSize emitterSizeConfigStringToEnum(const std::string & configName);
		EmitterUVs emitterUVConfigStringToEnum(const std::string & configName);
	public:
		bool loadEmitter(TEmitter& emitter, const json& jData);
		bool saveEmitter(TEmitter& emitter);

		TRange loadRange(const json& jData, const std::string& attr, const TRange& defaultValue);
		void saveRange(json& jData, const TRange& range);
	};
}
