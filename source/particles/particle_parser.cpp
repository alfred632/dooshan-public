#pragma once

#include "mcv_platform.h"
#include "particles/particle_parser.h"
#include "render/textures/material.h"
#include "engine.h"
#include <fstream>
#include <iomanip>
#include "utils/track.h"

#include "render/textures/material_buffers.h"

class CParticleEmiterResourceType : public CResourceType {
public:
	const char* getExtension(int idx) const override { return "particles"; }
	const char* getName() const override {
		return "Particles";
	}
	IResource* create(const std::string& name) const override {
		particles::TEmitter* emitter = new particles::TEmitter;
		particles::CParser parser;
		const json jData = Utils::loadJson(name);
		bool is_ok = parser.loadEmitter(*emitter, jData);
		assert(is_ok);
		emitter->setNameAndType(name, this);
		return emitter;
	}
};

template<>
const CResourceType* getResourceTypeFor<particles::TEmitter>() {
	static CParticleEmiterResourceType resource_type;
	return &resource_type;
}

namespace particles
{
	// Buffer for particles materials. We create it here as we only use it for particles materials.
	class ParticlesConstantBuffers : public MaterialConstantBuffers {
	public:
		ParticlesConstantBuffers() {
			auto ctes = new CCteBuffer<TCtesParticles>(CTE_BUFFER_SLOT_PARTICLES);
			ctes->create("ParticlesEmitter");
			ct_buffer = ctes;
		}

		void load(const json & dataToLoad) {}
		void load(TEmitter& emitter) {
			auto ctes = (CCteBuffer<TCtesParticles> *)ct_buffer;
			ctes->psystem_frame_size = emitter.frameSize;
			ctes->psystem_nframes.x = 1.0f / emitter.frameSize.x;
			ctes->psystem_nframes.y = 1.0f / emitter.frameSize.y;
			ctes->psystem_stretched_billboard = emitter.is_stretched_billboard;
			ctes->psystem_stretch = emitter.stretch;
			ctes->psystem_velocity_affects_stretch = emitter.velocity_affects_stretch;
			ctes->psystem_random_rotation = emitter.random_rotation;
			ctes->psystem_rotation_offset = emitter.rotation_offset;
			ctes->psystem_random_color = emitter.colorMethod == "normal" ? false : true;
			ctes->psystem_random_dynamic_color = emitter.colorMethod == "random_dynamic_color" ? true : false;
			ctes->psystem_random_size = emitter.sizeMethod == "normal" ? false : true;
			ctes->psystem_random_uvs = emitter.sizeMethod == "normal" ? false : true;
			ctes->psystem_random_uvs_change_with_time = emitter.sizeMethod == "random_uv_dynamic" ? true : true;
			ctes->psystem_uvs_blend = emitter.blendBetweenTexturesInUv;
			ctes->psystem_uvs_speed_change = emitter.uv_offset_movement_during_lifetime;
			ctes->updateGPU();
		}
	};

	bool CParser::loadEmitter(TEmitter& emitter, const json& jData)
	{
		emitter.jConfig = jData;

		// Animated particles
		emitter.frameCount = jData.value<int>("frame_count", emitter.frameCount);
		emitter.initialFrame = jData.value<int>("initial_frame", emitter.initialFrame);
		emitter.frameSize = loadVector2(jData, "frame_size", emitter.frameSize);
		emitter.particles_type_idx = jData.value<int>("particles_type", 0);
		emitter.uv_offset_movement_during_lifetime = jData.value<float>("uv_offset_movement_during_lifetime", emitter.uv_offset_movement_during_lifetime);
		emitter.blendBetweenTexturesInUv = jData.value("blend_between_textures", emitter.blendBetweenTexturesInUv);

		// How the particle is rendered
		emitter.particleType = jData.value("type", emitter.particleType);
		emitter.particles_type_idx = EngineParticles.particle_type_names[emitter.particleType];
		emitter.random_rotation = jData.value("random_rotation", true);
		if (!emitter.random_rotation) {
			emitter.rotation_offset = jData.value("rotation_offset", emitter.rotation_offset);
		}
		emitter.is_stretched_billboard = jData.value("is_stretched_billboard", false);
		assert(emitter.particleType == "billboard" || !emitter.is_stretched_billboard && "Stretched billboard incompatible with non billboard particles");
		if (emitter.is_stretched_billboard) {
			emitter.stretch = jData.value("stretch", emitter.stretch);
			emitter.velocity_affects_stretch = jData.value("velocity_affects_stretch", emitter.velocity_affects_stretch);
		}

		// material
		ParticlesConstantBuffers * ctes = nullptr;
		if (jData.count("material")) {
			emitter.material = EngineResources.getResource(jData["material"])->as<CMaterial>();
		}
		else {
			// We need to create one, using defaults close to the particles
			CMaterial * material = new CMaterial();
			
			// Create a fake json... it might be easier to just delegate the job to the json
			json j = json::object();
			j["textures"]["albedo"] = jData["texture"];
			bool is_additive = jData.value<bool>("additive", false);
			std::string default_tech = is_additive ? "instances_particles_additive.tech" : "instances_particles_combinative.tech";
			j["technique"] = jData.value<std::string>("technique", default_tech);
			j["casts_shadows"] = jData.value<bool>("casts_shadows", false);;
			j["category"] = jData.value<std::string>("category", "particles");
			bool is_ok = material->create(j);

			// Assign a unique name and register it in the resources manager
			char name[64];
			sprintf(name, "particles_%p.material", &emitter);
			material->setNameAndType(name, getResourceTypeFor<TEmitter>());
			assert(is_ok);

			emitter.material = material;

			// Only if the material is custom
			ctes = new ParticlesConstantBuffers();
			material->setCtesMaterialExtra(ctes);
			emitter.ctes = (CCteBuffer<TCtesParticles> *)ctes->getCtBuffer();
		}

		// Particle generation
		emitter.count = loadRange(jData, "count", emitter.count);
		emitter.maxCount = jData.value<unsigned>("max_count", emitter.maxCount);
		emitter.interval = loadRange(jData, "interval", emitter.interval);
		emitter.direction = loadVector3(jData, "direction", emitter.direction);
		emitter.coneAngle = jData.value<float>("cone_angle", emitter.coneAngle);
		emitter.speed = loadRange(jData, "speed", emitter.speed);
		emitter.gravityFactor = loadRange(jData, "gravity_factor", emitter.gravityFactor);
		emitter.duration = loadRange(jData, "duration", emitter.duration);
		if (emitter.particles_type_idx != 0)
			if (jData.count("rotation_sensitivity") > 0)
				emitter.rotationSensitivity = loadRange(jData, "rotation_sensitivity", emitter.rotationSensitivity);

		// PhysX
		emitter.is_physx_based = jData.value("physx_based", emitter.is_physx_based);
		emitter.default_gravity = jData.value("default_gravity", emitter.default_gravity);
		emitter.physx_gravity = loadVector3(jData, "physx_gravity");
		emitter.two_way_interaction = jData.value("two_way_interaction", emitter.two_way_interaction);
		emitter.mass = jData.value("mass", emitter.mass);
		emitter.damping = jData.value("damping", emitter.damping);
		emitter.rest_offset = jData.value("rest_offset", emitter.rest_offset);
		emitter.contact_offset = jData.value("contact_offset", emitter.contact_offset);
		assert(emitter.contact_offset > emitter.rest_offset && "Contact offset MUST be greater than rest offset");
		emitter.restitution = jData.value("restitution", emitter.restitution);
		emitter.dynamic_collision = jData.value("dynamic_collision", emitter.dynamic_collision);
		emitter.dynamic_friction = jData.value("dynamic_friction", emitter.dynamic_friction);
		emitter.static_friction = jData.value("static_friction", emitter.static_friction);
		emitter.remove_on_contact = jData.value("remove_on_contact", emitter.remove_on_contact);
		if (jData.count("max_distance_per_frame") > 0)
			emitter.max_distance_per_frame = jData["max_distance_per_frame"];
		else
			emitter.max_distance_per_frame = emitter.speed.maxValue * (1.0f / 60.0f);
		emitter.physXMask = EnginePhysics.loadFilter(jData, "physx_mask", EnginePhysics.ScenarioLow | EnginePhysics.Player | EnginePhysics.Enemy);

		// Particle start location
		emitter.localSpace = jData.value("localSpace", emitter.localSpace);
		emitter.start3DBox = jData.value("start3DBox", emitter.start3DBox);
		emitter.boxSize = loadVector3(jData, "boxSize", emitter.boxSize);
		if (!emitter.start3DBox) {
			emitter.start3DSphere = jData.value("start3DSphere", emitter.start3DSphere);
			emitter.sphereRadius = jData.value("sphereRadius", emitter.sphereRadius);
			if (!emitter.start3DSphere) {
				emitter.start3DCircle = jData.value("start3DCircle", emitter.start3DCircle);
				emitter.circleRadius = loadRange(jData, "circleRadius", emitter.circleRadius);
			}
		}

		// Particle gravity to point
		emitter.gravityPoint = loadVector3(jData, "gravityPoint", emitter.gravityPoint);
		emitter.localGravityPoint = jData.value<bool>("localGravityPoint", emitter.localGravityPoint);
		emitter.gravityPointFactor = loadRange(jData, "gravityPointFactor", emitter.gravityPointFactor);

		// Particle spawning methods.
		emitter.colorMethod = emitterColorConfigStringToEnum(jData.value("color_method" , ""));
		emitter.sizeMethod = emitterSizeConfigStringToEnum(jData.value("size_method", ""));
		emitter.uvMethod = emitterUVConfigStringToEnum(jData.value("uv_method", ""));

		// Particle life parameters
		// sizes
		VEC3 defaultSize = VEC3::One;
		if (jData.count("size") == 0)
			defaultSize = VEC3::One;
		else if (jData["size"].is_string())
			defaultSize = loadVector3(jData, "size");
		else if (jData["size"].is_number())
			defaultSize = VEC3(jData["size"], jData["size"], jData["size"]);
		emitter.sizes.setDefault(defaultSize);
		if (jData.count("sizes"))
		{
			for (auto& jSize : jData["sizes"])
			{
				const float ratio = jSize[0].get<float>();
				const float size = jSize[1].get<float>();
				float size_mod = 1.0;
				if(jSize.size() > 2)
					size_mod = jSize[2].get<float>();

				float min_size = size;
				if (jSize.size() > 3)
					min_size = jSize[3].get<float>();

				emitter.sizes.set(ratio, Vector3(size, min_size, size_mod));
			}
			emitter.sizes.sort();
		}

		emitter.sampleSizesOverTime();

		// colors
		const VEC4 defaultColor = loadVector4(jData, "color", VEC4::One);
		emitter.colors_min.setDefault(defaultColor);
		emitter.colors_max.setDefault(defaultColor);
		if (jData.count("colors"))
		{
			for (auto& jColor : jData["colors"])
			{
				const float ratio = jColor[0].get<float>();
				const VEC4 color = loadVector4(jColor[1]);
				emitter.colors_min.set(ratio, color);
			}
			emitter.colors_min.sort();
		}
		if (jData.count("colors_max"))
		{
			for (auto& jColor : jData["colors_max"])
			{
				const float ratio = jColor[0].get<float>();
				const VEC4 color = loadVector4(jColor[1]);
				emitter.colors_max.set(ratio, color);
			}
			emitter.colors_max.sort();
		}
		else {
			emitter.colors_max = emitter.colors_min;
		}

		emitter.sampleColorsOverTime();

		// Lights.
		emitter.spawn_lights = jData.value<bool>("emitter_spawn_lights", emitter.spawn_lights);
		emitter.lightType = jData.value<std::string>("emitter_light_type", emitter.lightType);
		emitter.light_base_color = loadVector4(jData, "emitter_light_base_color");
		emitter.light_base_intensity = jData.value<float>("emitter_light_base_intensity", emitter.light_base_intensity);
		emitter.light_base_radius = jData.value<float>("emitter_light_base_radius", emitter.light_base_radius);
		emitter.particles_to_light_ratio = jData.value<float>("emitter_particles_to_light_ratio", emitter.particles_to_light_ratio);
		emitter.use_particles_color_for_light = jData.value<bool>("emitter_use_particles_color_for_light", emitter.use_particles_color_for_light);
		emitter.size_affects_range = jData.value<bool>("emitter_size_affects_range", emitter.size_affects_range);
		emitter.alpha_affects_intensity = jData.value<bool>("emitter_alpha_affects_intensity", emitter.alpha_affects_intensity);
		emitter.range_multiplier = jData.value<float>("emitter_range_multiplier", emitter.range_multiplier);
		emitter.intensity_multiplier = jData.value<float>("emitter_intensity_multiplier", emitter.intensity_multiplier);
		emitter.maximum_lights = jData.value<int>("emitter_maximum_lights", emitter.maximum_lights);

		// Active emitter time
		emitter.emitterDuration = jData.value<float>("emitter_duration", emitter.emitterDuration);
		emitter.deleteEmitterOwner = jData.value<bool>("delete_emitter_owner", emitter.deleteEmitterOwner);
		emitter.stopNewEmitsAfter = jData.value<float>("stop_emitter_after", emitter.stopNewEmitsAfter);

		// Load cts until now.
		if (ctes)
			ctes->load(emitter);

		return true;
	}

	EmitterColor CParser::emitterColorConfigStringToEnum(const std::string & configName) {
		auto it = colorModes.find(configName);
		if (it == colorModes.end()) return "normal";
		return *it;
	}

	EmitterSize CParser::emitterSizeConfigStringToEnum(const std::string & configName) {
		auto it = sizeModes.find(configName);
		if (it == sizeModes.end()) return "normal";
		return *it;
	}

	EmitterUVs CParser::emitterUVConfigStringToEnum(const std::string & configName) {
		auto it = uvsModes.find(configName);
		if (it == uvsModes.end()) return "normal";
		return *it;
	}

	bool CParser::saveEmitter(TEmitter& emitter) {
		json json;

		// Material
		json["texture"] = emitter.material->getTexture(0)->getName();
		json["technique"] = emitter.material->tech->getName();
		json["casts_shadows"] = emitter.material->castsShadows();
		json["category"] = emitter.material->getCategoryName();

		// How the particle is rendered
		json["type"] = emitter.particleType;
		json["random_rotation"] = emitter.random_rotation;
		if (!emitter.random_rotation)
			json["rotation_offset"] = emitter.rotation_offset;
		if (emitter.is_stretched_billboard && emitter.particleType == "billboard") {
			json["is_stretched_billboard"] = emitter.is_stretched_billboard;
			json["stretch"] = emitter.stretch;
			if (emitter.velocity_affects_stretch)
				json["velocity_affects_stretch"] = emitter.velocity_affects_stretch;
		}

		// Particle generation
		saveRange(json["count"], emitter.count);
		json["max_count"] = emitter.maxCount;
		saveRange(json["interval"], emitter.interval);
		json["direction"] = std::to_string(emitter.direction.x) + " " + std::to_string(emitter.direction.y) + " " + std::to_string(emitter.direction.z);
		if (emitter.coneAngle > 0.0f)
			json["cone_angle"] = emitter.coneAngle;
		saveRange(json["speed"], emitter.speed);
		saveRange(json["duration"], emitter.duration);

		// Physics parameters
		saveRange(json["gravity_factor"], emitter.gravityFactor);
		saveRange(json["rotation_sensitivity"], emitter.rotationSensitivity);

		// Particle gravity to point
		json["gravityPoint"] = std::to_string(emitter.gravityPoint.x) + " " + std::to_string(emitter.gravityPoint.y) + " " + std::to_string(emitter.gravityPoint.z);
		json["localGravityPoint"] = emitter.localGravityPoint;
		saveRange(json["gravityPointFactor"], emitter.gravityPointFactor);
		// PhysX
		json["physx_based"] = emitter.is_physx_based;
		if (emitter.is_physx_based) {
			json["default_gravity"] = emitter.default_gravity;
			if (!emitter.default_gravity)
				json["physx_gravity"] = std::to_string(emitter.physx_gravity.x) + " " + std::to_string(emitter.physx_gravity.y) + " " + std::to_string(emitter.physx_gravity.z);
			json["two_way_interaction"] = emitter.two_way_interaction;
			if (emitter.two_way_interaction)
				json["mass"] = emitter.mass;
			json["damping"] = emitter.damping;
			json["rest_offset"] = emitter.rest_offset;
			json["contact_offset"] = emitter.contact_offset;
			json["restitution"] = emitter.restitution;
			json["dynamic_collision"] = emitter.dynamic_collision;
			json["dynamic_friction"] = emitter.dynamic_friction;
			json["static_friction"] = emitter.static_friction;
			json["remove_on_contact"] = emitter.remove_on_contact;
			json["max_distance_per_frame"] = emitter.max_distance_per_frame;
			if (emitter.jConfig.count("physx_mask") > 0)
				json["physx_mask"] = emitter.jConfig["physx_mask"];
		}

		// Particle start location
		if (emitter.localSpace)
			json["localSpace"] = emitter.localSpace;
		if (emitter.start3DBox) {
			json["start3DBox"] = emitter.start3DBox;
			json["boxSize"] = std::to_string(emitter.boxSize.x) + " " + std::to_string(emitter.boxSize.y) + " " + std::to_string(emitter.boxSize.z);
		}
		else if (emitter.start3DSphere) {
			json["start3DSphere"] = emitter.start3DSphere;
			json["sphereRadius"] = emitter.sphereRadius;
		}
		else if (emitter.start3DCircle) {
			json["start3DCircle"] = emitter.start3DCircle;
			saveRange(json["circleRadius"], emitter.circleRadius);
		}

		// Particle life parameters
		// sizes
		std::vector<TTrack<Vector3>::TKeyframe>& sizeKeyframes = emitter.sizes.getKeyframes();
		if (sizeKeyframes.size() > 0) {
			for (auto& kf : sizeKeyframes) {
				nlohmann::json element;
				element.push_back(kf.time);
				element.push_back(kf.value.x);
				element.push_back(kf.value.y);
				element.push_back(kf.value.z);
				json["sizes"].push_back(element);
			}
		}
		else {
			json["size"] = "\"" + std::to_string(emitter.sizes.getDefault().x) + " " + std::to_string(emitter.sizes.getDefault().y) + " " + std::to_string(emitter.sizes.getDefault().z) + "\"";
		}

		// colors
		std::vector<TTrack<Vector4>::TKeyframe> & colorKeyframes = emitter.colors_min.getKeyframes();
		if (colorKeyframes.size() > 0) {
			for (auto& kf : colorKeyframes) {
				nlohmann::json element;
				element.push_back(kf.time);
				element.push_back(saveVector4(kf.value));
				json["colors"].push_back(element);
			}
		}
		else
			json["color"] = saveVector4(emitter.colors_min.getDefault());

		std::vector<TTrack<Vector4>::TKeyframe> & colorMaxKeyframes = emitter.colors_max.getKeyframes();
		if (colorMaxKeyframes.size() > 0) {
			for (auto & kf : colorMaxKeyframes) {
				nlohmann::json element;
				element.push_back(kf.time);
				element.push_back(saveVector4(kf.value));
				json["colors_max"].push_back(element);
			}
		}

		// Animated particles
		if (emitter.frameCount > 1) {
			json["frame_count"] = emitter.frameCount;
			json["initial_frame"] = emitter.initialFrame;
			json["blend_between_textures"] = emitter.blendBetweenTexturesInUv;
			json["uv_offset_movement_during_lifetime"] = emitter.uv_offset_movement_during_lifetime;
			json["frame_size"] = std::to_string(emitter.frameSize.x) + " " + std::to_string(emitter.frameSize.y);
		}

		json["color_method"] = emitter.colorMethod;
		json["size_method"] = emitter.sizeMethod;
		json["uv_method"] = emitter.uvMethod;

		// Lights.
		json["emitter_spawn_lights"] = emitter.spawn_lights;
		json["emitter_light_type"] = emitter.lightType;
		json["emitter_light_base_color"] = std::to_string(emitter.light_base_color.x) + " " + std::to_string(emitter.light_base_color.y) + " " + std::to_string(emitter.light_base_color.z) + " " + std::to_string(emitter.light_base_color.w);
		json["emitter_light_base_intensity"] = emitter.light_base_intensity;
		json["emitter_light_base_radius"] = emitter.light_base_radius;
		json["emitter_particles_to_light_ratio"] = emitter.particles_to_light_ratio;
		json["emitter_use_particles_color_for_light"] = emitter.use_particles_color_for_light;
		json["emitter_size_affects_range"] = emitter.size_affects_range;
		json["emitter_alpha_affects_intensity"] = emitter.alpha_affects_intensity;
		json["emitter_range_multiplier"] = emitter.range_multiplier;
		json["emitter_intensity_multiplier"] = emitter.intensity_multiplier;
		json["emitter_maximum_lights"] = emitter.maximum_lights;

		// Active emitter time
		if (emitter.stopNewEmitsAfter > 0.0f) 
			json["stop_emitter_after"] = emitter.stopNewEmitsAfter;

		if (emitter.emitterDuration > 0.0f) {
			json["emitter_duration"] = emitter.emitterDuration;
			json["delete_emitter_owner"] = emitter.deleteEmitterOwner;
		}

		std::ofstream file(emitter.getName());
		file << std::setw(4) << json << std::endl;

		return true;
	}

	TRange CParser::loadRange(const json& jData, const std::string& attr, const TRange& defaultValue)
	{
		if (jData.count(attr) <= 0)
		{
			return defaultValue;
		}

		const json& jValue = jData[attr];
		if (jValue.is_array())
		{
			return TRange{ jValue.at(0), jValue.at(1), true };
		}
		else
		{
			const float value = jData.value<float>(attr, 0.f);
			return TRange{ value, value, false };
		}
	}

	void CParser::saveRange(json& jData, const TRange& range) {
		jData.push_back(range.minValue);
		jData.push_back(range.maxValue);
	}

	void TEmitter::sampleColorsOverTime() {
		for (int i = 0; i < max_colors; ++i) {
			float t = (float)i / (float)(max_colors - 1);
			ctes->psystem_min_colors_over_time[i] = colors_min.get(t);
			ctes->psystem_max_colors_over_time[i] = colors_max.get(t);
		}
		ctes->updateGPU();
	}

	void TEmitter::sampleSizesOverTime() {
		for (int i = 0; i < max_sizes; ++i) {
			float t = (float)i / (float)(max_sizes - 1);
			VEC3 size = sizes.get(t);
			ctes->psystem_sizes_over_time[i] = VEC4(size.x, size.y - size.x, 0, size.z); // First value is size. Second value is max size it can increase to. Last value is size modifier.
		}
		ctes->updateGPU();
	}


	bool renderInMenuRandomColorMode(TTrack<VEC4> & min_color, TTrack<VEC4> & max_color) {
		bool changed = false;

		if (ImGui::Button("Sort keyframes by time")) {
			min_color.sort();
			max_color.sort();
		}

		if (ImGui::Button("Add keyframe")) {
			min_color.set(1.0f, Vector4(1.0f, 1.0f, 1.0f, 1.0f));
			max_color.set(1.0f, Vector4(1.0f, 1.0f, 1.0f, 1.0f));
		}

		ImGui::Separator();
		if (min_color.getKeyframes().size() == 0) {
			ImGui::ColorEdit3("Default value", static_cast<float*>(&min_color.getDefault().x), ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR);
			ImGui::DragFloat("Blending alpha value at this time", &min_color.getDefault().w, 0.01f, 0.0f, 1.0f);
			max_color = min_color;
		}
		else {
			int i = 0;
			auto & max_keyframes = max_color.getKeyframes();
			for (auto & kf : min_color.getKeyframes()) {
				ImGui::PushID(i++);
				if (ImGui::Button("Delete this keyframe")) {
					min_color.remove(kf.time);
					max_color.remove(kf.time);
					ImGui::PopID();
					break;
				}

				changed |= (ImGui::DragFloat("Time (0..1)", &kf.time, 0.01f, 0.0f, 1.0f));
				changed |= (ImGui::ColorEdit4("Min color", static_cast<float*>(&kf.value.x), ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR));
				changed |= (ImGui::ColorEdit4("Max color", static_cast<float*>(&max_keyframes[i-1].value.x), ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR));
				max_keyframes[i-1].time = kf.time;

				ImGui::Separator();
				ImGui::PopID();
			}
		}

		if (changed) {
			min_color.sort();
			max_color.sort();
		}

		return changed;
	}

	void TEmitter::renderInMenu() {
		unsigned limitMax = 200;
		bool physxReInitNeeded = false;

		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.75f, 0.75f, 1.0f));
		ImGui::Text("[Particle generation parameters]");
		ImGui::PopStyleColor();
		ImGui::DragFloat2("Particle emit count (min/max): ", &count.minValue, 1.0f, 0.0f, count.maxValue);
		physxReInitNeeded |= ImGui::DragScalar("Particle max count alive: ", ImGuiDataType_U32, (unsigned*)& maxCount, 1.0f, 0, &limitMax);
		ImGui::DragFloat2("Interval to spawn (min/max): ", &interval.minValue, 0.05f, 0.0f, 60.0f);
		ImGui::DragFloat3("Direction: ", &direction.x, 0.01f, -1.0f, 1.0f);
		ImGui::DragFloat("Cone angle: ", &coneAngle, 0.01f, 0.0f, 180.0f);
		physxReInitNeeded |= ImGui::DragFloat2("Initial particle speed (min/max): ", &speed.minValue, 0.01f, 0.0f, 50.0f);
		ImGui::DragFloat2("Particle duration (min/max): ", &duration.minValue, 0.01f, 0.0f, 20.0f);

		ImGui::Separator();

		// Physics parameters.
		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.75f, 0.75f, 1.0f));
		ImGui::Text("[Physics parameters]");
		ImGui::PopStyleColor();
		ImGui::Spacing(0, 5);
		// PhysX
		physxReInitNeeded |= ImGui::Checkbox("PhysX based", &is_physx_based);
		if (is_physx_based) {
			physxReInitNeeded |= ImGui::Checkbox("Default gravity", &default_gravity);
			if (!default_gravity)
				physxReInitNeeded |= ImGui::DragFloat3("Gravity", &physx_gravity.x, 0.01f, -20.0f, 20.0f);
			physxReInitNeeded |= ImGui::Checkbox("Dynamic collision", &dynamic_collision);
			physxReInitNeeded |= ImGui::Checkbox("Two way interaction", &two_way_interaction);
			if (two_way_interaction)
				physxReInitNeeded |= ImGui::DragFloat("Mass", &mass, 0.01f, 0.0f, 20.0f);
			physxReInitNeeded |= ImGui::DragFloat("Damping", &damping, 0.01f, 0.0f, 20.0f);
			if (ImGui::DragFloat("Rest offset", &rest_offset, 0.01f, 0.0f, contact_offset - 0.01f)) {
				if (rest_offset >= contact_offset)
					rest_offset = contact_offset / 2;
				if (rest_offset == 0)
					rest_offset = 0.01f;
				physxReInitNeeded = true;
			}
			if (ImGui::DragFloat("Contact offset", &contact_offset, 0.01f, rest_offset + 0.01f, 20.0f)) {
				if (rest_offset >= contact_offset)
					contact_offset = rest_offset * 2;
				if (contact_offset == 0)
					contact_offset = 0.02f;
				physxReInitNeeded = true;
			}
			physxReInitNeeded |= ImGui::DragFloat("Restitution", &restitution, 0.01f, 0.0f, 20.0f);
			physxReInitNeeded |= ImGui::DragFloat("Dynamic friction", &dynamic_friction, 0.01f, 0.0f, 20.0f);
			physxReInitNeeded |= ImGui::DragFloat("Static friction", &static_friction, 0.01f, 0.0f, 20.0f);
			physxReInitNeeded |= ImGui::DragFloat("Max distance per frame", &max_distance_per_frame, 0.01f, 0.0f, 20.0f);
			ImGui::Checkbox("Remove on contact", &remove_on_contact);
		}
		else {
			ImGui::DragFloat2("Gravity factor (min/max): ", &gravityFactor.minValue, 0.01f, -10.0f, 10.0f);
			
			if (localSpace) {
				ImGui::DragFloat3("Local point", &gravityPoint.x, 0.01f, -20.0f, 20.0f);
			}
			else {
				ImGui::Checkbox("Local point space", &localGravityPoint);
				if (localGravityPoint)
					ImGui::DragFloat3("Local point", &gravityPoint.x, 0.01f, -20.0f, 20.0f);
				else
					ImGui::DragFloat3("World point", &gravityPoint.x, 0.01f, -5000.f, 5000.f);
			}
			ImGui::DragFloat2("Gravity point factor", &gravityPointFactor.minValue, 0.01f, -10.f, 10.f);
		}

		if (physxReInitNeeded) {
			EngineParticles.resetPhysXForEmitter(this);
		}

		ImGui::Separator();

		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.75f, 0.75f, 1.0f));
		ImGui::Text("[Particle color, size, uv methods]");
		ImGui::PopStyleColor();
		
		bool updatects = false;

		ImGui::ListBoxHeader("Color mode");
		for (auto & color_mode : colorModes)
		{
			const std::string & item_name = color_mode;
			if (ImGui::Selectable(color_mode.c_str(), item_name == colorMethod)) {
				colorMethod = color_mode;
				updatects = true;
			}
		}
		ImGui::ListBoxFooter();

		ImGui::ListBoxHeader("Size mode");
		for (auto & size_mode : sizeModes)
		{
			const std::string & item_name = size_mode;
			if (ImGui::Selectable(size_mode.c_str(), item_name == sizeMethod)) {
				sizeMethod = size_mode;
				updatects = true;
			}
		}
		ImGui::ListBoxFooter();

		ImGui::ListBoxHeader("Uv mode");
		for (auto & uv_mode : uvsModes)
		{
			const std::string & item_name = uv_mode;
			if (ImGui::Selectable(uv_mode.c_str(), item_name == uvMethod)) {
				uvMethod = uv_mode;
				// Don't blend int this method as uvs don't change with time. Use dynamic for that.
				// We set this flago to false toa void weird blending artifacts.
				if (uvMethod == "random_uv")
					blendBetweenTexturesInUv = false;
				updatects = true;
			}
		}
		ImGui::ListBoxFooter();

		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.75f, 0.75f, 1.0f));
		ImGui::Text("[Particle UV parameters]");
		ImGui::PopStyleColor();

		if (uvMethod == "random_uv")
			ImGui::Text("Uvs can't be blended in this method as they don't change.");
		else
			updatects |= ImGui::Checkbox("Blend uvs", &blendBetweenTexturesInUv);

		updatects |= ImGui::DragFloat("Uv offset movement", &uv_offset_movement_during_lifetime, 0.01f, -1000.0f, 1000.0f);

		if (updatects) {
			ctes->psystem_random_color = colorMethod == "normal" ? false : true;
			ctes->psystem_random_dynamic_color = colorMethod == "random_dynamic_color" ? true : false;
			ctes->psystem_random_size = sizeMethod == "normal" ? false : true;
			ctes->psystem_random_uvs = uvMethod == "normal" ? false : true;
			ctes->psystem_random_uvs_change_with_time = uvMethod == "random_uv_dynamic" ? true : false;
			ctes->psystem_uvs_blend = blendBetweenTexturesInUv;
			ctes->psystem_uvs_speed_change = uv_offset_movement_during_lifetime;
			ctes->updateGPU();
		}

		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.75f, 0.75f, 1.0f));
		ImGui::Text("[Particle life parameters]");
		ImGui::PopStyleColor();
		ImGui::PushItemWidth(300);
		
		if (ImGui::TreeNode("Particle size over time")) {
			if (sizeMethod == "random_size_and_modifier") {
				ImGui::Text("Size gets initialized on start based on min and max values.");
				ImGui::Text("Size modifiers increase or decrease the start size based on the given timestamp.");

				if (sizes.renderInMenuSizeMode<VEC3>())
					sampleSizesOverTime();
			}
			else if (sizeMethod == "random_size_dynamic_and_modifier") {
				ImGui::Text("Size gets initialized on start based on min and max values.");
				ImGui::Text("The size will then change based on both the size modifier and the other sizes indicated as timestamps.");

				if (sizes.renderInMenuSizeModeDynamic<VEC3>())
					sampleSizesOverTime();
			}
			else {
				if (sizes.renderInMenu<VEC3>())
					sampleSizesOverTime();
			}
			ImGui::TreePop();
		}
		if (ImGui::TreeNode("Particle color over time")) {
			if (colorMethod == "random_dynamic_color" || colorMethod == "random_color") {
				ImGui::Text("Random colors are created on the interpolation between min and max values.");
				ImGui::Text("Dynamic random makes the value change with time so colors change their starting color with time.");

				if(ImGui::Button("Random color keyframes")){
					colors_min.clearKeyframes();
					colors_min.set(0, Vector4(0.0, 0.0, 0.0, 0.0));
					colors_max.clearKeyframes();
					colors_max.set(0, Vector4(1.0, 1.0, 1.0, 1.0));
					sampleColorsOverTime();
				}

				if (renderInMenuRandomColorMode(colors_min, colors_max))
					sampleColorsOverTime();
			}
			else {
				if (colors_min.renderInMenu<VEC4>())
					sampleColorsOverTime();
			}
			ImGui::TreePop();
		}

		// 3D start location
		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.75f, 0.75f, 1.0f));
		ImGui::Text("[Particle 3D start location]");
		ImGui::PopStyleColor();
		if (!is_physx_based)
			ImGui::Checkbox("Local Space", &localSpace);
		static int option;
		if (start3DBox)			option = 1;
		else if (start3DSphere)	option = 2;
		else if (start3DCircle)	option = 3;
		else						option = 0; //No 3D start location
		ImGui::RadioButton("Normal", &option, 0);	ImGui::SameLine();
		ImGui::RadioButton("Box", &option, 1);		ImGui::SameLine();
		ImGui::RadioButton("Sphere", &option, 2);	ImGui::SameLine();
		ImGui::RadioButton("Circle", &option, 3);

		start3DBox = false;
		start3DSphere = false;
		start3DCircle = false;

		if (option == 1)			start3DBox = true;
		if (option == 2)			start3DSphere = true;
		if (option == 3)			start3DCircle = true;

		if (option == 1)
			ImGui::DragFloat3("Box Size (X:Y)", &boxSize.x, 0.01f, 0.0f, 25.0f);
		else if (option == 2)
			ImGui::DragFloat("Sphere radius", &sphereRadius, 0.01f, 0.0f, 25.0f);
		else if (option == 3)
			ImGui::DragFloat2("Circle radius", &circleRadius.minValue, 0.01f, 0.0f, 25.0f);

		// Rotation parameters.
		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.75f, 0.75f, 1.0f));
		ImGui::Text("[Rotation parameters]");
		ImGui::PopStyleColor();
		ImGui::Spacing(0, 5);

		if (ImGui::TreeNode("Rotation parameters")) {

			if (ImGui::Checkbox("Random rotation", &random_rotation)) {
				ctes->psystem_random_rotation = random_rotation;
				ctes->updateGPU();
			}

			if (particles_type_idx == 0) {
				if (!random_rotation) {
					if (ImGui::DragFloat("Rotation offset", &rotation_offset, 0.01f, -2.0f * PI, 2.0f * PI)) {
						ctes->psystem_rotation_offset = rotation_offset;
						ctes->updateGPU();
					}
				}
			}
			else {
				ImGui::DragFloat2("Rotation sensitivity", &rotationSensitivity.minValue, 0.01f, 0.f, 20.f);
			}

			ImGui::TreePop();
		}
		ImGui::Spacing(0, 5);

		// Stretched billboard parameters.
		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.75f, 0.75f, 1.0f));
		ImGui::Text("[Stretched billboard parameters]");
		ImGui::PopStyleColor();
		ImGui::Spacing(0, 5);

		if (ImGui::TreeNode("Stretched billboard")) {
			if (ImGui::Checkbox("Stretched billboard", &is_stretched_billboard)) {
				ctes->psystem_stretched_billboard = is_stretched_billboard;
				ctes->updateGPU();
			}
			if (is_stretched_billboard) {
				if (ImGui::DragFloat("Stretch", &stretch, 0.01f, 0.01f, 10.0f)) {
					ctes->psystem_stretch = stretch;
					ctes->updateGPU();
				}
				if (ImGui::Checkbox("Velocity affects stretch", &velocity_affects_stretch)) {
					ctes->psystem_velocity_affects_stretch = velocity_affects_stretch;
					ctes->updateGPU();
				}
			}

			ImGui::TreePop();
		}
		ImGui::Spacing(0, 5);


		// Light parameters.
		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.75f, 0.75f, 1.0f));
		ImGui::Text("[Light parameters]");
		ImGui::PopStyleColor();
		ImGui::Spacing(0, 5);

		if (ImGui::TreeNode("Light parameters")) {
			ImGui::Checkbox("Spawn lights", &spawn_lights);
			
			ImGui::ListBoxHeader("Light spawns");
			for (auto & light_mode : lightModes)
			{
				const std::string & item_name = light_mode;
				if (ImGui::Selectable(item_name.c_str(), item_name == lightType))
					lightType = item_name;
			}
			ImGui::ListBoxFooter();

			ImGui::ColorEdit3("Light base color", &light_base_color.x, ImGuiColorEditFlags_::ImGuiColorEditFlags_HDR);
			ImGui::DragFloat("Light base intensity", &light_base_intensity, 0.01f, 0.0f, 10000.0f);
			ImGui::DragFloat("Light base radius", &light_base_radius, 0.01f, 0.0f, 10000.0f);
			
			ImGui::DragFloat("Particles To Lights Ratio", &particles_to_light_ratio, 0.01f, 0.0f, 1.0f);
			ImGui::Checkbox("Use Particle Color For Light", &use_particles_color_for_light);

			ImGui::Checkbox("Size affects light range", &size_affects_range);
			ImGui::DragFloat("Range multiplier based on size.", &range_multiplier, 0.01f, 0.0f, 1.0f);

			ImGui::Checkbox("Particles alpha affects intensity", &alpha_affects_intensity);
			ImGui::DragFloat("Intensity multiplier.", &intensity_multiplier, 0.01f, 0.0f, 1.0f);

			ImGui::DragInt("Maximum number of lights", &maximum_lights, 0.0, 1000.0);
			ImGui::TreePop();
		}
		ImGui::Spacing(0, 5);

		// Emitter parameters
		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0f, 0.75f, 0.75f, 1.0f));
		ImGui::Text("[Emitter parameters]");
		ImGui::PopStyleColor();
		ImGui::PushItemWidth(200);
		ImGui::DragFloat("Emitter active duration: ", &emitterDuration, 0.01f, 0.0f, 20.0f);
		ImGui::DragFloat("Emitter stop emissions at: ", &stopNewEmitsAfter, 0.01f, 0.0f, 20.0f);
		ImGui::PopItemWidth();
	}
}