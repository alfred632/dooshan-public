#pragma once

#include "modules/module.h"
#include "particles/particle.h"

class CMeshInstanced;

namespace particles
{
	struct TEmitter;
	class CSystem;

	enum LightType {
		POINT_LIGHT = 0,
		NUM_LIGHTS
	};

	// An entity created for being able to edit particles inside the engine.
	// Hols a system and emitter pointer to spawn the given particle.
	struct TParticleEditor {
		CHandle entity;
		std::string name;
		CSystem * system;
		particles::TEmitter * emitter;
	};

	class CModuleParticles : public IModule
	{
	public:
		CModuleParticles(const std::string & name);

		bool start() override;
		void stop() override;
		void update(float dt) override;

		CSystem * launchSystem(TEmitter * emitter, CHandle owner, const Vector3 & offsetOwner = Vector3::Zero);
		CSystem * launchSystem(TEmitter * emitter, CHandle owner, const std::string& boneToFollow, const Vector3 & offsetOwner = Vector3::Zero);
		CSystem * launchSystem(TEmitter * emitter, CTransform & nTransform);
		void setEmittingSystemActiveStatus(CSystem * system, bool activeStatus);
		void disableSystem(CSystem * system);
		void disableSystemDelayed(CSystem * system, float timeForDisabling);
		void loadParticlesJson();
		void saveParticlesJson();
		void reloadParticlesJson();

		void resetPhysXForEmitter(TEmitter* emitter);

		void renderInMenu() override;
		void renderDebug() override;

	private:
		CHandle _particlesEditor;
		std::vector<CSystem *> _activeSystems;
		std::vector<TParticleEditor *> _testingSystems;
		std::vector<CSystem *> systemsToRemove;

		char newName[50];
		bool cfgLoaded = false;
		bool showEditorWindow = false;
		TParticleEditor* editingParticle;

		CSystem * addSystem(CSystem * nSystem, TEmitter * emitter);
		void registerSystem(CSystem* system);
		void sortActiveSystems();
		
		struct TRenderDetail {
			const CSystem * system = nullptr;
			uint32_t        num_particles = 0;
			uint32_t        base = 0;
		};

		// Each instanced mesh type ( billboards, spheres, rocks ) rendered 
		// requires a CMeshInstance, and a uploadCall.
		struct TParticlesType {
		  // Each vertex of this GPU mesh represents a particle when rendered
		  CMeshInstanced*              mesh = nullptr;
		  // Each emitter with have a group of the prev mesh assigned
		  uint32_t                     num_emitters = 0;
		  // Total particles accumulated during current frame
		  uint32_t                     num_particles = 0;
		  
		  // This is the version in the cpu.
		  std::vector< TRenderData >   particles_cpu;
		  // Hodls all the emitters that use the given particle type together with the number of particles they have and the base value for rendering.
		  std::vector< TRenderDetail > render_details;

		  TRenderData * top() { return particles_cpu.data() + num_particles; }
		  static const size_t max_particles_allowed_to_be_drawn_by_emitterType_and_frame = 512;
		  TRenderData * max_top() { return particles_cpu.data() + (particles_cpu.capacity() - max_particles_allowed_to_be_drawn_by_emitterType_and_frame); }
		  TRenderData * limit() { return particles_cpu.data() + particles_cpu.capacity(); }
		};

		// Vector holding all particles types.
		std::vector< TParticlesType > particles_types;
		// Vector holding the name for each element in particles_types
		std::unordered_map<std::string, size_t> particle_type_names;
		// Vector holding all light types that can be spawned binded to particles.
		std::vector< json > light_types;

		// This is an entity in the scene to be able to register something in the 
		// render manager
		CHandle h_all_particles_entity;
		CHandle h_render;

		void uploadRenderDetailsToGPU();
		void saveRenderDetailsOfSystem(const CSystem* system, uint32_t nparticles);

		// Spawn light entities for particles.
		CHandle spawnLightType(LightType lightType);

		// Editor windows.
		void createParticleContainer();
		void duplicateParticleEmitter(TParticleEditor * pEditor);
		void deleteParticleEmitter(TParticleEditor * pEditor);
		void disable3DLocation(TParticleEditor * pEditor);
		void drawEditorWindow(bool * p_open);
		void clearParticleEditor();

		friend class CSystem;
		friend class CParser;
	};
}
