#include "mcv_platform.h"
#include "WeightController.h"

void WeightController::TLinearTimeWeightController::load(const json & j){
	isActive = j.value("active", false);

	// Get blend parameters.
	fade_in = j.value("fadeIn", 0.0f);
	fade_out = j.value("fadeOut", 0.0f);
}

void WeightController::TLinearTimeWeightController::update(const float dt, float & weight_to_modify){
	// Calculate the amount for the given animation.
	if (!fadingOut) {
		current_time = Maths::clamp(current_time + dt, 0.0f, fade_in);
		weight_to_modify = current_time / fade_in;
	}
	else {
		current_time = Maths::clamp(current_time - dt, 0.0f, fade_out);
		weight_to_modify = current_time / fade_out;
		if (weight_to_modify == 0.0f)
			isActive = false;
	}
}

void WeightController::TLinearTimeWeightController::renderInMenu() {
	ImGui::Text("Is Fading In %b", (current_time < fade_in && !fadingOut));
	ImGui::Text("Fade In Time %f", fade_in);
	ImGui::Text("Is Fading Out %b", (current_time > 0.0f && fadingOut));
	ImGui::Text("Fade Out Time %f", fade_out);
}

void WeightController::TLinearTimeWeightController::setWeightControllerStatus(bool nActive) {
	if (nActive) {
		isActive = nActive;
		fadingOut = false;
	}
	else {
		if (isActive)
			fadingOut = true;
	}
}

WeightController::IWeightController * WeightController::WeightControllerManager::createState(const std::string & type) {
	const auto & factory = weightControllerFactories.find(type);
	if (factory == weightControllerFactories.end()) return nullptr;
	return factory->second->create();
}

void WeightController::WeightControllerManager::start() {
	if (loaded) return;

	/* Add here the weight controller factory so it can be loaded from a json. */
	weightControllerFactories["linearTime"] = new WeightControllerFactory<TLinearTimeWeightController>();
	weightControllerFactories["footIK"] = new WeightControllerFactory<TFootIKWeightController>();

	loaded = true;
}