#include "mcv_platform.h"
#include "surface_querier.h"

#include "../../engine.h"
#include "../../utils/utils.h"
#include "../../utils/json_resource.h"
#include "../../logic/logic_manager.h"
#include "../components/common/comp_tags.h"


namespace SurfaceQuerier {

	bool SurfaceQuerierManager::SurfaceQuerierManagerLoaded = false;
	std::vector<SurfaceQuerierManager::SurfaceTagInfo> SurfaceQuerierManager::surfaces; // Holds all surfaces.
	std::map<Tag, std::vector<SurfaceIndx>> SurfaceQuerierManager::surfacesPerTag; // Holds all surfaces a tag is present in.

	void SurfaceQuerierManager::Init(const std::string & surfaceTypesFilePath) {
		if (SurfaceQuerierManagerLoaded) return;
		SurfaceQuerierManagerLoaded = true;

		json & dataLoaded = Utils::loadJson(surfaceTypesFilePath);

		// No surfaces where found. We don't load anything.
		if (dataLoaded.count("surfaces") == 0) {
			Utils::dbg("Surfaces data was not found in the surface_data file with path: %s", surfaceTypesFilePath);
			return;
		}

		// Surfaces is not an array.
		if (dataLoaded["surfaces"].is_array() == false) {
			Utils::dbg("Surfaces data is not an array. Make it an array.");
			return;
		}

		// Get all surfaces data.
		int numSurfaces = 0;
		json & surfacesData = dataLoaded["surfaces"];
		for (auto & surface : surfacesData) {
			numSurfaces++;

			SurfaceType surfaceID = surface.value("surface_id", "");
			if (surfaceID == "") {
				Utils::dbg("Surface ID was not found for surface number %d.", numSurfaces);
				continue;
			}

			if (surface.count("surface_tags_needed") == 0 || !surface["surface_tags_needed"].is_array()) {
				Utils::dbg("Surface ID doesn't have an array for the tag/tags presents.");
				continue;
			}

			// Get index we are inserting the surface into.
			int surfaceDataIndex = (int)surfaces.size();

			// Surface tag info.
			SurfaceTagInfo surfaceData;
			surfaceData.surface_id = surfaceID;
			for (auto tag : surface["surface_tags_needed"]) { // Add all tags present in surface.
				std::string tagAsString = tag;
				Tag tagAsNum = Utils::getID(tagAsString.c_str());

				// Create the surfaces per tag entry for the tag if not present. Add the surface to the tag.
				if (surfacesPerTag.count(tagAsNum) == 0)
					surfacesPerTag[tagAsNum] = std::vector<SurfaceIndx>();
				surfacesPerTag[tagAsNum].push_back(surfaceDataIndex);
				surfaceData.tagsNecessaryForSurface.insert(tagAsNum);
			}

			// Add it into the surface vector.
			surfaces.push_back(surfaceData);
		}
	}

	SurfaceType SurfaceQuerierManager::GetSurfaceType(TCompTags * tags) {
		bool fetchedSurfaces = false;
		std::vector<FetchedSurface> fetchedSurfacesVector; // stores all possible results, at the end we return the best one if there was one.

		for (int i = 0; i < tags->max_tags; ++i) {
			if (tags->isTagFromIndexNull(i)) continue; // Continue if null.
			Tag tag = tags->getTagFromIndex(i);
			if (surfacesPerTag.count(tag) == 0) continue; // Tag was not present.

			// Add surfaces to the array if we still don't have any. We take the surface indx and the information of the number of tags necessary
			// we substract one to count for this tag already found.
			if (!fetchedSurfaces) {
				for (auto surfaceIndx : surfacesPerTag[tag])
					fetchedSurfacesVector.push_back(FetchedSurface(surfaceIndx, (int)(surfaces[surfaceIndx].tagsNecessaryForSurface.size() - 1)));
			}

			// Now, loop through all fetched surfaces and check if our tag is in them. Note that the first tag fetches the surfaces but doesn't do this as it's not necessary for him.
			if (fetchedSurfaces) {
				for (auto & surface : fetchedSurfacesVector) {
					// If tags found, decrease the number of tags missing.
					if (surfaces[surface.fetchedSurfaceIndx].tagsNecessaryForSurface.count(tag))
						surface.numTagsMissing--;
				}
			}
			fetchedSurfaces = true;
		}

		if (fetchedSurfaces == false) return ""; // No surface was found.

		int surfaceIndxToReturnAsResult = -1;
		int surfaceWithMoreNumbersOfTagsFullfilled = -1; // How many tags has the most fulfilled surface.
		for (auto & surface : fetchedSurfacesVector) {
			if (surface.numTagsMissing > 0) continue;
			
			int surfaceIndex = surface.fetchedSurfaceIndx;
			int numberOfTagsForSurface = (int)surfaces[surfaceIndex].tagsNecessaryForSurface.size();

			// If we find a surface that has all necessary tags found in the tag component and has a bigger amount of them
			// (i.e we assume it to be more specific) we return it.
			if (numberOfTagsForSurface > surfaceWithMoreNumbersOfTagsFullfilled) {
				surfaceIndxToReturnAsResult = surfaceIndex;
				surfaceWithMoreNumbersOfTagsFullfilled = numberOfTagsForSurface;
			}
		}

		if (surfaceIndxToReturnAsResult == -1) return ""; // No surface with all necessary tags was found.

		// Return surface information.
		return surfaces[surfaceIndxToReturnAsResult].surface_id;
	}
}