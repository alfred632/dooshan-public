#pragma once

#include "components\common\comp_base.h"
#include "entity/entity.h"

/* This component manages the locking and unlocking of the mouse.
By default the mouse starts locked. We will use it while developing the game
to unlock the camera by pressing a shortcut. Once a menu is implemented, it should
lock or unlock the mouse itself. */
class TCompMouseManagerDebug : public TCompBase {
	
public:
	void load(const json & j, TEntityParseContext& ctx);
	void update(float dt);
};
