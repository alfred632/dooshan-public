#pragma once
#include "entity\entity.h"
#include "entity\common_msgs.h"
#include "components\common\comp_base.h"
#include "engine.h"

class TCompMeleeTutorialCrouch : public TCompBase {
	
public:
	static void registerMsgs();

	DECL_SIBLING_ACCESS();
	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();

	void onEntityCreated(const TMsgEntityCreated& msg);
	void onEntityDead(const TMsgEntityDead& msg);

private:
	float _deathTime = 0.0f;
	CClock _deathTimer;
};