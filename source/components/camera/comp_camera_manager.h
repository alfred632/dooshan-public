#pragma once

#include "components\common\comp_base.h"
#include "entity/entity.h"
 
#include <map>

class TCompCamera;

/* This Camera Manager is used for travelling between cameras.
Any camera that can be viewed in debug must be added. */

class TCompCameraManager : public TCompBase {
	static std::map< int, CHandle > all_cameras; 	// Stores handles to all cameras, retrieval is done by their ID.
	static std::vector< int > cameras_indexes; // Used for travel between cameras. Sorted.
	static int currentCamera; // ID of the camera activated.
	static bool firstFrame;

public:
	// Used for fast access.
	enum Cameras {
		Final_Camera = 0,
		Debug_Camera = 1
	};

	~TCompCameraManager();

	void onStart();
	void load(const json& j, TEntityParseContext & ctx);
	void debugInMenu();
	void update(float dt);
	
	// Register a camera in the given index.
	// If another camera exists, won't do anything.
	static void registerCamera(int cameraIndex, TCompCamera * camera);
	static int registerCamera(TCompCamera * camera);
	static void unRegisterCamera(int cameraIndex);

	// Get cameras.
	static CHandle getCamera(int cameraID);
	static CHandle getCurrentCamera();
	static Vector3 getCurrentCameraPosition();
	int getCurrentCameraID() { return currentCamera; }

private:
	// Called for activating a new camera and deactivating the old one.
	static void setActiveCamera(int nVectorIndx);
	// Activates or deactivates a normal camera.
	static void setStatusNormalCamera(CHandle camera, bool active);
	// Activates or deactivates the output camera.
	// We activate it and all the cameras being blended.
	static void setStatusOutputCamera(bool active);
};