#include "mcv_platform.h"
#include "comp_camera_lookat.h"
#include "components/common/comp_transform.h"
#include "entity/common_msgs.h"

DECL_OBJ_MANAGER("camera_lookat", TCompCameraLookat);

#include "engine.h"
#include "components/common/comp_name.h"

TCompCameraLookat::~TCompCameraLookat() {
	EngineScripting.UnregisterNewComponentEventForScripts(CHandle(this), "OnTargetReached");
}

void TCompCameraLookat::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", false);
	target_name = j.value("target_name", "");
	target_offset = loadVector3("target_offset");
	interpolation_time_horizontal = j.value("interpolation_time_horizontal", 0.0f);
	interpolation_time_vertical = j.value("interpolation_time_vertical", interpolation_time_horizontal);
	interpolation = ((interpolation_time_horizontal > 0.0f) || (interpolation_time_vertical > 0.0f)) ? true : false;

	OnTargetReached = EngineScripting.RegisterNewComponentEventForScripts(CHandle(this), "OnTargetReached");
}

void TCompCameraLookat::declareInLua() {
	EngineScripting.declareComponent<TCompCameraLookat>(
		"TCompCameraLookat",
		// attribute that points to a getter, can also point to a setter (second argument in property)
		"setActive", &TCompCameraLookat::setActive,
		"setTarget",
		sol::overload
		(
			sol::resolve<void(CHandle)>(&TCompCameraLookat::setTarget),
			sol::resolve<void(const std::string &)>(&TCompCameraLookat::setTarget)
		),
		"setInterpolation",
		&TCompCameraLookat::setInterpolation,
		"setInterpolationHorizontalTime",
		&TCompCameraLookat::setInterpolationHorizontalTime,
		"setInterpolationVerticalTime",
		&TCompCameraLookat::setInterpolationVerticalTime
	);
}

void TCompCameraLookat::registerMsgs() {
	DECL_MSG(TCompCameraLookat, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompCameraLookat, TMsgInputControllerStatus, OnInputStatus);
}

void TCompCameraLookat::debugInMenu()
{
	ImGui::Text("Target name: %.s", Target.isValid() ? ((CEntity*)Target)->getName() : "...");
	ImGui::DragFloat3("Target offset: ", &target_offset.x, 0.01f, 0.0f, 10.f);

	bool changedInterpol = ImGui::DragFloat("interpolation_time_horizontal", &interpolation_time_horizontal, 0.01f, 0.0f, 20.f);
	changedInterpol &= ImGui::DragFloat("interpolation_time_vertical", &interpolation_time_vertical, 0.01f, 0.0f, 20.f);

	if (changedInterpol)
		interpolation = ((interpolation_time_horizontal > 0.0f) || (interpolation_time_vertical > 0.0f)) ? true : false;
}

void TCompCameraLookat::update(float dt)
{
	if (checkHandles() == false) return;

	// Get camera transform.
	TCompTransform* cTransform = getComponent<TCompTransform>();
	if (!cTransform) return;

	// Get player transform.
	TCompTransform* cTargetTransform = TargetTransform;
	if (!cTargetTransform) return;

	VEC3 targetPos = getPositionTargetToAimTo(cTargetTransform);
	float yawToGetTo, pitchToGetTo;
	cTransform->getAnglesToFaceTarget(targetPos, yawToGetTo, pitchToGetTo);
	if (interpolation) {
		float currentYaw, currentPitch;
		cTransform->getAngles(&currentYaw, &currentPitch);
		float finalYaw = Maths::SmoothDampAngle(currentYaw, yawToGetTo, yawVelocitySmoothDamp, interpolation_time_horizontal, dt);
		float finalPitch = Maths::SmoothDampAngle(currentPitch, pitchToGetTo, pitchVelocitySmoothDamp, interpolation_time_vertical, dt);
		cTransform->setAngles(finalYaw, finalPitch);
	}
	else
		cTransform->setAngles(yawToGetTo, pitchToGetTo);

	if (isOnTarget(cTransform, targetPos)) {
		active = false;
		OnTargetReached.CallEventForAllScripts();
	}
}

bool TCompCameraLookat::isOnTarget(TCompTransform * CompTransform, const Vector3 & TargetPosition) {
	float CurrentYaw, CurrentPitch, YawToGetTo, PitchToGetTo;
	CompTransform->getAngles(&CurrentYaw, &CurrentPitch);
	CompTransform->getAnglesToFaceTarget(TargetPosition, YawToGetTo, PitchToGetTo);
	return abs(YawToGetTo - CurrentYaw) <= 0.005f && abs(CurrentPitch - PitchToGetTo) <= 0.005f;
}

bool TCompCameraLookat::checkHandles() {
	if (!Target.isValid())
	{
		Target = getEntityByName(target_name);
		if (!Target.isValid()) return false;
		CEntity * TargetEntity = Target;
		TargetTransform = TargetEntity->getComponent<TCompTransform>();
	}
	return true;
}

void TCompCameraLookat::setTarget(CHandle nTarget){
	Target = nTarget;
	CEntity * TargetEntity = Target;
	if (!TargetEntity) return;
	
	TargetTransform = TargetEntity->getComponent<TCompTransform>();
	TCompName * Name = TargetEntity->getComponent<TCompName>();
	target_name = Name->getName();
}

void TCompCameraLookat::setTarget(const std::string & nTargetName){
	setTarget(getEntityByName(nTargetName));
}

Vector3 TCompCameraLookat::getPositionTargetToAimTo(TCompTransform * cTargetTransform) {
	return cTargetTransform->getPosition() + cTargetTransform->getFront() * target_offset.z
		+ cTargetTransform->getLeft() * target_offset.x + cTargetTransform->getUp() * target_offset.y;
}

void TCompCameraLookat::OnInputStatus(const TMsgInputControllerStatus & msg) {
	active = msg.activate;
}