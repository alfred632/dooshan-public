#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "../module_scripting.h"

class TCompTransform;
class TMsgInputControllerStatus;

/* This component allows a camera to have a look at behaviour. */
class TCompCameraLookat : public TCompBase
{
	DECL_SIBLING_ACCESS();

	// Target
	CHandle Target;
	CHandle TargetTransform;

	std::string target_name;
	Vector3 target_offset;

	ComponentEventHandle OnTargetReached;

	bool interpolation = false;
	float interpolation_time_horizontal; // How fast the horizontal interpolation is.
	float interpolation_time_vertical; // How fast the vertical interpolation is.
	float yawVelocitySmoothDamp = 0.0f;
	float pitchVelocitySmoothDamp = 0.0f;

	/* Private functions. */
	bool checkHandles();
	Vector3 getPositionTargetToAimTo(TCompTransform * cTargetTransform);
	bool isOnTarget(TCompTransform * CompTransform, const Vector3 & TargetPosition);

	/* Private messages. */
	void OnInputStatus(const TMsgInputControllerStatus & msg);

public:
	~TCompCameraLookat();

	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	static void registerMsgs();
	static void declareInLua();

	void setTarget(CHandle nTarget);
	void setTarget(const std::string & nTargetName);
	void setInterpolation(bool nInterpolation) { interpolation = nInterpolation; }
	void setInterpolationHorizontalTime(float nInterpolHorizontal) { interpolation_time_horizontal = nInterpolHorizontal; }
	void setInterpolationVerticalTime(float nInterpolVertical) { interpolation_time_vertical = nInterpolVertical; }
};

