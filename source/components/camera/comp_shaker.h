#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "engine.h"

/* This component is used for shaking a transform.
We use them for shaking the camera. */

/* ˇImportant! This component moves the Transform of the object.
It may affect the behaviour of certain controllers, for example
the camera orbit as it will change the orientation and position
of the camera. Get sure to save the original transform in the
controller and update it in each frame if you need it. Or create
a child component that gets the transform of the new component.
We did the last method for the shake in the player camera. */

class TCompTransform;

class TCompShaker : public TCompBase {
	DECL_SIBLING_ACCESS();

	float trauma = 0.0f;
	float traumaDecrease = 0.1f; // Trauma substracted each second.
	float shake = 0.0f; // The trauma value squared or cubed.

	// Translational shake.
	bool translationShake = false;
	Vector3 maxOffsetTranslation;
	
	// Rotational shake.
	bool rotationShake = true;
	Vector3 maxOffsetRotation; // Pitch, yaw, roll

	CHandle transformH; // Transform of the object.

	void updateTrauma(float dt);
	void translationalShake(TCompTransform * transform);
	void rotationalShake(TCompTransform * transform);
	float getRandomFloatNegOneToOne();

	void onEntityCreated(const TMsgEntityCreated & msg);
	void onTraumaDelta(const TMsgTraumaDelta & msg);
public:
	void load(const json& j, TEntityParseContext & ctx);
	static void registerMsgs();
	void renderDebug();
	void debugInMenu();
	void update(float dt);
};