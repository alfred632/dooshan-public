#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "engine.h"
#include "modules/module_physics.h"

/* This component represents a blender used by the camera. It listens
for entities entering the trigger in front of a camera and adds them
to be blended */

// Sent by the camera controller to activate or deactivate the blender of objects.
struct TMsgBlenderStatus {
	bool activate = false;
	DECL_MSG_ID();
};

// Sent to entities to know they should blend or not.
// If true. It will start blending.
struct TMsgBlendEntity {
	bool activate = false; // Not really necessary but makes code slightly clearer.
	DECL_MSG_ID();
};

class TCompBlenderCamera : public TCompBase {
	DECL_SIBLING_ACCESS();

	CHandle transformH; // Transform of the camera.
	CHandle colliderH; // The trigger of the camera.
	Vector3 colliderOffset;

	bool activateBlending = true;
	std::unordered_map<int, CHandle> entitiesBeingBlended;

	void onEntityCreated(const TMsgEntityCreated & msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter & msg);
	void onTriggerExit(const TMsgEntityTriggerExit & msg);
	void onStatusChange(const TMsgBlenderStatus & msg);

public:
	void load(const json& j, TEntityParseContext & ctx);
	static void registerMsgs();
	void renderDebug();
	void debugInMenu();
	void update(float dt);
};