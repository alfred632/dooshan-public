#include "mcv_platform.h"
#include "comp_curve_controller.h"
#include "components/common/comp_transform.h"

#include "engine.h"

DECL_OBJ_MANAGER("curve_controller", TCompCurveController);

TCompCurveController::~TCompCurveController(){
	EngineScripting.UnregisterNewComponentEventForScripts(CHandle(this), "onCurveEnded");
}

void TCompCurveController::load(const json& j, TEntityParseContext& ctx)
{
	_curve = EngineResources.getResource(j.value("curve", ""))->as<CCurve>();
	_ratio = j.value("ratio", _ratio);
	active = j.value("active", active);
	_speed = j.value("speed", _speed);
	_targetName = j.value("target", "");

	// Create scripting events so scripts can bind to this component.
	OnCurveEnded = EngineScripting.RegisterNewComponentEventForScripts(CHandle(this), "onCurveEnded");
}

void TCompCurveController::debugInMenu() {
	ImGui::DragFloat("Max Speed", &_speed, 0.1f, 1.f, 100.f);
}

void TCompCurveController::setRatio(float ratio)
{
	_ratio = ratio;
	applyRatio();

	if (_ratio >= 1.0)
		OnFinishTransition();
	else
		FinishCalled = false;
}

void TCompCurveController::OnFinishTransition(){
	if (!FinishCalled) return;
	FinishCalled = true;
	OnCurveEnded.CallEventForAllScripts(); // Throws event so all scripts binded will receive it.
}

void TCompCurveController::applyRatio()
{
	if (!_curve) return;

	if (!_target.isValid()){
		_target = getEntityByName(_targetName);
		if (!_target.isValid()) return;
	}

	TCompTransform * c_transform = getComponent<TCompTransform>();
	if (!c_transform) return;

	CEntity * eTarget = _target;
	TCompTransform * cTargetTransform = eTarget->getComponent<TCompTransform>();
	if (!cTargetTransform) return;
  
	const VEC3 pos = VEC3::Transform(_curve->evaluate(_ratio), c_transform->asMatrix());
	cTargetTransform->setPosition(pos);
}

void TCompCurveController::renderDebug()
{
	if (_curve)
	{
		TCompTransform* c_transform = getComponent<TCompTransform>();
		if (!c_transform) return;
		_curve->renderDebug(*c_transform);
	}
}
