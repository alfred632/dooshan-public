#include "mcv_platform.h"
#include "components\common\comp_transform.h"
#include "comp_camera.h"
#include "comp_flyover_camera_controller.h"

#include "engine.h"

DECL_OBJ_MANAGER("camera_flyover_controller", TCompCameraFlyOverController);

void TCompCameraFlyOverController::registerMsgs() {
	DECL_MSG(TCompCameraFlyOverController, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompCameraFlyOverController, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompCameraFlyOverController, TMsgInputControllerStatus, OnInputStatus);
}

void TCompCameraFlyOverController::onEntityCreated(const TMsgEntityCreated & msg) {
	TCompTransform * transform = getComponent<TCompTransform>();
	if (!transform) return;
}

void TCompCameraFlyOverController::OnInputStatus(const TMsgInputControllerStatus & msg) {
	active = msg.activate;
}

void TCompCameraFlyOverController::load(const json& j, TEntityParseContext& ctx) {
	camera_speed = j.value("camera_speed", camera_speed);
	camera_sprint_speed = j.value("camera_sprint_speed", camera_sprint_speed);
	vertical_sensitivity = DEG2RAD(j.value("v_sensitivity", vertical_sensitivity));
	horizontal_sensitivity = DEG2RAD(j.value("h_sensitivity", horizontal_sensitivity));
}

void TCompCameraFlyOverController::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::DragFloat("Camera Speed", &camera_speed, 0.1f, 0.f, 100.f);
	ImGui::DragFloat("Camera Sprint Speed", &camera_sprint_speed, 0.1f, 0.f, 100.f);

	ImGui::Spacing();
	ImGui::Separator();

	float vSens = RAD2DEG(vertical_sensitivity);
	if (ImGui::DragFloat("Vertical Sensitivity", &vSens, 0.1f, 0.f, 100.f))
		vertical_sensitivity = vSens;
	float hSens = RAD2DEG(horizontal_sensitivity);
	if (ImGui::DragFloat("Horizontal Sensitivity", &hSens, 0.1f, 0.f, 100.f))
		horizontal_sensitivity = hSens;
}

void TCompCameraFlyOverController::update(float dt){
	TCompCamera * cam = getComponent<TCompCamera>();
	if (cam == nullptr) return;

	TCompTransform * transform = getComponent<TCompTransform>();
	if (!transform) return;

	// First compute the movement direction.
	VEC3 direction;
	if (EngineInput["forward"].isPressed())
		direction.z += 1.f;
	if (EngineInput["backwards"].isPressed())
		direction.z -= 1.f;
	if (EngineInput["left"].isPressed())
		direction.x += 1.f;
	if (EngineInput["right"].isPressed())
		direction.x -= 1.f;
	if (EngineInput['R'].isPressed())
		direction.y += 1.f;
	if (EngineInput['F'].isPressed())
		direction.y -= 1.f;

	// Check for sprint key.
	float finalSpeed;
	if (EngineInput[VK_SHIFT])
		finalSpeed = camera_sprint_speed * dt;
	else
		finalSpeed = camera_speed * dt;

	// Now calculate movement.
	VEC3 deltaMovement;
	deltaMovement += transform->getFront() * direction.z * finalSpeed;
	deltaMovement += transform->getLeft() * direction.x * finalSpeed;
	deltaMovement += transform->getUp() * direction.y * finalSpeed;

	// Calculate rotation
	float cameraYaw, cameraPitch;
	transform->getAngles(&cameraYaw, &cameraPitch);

	if (cam->isLocked()) {
		VEC2 mouseDelta = EngineInput.mouse().getRawDelta();
		cameraYaw -= mouseDelta.x * horizontal_sensitivity; // Yaw rotation.
		cameraPitch += mouseDelta.y * vertical_sensitivity; // Pitch rotation.
		cameraPitch = Maths::clamp(cameraPitch, -maxPitch, maxPitch);
	}

	VEC3 finalPosition = transform->getPosition() + deltaMovement;
	VEC3 newFront = yawPitchToVector(cameraYaw, cameraPitch);
	transform->lookAt(finalPosition, finalPosition + newFront);
}