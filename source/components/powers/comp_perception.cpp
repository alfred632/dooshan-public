#include "mcv_platform.h"
#include "components\powers\comp_perception.h"
#include "components\camera\comp_camera_manager.h"
#include "components\common\comp_render.h"
#include "entity/msgs.h"
#include "engine.h"

#include "components\common\comp_tags.h"
#include "ui\widgets\ui_image.h"

DECL_OBJ_MANAGER("perception", TCompPerception);

void TCompPerception::declareInLua() {
	EngineScripting.declareComponent<TCompPerception>
		(
			"TCompPerception",
			"use", &TCompPerception::Use,
			"isPerceptionActive", &TCompPerception::isPerceptionActive,
			"isActive", &TCompPerception::isActive
			);
}

TCompPerception::~TCompPerception() {
	if (perceptionActivated)
		deactivate();
}

void TCompPerception::load(const json& j, TEntityParseContext& ctx) {
	// How much trauma is sent to the camera when perception is activated.
	traumaCamera = j.value("trauma_camera", traumaCamera);

	// Time to wait while ability on cooldown.
	cooldownWaitingTime = j.value("cooldown_waiting_time", cooldownWaitingTime);

	// How long can the ability be used before it stops.
	maxUseTime = j.value("max_use_time", maxUseTime);

	_activateSoundEvent = j.value("activate_sound_event", _activateSoundEvent);
	_ambientSoundEvent = j.value("ambient_sound_event", _ambientSoundEvent);
	if (j.count("ambient_sound_delay"))
		_ambientSoundDelay.setTime(j["ambient_sound_delay"]);
}

void TCompPerception::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::Checkbox("Perception active: ", &perceptionActivated);
	ImGui::Spacing(0, 5);
	ImGui::DragFloat("Trauma camera on perception activated", &traumaCamera, 0.01f, 0.0f, 1.0f);
	ImGui::Spacing(0, 5);
	ImGui::Text("Current use time: %f", currentUseTime);
	ImGui::DragFloat("Maximum ability use in seconds", &maxUseTime, 0.01f, 0.0f, 1000.0f);
	ImGui::Spacing(0, 5);
	ImGui::Text("Current cooldown time: %f", currentCooldownWaitingTime);
	ImGui::DragFloat("Cooldown waiting time in seconds", &cooldownWaitingTime, 0.01f, 0.0f, 1000.0f);

}

void TCompPerception::registerMsgs() {
	DECL_MSG(TCompPerception, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompPerception, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompPerception, TMsgPerceptionActivateEntities, onActivateEntitiesStatus);
}

void TCompPerception::onEntityCreated(const TMsgEntityCreated & msg) {
	player = CHandle(this).getOwner();
	outputCamera = TCompCameraManager::getCamera(TCompCameraManager::Cameras::Final_Camera).getOwner();
	cameraPlayer = getEntityByName("Camera-Player-Output");
}

// Recieved when perception is activated or deactivated. Sent by postprocess effect.
// Now, we can tell all entities to set their status to perception so they get painted with perception materials.
void TCompPerception::onActivateEntitiesStatus(const TMsgPerceptionActivateEntities & msg) {
	// Set state to objects to change materials to perception.
	VHandles handles = CTagsManager::get().getAllEntitiesByTag(Utils::getID("perception"));
	for (auto & h : handles)
		LogicManager::Instance().setStatusPerceptionEntity(h, msg.active);
}

void TCompPerception::playUIAnimation(std::string animationName, float currentAnimationTime)
{
	// UI
	UI::CWidget iconPerception;
	if (EngineUI.findWidgetByName(iconPerception, "abilities_perception")) {
		for (auto& child : iconPerception.getChildren()) {
			if (UI::CImage * imgChild = dynamic_cast<UI::CImage*>(child)) {
				imgChild->playAnimation(animationName, false, currentAnimationTime);
			}
		}
	}
}

void TCompPerception::stopUIAnimation(std::string animationName)
{
	// UI
	UI::CWidget iconPerception;
	if (EngineUI.findWidgetByName(iconPerception, "abilities_perception")) {
		for (auto& child : iconPerception.getChildren()) {
			if (UI::CImage * imgChild = dynamic_cast<UI::CImage*>(child)) {
				imgChild->stopAnimation(animationName);
			}
		}
	}
}

void TCompPerception::renderDebug() {}

void TCompPerception::update(float dt){
	if (perceptionActivated) {
		currentUseTime += dt;

		// Check if we have been used the ability above it's maximum use. If so, deactivate.
		if (currentUseTime > maxUseTime) {
			stopUIAnimation("PerceptionActivated");
			playUIAnimation("AbilityCD", currentCooldownWaitingTime);
			playUIAnimation("AbilityCDButton", currentCooldownWaitingTime);
			deactivate();
		}
	}
	else {
		// Update current cooldown time.
		currentCooldownWaitingTime = std::clamp(currentCooldownWaitingTime - dt, 0.0f, cooldownWaitingTime);
	}
	if (!_ambientSoundEvent.empty() && _ambientSoundDelay.isFinished()) {
		EngineAudio.playEvent(_ambientSoundEvent);
		_ambientSoundDelay.stop();
	}
}

void TCompPerception::Use() {

	if (!perceptionActivated && currentCooldownWaitingTime == 0.0f) {
		playUIAnimation("PerceptionActivated");
		activate();
	}
	else if (perceptionActivated) {
		stopUIAnimation("PerceptionActivated");
		playUIAnimation("AbilityCD", currentCooldownWaitingTime);
		playUIAnimation("AbilityCDButton", currentCooldownWaitingTime);
		deactivate();
	}
}

void TCompPerception::activate(){
	if (perceptionActivated)
		return;

	perceptionActivated = true;
	// TODO: Activate UI
	//EngineUI.setChildWidgetVisibleByTime()
	
	// Send msg to camera output to activate perception.
	if (!fetchOutputCamera()) return;
	CEntity * cameraOutputEnt = outputCamera;
	TMsgPerceptionStatusChanged msg = { perceptionActivated };
	cameraOutputEnt->sendMsg(msg);

	// Also send trauma message for shockwave effect.
	if (!fetchPlayerCamera()) return;
	CEntity * cameraPlayerEnt = cameraPlayer;
	TMsgTraumaDelta traumaMsg = { traumaCamera, traumaCamera };
	cameraPlayerEnt->sendMsg(traumaMsg);

	if (!_activateSoundEvent.empty())
		EngineAudio.playEvent(_activateSoundEvent);
	EngineAudio.setEventParameter(_activateSoundEvent, _audioParamName, 1.0f);
}

void TCompPerception::deactivate(){
	if (!perceptionActivated)
		return;
 
	perceptionActivated = false;

	// Send msg to camera output to deactivate perception.
	if (!fetchOutputCamera()) return;
	CEntity * cameraOutputEnt = outputCamera;
	TMsgPerceptionStatusChanged msg = { perceptionActivated };
	cameraOutputEnt->sendMsg(msg);

	// Now we need to wait x time for ability to cooldown.
	currentUseTime = 0.0f;
	currentCooldownWaitingTime = cooldownWaitingTime;

	// Also send trauma message for shockwave effect.
	if (!fetchPlayerCamera()) return;
	CEntity * cameraPlayerEnt = cameraPlayer;
	TMsgTraumaDelta traumaMsg = { traumaCamera/2.0, traumaCamera/2.0 };
	cameraPlayerEnt->sendMsg(traumaMsg);

	EngineAudio.setEventParameter(_activateSoundEvent, _audioParamName, 0.0f);
}

bool TCompPerception::fetchOutputCamera(){
	if (!outputCamera.isValid())
		outputCamera = TCompCameraManager::getCamera(TCompCameraManager::Cameras::Final_Camera).getOwner();
	else
		return true;
	return outputCamera.isValid();
}

bool TCompPerception::fetchPlayerCamera(){
	if (!cameraPlayer.isValid())
		cameraPlayer = getEntityByName("Camera-Player-Output");
	else
		return true;
	return cameraPlayer.isValid();
}