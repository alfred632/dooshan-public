#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

struct TMsgPerceptionActivateEntities {
	bool active;
	DECL_MSG_ID();
};


class TCompPerception : public TCompBase {
	CHandle player;
	CHandle outputCamera; // For activating the postprocessing for rendering perception.
	CHandle cameraPlayer; // For sending trauma events.

	std::string _activateSoundEvent = "";
	std::string _ambientSoundEvent = "";
	CClock _ambientSoundDelay = CClock(2.0f);

	bool perceptionActivated = false; // Whether the ability is being used or not.

	float traumaCamera = 0.2f; // How much trauma is sent to the camera when perception is activated.
	
	float currentCooldownWaitingTime = 0.0f; // When 0.0f, we can use the ability.
	float cooldownWaitingTime = 2.0f; // Time to wait while ability on cooldown.

	float currentUseTime = 0.0f; // How long has the ability been used.
	float maxUseTime = 10.0; // How long can the ability be used before it stops.
	
	// Activates or deactivated the power, sends the necessary messages to other entities.
	void activate();
	void deactivate();

	// Fetch cameras. Returns false if not found.
	bool fetchOutputCamera();
	bool fetchPlayerCamera();

	// On message functions.
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onActivateEntitiesStatus(const TMsgPerceptionActivateEntities & msg); // Recieved when perception is activated or deactivated. Sent by postprocess effect.

	void playUIAnimation(std::string animationName, float currentAnimationTime = 0.0f);
	void stopUIAnimation(std::string animationName);
public:
	DECL_SIBLING_ACCESS();

	~TCompPerception();

	void load(const json& j, TEntityParseContext & ctx);
	void update(float dt);
	void debugInMenu();
	void renderDebug();
	static void registerMsgs();
	static void declareInLua();

	void Use(); // Use perception.
	bool isPerceptionActive() { return perceptionActivated; } // Is perception active.
	float getCurrentUseAsPercentage() { return currentUseTime / maxUseTime; } // 0.0f not used, 1.0f fully used.
	float getCurrentCooldownAsPercentage() { return currentCooldownWaitingTime / cooldownWaitingTime; } // 0.0f no cooldown, 1.0f, full cooldown.
private:
	const std::string _audioParamName = "CorruptionActivated";
};
