#include "mcv_platform.h"
#include "comp_scythe.h"
#include "components/common/comp_transform.h"
#include "entity/entity_parser.h"
#include "entity/msgs.h"
#include "utils/phys_utils.h"
#include "components/common/comp_name.h"

using namespace physx;


DECL_OBJ_MANAGER("scythe", TCompScythe);

void TCompScythe::load(const json & j, TEntityParseContext & ctx)
{
	_damage = j.value("damage", 20.0f);
	_timeToLive = j.value("timeToLive", 0.5f);
}

void TCompScythe::update(float dt)
{
	_timeToLive -= dt;
	if (_timeToLive < 0) {
		this->getEntity().destroy();
		return;
	}
	//updateRotation(dt);
}

void TCompScythe::updateRotation(float dt) {
	TCompTransform* c_trans = getComponent<TCompTransform>();
	CEntity* e = getEntityByName("Player");
	TCompTransform* playerTransform = e->getComponent<TCompTransform>();
	TCompCollider* c_col = getComponent<TCompCollider>();

	float currentYaw, currentPitch;
	c_trans->getAngles(&currentYaw, &currentPitch);

	float yaw = Maths::lerpAngle(currentYaw, currentYaw + 45.0f, dt);
	c_trans->setAngles(yaw, currentPitch);
	PxVec3 pos = VEC3_TO_PXVEC3(playerTransform->getPosition()) + PxVec3(0, _offsetY, 0);
	PxTransform pxTrans;
	pxTrans.p = pos;
	pxTrans.q = QUAT_TO_PXQUAT(QUAT::CreateFromYawPitchRoll(yaw, currentPitch, 0.0f));

	PxRigidDynamic* scythe_actor = c_col->actor->is<PxRigidDynamic>();
	scythe_actor->setKinematicTarget(pxTrans);
}

void TCompScythe::onTriggerEnter(const TMsgEntityTriggerEnter & trigger_enter)
{
	//Check if enemy
	CEntity* e = trigger_enter.h_entity;
	CHandle h = e;
	if (!h.isValid())
		return;

	TCompCollider* c_col = e->getComponent<TCompCollider>();
	const PxU32 numShapes = c_col->actor->getNbShapes();
	std::vector<PxShape*> shapes;
	shapes.resize(numShapes);
	c_col->actor->getShapes(&shapes[0], numShapes);

	bool isEnemy = false;
	for (auto shape : shapes) {
		if (shape->getQueryFilterData().word0 == CModulePhysics::FilterGroup::Enemy)
			isEnemy = true;
	}
	if (!isEnemy)
		return;

	//else check if new to the vector and needs to be added
	if (std::find(_enemies.begin(), _enemies.end(), trigger_enter.h_entity) != _enemies.end()) {
		Utils::dbg("[TCompScythe::onTriggerEnter] Entity already on the enemies list wtf.\n");
	}
	else {
		_enemies.push_back(trigger_enter.h_entity);
		CEntity* e = trigger_enter.h_entity; 
		e->sendMsg(_msg);
	}
}

void TCompScythe::onCreatedUpdatePositon(const TMsgUpdateIndicator& msg) {
	TCompTransform* c_trans = getComponent<TCompTransform>();
	TCompCollider* c_col = getComponent<TCompCollider>();
	c_trans->setPosition(msg.position);
	float yaw = vectorToYaw(msg.front);
	c_trans->setAngles(yaw, 0.f, 0.f);
	c_col->actor->setGlobalPose(toPxTransform(*c_trans));

	_msg.damage = _damage;
	_msg.h_sender = this;

	CEntity* e = getEntityByName("Player");
	TCompTransform* playerTransform = e->getComponent<TCompTransform>();

	_offsetY = msg.position.y - playerTransform->getPosition().y;
}

void TCompScythe::registerMsgs() {
	DECL_MSG(TCompScythe, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompScythe, TMsgUpdateIndicator, onCreatedUpdatePositon);
	DECL_MSG(TCompScythe, TMsgToogleComponent, onToggleComponent);
}

void TCompScythe::debugInMenu()
{
	TCompBase::debugInMenu();
}

void TCompScythe::renderDebug()
{
}
