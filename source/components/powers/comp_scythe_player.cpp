#include "mcv_platform.h"
#include "components/common/comp_collider.h"
#include "comp_scythe_player.h"
#include "entity/entity_parser.h"
#include "entity/msgs.h"
#include "engine.h"
#include "utils/phys_utils.h"

using namespace physx;


DECL_OBJ_MANAGER("scythe_player", TCompScythePlayer);

void TCompScythePlayer::load(const json & j, TEntityParseContext & ctx)
{
	_powerCooldown = j.value("powerCooldown", 2.0f);
	_damage = j.value("damage", _damage);

	_swingSoundEvent = j.value("swing_sound_event", _swingSoundEvent);
	_weaponEntity = j["weapon_entity"];
	_trailEntity = j["trail_entity"];
	_trailStartClock.setTime(j.value("trail_start_time", _trailStartClock.initialTime()));
	_trailDurationClock.setTime(j.value("trail_duration", _trailDurationClock.initialTime()));
}

void TCompScythePlayer::update(float dt) {
	if (isCooldownOver()) {
		TCompScytheController* scytheWeapon = getScytheController();
		scytheWeapon->disableDamage();
	}
	else
		_remainingCooldown -= dt;

	if (_trailStartClock.isFinished()) {
		TCompTrailRender* trailRender = getTrailRender();
		trailRender->unlockPosistion();
		_trailStartClock.stop();
		_trailDurationClock.start();
	}
	if (_trailDurationClock.isFinished()) {
		TCompTrailRender* trailRender = getTrailRender();
		trailRender->lockPosistion();
		_trailDurationClock.stop();
	}
}

void TCompScythePlayer::debugInMenu()
{
	TCompBase::debugInMenu();
	ImGui::DragFloat("Power cooldown", &_powerCooldown, 2.0f, 0.5f, 10.f);
}

void TCompScythePlayer::renderDebug()
{
}

void TCompScythePlayer::registerMsgs() {
	DECL_MSG(TCompScythePlayer, TMsgToogleComponent, onToggleComponent);
}

void TCompScythePlayer::declareInLua() {
	EngineScripting.declareComponent<TCompScythePlayer>
		(
			"TCompScythePlayer",
			"activatePower", &TCompScythePlayer::activatePower
		);
}

void TCompScythePlayer::activatePower() {
	if (isCooldownOver()) {
		//Spawn the scythe power
		TCompCollider* c_col = getComponent<TCompCollider>();
		TCompScytheController* scytheWeapon = getScytheController();
		TCompTrailRender* trailRender = getTrailRender();

		float height = c_col->controller->getHeight();
		scytheWeapon->enableDamage(_damage);

		_trailStartClock.start();

		if (!_swingSoundEvent.empty())
			EngineAudio.playEvent(_swingSoundEvent);

		//Start the power cooldown
		_remainingCooldown = _powerCooldown;
	}
}

bool TCompScythePlayer::isCooldownOver() {
	return (_remainingCooldown <= 0.0f);
}


