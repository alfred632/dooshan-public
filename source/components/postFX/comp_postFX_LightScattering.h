#pragma once

#include "components\common\comp_base.h"
#include "entity/entity.h"

class CTechnique;
class CRenderToTexture;
class CDeferredRenderer;
class TCompTransform;

/* This component applies a post processing effect for
simulating a corruption effect around the player's screen. */
class TCompPostFXLightScattering : public TCompBase {
	DECL_SIBLING_ACCESS();

	/* Silhouette variables */
	CRenderToTexture * rt_silhouette_full_res; // Texture where we render god rays to in full_resolution. (We rescale half_res).
	CRenderToTexture * rt_silhouette; // Texture to render the silhouette to in half_res.
	const CTechnique * silhouette_tech;	// Technique for silhouette generation.
	const CTechnique * copy_tech; // Technique for silhouette scaling.

	/* God rays variables */
	CRenderToTexture * rt_rays; // Texture where we render god rays to in half_res.
	const CTechnique * god_rays_tech; // Technique for god rays generation.
	const CTechnique * god_rays_additive_tech; // Technique for god rays generation. Additive part.

	CCteBuffer<TCtesLightScattering> cts_light_scattering = CCteBuffer<TCtesLightScattering>(CTE_BUFFER_SLOT_CORRUPTION);

	std::string sunName;
	CHandle lightTransformH;

public:
	TCompPostFXLightScattering();
	~TCompPostFXLightScattering();

	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();

	// Applies the effect, returns the CRenderToTexture * with the applied effect.
	CRenderToTexture * apply(CDeferredRenderer * renderer, CRenderToTexture * textureToApplyPPE, CRenderToTexture * rt_depth_mask);

private:
	bool createOrResizeRT(int width, int height);
	void renderSceneSilhouette(CDeferredRenderer * renderer, CRenderToTexture * rt_depth_mask);
	void renderGodrays();
	void renderGodraysToScreen(CRenderToTexture * textureToApplyPPE);

	TCompTransform * getSunTransform();
	Vector2 computeScreenSpaceLightPosition(TCompTransform * sunTransform);
};