#include "mcv_platform.h"
#include "comp_postFX_LightScattering.h"

#include "engine.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_tags.h"
#include "render/render_manager.h"

DECL_OBJ_MANAGER("postfx_light_scattering", TCompPostFXLightScattering);

TCompPostFXLightScattering::TCompPostFXLightScattering()
	: cts_light_scattering(CTE_BUFFER_SLOT_LIGHT_SCATTERING) {
	bool is_ok = cts_light_scattering.create("light_scattering");
	assert(is_ok);
}

TCompPostFXLightScattering::~TCompPostFXLightScattering(){
	if (rt_silhouette) rt_silhouette->destroy();
	if (rt_rays) rt_rays->destroy();
	if (rt_silhouette_full_res) rt_silhouette_full_res->destroy();
	cts_light_scattering.destroy();
}

void TCompPostFXLightScattering::load(const json & j, TEntityParseContext& ctx){
	active = j.value("active", true);

	silhouette_tech = EngineResources.getResource("light_scattering_silhouette.tech")->as<CTechnique>();	// Technique for silhouette generation.
	god_rays_tech = EngineResources.getResource("light_scattering_god_rays_generation.tech")->as<CTechnique>();	// Technique for god rays generation.
	copy_tech = EngineResources.getResource("texture_to_screen.tech")->as<CTechnique>();	// Technique for god rays half to full resolution.
	god_rays_additive_tech = EngineResources.getResource("ligh_scattering_additive.tech")->as<CTechnique>();	// Technique for god rays half to full resolution.

	cts_light_scattering.LightScatteringSunPosInScreenSpace = loadVector2(j, "sunPosInScreenSpace");
	cts_light_scattering.LightScatteringExposure = j.value("LightScatteringExposure", 1.0);
	cts_light_scattering.LightScatteringDecay = j.value("LightScatteringDecay", 1.0);
	cts_light_scattering.LightScatteringDensity = j.value("LightScatteringDensity", 1.0);
	cts_light_scattering.LightScatteringWeight = j.value("LightScatteringWeight", 0.01);

	sunName = j.value("Sun Name", "Sun");

	if (!createOrResizeRT(Render.getWidth(), Render.getHeight()))
		Utils::dbg("Light scattering rts couldn't be created\n");
}

void TCompPostFXLightScattering::debugInMenu(){
	TCompBase::debugInMenu();

	bool changed = false;
	changed |= ImGui::DragFloat("Ligth Scattering Exposure", &cts_light_scattering.LightScatteringExposure, 0.001f, 0.0f, 100.0f);
	changed |= ImGui::DragFloat("Ligth Scattering Decay", &cts_light_scattering.LightScatteringDecay, 0.001f, 0.0f, 1.0f);
	changed |= ImGui::DragFloat("Ligth Scattering Density", &cts_light_scattering.LightScatteringDensity, 0.001f, 0.0f, 100.0f);
	changed |= ImGui::DragFloat("Ligth Scattering Weight", &cts_light_scattering.LightScatteringWeight, 0.001f, 0.0f, 100.0f);

	if (changed)
		cts_light_scattering.updateGPU();
}

bool TCompPostFXLightScattering::createOrResizeRT(int width, int height){
	if (!rt_silhouette)
		rt_silhouette = new CRenderToTexture();

	if (!rt_rays)
		rt_rays = new CRenderToTexture();

	if (!rt_silhouette_full_res)
		rt_silhouette_full_res = new CRenderToTexture();

	// Silhouette is half the actual rendering.
	int half_width = width / 2.0;
	int half_height = height / 2.0;
	if (rt_silhouette->getWidth() != half_width || rt_silhouette->getHeight() != half_height) {
		char rt_fxaa[64];
		sprintf(rt_fxaa, "LightScattering_silhouette_%08x", CHandle(this).asUnsigned());
		if (!rt_silhouette->create(rt_fxaa, half_width, half_height, DXGI_FORMAT_R16G16B16A16_FLOAT))
			return false;
	}

	if (rt_rays->getWidth() != half_width || rt_rays->getHeight() != half_height) {
		char rt_fxaa[64];
		sprintf(rt_fxaa, "LightScattering_god_rays_%08x", CHandle(this).asUnsigned());
		if (!rt_rays->create(rt_fxaa, half_width, half_height, DXGI_FORMAT_R16G16B16A16_FLOAT))
			return false;
	}

	if (rt_silhouette_full_res->getWidth() != width || rt_silhouette_full_res->getHeight() != height) {
		char rt_fxaa[64];
		sprintf(rt_fxaa, "LightScattering_god_rays_final_%08x", CHandle(this).asUnsigned());
		if (!rt_silhouette_full_res->create(rt_fxaa, width, height, DXGI_FORMAT_R16G16B16A16_FLOAT))
			return false;
	}

	return true;
}

void TCompPostFXLightScattering::renderSceneSilhouette(CDeferredRenderer * renderer, CRenderToTexture * rt_depth_mask) {
	// Generate silhouette.
	{
		PROFILE_FUNCTION("PostFx - Light Scattering Silhouette");
		CGpuScope gpu_scope("Light Scattering Silhouette rendering");

		{
			PROFILE_FUNCTION("Light Scattering Silhouette - Rendering");

			// Get the silhouette image of our rendered image.
			rt_silhouette_full_res->activateRTWithDepth(Render.getDepthStencilView());
			drawFullScreenQuad(silhouette_tech, rt_depth_mask);

			// Render the light as a plain object (this means only albedo and nothing else) into the silhouette image.
			CRenderManager::get().render(eRenderCategory::CATEGORY_SKYBOX_LIGHTS);

			// Downscale it.
			rt_silhouette->activateRT();
			drawFullScreenQuad(copy_tech, rt_silhouette_full_res);
		}
	}
}

TCompTransform * TCompPostFXLightScattering::getSunTransform() {
	if (!lightTransformH.isValid()) {
		CHandle sunEntity = getEntityByName(sunName);
		if (!sunEntity.isValid()) return nullptr;
		CEntity * entity = sunEntity;
		lightTransformH = entity->getComponent<TCompTransform>();
		if (!lightTransformH.isValid()) return nullptr;
	}
	return lightTransformH;
}

Vector2 TCompPostFXLightScattering::computeScreenSpaceLightPosition(TCompTransform * sunTransform) {
	Vector3 point = sunTransform->getPosition();
	Vector4 sunPosition = Vector4(point.x, point.y, point.z, 1.0);

	// Get sunPosition in screen space. Screen space requires a position between [-1, +1]. Some extra steps are necessary.
	Vector4 sunPositionInScreenSpace = Vector4::Transform(sunPosition, ctes_camera.ViewProjection);

	// Apply perspective division to normalize coordinates. Now we actually have our coordinatesin the range between [-1, +1].
	sunPositionInScreenSpace = sunPositionInScreenSpace / (sunPositionInScreenSpace.w);

	// Finally, we need to pass the screen position from range [-1, +1] to range [0, +1].
	sunPositionInScreenSpace = sunPositionInScreenSpace + Vector4(1.0, 1.0, 0.0, 0.0); // From [-1, +1] to [0, +2]. 
	sunPositionInScreenSpace = sunPositionInScreenSpace * 0.5; // From [0, +1]

	// Return the sun position in screen space from [0, 1]. The 1.0 in the y axis is to return the correct coordinate for
	// DirectX, in OpenGL it wouldn't be necessary.
	return Vector2(sunPositionInScreenSpace.x, 1.0 - sunPositionInScreenSpace.y);
}

void TCompPostFXLightScattering::renderGodrays() {
	// Generate godrays.
	{
		PROFILE_FUNCTION("PostFx - Light Scattering Generation");
		CGpuScope gpu_scope("Light Scattering Generation rendering");

		{
			PROFILE_FUNCTION("Light Scattering Generation - Rendering");

			// Get the silhouette image of our rendered image.
			rt_rays->activateRT();
			drawFullScreenQuad(god_rays_tech, rt_silhouette);
		}
	}
}

void TCompPostFXLightScattering::renderGodraysToScreen(CRenderToTexture * textureToApplyPPE){
	// Generate godrays to screen.
	{
		PROFILE_FUNCTION("PostFx - Light Scattering - Additive");
		CGpuScope gpu_scope("Light Scattering Additive rendering");

		{
			PROFILE_FUNCTION("Light Scattering Additive - Rendering");

			// Add the god rays to the image.
			textureToApplyPPE->activateRT();
			drawFullScreenQuad(god_rays_additive_tech, rt_rays);
		}
	}
}

// Applies the effect, returns the CRenderToTexture * with the applied effect.
CRenderToTexture * TCompPostFXLightScattering::apply(CDeferredRenderer * renderer, CRenderToTexture * textureToApplyPPE, CRenderToTexture * rt_depth_mask) {
	TCompTransform * sunTransform = getSunTransform();
	if (!sunTransform) return textureToApplyPPE;
	
	// First we render the silhouette into a new texture.
	renderSceneSilhouette(renderer, rt_depth_mask);

	// Get sun screen position. Activate coordinates.
	cts_light_scattering.LightScatteringSunPosInScreenSpace = computeScreenSpaceLightPosition(sunTransform);
	cts_light_scattering.updateGPU();
	cts_light_scattering.activate();

	// This part creates the godrays in a texture of half the resolution.
	renderGodrays();

	// This part adds the ray into the image.
	renderGodraysToScreen(textureToApplyPPE);

	return textureToApplyPPE;
}