#include "mcv_platform.h"
#include "comp_postFX_motion_blur.h"
#include "resources/resource.h"
#include "render/textures/render_to_texture.h"

#include "engine.h"
#include "render/module_render.h"
#include "components/camera/comp_camera.h"

DECL_OBJ_MANAGER("postfx_motion_blur", TCompPostFXMotionBlur);

TCompPostFXMotionBlur::TCompPostFXMotionBlur()
: cte_motion_blur(CTE_BUFFER_SLOT_MOTION_BLUR)
{
	bool is_ok = cte_motion_blur.create("Motion Blur");
	assert(is_ok);
}

TCompPostFXMotionBlur::~TCompPostFXMotionBlur() {
	cte_motion_blur.destroy();
}

void TCompPostFXMotionBlur::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", true);
	cte_motion_blur.MotionBlurCameraLastFrameViewProjectionMatrix = Matrix::Identity;

	cte_motion_blur.MotionBlurSteps = j.value("motion_blur_steps", 8.0f);
	assert("Motion blur steps can not be 0!" && !(cte_motion_blur.MotionBlurSteps < 1.0f));
	cte_motion_blur.MotionBlurIntensity = j.value("motion_blur_intensity", 0.20f);

	tech = EngineResources.getResource("motion_blur.tech")->as<CTechnique>();

	// with the first use, init with the input resolution
	if (!createOrResizeRT(Render.getWidth(), Render.getHeight()))
		Utils::dbg("Motion rt couldn't be created\n");
}

bool TCompPostFXMotionBlur::createOrResizeRT(int width, int height) {
	if (!rt)
		rt = new CRenderToTexture();

	if (rt->getWidth() != width || rt->getHeight() != height) {
		char rt_focus[64];
		sprintf(rt_focus, "Motion_Blur_%08x", CHandle(this).asUnsigned());
		return rt->create("RT_Motion_Blur", Render.width, Render.height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	}
	return true;
}

void TCompPostFXMotionBlur::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::DragFloat("Motion Blur Intensity", &cte_motion_blur.MotionBlurIntensity, 0.001, 0.0, 10.0);
	ImGui::DragFloat("Motion Blur Steps", &cte_motion_blur.MotionBlurSteps, 1.0, 1.0, 100.0);
}

CRenderToTexture * TCompPostFXMotionBlur::apply(CRenderToTexture * textureToApplyPPE) {
	if (!active) {
		updateLastFrameViewMatrix();
		return textureToApplyPPE;
	}

	if (!createOrResizeRT(textureToApplyPPE->getWidth(), textureToApplyPPE->getHeight())) return nullptr;

	// Do motion blur.
	{
		PROFILE_FUNCTION("PostFx - Motion Blur");
		CGpuScope gpu_scope("Motion Blur rendering");

		// Activate constants.
		{
			PROFILE_FUNCTION("Motion Blur - Upload constants");
			cte_motion_blur.MotionBlurCurrentFPS = EngineRender.getCurrentFPS();
			cte_motion_blur.MotionBlurTargetFPS = EngineRender.getTargetFPS();
			cte_motion_blur.activate();
			cte_motion_blur.updateGPU();
		}

		// Render.
		{
			PROFILE_FUNCTION("Motion Blur - Rendering");
			rt->activateRT();
			drawFullScreenQuad(tech, textureToApplyPPE);
		}


		// Update view matrix with last frame.
		{
			PROFILE_FUNCTION("Motion Blur - Last frame view matrix");
			updateLastFrameViewMatrix();
		}
	}

	return rt;
}

void TCompPostFXMotionBlur::updateLastFrameViewMatrix() {
	// Always update the view matrix with the last one.
	TCompCamera* cameraRendering = EngineRender.getRenderCamera();
	cte_motion_blur.MotionBlurCameraLastFrameViewProjectionMatrix = cameraRendering->getViewProjection();
}