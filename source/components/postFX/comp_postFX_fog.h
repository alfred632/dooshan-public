#pragma once

#include "components\common\comp_base.h"
#include "entity/entity.h"

class CTecnique;
class CRenderToTexture;
struct TMsgEntityCreated;

/* This component applies post processing effects */
class TCompPostFXFog : public TCompBase {
	DECL_SIBLING_ACCESS();

	/* Texture to render the effect to. */
	CRenderToTexture * rt;
	
	/* Technique and constants. */
	const CTechnique * fog; // Fog technique with the shader to use for fog.
	CCteBuffer<TCtesFog> cts_fog_shader = CCteBuffer<TCtesFog>(CTE_BUFFER_SLOT_FOG);
	const CTexture * currentFogGradient = nullptr; // A gradient texture used for stylistic, colored, fogs.
	const CTexture * nextFogGradient = nullptr; // A gradient texture used for stylistic, colored, fogs.

	/* Fog variables. */
	Vector3 FogColor;
	float FogStartDistance;
	float FogEndDistance;
	float FogIntensity;
	float FogDensity;

public:
	~TCompPostFXFog() {
		if(rt) rt->destroy();
		cts_fog_shader.destroy();
	}

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	// Applies the effect, returns the CRenderToTexture * with the applied effect.
	CRenderToTexture * apply(CRenderToTexture * textureToApplyPPE);
	void setFogLerp(float nLerp);

private:
	bool createOrResizeRT(int width, int height);
};