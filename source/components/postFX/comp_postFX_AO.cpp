#include "mcv_platform.h"
#include "comp_postFX_AO.h"

#include "engine.h"
#include "render/render.h"
#include "render/textures/render_to_texture.h"

#include "components/camera/comp_camera.h"

DECL_OBJ_MANAGER("postfx_ao", TCompPostFXAO);

void TCompPostFXAO::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", true);
	
	// Juan implementation.
	amount = j.value("amount", amount);
	radius = j.value("radius", radius);
	zrange_discard = j.value("zrange_discard", zrange_discard);
	amount_spreading = j.value("amount_spreading", amount_spreading);

	// Intel ASSAO implementation.
	settings.Radius = j.value("ASSAO-Radius", 4.20);
	settings.ShadowMultiplier = j.value("ASSAO-ShadowMultiplier", 1.25);
	settings.ShadowPower = j.value("ASSAO-ShadowPower", 1.65);
	settings.FadeOutFrom = j.value("ASSAO-FadeOutFrom", 50.);
	settings.FadeOutTo = j.value("ASSAO-FadeOutTo", 300);
	settings.Sharpness = j.value("ASSAO-Sharpness", 0.5);
	settings.DetailShadowStrength = j.value("ASSAO-DetailShadowStrength", 0.5);
	settings.HorizonAngleThreshold = j.value("ASSAO-HorizonAngleThreshold", 0.06);
	settings.ShadowClamp = j.value("ASSAO-ShadowClamp", 0.98);

	if (!createOrResizeRT(Render.width, Render.height))
		Utils::dbg("AO rt couldn't be created\n");

	white = EngineResources.getResource("data/textures/white.dds")->as<CTexture>();
	tech = EngineResources.getResource("ao.tech")->as<CTechnique>();
}

bool TCompPostFXAO::createOrResizeRT(int width, int height) {
	bool created = false;
	static int g_counter = 0;

	if (!rt_output) {
		rt_output = new CRenderToTexture();
		created = true;
	}

	if (rt_output->getWidth() != width || rt_output->getHeight() != height) {
		char rt_name[64];
		sprintf(rt_name, "AO_%02d", g_counter);
		//  bool is_ok = rt_output->createRT(rt_name, Render.width, Render.height, DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_FORMAT_UNKNOWN);
		bool is_ok = rt_output->create(rt_name, Render.width, Render.height, DXGI_FORMAT_R8_UNORM, DXGI_FORMAT_UNKNOWN);
		assert(is_ok);
		return is_ok;
	}

	if (created)
		g_counter++;
	
	return true;
}

void TCompPostFXAO::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::Checkbox("Use ASSAO", &use_assao);

	if (use_assao)
		debugIntelASSAO();
	else
		debugSSAO();
}

void TCompPostFXAO::debugIntelASSAO(){
	ImGui::Text("Performance/quality settings:");

	// extension for "Lowest"
	int qualityLevelUI = settings.QualityLevel + 1;

	ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.8f, 0.8f, 1.0f));
#ifdef INTEL_SSAO_ENABLE_ADAPTIVE_QUALITY
	ImGui::Combo("Quality level", &qualityLevelUI, "Lowest\0Low\0Medium\0High\0Highest (adaptive)\0\0");  // Combo using values packed in a single constant string (for really quick combo)
#else
	ImGui::Combo("Quality level", &qualityLevelUI, "Lowest\0Low\0Medium\0High\0\0");  // Combo using values packed in a single constant string (for really quick combo)
#endif
	if (ImGui::IsItemHovered()) ImGui::SetTooltip("Each quality level is roughly 2x more costly than the previous, except the Highest (adaptive) which is variable but, in general, above High");
	ImGui::PopStyleColor(1);

	// extension for "Lowest"
#ifdef INTEL_SSAO_ENABLE_ADAPTIVE_QUALITY
	settings.QualityLevel = Maths::clamp(qualityLevelUI - 1, -1, 3);
#else
	settings.QualityLevel = Maths::clamp(qualityLevelUI - 1, -1, 2);
#endif

	ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.75f, 0.75f, 0.75f, 1.0f));

	if (settings.QualityLevel == 3)
	{
#ifdef INTEL_SSAO_ENABLE_ADAPTIVE_QUALITY
		ImGui::Indent();
		ImGui::SliderFloat("Adaptive quality target", &settings.AdaptiveQualityLimit, 0.0f, 1.0f, "%.3f");
		settings.AdaptiveQualityLimit = Maths::clamp(settings.AdaptiveQualityLimit, 0.0f, 1.0f);
		ImGui::Unindent();
#endif
	}

	if (settings.QualityLevel <= 0)
	{
		ImGui::InputInt("Simple blur passes (0-1)", &settings.BlurPassCount);
		if (ImGui::IsItemHovered()) ImGui::SetTooltip("For Low quality, only one optional simple blur pass can be applied (recommended); settings above 1 are ignored");
	}
	else
	{
		ImGui::InputInt("Smart blur passes (0-6)", &settings.BlurPassCount);
		if (ImGui::IsItemHovered()) ImGui::SetTooltip("The amount of edge-aware smart blur; each additional pass increases blur effect but adds to the cost");
	}
	settings.BlurPassCount = Maths::clamp(settings.BlurPassCount, 0, 6);

	ImGui::Separator();
	ImGui::Text("Visual settings:");
	ImGui::InputFloat("Effect radius", &settings.Radius, 0.05f, 0.0f, 2);
	if (ImGui::IsItemHovered()) ImGui::SetTooltip("World (viewspace) effect radius");
	ImGui::InputFloat("Occlusion multiplier", &settings.ShadowMultiplier, 0.05f, 0.0f, 2);
	if (ImGui::IsItemHovered()) ImGui::SetTooltip("Effect strength");
	ImGui::InputFloat("Occlusion power curve", &settings.ShadowPower, 0.05f, 0.0f, 2);
	if (ImGui::IsItemHovered()) ImGui::SetTooltip("occlusion = pow( occlusion, value ) - changes the occlusion curve");
	ImGui::InputFloat("Fadeout distance from", &settings.FadeOutFrom, 1.0f, 0.0f, 1);
	if (ImGui::IsItemHovered()) ImGui::SetTooltip("Distance at which to start fading out the effect");
	ImGui::InputFloat("Fadeout distance to", &settings.FadeOutTo, 1.0f, 0.0f, 1);
	if (ImGui::IsItemHovered()) ImGui::SetTooltip("Distance at which to completely fade out the effect");
	ImGui::InputFloat("Sharpness", &settings.Sharpness, 0.01f, 0.0f, 2);
	if (ImGui::IsItemHovered()) ImGui::SetTooltip("How much to bleed over edges; 1: not at all, 0.5: half-half; 0.0: completely ignore edges");

	ImGui::Separator();
	ImGui::Text("Advanced visual settings:");
	ImGui::InputFloat("Detail occlusion multiplier", &settings.DetailShadowStrength, 0.05f, 0.0f, 2);
	if (ImGui::IsItemHovered()) ImGui::SetTooltip("Additional small radius / high detail occlusion; too much will create aliasing & temporal instability");
	ImGui::InputFloat("Horizon angle threshold", &settings.HorizonAngleThreshold, 0.01f, 0.0f, 2);
	if (ImGui::IsItemHovered()) ImGui::SetTooltip("Reduces precision and tessellation related unwanted occlusion");
	ImGui::InputFloat("Occlusion max clamp", &settings.ShadowClamp, 0.01f, 0.0f, 2);
	if (ImGui::IsItemHovered()) ImGui::SetTooltip("occlusion = min( occlusion, value ) - limits the occlusion maximum");
	//ImGui::InputFloat( "Radius distance-based modifier",    &settings.RadiusDistanceScalingFunction   , 0.05f, 0.0f, 2 );
	//if( ImGui::IsItemHovered( ) ) ImGui::SetTooltip( "Used to modify ""Effect radius"" based on distance from the camera; for 1.0, effect world radius is constant (default);\nfor values smaller than 1.0, the effect radius will grow the more distant from the camera it is; if changed, ""Effect Radius"" often needs to be rebalanced as well" );

	settings.Radius = Maths::clamp(settings.Radius, 0.0f, 100.0f);
	settings.HorizonAngleThreshold = Maths::clamp(settings.HorizonAngleThreshold, 0.0f, 1.0f);
	settings.ShadowMultiplier = Maths::clamp(settings.ShadowMultiplier, 0.0f, 5.0f);
	settings.ShadowPower = Maths::clamp(settings.ShadowPower, 0.5f, 5.0f);
	settings.ShadowClamp = Maths::clamp(settings.ShadowClamp, 0.1f, 1.0f);
	settings.FadeOutFrom = Maths::clamp(settings.FadeOutFrom, 0.0f, 1000000.0f);
	settings.FadeOutTo = Maths::clamp(settings.FadeOutTo, 0.0f, 1000000.0f);
	settings.Sharpness = Maths::clamp(settings.Sharpness, 0.0f, 1.0f);
	settings.DetailShadowStrength = Maths::clamp(settings.DetailShadowStrength, 0.0f, 5.0f);
	//settings.RadiusDistanceScalingFunction    = vaMath::Clamp( settings.RadiusDistanceScalingFunction   , 0.1f, 1.0f        );

	ImGui::PopStyleColor(1);
}

void TCompPostFXAO::debugSSAO(){
	ImGui::DragFloat("Amount", &amount, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("Radius", &radius, 0.01f, 0.0f, 10.0f);
	ImGui::DragFloat("ZRange Discard", &zrange_discard, 0.001f, 0.0f, 0.1f);
	ImGui::DragFloat("Amount Spread", &amount_spreading, 0.01f, 0.0f, 1.0f);
}

void TCompPostFXAO::uploadSSAOCts(CTexture * linear_depth_texture) {
	linear_depth_texture->activate(TS_DEFERRED_LINEAR_DEPTH);

	ctes_shared.GlobalFXAmount = amount;
	ctes_shared.GlobalFXVal1 = radius;
	ctes_shared.GlobalFXVal2 = zrange_discard;
	ctes_shared.GlobalFXVal3 = 10 * (1.0f - amount_spreading);
	ctes_shared.updateGPU();

	drawFullScreenQuad(tech, nullptr);
}

void TCompPostFXAO::uploadIntelASSAOCts(CTexture * normals, ASSAO_Effect * fx) {
	TCompCamera * c_camera = getComponent<TCompCamera>();
	if (!c_camera) return;

	assao_inputs.ScissorRight = Render.width;
	assao_inputs.ScissorBottom = Render.height;
	assao_inputs.ViewportWidth = Render.width;
	assao_inputs.ViewportHeight = Render.height;
	assao_inputs.DepthSRV = Render.getShaderResourceView();
	assao_inputs.OverrideOutputRTV = nullptr;
	assao_inputs.DeviceContext = Render.ctx;
	assao_inputs.NormalSRV = nullptr;
	assao_inputs.DrawOpaque = true;
	assao_inputs.MatricesRowMajorOrder = true;
	MAT44 view = c_camera->getView();
#ifdef INTEL_SSAO_ENABLE_NORMAL_WORLD_TO_VIEW_CONVERSION
	assao_inputs.NormalSRV = normals->getShaderResourceView();
	//view = view.Transpose();
	assao_inputs.NormalsWorldToViewspaceMatrix = *(ASSAO_Float4x4*)&view;
	//assao_inputs.NormalsWorldToViewspaceMatrix = *(ASSAO_Float4x4*)&MAT44::Identity;
#endif
	MAT44 proj = c_camera->getProjection();
	//proj = proj.Transpose();
	assao_inputs.ProjectionMatrix = *(ASSAO_Float4x4*)&proj;
	fx->Draw(settings, &assao_inputs);
}

const CTexture* TCompPostFXAO::apply(CTexture * linear_depth_texture, CTexture * normals, ASSAO_Effect * fx) {
	if (!active) return white;

	if (!createOrResizeRT(Render.getWidth(), Render.getHeight())) return nullptr;

	CGpuScope gpu_scope("AO");
	rt_output->activateRT();
	if (use_assao)
		uploadIntelASSAOCts(normals, fx);
	else
		uploadSSAOCts(linear_depth_texture);

	ID3D11RenderTargetView* null_rt = nullptr;
	Render.ctx->OMSetRenderTargets(1, &null_rt, nullptr);

	return rt_output;
}