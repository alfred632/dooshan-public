#include "mcv_platform.h"
#include "comp_postFX_bloom.h"
#include "render/textures/render_to_texture.h"
#include "resources/resource.h"
#include "render/render_utils.h"
#include "render/blur/blur_step.h"

#include "engine.h"

DECL_OBJ_MANAGER("postfx_bloom", TCompPostFXBloom);

TCompPostFXBloom::TCompPostFXBloom()
  : cte_bloom(CTE_BUFFER_SLOT_BLOOM)
{
	bool is_ok = cte_bloom.create("bloom");
	assert(is_ok);
	auto & bloomProf = bloom_profiles["default"];

	// How we mix each downsampled scale
	bloomProf.add_weights = VEC4(3.0f, 1.0, 0.6f, 0.4f);
	current_active_profile = &bloomProf;
}

TCompPostFXBloom::~TCompPostFXBloom() {
	cte_bloom.destroy();
}

void TCompPostFXBloom::load(const json& j, TEntityParseContext& ctx) {
	TCompPostFXBlur::load(j, ctx);
	if (j.count("profiles") > 0) {
		for (auto & profile : j["profiles"]){
			if (profile.count("profile_name") == 0) continue;
			
			std::string & bloomProfName = profile.value("profile_name", "");
			// Get sure profile doesn't exist.
			assert((bloom_profiles.count(bloomProfName) == 0 || bloomProfName == "default") && "Bloom profile with same name already exists.");

			// Create bloom_profile.
			auto & bloom_prof = bloom_profiles[bloomProfName];
			
			bloom_prof.bloomProfileName = bloomProfName;
			if (profile.count("weights"))
				bloom_prof.add_weights = loadVector4(profile, "weights");

			bloom_prof.threshold_min = profile.value("threshold_min", bloom_prof.threshold_min);
			bloom_prof.threshold_max = profile.value("threshold_max", bloom_prof.threshold_max);
			bloom_prof.multiplier = profile.value("multiplier", bloom_prof.multiplier);
		}
	}

	// with the first use, init with the input resolution
	if (!createOrResizeRT(Render.getWidth(), Render.getHeight()))
		Utils::dbg("Bloom rt couldn't be created\n");

	tech_filter = EngineResources.getResource("bloom_filter.tech")->as<CTechnique>();
	tech_add = EngineResources.getResource("bloom_add.tech")->as<CTechnique>();
}

bool TCompPostFXBloom::createOrResizeRT(int width, int height) {
	if (!rt_highlights)
		rt_highlights = new CRenderToTexture();

	if (rt_highlights->getWidth() != width || rt_highlights->getHeight() != height) {
		char rt_name[64];
		sprintf(rt_name, "BloomFiltered_%08x", CHandle(this).asUnsigned());
		return rt_highlights->create(rt_name, Render.width, Render.height, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_UNKNOWN);
	}
	return true;
}

void TCompPostFXBloom::debugInMenu() {
	TCompPostFXBlur::debugInMenu();

	ImGui::Spacing(0, 5);
	ImGui::TextColored(ImVec4(1,1,0,1), "Bloom Parameters");

	ImGui::Text("Current Bloom Profile: %s", current_active_profile->bloomProfileName.c_str());
	ImGui::Spacing(0, 5);

	for (auto & profile : bloom_profiles) {
		ImGui::PushID(profile.first.c_str());
		ImGui::Spacing(0, 5);
		ImGui::Text("Bloom Profile: %s", profile.first.c_str());
		ImGui::DragFloat("Threshold Min", &profile.second.threshold_min, 0.01f, 0.f, 1.f);
		ImGui::DragFloat("Threshold Max", &profile.second.threshold_max, 0.01f, 0.f, 100.f);
		ImGui::DragFloat("Multiplier", &profile.second.multiplier, 0.01f, 0.f, 100.f);
		ImGui::DragFloat4("Add Weights", &profile.second.add_weights.x, 0.02f, 0.f, 3.f);
		ImGui::Spacing(0, 5);
		ImGui::Separator();
		ImGui::PopID();
	}
}

void TCompPostFXBloom::activateBloomProfile(const std::string & bloomProfileName) {
	assert(bloom_profiles.count(bloomProfileName) != 0 && "Bloom profile was not found");
	if (bloom_profiles.count(bloomProfileName) == 0) return;
	current_active_profile = &bloom_profiles[bloomProfileName];
}


void TCompPostFXBloom::apply(const std::string & bloomProfileName) {
	if (!active || nactive_steps == 0)
		return;

	activateBloomProfile(bloomProfileName);
	auto & bloomProf = *current_active_profile;

	// Do bloom.
	{
		PROFILE_FUNCTION("PostFx - Bloom");
		CGpuScope gpu_scope("Bloom rendering");

		{
			PROFILE_FUNCTION("PostFx - Bloom - Constants");
			cte_bloom.bloom_weights = bloomProf.add_weights * bloomProf.multiplier;
			cte_bloom.bloom_threshold_min = bloomProf.threshold_min * bloomProf.threshold_max;    // So min is always below max
			cte_bloom.bloom_threshold_max = bloomProf.threshold_max;

			// Remove weights of unused steps
			if (nactive_steps < 4)
				cte_bloom.bloom_weights.w = 0.f;
			if (nactive_steps < 3)
				cte_bloom.bloom_weights.z = 0.f;
			if (nactive_steps < 2)
				cte_bloom.bloom_weights.y = 0.f;

			cte_bloom.updateGPU();
			cte_bloom.activate();
		}

		assert(tech_add);

		{
			PROFILE_FUNCTION("PostFx - Bloom - Rendering");
			// Activate the slots in the range 0..N
			// The slot0 gets the most blurred whites
			int i = nactive_steps - 1;
			int nslot = 0;
			while (nslot < 4 && i >= 0) {
				steps[i]->getRTOutput()->activate(nslot);
				++nslot;
				--i;
			}

			drawFullScreenQuad(tech_add, nullptr);
		}
	}
}

// ---------------------------------------
void TCompPostFXBloom::generateHighlights(CRenderToTexture * in_texture) {
	if (!active) return;

	if (!createOrResizeRT(in_texture->getWidth(), in_texture->getHeight())) return;
	
	// Do bloom.
	{
		PROFILE_FUNCTION("PostFx - Bloom Highlights");
		CGpuScope gpu_scope("Bloom highlights rendering");

		{
			// Filter input image to gather only the highlights
			auto prev_rt = rt_highlights->activateRT();
			assert(prev_rt);

			cte_bloom.updateGPU();
			cte_bloom.activate();

			drawFullScreenQuad(tech_filter, in_texture);

			// Blur the highlights
			TCompPostFXBlur::apply(rt_highlights);

			// Restore the prev rt
			prev_rt->activateRT();
		}
	}
}

