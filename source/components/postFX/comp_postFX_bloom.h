#ifndef INC_COMPONENT_RENDER_BLOOM_H_
#define INC_COMPONENT_RENDER_BLOOM_H_

#include "comp_postFX_blur.h"

class TCompPostFXBloom : public TCompPostFXBlur {
	CCteBuffer< TCtesBloom >       cte_bloom;
	CRenderToTexture *             rt_highlights = nullptr;
	const CTechnique *             tech_filter = nullptr;
	const CTechnique *             tech_add = nullptr;

	// A Bloom Profile holds information about the bloom parameters.
	// We have multiple bloom configurations for different elements of the scene.
	struct BloomProfile {
		std::string                    bloomProfileName;
		Vector4                        add_weights;
		float                          threshold_min = 0.8f;
		float                          threshold_max = 1.f;
		float                          multiplier = 1.f;
	};
	std::map<std::string, BloomProfile> bloom_profiles;
	BloomProfile * current_active_profile = nullptr;


	// Activates a bloom profile to be used for the bloom pass.
	void activateBloomProfile(const std::string & bloomProfileName);
public:
	TCompPostFXBloom();
	~TCompPostFXBloom();
	
	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();

	// Generates the highlights texture based on the thresholds set to the bloom postfx.
	void generateHighlights(CRenderToTexture * in_texture);
	// Adds the bloom over the active rt.
	void apply(const std::string & bloomProfileName);

private:
	bool createOrResizeRT(int width, int height);
};

#endif
