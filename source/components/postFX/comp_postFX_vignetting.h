#pragma once

#include "components\common\comp_base.h"
#include "entity/entity.h"

class CTecnique;
class CRenderToTexture;
struct TMsgEntityCreated;

/* This component applies post processing effects */
class TCompPostFXVignetting : public TCompBase {
	DECL_SIBLING_ACCESS();

	/* Texture to render the effect to. */
	CRenderToTexture * rt;
	
	/* Technique and constants. */
	const CTechnique * vignettingTech; // Viggneting technique with the shader to show the effect.
	CCteBuffer<TCtesVignetting> cts_vignetting = CCteBuffer<TCtesVignetting>(CTE_BUFFER_SLOT_VIGNETTING);

	// Variables.
	bool fadeIn = true;
	float current_fade_time = 0.0f;
	float fall_time = 0.0001f;
	float vignettingAmount = 0.0f;

public:
	~TCompPostFXVignetting() {
		if(rt) rt->destroy();
		cts_vignetting.destroy();
	}

	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	
	// Applies the effect, returns the CRenderToTexture * with the applied effect.
	CRenderToTexture * apply(CRenderToTexture * textureToApplyPPE);


	/* Getters and setters. */
	float getVignettingValue() { return vignettingAmount; }
	
	void setVignettingValue(const float vignettingVal) { vignettingAmount = vignettingVal; }
	void setVignettingFallTime(float nFallTime) { fadeIn = nFallTime; }
	void setActive(bool nActive) {
		fadeIn = nActive ? true : false;
		active = nActive ? true : active;
	}


private:
	bool createOrResizeRT(int width, int height);
};