#include "mcv_platform.h"
#include "comp_postFX_burningfade.h"

#include "engine.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "render/textures/render_to_texture.h"

#include "components/common/comp_transform.h"

DECL_OBJ_MANAGER("postfx_burning_fade", TCompPostFXBurningFade);

void TCompPostFXBurningFade::load(const json& j, TEntityParseContext& ctx) {
	active = j.value("active", true);

	/* Fog technique. */
	std::string tech_filename = j.value("burning_fade_technique", "burning_fade.tech");
	burningFadeTech = EngineResources.getResource(tech_filename)->as<CTechnique>();

	if(!cts_burning_fade.create("BurningFade"))
		Utils::fatal("Failed while creating the cts_burning_fade constants.");

	edgeBurningColor = loadVector3(j, "edge_burning_color");
	burningEdgeSize = j.value("burning_edge_size", burningEdgeSize);
	colorIntensity = j.value("burning_color_intensity", colorIntensity);
	current_time = 0.0f;
	time_to_finish_burning_fade = j.value("burning_fade_time_to_finish", time_to_finish_burning_fade);
	background_treshold = j.value("burning_fade_background_treshold", background_treshold);

	cts_burning_fade.BurningFadeColorEdgeBurning = edgeBurningColor;
	cts_burning_fade.BurningFadeEdgeSize = burningEdgeSize;
	cts_burning_fade.BurningFadeColorIntensity = colorIntensity;
	cts_burning_fade.BurningFadeTimeToFinish = time_to_finish_burning_fade;
	cts_burning_fade.BurningFadeShowBackgroundThreshold = background_treshold;

	if(!createOrResizeRT(Render.getWidth(), Render.getHeight()))
		Utils::dbg("Burning fade rt couldn't be created\n");
}

bool TCompPostFXBurningFade::createOrResizeRT(int width, int height) {
	if (!rt)
		rt = new CRenderToTexture();

	if (rt->getWidth() != width || rt->getHeight() != height) {

		char rt_fog[64];
		sprintf(rt_fog, "Fog_%08x", CHandle(this).asUnsigned());
		return rt->create(rt_fog, width, height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	}
	return true;
}

void TCompPostFXBurningFade::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::Spacing(0, 5);
	
	if(ImGui::DragFloat3("Edge Burning Color: ", &edgeBurningColor.x, 0.01f, 0.0f, 1.0f))
		cts_burning_fade.BurningFadeColorEdgeBurning = edgeBurningColor;

	if (ImGui::DragFloat("Burning Edge Size: ", &burningEdgeSize, 0.01f, 0.0f, 10.0f))
		cts_burning_fade.BurningFadeEdgeSize = burningEdgeSize;

	if (ImGui::DragFloat("Color intensity edge burning: ", &colorIntensity, 0.01f, 0.0f, 10.0f))
		cts_burning_fade.BurningFadeColorIntensity = colorIntensity;

	if (ImGui::DragFloat("Time to finish burning_fade: ", &time_to_finish_burning_fade, 0.01f, 0.0f, 10.0f))
		cts_burning_fade.BurningFadeTimeToFinish = time_to_finish_burning_fade;
}

CRenderToTexture * TCompPostFXBurningFade::apply(CRenderToTexture * textureToApplyPPE) {
	if (!active) return textureToApplyPPE;

	if (!createOrResizeRT(textureToApplyPPE->getWidth(), textureToApplyPPE->getHeight())) return nullptr;

	current_time += Time.delta;
	while(current_time > time_to_finish_burning_fade)
		current_time -= time_to_finish_burning_fade;

	// Do Burning Fade
	{
		PROFILE_FUNCTION("PostFx - Burning Fade");
		CGpuScope gpu_scope("Burning Fade rendering");
		
		// Set constants.
		{
			PROFILE_FUNCTION("Burning Fade - Constants");
			cts_burning_fade.BurningFadeCurrentTime = current_time;
			cts_burning_fade.activate();
			cts_burning_fade.updateGPU();
		}

		{
			PROFILE_FUNCTION("Burning Fade - Rendering");
			rt->activateRT();
			drawFullScreenQuad(burningFadeTech, textureToApplyPPE);
		}
	}

	return rt;
}