#ifndef INC_COMP_GROUP_H_
#define INC_COMP_GROUP_H_

#include "comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class CTransform;

class TCompGroup : public TCompBase {
	DECL_SIBLING_ACCESS();
	VHandles handles;

	void forwardDefineLocalAABB(const TMsgDefineLocalAABB& msg);
	void createGroupAbsAABB(const TMsgDefineAbsAABB& msg);
	void onGroupStatus(const TMsgToogleComponent & msg);
public:
	~TCompGroup();
	static void registerMsgs();
	static void declareInLua();

	void add(CHandle h_new_child);
	void debugInMenu();

	template<typename T> void sendMsgToEntitiesInGroup(const T & msg) {
		for (auto h : handles) {
			CEntity * entity = h;
			entity->sendMsg(msg);
		}
	}

	CHandle GetEntityInsideGroupByName(const std::string & entityNameToSearch);
	VHandles & getEntitiesInsideGroup() { return handles; }
};

#endif
