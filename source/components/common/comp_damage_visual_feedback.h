#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompDamageVisualFeedback : public TCompBase {

public:
	static void registerMsgs();

	DECL_SIBLING_ACCESS();

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();

private:
	int _feedbackState = 3;
	int _storedState;
	float _feedbackTime = 0.1f;
	float _feedbackCurTime = 0.0f;
	bool _isActive;

	void onBulletDamage(const TMsgBulletDamage & msg);
	void onNailDamage(const TMsgNailDamage & msg);
	void onHitDamage(const TMsgHitDamage & msg);
	void startVisualFeedback();

};