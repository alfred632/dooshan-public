#include "mcv_platform.h"
#include "comp_group.h"
#include "comp_hierarchy.h"
#include "engine.h"
#include "comp_name.h"

DECL_OBJ_MANAGER("group", TCompGroup);

TCompGroup::~TCompGroup() {
	for (auto h : handles)
		h.destroy();
}

void TCompGroup::registerMsgs() {
	// Uncomment to enable aabb of parent includes aabb of all children on creation
	DECL_MSG(TCompGroup, TMsgDefineLocalAABB, forwardDefineLocalAABB);
	DECL_MSG(TCompGroup, TMsgDefineAbsAABB, createGroupAbsAABB);
	DECL_MSG(TCompGroup, TMsgToogleComponent, onGroupStatus);
}

void TCompGroup::declareInLua() {
	EngineScripting.declareComponent<TCompGroup>
		(
			"TCompGroup",
			"GetEntityInsideGroupByName", &TCompGroup::GetEntityInsideGroupByName,
			"entities", sol::readonly(&TCompGroup::handles)
			);
}

void TCompGroup::add(CHandle h_new_child) {
	// If it has a parent, ignore it.
	if (h_new_child.getOwner().isValid()) return;

	handles.push_back(h_new_child);
	// Using the handle owner to set his parent my entity
	h_new_child.setOwner(CHandle(this).getOwner());
}

void TCompGroup::debugInMenu() {
	for (auto h : handles)
		h.debugInMenu();
}

void TCompGroup::forwardDefineLocalAABB(const TMsgDefineLocalAABB& msg) {
	// Update msg.aabb using all children's aabb
	for (auto h : handles) {
		// Get local aabb of child
		AABB child_local_aabb;
		h.sendMsg(TMsgDefineLocalAABB{ &child_local_aabb });

		// If the child has a comp_hierarchy...
		CEntity* e_child = h;
		TCompHierarchy* c_hierarchy = e_child->getComponent<TCompHierarchy>();
		if (!c_hierarchy)
			continue;

		// Transform his local space into my local space
		AABB child_in_my_local_space;
		child_local_aabb.Transform(child_in_my_local_space, c_hierarchy->asMatrix());

		// And combine it with the original msg aabb
		AABB::CreateMerged(*msg.aabb, *msg.aabb, child_in_my_local_space);
	}
}

void TCompGroup::createGroupAbsAABB(const TMsgDefineAbsAABB& msg) {
	// Update msg.aabb using all children's aabb
	for (auto h : handles) {
		AABB result;

		// Get aabb of child
		h.sendMsg(TMsgDefineLocalAABB{ &result });

		// If the child has a comp_hierarchy...
		CEntity* e_child = h;
		TCompHierarchy* c_hierarchy = e_child->getComponent<TCompHierarchy>();
		// Transform his local space into my local space
		if (c_hierarchy)
			result.Transform(result, c_hierarchy->asMatrix());

		// And combine it with the original msg aabb
		AABB::CreateMerged(*msg.aabb, *msg.aabb, result);
	}
}

void TCompGroup::onGroupStatus(const TMsgToogleComponent & msg) {
	active = msg.active;
	TMsgToogleEntity msgToEntity = { active };
	for (auto h : handles) {
		CEntity * entity = h;
		entity->sendMsg(msgToEntity);
	}
}

CHandle TCompGroup::GetEntityInsideGroupByName(const std::string & entityNameToSearch) {
	for (auto h : handles) {
		CEntity * entity = h;
		if (!entity) continue;
		TCompName * name = entity->getComponent<TCompName>();
		if (!name) continue;
		std::string currentEntityName = name->getName();
		if (currentEntityName.compare(entityNameToSearch) == 0) // Equals.
			return h;
	}
	return CHandle();
}