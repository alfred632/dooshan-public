#include "mcv_platform.h"
#include "comp_ragdoll.h"
#include "components/common/comp_transform.h"
#include "skeleton/comp_skeleton.h"
#include "modules/module_manager.h"
#include "entity/entity.h"
#include "modules/module_physics.h"
#include "render/meshes/mesh.h"
#include "skeleton/skeleton_shape.h"
#include "skeleton/skeleton_shape_core.h"
#include "cal3d/cal3d.h"
#include "skeleton/cal3d2engine.h"
#include "skeleton/skeleton_shape.h"
#include "utils/json_resource.h"
#include "engine.h"

using namespace physx;

DECL_OBJ_MANAGER("ragdoll", TCompRagdoll);

// Simplify this
#include "resources/resource.h"
#include "render/render.h"
#include "comp_tags.h"
#include "comp_render.h"

TCompRagdoll::~TCompRagdoll() {
	if (articulation && EnginePhysics.isActive()) {
		articulation->release();
		articulation = nullptr;
	}
}

void TCompRagdoll::registerMsgs() {
	DECL_MSG(TCompRagdoll, TMsgEntityCreated, onCreated);
	DECL_MSG(TCompRagdoll, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompRagdoll, TMsgResetComponent, onComponentReset);
	DECL_MSG(TCompRagdoll, TMsgEntityDead, onEntityDead);
	DECL_MSG(TCompRagdoll, TMsgFullRestore, onResurrectionMsg);
}

void TCompRagdoll::load(const json& j, TEntityParseContext& ctx) {
	_initialForce = j.value("initial_force", _initialForce);
	if (j.count("dead_config"))
		_deadConfigPath = j.value<std::string>("dead_config", "");
	_staticBody = j.value<bool>("static_body", _staticBody);
}

void TCompRagdoll::onCreated(const TMsgEntityCreated&) {
	EnginePhysics.createArticulations(*this);

	if (!_deadConfigPath.empty())
		applyDeadConfiguration();
}

void TCompRagdoll::onToggleComponent(const TMsgToogleComponent& msg) {
	active = msg.active;

	// Delete in both cases, we create a new one if we want to activate it.
	if (articulation)
		deleteArticulation();

	if (active) {
		EnginePhysics.createArticulations(*this);

		if (_activated) {
			updateRagdollFromSkeleton();
			EnginePhysics.getScene()->addArticulation(*articulation);
		}
	}
}

void TCompRagdoll::onComponentReset(const TMsgResetComponent& msg) {
	active = true;
	_activated = false;

	if (_railgunJoint) {
		_railgunJoint->release();
		_railgunJoint = nullptr;
	}

	return;

	//// Reset their force and velocity
	//for (int i = 0; i < ragdoll.num_bones; i++) {
	//	ragdoll.bones[i].link->clearForce();
	//	ragdoll.bones[i].link->clearTorque();
	//	ragdoll.bones[i].link->setAngularVelocity(PxVec3(0.0f));
	//	ragdoll.bones[i].link->setLinearVelocity(PxVec3(0.0f));
	//}

	//EnginePhysics.getScene()->removeArticulation(*articulation);
}

void TCompRagdoll::onEntityDead(const TMsgEntityDead& msg) {
	PxVec3 forceVector = VEC3_TO_PXVEC3(VEC3(msg.hitDirection * msg.hitForce));
	activateRagdoll(forceVector, msg.hitForce != 0.0f);
}

void TCompRagdoll::onResurrectionMsg(const TMsgFullRestore& msg) {
	deactivateRagdoll();
}

void TCompRagdoll::declareInLua() {
	EngineScripting.declareComponent<TCompRagdoll>
		(
			"TCompRagdoll",
			"activateRagdoll", &TCompRagdoll::activateRagdoll
			);
}

void TCompRagdoll::updateRagdollFromSkeleton() {
	PROFILE_FUNCTION("updateRagdollFromSkeleton");
	CHandle h_comp_ragdoll(this);
	CEntity* ent = h_comp_ragdoll.getOwner();
	if (!ent)
		return;
	TCompTransform* comp_transform = ent->getComponent<TCompTransform>();
	CTransform* extra_trans = comp_transform;
	TCompSkeleton* comp_skel = ent->getComponent<TCompSkeleton>();
	if (comp_skel) {
		auto model = comp_skel->getModel();
		if (model) {
			auto core_model = model->getCoreModel();
			if (core_model) {
				auto skel = model->getSkeleton();
				if (skel) {
					auto core_skel = skel->getCoreSkeleton();
					if (core_skel) {

						int root_core_bone_id = core_skel->getVectorRootCoreBoneId()[0];

						for (int i = 0; i < ragdoll.num_bones; ++i) {
							auto& ragdoll_bone = ragdoll.bones[i];

							CalBone* cal_bone = skel->getBone(ragdoll_bone.idx);

							CTransform transform;
							transform.setPosition(Cal2DX(cal_bone->getTranslationAbsolute()));
							transform.setRotation(Cal2DX(cal_bone->getRotationAbsolute()));

							physx::PxTransform px_transform;
							px_transform.p = VEC3_TO_PXVEC3(transform.getPosition());
							px_transform.q = QUAT_TO_PXQUAT(transform.getRotation());

							ragdoll_bone.link->setGlobalPose(px_transform);
						}
					}
				}
			}
		}
	}
}

void TCompRagdoll::updateSkeletonFromRagdoll() {
	PROFILE_FUNCTION("updateSkeletonFromRagdoll");
	CHandle h_comp_ragdoll(this);
	CEntity* ent = h_comp_ragdoll.getOwner();
	if (!ent)
		return;
	TCompSkeleton* comp_skel = ent->getComponent<TCompSkeleton>();
	if (comp_skel) {
		auto model = comp_skel->getModel();
		if (model) {
			auto core_model = model->getCoreModel();
			if (core_model) {
				auto skel = model->getSkeleton();
				if (skel) {
					auto& cal_bones = skel->getVectorBone();
					assert(cal_bones.size() < MAX_SUPPORTED_BONES);

					for (int i = 0; i < cal_bones.size(); ++i) {
						bool found = false;
						for (int j = 0; j < ragdoll.num_bones; ++j) {
							const auto& ragdoll_bone = ragdoll.bones[j];
							if (ragdoll_bone.idx == i) {
								CalBone* bone = cal_bones[ragdoll_bone.idx];

								CTransform trans;
								auto t = ragdoll_bone.link->getGlobalPose();

								trans.setPosition(PXVEC3_TO_VEC3(t.p));
								trans.setRotation(PXQUAT_TO_QUAT(t.q));

								bone->clearState();
								bone->blendState(1.f, DX2Cal(trans.getPosition()), DX2Cal(trans.getRotation()));
								bone->calculateBoneSpace();
								found = true;
								break;
							}
						}

						if (!found) {
							CalBone* bone = cal_bones[i];
							bone->setCoreState();
							bone->calculateBoneSpace();
						}
					}
				}
			}
		}
	}
}

std::pair<std::string, physx::PxArticulationLink*> TCompRagdoll::getBoneFromGlobalPos(VEC3 pos)
{
	physx::PxArticulationLink* closestBone = nullptr;
	std::string boneName;
	float closestDistance = 1000;
	int i = 0;
	while (i < ragdoll.num_bones) {
		TSkeletonShapes::TSkeletonBoneShape bone = ragdoll.bones[i];
		if (VEC3::Distance(PXVEC3_TO_VEC3(bone.link->getGlobalPose().p), pos) < closestDistance) {
			closestBone = bone.link;
			boneName = bone.core->bone;
			closestDistance = VEC3::Distance(PXVEC3_TO_VEC3(bone.link->getGlobalPose().p), pos);
		}
		i++;
	}
	return std::pair<std::string, physx::PxArticulationLink*>(boneName, closestBone);
}

void TCompRagdoll::clearRagdollForces()
{
	int i = 0;
	while (i < ragdoll.num_bones) {
		ragdoll.bones[i].link->clearForce();
		ragdoll.bones[i].link->clearTorque();
		ragdoll.bones[i].link->setAngularVelocity(physx::PxVec3(0));
		ragdoll.bones[i].link->setLinearVelocity(physx::PxVec3(0));
		i++;
	}
}

void TCompRagdoll::toggleRagdollGravity(bool active) {
	for (int i = 0; i < ragdoll.num_bones; i++) {
		auto link = ragdoll.bones[i].link;
		link->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, !active);
	}
}

/* Right now, since it's only used by the explosion component, the force will only
act on the middle bone */
void TCompRagdoll::activateRagdoll(physx::PxVec3 forceVec, bool useForce) {
	_activated = true;

	if (!articulation)
		EnginePhysics.createArticulations(*this);
	EnginePhysics.getScene()->addArticulation(*articulation);

	updateRagdollFromSkeleton();

	if (useForce) {
		if (forceVec.isZero()) {
			forceVec.x = RNG.f(1.0f);
			forceVec.y = RNG.f(1.0f);
			forceVec.z = RNG.f(1.0f);
			forceVec.normalize();
			forceVec *= RNG.f(_initialForce);
		}

		auto link = ragdoll.bones[0].link;
		if (link)
			link->addForce(forceVec / 100.0f, physx::PxForceMode::eIMPULSE);
	}
}

void TCompRagdoll::deactivateRagdoll() {
	_activated = false;
	if (articulation)
		deleteArticulation();
	return;
}

void TCompRagdoll::deleteArticulation()
{
	EnginePhysics.getScene()->removeArticulation(*articulation);
	articulation->release();
	articulation = nullptr;

	for (int i = 0; i < ragdoll.num_bones; ++i)
		ragdoll.bones[i].link = nullptr;
	ragdoll.num_bones = 0;
}

void TCompRagdoll::update(float elapsed) {
	if (!_deadConfigPath.empty() && _staticBody)
		return;

	if (!_deadConfigPath.empty() && !_staticBody)
		updateSkeletonFromRagdoll();

	if (_activated) {
		updateSkeletonFromRagdoll();
		updateFlyingRagdoll();
		updatePostJointRagdoll();
	}
	else
		if (updateRagdollFromSkel)
			updateRagdollFromSkeleton();

}

void TCompRagdoll::startFlyingRagdoll(Vector3 destination, float magnitude, physx::PxArticulationLink* link) {
	if (!_activated)
		return;
	_isFlyingRagdool = true;
	_flyingDestination = destination;
	_flyingMagnitude = magnitude;
	_linkToApplyForce = link;
}

void TCompRagdoll::stopFlyingRagdoll() {
	_isFlyingRagdool = false;
	_flyingDestination = Vector3::Zero;
	_flyingMagnitude = 0.0f;
	_linkToApplyForce = nullptr;

	_railgunJointJustCreated = true;
}

void TCompRagdoll::updateFlyingRagdoll() {
	PROFILE_FUNCTION("updateFlyingRagdoll");
	if (!_linkToApplyForce)
		return;

	// Recalculate the direction
	PxVec3 forceDir = VEC3_TO_PXVEC3(_flyingDestination) - _linkToApplyForce->getGlobalPose().p;
	forceDir.normalize();
	physx::PxVec3 _flyingForce = forceDir * _flyingMagnitude;

	_linkToApplyForce->clearForce();
	_linkToApplyForce->addForce(_flyingForce);
}

void TCompRagdoll::updatePostJointRagdoll() {
	PROFILE_FUNCTION("updatePostJointRagdoll");
	if (!_railgunJointJustCreated)
		return;

	// Clock finished
	if (_railgunJointClock.isStarted() && _railgunJointClock.isFinished()) {
		_railgunJointClock.stop();
		_railgunJointJustCreated = false;
	}
	// Clock start first time
	else if (!_railgunJointClock.isStarted())
		_railgunJointClock.start();

	// Clear forces
	clearRagdollForces();
}

void TCompRagdoll::updatePhysxBoneShape() {
	//1.- Delete all pointers from joints, links and the articulation
	ragdoll = {}; //Restart the physx struct

	//2.- Create the articulations from module_physics again
	EnginePhysics.createArticulations(*this);
}

std::string TCompRagdoll::dumpRagdolls() {
	std::string returnString = "";
	json j;
	for (int i = 0; i < ragdoll.num_bones; i++)
	{
		j["bone"] = ragdoll.bones[i].core->bone;
		j["height"] = ragdoll.bones[i].core->height;
		j["radius"] = ragdoll.bones[i].core->radius;
		j["parent_bone"] = ragdoll.bones[i].core->parent_bone;

		VEC3 pos = ragdoll.bones[i].core->articulation.joint_parent_local_position;
		VEC3 rot = ragdoll.bones[i].core->articulation.joint_parent_local_rotation;
		VEC2 swingLimit = ragdoll.bones[i].core->articulation.swing_limit;
		bool swing = ragdoll.bones[i].core->articulation.swing_limit_enabled;
		VEC2 twistLimit = ragdoll.bones[i].core->articulation.twist_limit;
		bool twist = ragdoll.bones[i].core->articulation.twist_limit_enabled;

		//If not the root bone, then it has articulation and joint
		if (ragdoll.bones[i].core->parent_bone != "") {
			json parentJoint;
			parentJoint["joint_parent_local_position"] = std::to_string(pos.x) + " " + std::to_string(pos.y) + " " + std::to_string(pos.z);
			parentJoint["joint_parent_local_rotation"] = std::to_string(rot.x) + " " + std::to_string(rot.y) + " " + std::to_string(rot.z);
			parentJoint["swing_limit"] = std::to_string(swingLimit.x) + " " + std::to_string(swingLimit.y);
			parentJoint["swing_limit_enabled"] = swing;
			parentJoint["twist_limit"] = std::to_string(twistLimit.x) + " " + std::to_string(twistLimit.y);;
			parentJoint["twist_limit_enabled"] = twist;
			j["parent_joint"] = parentJoint;
		}

		returnString += ",\n" + j.dump(0);
	}
	// Remove first comma
	returnString.erase(returnString.begin());
	return returnString;
}

void TCompRagdoll::debugInMenu() {
	ImGui::PushID(this);

	ImGui::Text("Total active articulations: %i", EnginePhysics.getScene()->getArticulations(nullptr, 0));

	if (ImGui::Button("DEBUG: Toggle update ragdoll from skeleton"))
		updateRagdollFromSkel = !updateRagdollFromSkel;
	if (ImGui::Button("DEBUG: Activate component (ToggleComponent MSG)")) {
		TMsgToogleComponent msg;
		msg.active = true;
		onToggleComponent(msg);
	}
	if (ImGui::Button("DEBUG: Disable component (ToggleComponent MSG)")) {
		TMsgToogleComponent msg;
		msg.active = false;
		onToggleComponent(msg);
	}

	static int boneToAddForce = 0;
	ImGui::DragInt("DEBUG: Select bone to add force flying to the moon", &boneToAddForce, 1, 0, ragdoll.num_bones - 1);
	ImGui::Text(ragdoll.bones[boneToAddForce].core->bone.c_str());

	if (ImGui::Button("DEBUG: Make it fly to the moon!")) {
		auto link = ragdoll.bones[boneToAddForce].link;
		if (link)
			link->addForce(physx::PxVec3(0.0f, 5000.0f, 0.0f));
	}
	if (ImGui::Button("Debug: Enable gravity"))
		toggleRagdollGravity(true);
	ImGui::SameLine();
	if (ImGui::Button("Debug: Disable gravity"))
		toggleRagdollGravity(false);

	if (ImGui::Checkbox("activated", &_activated)) {
		if (_activated)
			//activateRagdoll(PxVec3(0.5f, 0.1f, 0.5f));
			activateRagdoll();
		else
			deactivateRagdoll();
	}

	if (ImGui::Button("updateSkelFromRag"))
		updateSkeletonFromRagdoll();
	ImGui::SameLine();
	if (ImGui::Button("updateRagFromSkel"))
		updateRagdollFromSkeleton();

	//Json dumper
	if (ImGui::Button("Show ragdolls dump")) {
		jsonStats = dumpRagdolls();
	}
	ImGui::SameLine();
	if (ImGui::Button("Dump ragdolls to clipboard")) {
		ImGui::LogToClipboard();
		ImGui::LogText(dumpRagdolls().c_str());
		ImGui::LogFinish();
	}
	if (ImGui::Button("Show dead ragdolls dump")) {
		jsonStats = dumpDeadRagdolls();
	}
	ImGui::SameLine();
	if (ImGui::Button("Dump dead ragdolls to clipboard")) {
		ImGui::LogToClipboard();
		ImGui::LogText(dumpDeadRagdolls().c_str());
		ImGui::LogFinish();
	}
	if (ImGui::Button("Hide dump"))
		jsonStats = "";
	ImGui::TextWrapped(jsonStats.c_str());

	// Show Ragdolls
	if (ImGui::TreeNode("All Ragdolls")) {
		//RenderDebug
		if (ImGui::Button("Show all render debug")) {
			for (int i = 0; i < ragdoll.num_bones; ++i) {
				auto ragdoll_bone = ragdoll.bones[i].core;
				ragdoll_bone->render_debug = true;
			}
		}
		if (ImGui::Button("Disable all render debug")) {
			for (int i = 0; i < ragdoll.num_bones; ++i) {
				auto ragdoll_bone = ragdoll.bones[i].core;
				ragdoll_bone->render_debug = false;
			}
		}

		//Configure each articulation
		for (int i = 0; i < ragdoll.num_bones; ++i) {
			auto json_bone_core = ragdoll.bones[i].core;
			TSkeletonShapes::TSkeletonBoneShape* bone_core = &ragdoll.bones[i];

			//Configure the articulation actor
			if (ImGui::TreeNode(json_bone_core->bone.c_str())) {
				ImGui::Checkbox("Render debug active: ", &json_bone_core->render_debug);
				//For every articulation actor we can modify the height, radius and offsets
				if (ImGui::DragFloat("Height", &json_bone_core->height, 0.001f, 0.01f, 10.0f))
					updatePhysxBoneShape();
				if (ImGui::DragFloat("Radius", &json_bone_core->radius, 0.001f, 0.01f, 10.0f))
					updatePhysxBoneShape();

				//If the bone is not the root articulation it means it has a parent and a joint
				if (!ragdoll.bones[i].isRoot) {
					ImGui::Spacing();
					ImGui::Separator();
					ImGui::Spacing();

					//For every articulation joint we can modify the height, radius and offsets
					if (ImGui::DragFloat3("localFrame0 position", &json_bone_core->articulation.joint_parent_local_position.x, 0.001f, -3.f, 3.f))
						updatePhysxBoneShape();
					if (ImGui::DragFloat3("localFrame0 rotation", &json_bone_core->articulation.joint_parent_local_rotation.x, 0.001f, -3.14f, 3.14f))
						updatePhysxBoneShape();
				}

				ImGui::Spacing();
				ImGui::Separator();
				ImGui::Spacing();

				ImGui::Checkbox("Swing limit enabled", &json_bone_core->articulation.swing_limit_enabled);
				if (json_bone_core->articulation.swing_limit_enabled) {
					if (ImGui::DragFloat2("Swing limit", &json_bone_core->articulation.swing_limit.x, 0.001f, -3.141592f, 3.141592f))
						updatePhysxBoneShape();
				}
				ImGui::Checkbox("Twist limit enabled", &json_bone_core->articulation.twist_limit_enabled);
				if (json_bone_core->articulation.swing_limit_enabled) {
					if (ImGui::DragFloat2("Twist limit", &json_bone_core->articulation.twist_limit.x, 0.001f, -3.141592f, 3.141592f))
						updatePhysxBoneShape();
				}

				ImGui::TreePop();
			}
		}
		ImGui::TreePop();
	}
	ImGui::PopID();

}

void TCompRagdoll::renderDebug() {
	VEC4 colour = VEC4(0.75, 0.0, 0.75, 1);
	VEC4 jointColour = VEC4(1.0, 0.5, 0.0, 1);
	TCompSkeleton* skel = getComponent<TCompSkeleton>();
	for (int i = 0; i < ragdoll.num_bones; ++i) {
		TSkeletonShapes::TSkeletonBoneShape& ragdoll_bone = ragdoll.bones[i];

		if (!ragdoll_bone.core->render_debug)
			continue;

		auto bone_core = ragdoll_bone.core;

		//Hack to render the core as his own father
		if (ragdoll_bone.isRoot)
			ragdoll_bone.parent_idx = 0;

		physx::PxTransform px_transform = ragdoll_bone.link->getGlobalPose();
		physx::PxTransform px_parent_transform = ragdoll.bones[ragdoll_bone.parent_idx].link->getGlobalPose();
		CTransform transform = toTransform(px_transform);
		CTransform parent_transform = toTransform(px_parent_transform);

		//Line between actual bone and parent bone
		MAT44 world1 = transform.asMatrix();
		MAT44 world2 = parent_transform.asMatrix();
		drawLine(world1.Translation(), world2.Translation(), VEC4(0.0, 1.0, 1.0, 1.0));

		//Spheres simulating the capsule collider
		VEC3 capsule_top_offset = VEC3(0, bone_core->height / 2.0f, 0);
		VEC3 capsule_bot_offset = VEC3(0, -bone_core->height / 2.0f, 0);

		// rotate displacement vector in order to get the right direction to draw two spheres simulating a capsule
		capsule_top_offset = DirectX::XMVector3Rotate(capsule_top_offset, transform.getRotation());
		capsule_bot_offset = DirectX::XMVector3Rotate(capsule_bot_offset, transform.getRotation());

		transform.setPosition(transform.getPosition() + capsule_top_offset);
		MAT44 top_sphere = transform.asMatrix();

		transform.setPosition(transform.getPosition() - capsule_top_offset + capsule_bot_offset);
		MAT44 bot_sphere = transform.asMatrix();

		//Render the two spheres simulating the capsule and a line between them
		drawLine(top_sphere.Translation(), bot_sphere.Translation(), colour);
		drawWiredSphere(top_sphere, ragdoll.bones[i].core->radius, colour);
		drawWiredSphere(bot_sphere, ragdoll.bones[i].core->radius, colour);

		//Continue the next iteration because the root articulation doesn't have a joint
		if (ragdoll_bone.isRoot)
			continue;

		//Calculate the joint position from the parent
		VEC3 rot = bone_core->articulation.joint_parent_local_rotation;
		CTransform localFrame0;
		localFrame0.setPosition(bone_core->articulation.joint_parent_local_position);
		localFrame0.setRotation(QUAT::CreateFromYawPitchRoll(rot.x, rot.y, rot.z));
		localFrame0.fromMatrix(localFrame0.asMatrix() * parent_transform.asMatrix());

		VEC3 localFrame0Dir = localFrame0.getLeft(); //We need to draw the left, because the front is the Z axis, and we need to align the X axis
		localFrame0Dir /= 2.0f;

		//Render the joint parent localFrame as a sphere and a line in the transform X axis
		drawWiredSphere(localFrame0.asMatrix(), 0.02f, jointColour);
		drawLine(localFrame0.getPosition(), localFrame0.getPosition() + localFrame0Dir, jointColour);
	}
}

std::string TCompRagdoll::dumpDeadRagdolls() {
	std::string returnString = "";
	json j;
	for (int i = 0; i < ragdoll.num_bones; i++)
	{
		PxTransform trans = ragdoll.bones[i].link->getGlobalPose();
		j["bone"] = ragdoll.bones[i].core->bone;
		j["position"] = std::to_string(trans.p.x) + " " + std::to_string(trans.p.y) + " " + std::to_string(trans.p.z);
		j["rotation"] = std::to_string(trans.q.x) + " " + std::to_string(trans.q.y) + " " + std::to_string(trans.q.z) + " " + std::to_string(trans.q.w);

		returnString += ",\n" + j.dump(0);
	}
	// Remove first comma
	returnString.erase(returnString.begin());
	return returnString;
}

void TCompRagdoll::applyDeadConfiguration() {
	// Check if file exists, and it is not empty
	if (!Utils::fileExists(_deadConfigPath.c_str())) return;
	const json& j_config = Utils::loadJson(_deadConfigPath);

	// If it is empty we delete the entity
	if (j_config.empty() || !j_config.is_array()) {
		this->getEntity().destroy();
		return;
	}

	// Activate the ragdoll
	_activated = true;

	// If the body is static we will not add the articulation to the scene and gain a bit of performance
	if (!_staticBody)
		EnginePhysics.getScene()->addArticulation(*articulation);

	// Load and set the links positions
	PxTransform trans{};
	PxArticulationLink* link = nullptr;
	for (int i = 0; i < ragdoll.num_bones; i++)
	{
		auto& jObj = j_config.at(i);
		std::string bone_name = jObj.value<std::string>("bone", "");
		assert(!bone_name.empty());

		for (auto& bone : ragdoll.bones) {
			if (bone.core->bone == bone_name) {
				link = bone.link;
				trans.p = VEC3_TO_PXVEC3(loadVector3(jObj, "position"));
				trans.q = QUAT_TO_PXQUAT(loadVector4(jObj, "rotation"));
				break;
			}
		}
		assert(link != nullptr);

		// set transform to link
		link->setGlobalPose(trans);

		// reset transform and link to default values
		link = nullptr;
		trans = PxTransform{};
	}

	updateSkeletonFromRagdoll();

	// disable armature component
	TCompSkeleton* c_skel = getComponent<TCompSkeleton>();
	if (c_skel)
		c_skel->setActive(false);

	// Send msg to attach aabb to ragdoll
	TMsgRagdollActivated msg;
	if (ragdoll.num_bones > 0) {
		msg.link = ragdoll.bones[0].link;
		if (msg.link != nullptr)
			getEntity().sendMsg(msg);
	}
	else
		return;
}
