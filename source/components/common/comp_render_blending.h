#pragma once

#include "comp_base.h"
#include "entity\entity.h"
#include "entity\common_msgs.h"
#include "components\camera\comp_blender_camera.h"

class CMesh;
class CTechnique;

struct TCompRenderBlending : public TCompBase {
	DECL_SIBLING_ACCESS();

	CHandle renderComp;

	bool active = false;
	float currentTime = 0.0f;
	float timeForFullBlending = 2.0f;
	float maxBlending = 0.5f;

	void onEntityCreated(const TMsgEntityCreated & msg);
	void onBlending(const TMsgBlendEntity & msg);
public:
	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();

	bool isActive() { return active; }
};