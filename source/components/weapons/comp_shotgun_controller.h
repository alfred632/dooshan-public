#pragma once
#include "components/weapons/comp_weapons_controller.h"
#include "entity/entity.h"
#include "components/common/comp_particles.h"
#include "components/controllers/comp_player_character_controller.h"

struct TMsgEntityCreated;
struct TMsgEntityTriggerEnter;
struct TMsgEntityTriggerExit;

class TCompShotgunController : public TCompWeaponsController {
	DECL_SIBLING_ACCESS();
	
	// Number of pellets.
	int _numShots = 6;
	float _specialShootForce = 100.0f;

	// Debug ray.
	bool _shootRaycast = false;
	std::vector<std::pair<VEC3, VEC3>> _debugRays;

	// Handle of the player.
	CHandle playerH;
	CHandle playerTransformH;

	// Transform of the weapon.
	CHandle gunTransform;

	std::string _shootSoundEvent = "";
	std::string _reloadSoundEvent = "";
	std::string _grenadeSoundEvent = "";

	std::string _smokeParticlesEntityName = "";
	int _smokeNumParticles = 48;
	std::string _sparksParticlesEntityName = "";
	int _sparksNumParticles = 24;
	std::string _fireParticlesEntityName = "";

	float ShotgunBulletForce = 10.0f;

	DECL_TCOMP_ACCESS(_smokeParticlesEntityName, TCompParticles, SmokePSystem);
	DECL_TCOMP_ACCESS(_sparksParticlesEntityName, TCompParticles, SparksPSystem);
	DECL_TCOMP_ACCESS(_fireParticlesEntityName, TCompParticles, FirePSystem);
	DECL_TCOMP_ACCESS("Player", TCompPlayerCharacterController, PlayerCharacterController);

	// On message functions.
	void onEntityCreated(const TMsgEntityCreated & msg);
	void onGroupCreated(const TMsgEntitiesGroupCreated & msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter & msg);
	void onTriggerExit(const TMsgEntityTriggerExit & msg);

	// UI
	void updateSpecialIconActivated(bool isSpecialModeActivated) override;

	// Private functions.
	bool checkPlayer();
public:
	TCompShotgunController();

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void update(float dt);
	void drawCrosshair();
	static void registerMsgs();

	bool shootNormal();
	bool shootSpecial();
	bool reload();
};