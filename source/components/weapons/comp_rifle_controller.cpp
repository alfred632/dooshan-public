#include "mcv_platform.h"
#include "comp_weapons_controller.h"
#include "comp_rifle_controller.h"
#include "modules/module_physics.h"
#include "engine.h"
#include "entity/msgs.h"
#include "utils/phys_utils.h"
#include "entity/entity_parser.h"

#include "components/common/comp_name.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_health.h"
#include "skeleton/comp_bone_tracker.h"
#include "skeleton/comp_skeleton.h"
#include "components/ui/bar/comp_reload_ui_controller.h"
#include "components/ui/comp_crosshair_ui_controller.h"
#include "components/camera/comp_camera.h"

using namespace physx;

DECL_OBJ_MANAGER("rifle", TCompRifleController);

TCompRifleController::TCompRifleController()
{
	_weaponName = "Rifle";
	_stats.damage = 3.0f;
	_stats.rof = 2.0f;
	_stats.dispersion = 1.0f;
	_stats.effectiveDistance = 30.0f;
	_stats.reloadTime = 3.0f;
	_timeToNextShot = 0;
	_capacity = 1;
	_currentAmmo = _capacity;
}

void TCompRifleController::load(const json& j, TEntityParseContext& ctx) {
	TCompWeaponsController::load(j, ctx);

	const json& jKnots = j["curve_knots"];
	for (auto& knot : jKnots) {
		_knotOffsets.push_back(loadVector3(knot, "knot"));
		_shootCurve.addKnot(loadVector3(knot, "knot"));
	}

	_hitRange = j.value("hitRange", _hitRange);
	_specialShootCD = j.value("specialShootCD", _specialShootCD);

	_lineRenderEntityName = j.value("line_render", "");
	_smokeParticlesEntityName = j.value("smoke_entity", _smokeParticlesEntityName);
	_smokeNumParticles = j.value("smoke_num_particles", _smokeNumParticles);
	_muzzleFlashEntityName = j.value("muzzle_flash_entity", _muzzleFlashEntityName);
	_shootSoundEvent = j.value("shoot_sound_event", _shootSoundEvent);
	_reloadSoundEvent = j.value("reload_sound_event", _reloadSoundEvent);
	_grenadeSoundEvent = j.value("grenade_launch_sound_event", _grenadeSoundEvent);
	RifleBulletForce = j.value("RifleBulletForce", RifleBulletForce);
}

void TCompRifleController::debugInMenu()
{
	TCompWeaponsController::debugInMenu();

	ImGui::Text("Knot editor");
	for (int i = 0; i < _knotOffsets.size(); i++) {
		ImGui::PushID(i);
		ImGui::DragFloat3("", &_knotOffsets[i].x, 0.01f, -10.0f, 10.0f);
		VEC3 knot = _shootCurve.getKnots()[i];
		ImGui::Text("X: %f Y: %f Z: %f", knot.x, knot.y, knot.z);
		ImGui::PopID();
	}
	if (ImGui::Button("Add knot")) {
		_knotOffsets.push_back(_knotOffsets.empty() ? VEC3(0, 0, 0) : _knotOffsets.back() + VEC3(0.0f, 0.0f, 0.5f));
		_shootCurve.addKnot(_knotOffsets.back() + getCannonPosition(gunTransform));
	}
	TCompTransform* t = gunTransform;
	ImGui::Text("gun Left: %f %f %f", t->getLeft().x, t->getLeft().y, t->getLeft().z);
	ImGui::Text("gun Up: %f %f %f", t->getUp().x, t->getUp().y, t->getUp().z);
	ImGui::Text("gun Front: %f %f %f", t->getFront().x, t->getFront().y, t->getFront().z);
}

void TCompRifleController::renderDebug()
{
	TCompTransform * transform = getComponent<TCompTransform>();
	if (!transform) return;
	drawWiredSphere(getCannonPosition(transform), 0.10f);

	drawLine(transform->getPosition(), transform->getPosition() + transform->getFront() * 2, VEC4(0,1,1,1));

	// Draw the raycast
	if (_shootRaycast)
		drawLine(_rayOrig, _rayDest, Vector4(1, 0, 0, 1));

	// Draw the grenade aim indicator
	_shootCurve.drawCurve();
	for (auto& knot : _shootCurve.getKnots())
		drawWiredSphere(knot, .2f, VEC4(1, 1, 0, 1));

}

void TCompRifleController::update(float dt) {
	updateAimCurve();
}

void TCompRifleController::drawCrosshair() {
	if (!checkHUD()) return;

	CEntity * hud = hudHandle;
	if (hud) {
		TCompCrosshairUIController * crosshair = hud->getComponent<TCompCrosshairUIController>();
		if (crosshair) {
			crosshair->draw(-1, 0.0f);
		}
	}
}

void TCompRifleController::registerMsgs() {
	DECL_MSG(TCompRifleController, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompRifleController, TMsgEntitiesGroupCreated, onGroupCreated);
	DECL_MSG(TCompRifleController, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompRifleController, TMsgEntityTriggerExit, onTriggerExit);
}

void TCompRifleController::onEntityCreated(const TMsgEntityCreated & msg) {
	gunTransform = getComponent<TCompTransform>();
	gunCollider = getComponent<TCompCollider>();
	cameraPlayerOutputH = getEntityByName(CameraPlayerName);

	_specialCDClock = CClock(_specialShootCD);
}

void TCompRifleController::onGroupCreated(const TMsgEntitiesGroupCreated & msg) {
	TCompWeaponsController::onGroupCreated(msg);

	bone_tracker_comp = getComponent<TCompBoneTracker>();
	if (starting_weapon)
		attachWeaponToHand();
	else
		attachWeaponToHolder();
}

void TCompRifleController::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	// Raise weapon animation. Change to block state so we can't shoot.
	// We can also check the type of entity if necessary.
}

void TCompRifleController::onTriggerExit(const TMsgEntityTriggerExit & msg) {
	// Low weapon animation. Change to normal state so we can shoot again.
	// We can also check the type of entity if necessary.
}

void TCompRifleController::updateSpecialIconActivated(bool isSpecialModeActivated)
{
	UI::CImage* img = EngineUI.getImageWidgetByName("special_icons", "special_rifle");
	if (img)
		img->getImageParams()->color = isSpecialModeActivated ? _specialActivatedColor : VEC4(1.0f, 1.0f, 1.0f, 1.0f);
}

bool TCompRifleController::shootNormal()
{
	if (!_isReloading) {
		TCompTransform* w_trans = getComponent<TCompTransform>();
		if (!w_trans)
			return false;

		if (_currentAmmo != 0 && _timeToNextShot < ImGui::GetTime()) {
			// TODO Future : throw shoot anim

			// Get camera transform, position and direction.
			// Also get player handle and transform.
			// If not found or invalid, cancel.
			if (!checkCamera()) return false;

			CEntity * camera = cameraPlayerH;
			TCompTransform * c_trans = cameraTransformH;
			TCompCamera * cameraComponent = cameraCameraH;

			VEC3 cameraPos = c_trans->getPosition();
			VEC3 cameraDirection = c_trans->getFront();
			float infiniteDistance = cameraComponent->getFar() + 1.0f; // Get as far as the camera zfar.

			// Shoot the ray from the camera front
			PxRaycastBuffer pointToShotAtHitData = PhysUtils::shootPlayerRaycast(c_trans->getPosition(), cameraDirection, infiniteDistance);

			// If we hit something, we will shoot to that point otherwise, we point to a place far away in the "infinite".
			VEC3 pointToShotAt = cameraPos + cameraDirection * infiniteDistance; // We point to the infinite.
			VEC3 pointToInfinite = pointToShotAt;
			if (pointToShotAtHitData.hasBlock)
				pointToShotAt = cameraPos + (cameraDirection * pointToShotAtHitData.block.distance); // Get the hitPoint on World Position

			// Get rifle position and shoot from there again
			CTransform r_trans;
			r_trans.setPosition(getCannonPosition(w_trans));
			r_trans.setRotation(w_trans->getRotation());
			Vector3 r_position = r_trans.getPosition();
			VEC3 dirToEnemy = pointToShotAt - r_position;
			if (cameraDirection.Dot(dirToEnemy) < 1.0f)
				dirToEnemy = pointToInfinite;
			dirToEnemy.Normalize();

			// Shot from the rifle to the point we hit in the camera.
			PxRaycastBuffer bulletHitData = PhysUtils::shootPlayerRaycast(r_position, dirToEnemy, _stats.effectiveDistance);

			// DEBUG RAY
			_shootRaycast = true;
			_rayOrig = r_trans.getPosition();
			_rayDest = r_trans.getPosition() + (dirToEnemy * _stats.effectiveDistance);

			if (bulletHitData.hasBlock) {
				_rayDest = r_trans.getPosition() + (dirToEnemy * bulletHitData.block.distance);
				// Get hit entity and it's collider component
				CHandle entityHit;
				CHandle h_comp;
				h_comp.fromVoidPtr(bulletHitData.block.actor->userData);

				if (h_comp.isValid())
					entityHit = h_comp.getOwner();

				if (!entityHit.isValid())
					return false;

				TMsgNailDamage msg;
				msg.h_sender = CHandle(this).getOwner();
				msg.damage = _stats.damage;
				msg.damageDirection = dirToEnemy;
				msg.damageForce = RifleBulletForce;
				msg.hitCoord = r_trans.getPosition() + (dirToEnemy * bulletHitData.block.distance);
				msg.hitDistance = bulletHitData.block.distance;
				msg.actorHit = bulletHitData.block.actor;
				msg.hitPos = PXVEC3_TO_VEC3(bulletHitData.block.position);
				msg.hitNormal = PXVEC3_TO_VEC3(bulletHitData.block.normal);
				entityHit.sendMsg(msg);

				// Spawn a decal on the entity.
				spawnParticlesAndDecalOnHit(entityHit, bulletHitData, dirToEnemy);
			}

			// Bullet tracer
			TCompWeaponManager* weaponManager = getWeaponManager();
			weaponManager->addTracer(r_position, _rayDest,
															 _stats.tracerThickness, _stats.tracerColor,
															 _stats.tracerDuration, _stats.tracerDelay, 
															 _stats.tracerStartFadeFactor
			);

			sendCameraShakeOnShot();

			TCompParticles* smoke = getSmokePSystem();
			if (smoke && smoke->isActive()) {
				smoke->spawnParticles(_smokeNumParticles);
			}			
			TCompParticles* muzzleFlash = getMuzzleFlashPSystem();
			if (muzzleFlash && muzzleFlash->isActive()) {
				muzzleFlash->spawnParticles(1);
			}

			_timeToNextShot = (float)ImGui::GetTime() + _stats.rof;
			_currentAmmo--;

			/* Play audio event*/
			if (!_shootSoundEvent.empty())
				EngineAudio.playEvent(_shootSoundEvent);

			return true;
		}
	}
	return false;
}

bool TCompRifleController::shootSpecial() {
	if (_specialCDClock.isFinished())
		_specialCDClock.stop();

	if (!_specialCDClock.isStarted()) {
		TEntityParseContext ctx;
		parseScene(_projecticlePath, ctx);

		//Disable the special mode and consume 1 special ammunition
		setSpecialMode(false);
		useSpecialAmmo();
		updateSpecialIcon();
		_specialCDClock.start();

		//For the normal mode
		_timeToNextShot = (float)ImGui::GetTime() + _stats.rof;

		if (!_grenadeSoundEvent.empty())
			EngineAudio.playEvent(_grenadeSoundEvent);

		return true;
	}

	return false;
}

bool TCompRifleController::reload()
{
	if (_storedAmmo > 0 && _currentAmmo < _capacity && !_activeReloadTried) {

		if (_isReloading) {

			_activeReloadTried = true;
			if (TCompReloadUIController* reloadBarComp = getComponent<TCompReloadUIController>())
				return reloadBarComp->checkIfSuccess();
		}
		else {
			_isReloading = true;

			if (TCompReloadUIController* reloadBarComp = getComponent<TCompReloadUIController>())
				reloadBarComp->startReload(_stats.reloadTime, _activeReloadOffset, _activeReloadPenaltyTime);

			if (!_reloadSoundEvent.empty())
				EngineAudio.playEvent(_reloadSoundEvent);

			return true;
		}

		return false;
	}

	return false;
}

/**** SPECIAL MODE ****/
void TCompRifleController::updateAimCurve(){
	TCompTransform* t = gunTransform;
	if(t)
		for (int i = 0; i < _knotOffsets.size(); i++) {
			VEC3 knotPos = DirectX::XMVector3Rotate(_knotOffsets[i], t->getRotation());
			_shootCurve.editKnot(getCannonPosition(gunTransform) + knotPos, i);
		}

}

