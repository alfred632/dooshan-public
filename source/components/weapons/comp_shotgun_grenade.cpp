#include "mcv_platform.h"
#include "engine.h"
#include "entity/msgs.h"
#include "utils/phys_utils.h"
#include "entity/entity_parser.h"

#include "comp_shotgun_grenade.h"
#include "components/common/comp_tags.h"
#include "components/game/status_effects/comp_status_effects.h"
#include "components/controllers/comp_character_controller.h"
#include "components/common/comp_render.h"

using namespace physx;

DECL_OBJ_MANAGER("shotgun_grenade", TCompShotgunGrenade);

TCompShotgunGrenade::~TCompShotgunGrenade() {
	for (auto handle : _buffedEntities) {
		if (handle.isValid()) {
			CEntity* e = handle;
			TCompTags* c_tags = e->getComponent<TCompTags>();
			TCompStatusEffects* c_effects = e->getComponent<TCompStatusEffects>();
			if (!c_tags || !c_effects) continue;

			// If player or enemy remove the buff
			if (c_tags->hasTag("player"))
				c_effects->removeStatusEffect("health_over_time_shotgun_grenade_heal");
			else if (c_tags->hasTag("enemy"))
				c_effects->removeStatusEffect("damage_over_time_shotgun_grenade_damage");
		}
	}
}

void TCompShotgunGrenade::load(const json& j, TEntityParseContext& ctx) {
	loadStatusEffects(j);
	_timeToLive = j.value("time_to_live", 6.0f);
	_remainingTimeToActivateVanish = j.value("remaining_time_to_activate_vanish", _remainingTimeToActivateVanish);
	_smokeSoundEvent = j.value("smoke_sound_event", _smokeSoundEvent);
}

void TCompShotgunGrenade::renderDebug() {

}

void TCompShotgunGrenade::debugInMenu() {

}

void TCompShotgunGrenade::update(float dt) {
	//Check for dead entities or invalid
	for (auto it = _buffedEntities.begin(); it != _buffedEntities.end(); it++) {
		if ((*it).isValid()) {
			CEntity* e = *it;
			TCompHealth* c_health = e->getComponent<TCompHealth>();
			if (c_health && c_health->isDead()) {
				_buffedEntities.erase(it);
				break;
			}
		}
		else {
			_buffedEntities.erase(it);
			break;
		}
	}

	//Check if the grenade has run out of time
	_timeToLive -= dt;
	if (_timeToLive < _remainingTimeToActivateVanish && !_activatedVanish)
		activateVanish();

	if (_timeToLive <= 0.0f)
		this->getEntity().destroy();
}

void TCompShotgunGrenade::activateVanish()
{
	_activatedVanish = true;

	TCompRender* render = getComponent<TCompRender>();
	if (render)
		render->setCurrentState("vanish");
}

void TCompShotgunGrenade::registerMsgs() {
	DECL_MSG(TCompShotgunGrenade, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompShotgunGrenade, TMsgEntityTriggerExit, onTriggerExit);
	DECL_MSG(TCompShotgunGrenade, TMsgEntityCreated, onEntityCreated);
}

void TCompShotgunGrenade::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	CEntity* e = msg.h_entity;
	if (!e) return;

	bool validEntity = false;

	TCompTags* c_tags = e->getComponent<TCompTags>();
	TCompStatusEffects* c_effects = e->getComponent<TCompStatusEffects>();
	if (!c_tags || !c_effects) return;

	// If player or enemy add the buff
	if (c_tags->hasTag("player")) {
		c_effects->parseStatusEffects(health_eff);
		validEntity = true;
	}
	else if (c_tags->hasTag("enemy")) {
		TCompCharacterController* c_con = e->getComponent< TCompCharacterController>();
		if (!c_con) return;
		if (c_con->isDead()) return; //Flying enemy due to the railgun
		c_effects->parseStatusEffects(damage_eff);
		validEntity = true;
	}

	// If the entity is valid add it to the vector
	if (!validEntity) return;

	if (std::find(_buffedEntities.begin(), _buffedEntities.end(), msg.h_entity) != _buffedEntities.end()) {
		Utils::dbg("[TCompShotgunGrenade::onTriggerEnter] Entity already on the buffed list.\n");
	}
	else {
		_buffedEntities.push_back(msg.h_entity);
	}
}

void TCompShotgunGrenade::onTriggerExit(const TMsgEntityTriggerExit & msg) {
	CEntity* e = msg.h_entity;
	if (!e) return;

	bool validEntity = false;

	TCompTags* c_tags = e->getComponent<TCompTags>();
	TCompStatusEffects* c_effects = e->getComponent<TCompStatusEffects>();
	if (!c_tags || !c_effects) return;

	// If player or enemy remove the buff
	if (c_tags->hasTag("player")) {
		c_effects->removeStatusEffect("health_over_time_shotgun_grenade_heal");
		validEntity = true;
	}
	else if (c_tags->hasTag("enemy")) {
		c_effects->removeStatusEffect("damage_over_time_shotgun_grenade_damage");
		validEntity = true;
	}

	// If the entity is valid remove it from the vector
	if (!validEntity) return;

	auto endIt = std::remove_if(_buffedEntities.begin(), _buffedEntities.end(), [msg](const CHandle& chandle) {
		return chandle.asVoidPtr() == msg.h_entity.asVoidPtr();
	});
	_buffedEntities.erase(endIt, _buffedEntities.end());
}

void TCompShotgunGrenade::onEntityCreated(const TMsgEntityCreated& msg) {
	if (TCompTransform* c_trans = getComponent<TCompTransform>()) {
		if (!_smokeSoundEvent.empty())
			EngineAudio.playEvent(_smokeSoundEvent, c_trans->getPosition());
		spawnDecal();
	}
}

void TCompShotgunGrenade::spawnDecal() {
	TCompTransform* c_trans = getComponent<TCompTransform>();
	if (!c_trans)
		return;

	//Shoot the ray to the floor
	physx::PxRaycastBuffer decalHitData = PhysUtils::shootBloodSplatterRaycast(c_trans->getPosition(), Vector3(0.0f, -1.0f, 0.0f));

	// Spawn water decal in the given position if we hit somewhere.
	if (decalHitData.hasBlock) {
		//Get position, lookAt and randomAngle
		physx::PxVec3 lookAtPhys = decalHitData.block.position + decalHitData.block.normal;
		Vector3 position = PXVEC3_TO_VEC3(decalHitData.block.position);
		Vector3 lookAt = PXVEC3_TO_VEC3(lookAtPhys);
		float randomAngle = (RNG.f(2.0f) - 1.0f) * PI;

		EngineLogicManager.SpawnDecal("shotgunGrenadeWater", position, lookAt, randomAngle);
	}
}

void TCompShotgunGrenade::loadStatusEffects(const json& j) {
	health_eff = j["status_effects_player"];
	damage_eff = j["status_effects_enemy"];
}