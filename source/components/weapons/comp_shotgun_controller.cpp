#include "mcv_platform.h"
#include "comp_weapons_controller.h"
#include "comp_shotgun_controller.h"
#include "modules/module_physics.h"
#include "engine.h"
#include "entity/msgs.h"
#include "utils/phys_utils.h"

#include "components/common/comp_name.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_health.h"
#include "skeleton/comp_bone_tracker.h"
#include "components/ui/bar/comp_reload_ui_controller.h"
#include "components/ui/comp_crosshair_ui_controller.h"
#include "components/camera/comp_camera_manager.h"
#include "components/camera/comp_camera.h"

#include "entity/entity_parser.h"

#define DISPERSION_SCREEN_FACTOR 0.04f

using namespace physx;

DECL_OBJ_MANAGER("shotgun", TCompShotgunController);

TCompShotgunController::TCompShotgunController() {
	_weaponName = "Shotgun";
	_stats.damage = 5.0f;
	_stats.rof = 1.0f;
	_stats.dispersion = 1.0f;
	_stats.effectiveDistance = 6.0f;
	_stats.reloadTime = 2.0f;
	_timeToNextShot = 0;
	_capacity = 10;
}

void TCompShotgunController::load(const json& j, TEntityParseContext& ctx) {
	TCompWeaponsController::load(j, ctx);
	_specialShootForce = j.value("specialShootForce", _specialShootForce);
	_debugRays.reserve(_numShots);
	for (int i = 0; i < _numShots; i++)
		_debugRays.push_back(std::pair<VEC3, VEC3>());
	_smokeParticlesEntityName = j.value("entity_smoke", _smokeParticlesEntityName);
	_sparksParticlesEntityName = j.value("entity_sparks", _sparksParticlesEntityName);
	_fireParticlesEntityName = j.value("entity_fire", _fireParticlesEntityName);
	_smokeNumParticles = j.value("smoke_num_particles", _smokeNumParticles);
	_sparksNumParticles = j.value("sparks_num_particles", _sparksNumParticles);
	_shootSoundEvent = j.value("shoot_sound_event", _shootSoundEvent);
	_reloadSoundEvent = j.value("reload_sound_event", _reloadSoundEvent);
	_grenadeSoundEvent = j.value("grenade_launch_sound_event", _grenadeSoundEvent);
	ShotgunBulletForce = j.value("ShotgunBulletForce", ShotgunBulletForce);
}

void TCompShotgunController::renderDebug() {
	TCompTransform * transform = gunTransform;
	if (!transform) return;
	drawWiredSphere(getCannonPosition(transform), 0.10f);

	// Draw the raycasts
	if (_shootRaycast) {
		for (auto ray : _debugRays) {
			drawLine(ray.first, ray.second, Vector4(1, 0, 0, 1));
		}
	}
}

void TCompShotgunController::debugInMenu() {
	TCompWeaponsController::debugInMenu();
}

void TCompShotgunController::update(float dt) {}

void TCompShotgunController::drawCrosshair() {
	if (!checkHUD()) return;

	CEntity * hud = hudHandle;
	if (hud) {
		TCompCrosshairUIController * crosshair = hud->getComponent<TCompCrosshairUIController>();
		if (crosshair) {
			float innerRadius = _stats.dispersion * WORLD_TO_SCREEN_FACTOR;
			crosshair->draw(-1, innerRadius);
		}
	}
}

void TCompShotgunController::registerMsgs() {
	DECL_MSG(TCompShotgunController, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompShotgunController, TMsgEntitiesGroupCreated, onGroupCreated);
	DECL_MSG(TCompShotgunController, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompShotgunController, TMsgEntityTriggerExit, onTriggerExit);
}

void TCompShotgunController::onEntityCreated(const TMsgEntityCreated & msg) {
	gunTransform = getComponent<TCompTransform>();
	cameraPlayerOutputH = getEntityByName(CameraPlayerName);
}

void TCompShotgunController::onGroupCreated(const TMsgEntitiesGroupCreated & msg) {
	TCompWeaponsController::onGroupCreated(msg);
	bone_tracker_comp = getComponent<TCompBoneTracker>();

	if (starting_weapon)
		attachWeaponToHand();
	else
		attachWeaponToHolder();
}

void TCompShotgunController::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	// Raise weapon animation. Change to block state so we can't shoot.
	// We can also check the type of entity if necessary.
}

void TCompShotgunController::onTriggerExit(const TMsgEntityTriggerExit & msg) {
	// Low weapon animation. Change to normal state so we can shoot again.
	// We can also check the type of entity if necessary.
}

void TCompShotgunController::updateSpecialIconActivated(bool isSpecialModeActivated)
{
	UI::CImage* img = EngineUI.getImageWidgetByName("special_icons", "special_shotgun");
	img->getImageParams()->color = isSpecialModeActivated ? _specialActivatedColor : VEC4(1.0f, 1.0f, 1.0f, 1.0f);
}

bool TCompShotgunController::checkPlayer() {
	if (playerH.isValid()) return true;

	playerH = getEntityByName("Player");
	if (!playerH.isValid())
		return false;

	CEntity * player = playerH;
	playerTransformH = player->getComponent<TCompTransform>();

	return true;
}

bool TCompShotgunController::shootNormal() {
	if (!_isReloading) {
		TCompTransform* w_trans = getComponent<TCompTransform>();
		if (!w_trans) return false;

		if (_currentAmmo != 0 && _timeToNextShot < ImGui::GetTime()) {
			// TODO Future : throw shoot anim

			// Get camera transform, position and direction.
			// Also get player handle and transform.
			// If not found or invalid, cancel.
			if (!checkCamera() || !checkPlayer()) return false;

			sendCameraShakeOnShot();

			CEntity * camera = cameraPlayerH;
			TCompTransform * c_trans = cameraTransformH;
			TCompCamera * cameraComponent = cameraCameraH;

			VEC3 cameraPos = c_trans->getPosition();
			VEC3 cameraDirection = c_trans->getFront();
			float infiniteDistance = cameraComponent->getFar() + 1.0f; // Get as far as the camera zfar.

			// Shoot the ray from the camera front. If we hit something, we will shoot to that point, otherwise, we point to a place
			// far away in the "infinite".
			PxRaycastBuffer pointToShotAtHitData = PhysUtils::shootPlayerRaycast(c_trans->getPosition(), cameraDirection, infiniteDistance);
			VEC3 pointToShotAt = cameraPos + cameraDirection * infiniteDistance; // We point to the infinite.
			VEC3 pointToInfinite = pointToShotAt;
			if (pointToShotAtHitData.hasBlock)
				pointToShotAt = cameraPos + (cameraDirection * pointToShotAtHitData.block.distance); // Get the hitPoint on World Position

			// Get shotgun cannon position and shoot from there again.
			CTransform r_trans;
			r_trans.setPosition(getCannonPosition(w_trans));
			r_trans.setRotation(w_trans->getRotation());
			Vector3 r_position = r_trans.getPosition();
			VEC3 dirToEnemy = pointToShotAt - r_position;
			
			// If camera is pointing behind the player shoot to the "infinite".
			// Prevents bullets going bacwards.
			if (cameraDirection.Dot(dirToEnemy) < 1.0f)
				dirToEnemy = pointToInfinite;
			dirToEnemy.Normalize();

			/* Apply dispersion */
			CEntity * player = playerH;
			TCompTransform * p_trans = playerTransformH;

			// Calculate scale factor so dispersion will be according to how close the camera is to the player
			float r, theta;
			VEC3 dirProjectile, deviation;
			float scale_factor = cameraDirection.Dot(p_trans->getPosition() - c_trans->getPosition());
			for (int i = 0; i < _numShots; i++) {
				// Get random polar coordinates for deviation
				r = RNG.f(_stats.dispersion) * scale_factor * DISPERSION_SCREEN_FACTOR;
				theta = (float) RNG.d(2.0 * M_PI);
				// Obtain deviation vector with x y coordinates
				deviation.x = r * cosf(theta);
				deviation.y = r * sinf(theta);
				// Rotate the deviation vector to match the direction of the enemy
				deviation = DirectX::XMVector3Rotate(deviation, c_trans->getRotation());
				// Add bullet deviation
				dirProjectile = dirToEnemy + deviation;
				dirProjectile.Normalize();

				// Shot from the rifle to the point we hit in the camera.
				PxRaycastBuffer bulletHitData = PhysUtils::shootPlayerRaycast(r_position, dirProjectile, _stats.maxDistance);

				// DEBUG RAY
				_shootRaycast = true;
				_debugRays[i].first = r_trans.getPosition();
				_debugRays[i].second = r_trans.getPosition() + (dirProjectile * _stats.maxDistance);

				if (bulletHitData.hasBlock) {
					float distance = bulletHitData.block.distance;
					_debugRays[i].second = r_trans.getPosition() + (dirProjectile * distance);
					// Get hit entity and it's collider component
					CHandle entityHit;
					CHandle h_comp;
					h_comp.fromVoidPtr(bulletHitData.block.actor->userData);

					if (h_comp.isValid())
						entityHit = h_comp.getOwner();

					if (!entityHit.isValid())
						return false;

					float damage = 0.0f;
					if (distance > _stats.effectiveDistance) {
						float falloffRatio = (distance - _stats.effectiveDistance) / (_stats.maxDistance - _stats.effectiveDistance);
						damage = floor(_stats.falloff(_stats.damage, 0.0f, falloffRatio));
					}
					else
						damage = floor(_stats.damage);

					if (damage > 0.0f) {
						TMsgBulletDamage msg;
						msg.h_sender = CHandle(this).getOwner();
						msg.damage = damage;
						msg.damageDirection = dirToEnemy;
						msg.damageForce = ShotgunBulletForce;
						msg.actorHit = bulletHitData.block.actor;
						msg.hitPos = PXVEC3_TO_VEC3(bulletHitData.block.position);
						msg.hitNormal = PXVEC3_TO_VEC3(bulletHitData.block.normal);
						entityHit.sendMsg(msg);

						// Spawn a decal on the entity.
						spawnParticlesAndDecalOnHit(entityHit, bulletHitData, dirToEnemy);
					}
				}
			}

			TCompParticles* smoke = getSmokePSystem();
			if (smoke && smoke->isActive()) {
				// Try to get the player velocity in Vector3 so the particles when spawned have a base velocity.
				TCompPlayerCharacterController* c_playerController =  getPlayerCharacterController();
				if (c_playerController) {
					smoke->setBaseVelocity(c_playerController->getCurrentVelocity() / 2.0f);
					smoke->clearBaseVelocityNextFrame();
				}
				smoke->spawnParticles(_smokeNumParticles);
			}
			TCompParticles* sparks = getSparksPSystem();
			if (sparks && sparks->isActive()) {
				sparks->spawnParticles(_sparksNumParticles);
			}
			TCompParticles* fire = getFirePSystem();
			if (fire) {
				fire->setActive(true);
			}

			_timeToNextShot = (float)ImGui::GetTime() + _stats.rof;
			_currentAmmo--;

			/* Play audio event*/
			if (!_shootSoundEvent.empty())
				EngineAudio.playEvent(_shootSoundEvent);

			return true;
		}
	}
	return false;
}

bool TCompShotgunController::shootSpecial() {
	if (!_isReloading) {
		if (_timeToNextShot > ImGui::GetTime())
			return false;

		TCompTransform* w_trans = getComponent<TCompTransform>();
		if (!w_trans)
			return false;

		if (!checkCamera() || !checkPlayer()) return false;

		//Disable the special mode and consume 1 special ammunition
		setSpecialMode(false);
		useSpecialAmmo();
		updateSpecialIcon();

		//The gun cannon will be the root transform.
		CTransform r_trans;
		r_trans.setPosition(getCannonPosition(w_trans));
		r_trans.setRotation(w_trans->getRotation());

		//Parse the grenade with the root transform
		TEntityParseContext ctx;
		ctx.root_transform = r_trans;
		parseScene("data/prefabs/weapons/shotgunGrenade.json", ctx);
		CEntity* e = ctx.entities_loaded[0];
		if (e) {
			TCompCollider* c_col = e->getComponent<TCompCollider>();
			if (c_col) {
				PxRigidDynamic* actor = static_cast<PxRigidDynamic*>(c_col->actor);
				if (actor) {
					CEntity* camera = cameraPlayerH;
					TCompTransform* c_trans = cameraTransformH;
					TCompCamera* cameraComponent = cameraCameraH;

					VEC3 cameraPos = c_trans->getPosition();
					VEC3 cameraDirection = c_trans->getFront();
					float infiniteDistance = cameraComponent->getFar() + 1.0f; // Get as far as the camera zfar.

					// Shoot the ray from the camera front. If we hit something, we will shoot to that point, otherwise, we point to a place
					// far away in the "infinite".
					PxRaycastBuffer pointToShotAtHitData = PhysUtils::shootPlayerRaycast(c_trans->getPosition(), cameraDirection, infiniteDistance);
					VEC3 pointToShotAt = cameraPos + cameraDirection * infiniteDistance; // We point to the infinite.
					VEC3 pointToInfinite = pointToShotAt;
					if (pointToShotAtHitData.hasBlock)
						pointToShotAt = cameraPos + (cameraDirection * pointToShotAtHitData.block.distance); // Get the hitPoint on World Position

					// Get shotgun cannon position and shoot from there again.
					Vector3 r_position = r_trans.getPosition();
					VEC3 dirToEnemy = pointToShotAt - r_position;
					dirToEnemy.Normalize();
					actor->addForce(VEC3_TO_PXVEC3(dirToEnemy) * _specialShootForce, PxForceMode::eIMPULSE);
					actor->setAngularVelocity(PxVec3(RNG.f(0.5f, 2.5f), RNG.f(0.5f, 2.5f), RNG.f(0.5f, 2.5f)));
				} 
			}
		}

		//For the normal mode
		_timeToNextShot = (float)ImGui::GetTime() + _stats.rof;

		/* Play audio event*/
		if (!_grenadeSoundEvent.empty())
			EngineAudio.playEvent(_grenadeSoundEvent);

		return true;
	}
	return false;
}

bool TCompShotgunController::reload() {
	if (_storedAmmo > 0 && _currentAmmo < _capacity && !_activeReloadTried) {
		if (_isReloading) {

			_activeReloadTried = true;
			if (TCompReloadUIController* reloadBarComp = getComponent<TCompReloadUIController>())
				return reloadBarComp->checkIfSuccess();
		}
		else {
			_isReloading = true;

			if (TCompReloadUIController * reloadBarComp = getComponent<TCompReloadUIController>())
				reloadBarComp->startReload(_stats.reloadTime, _activeReloadOffset, _activeReloadPenaltyTime);

			if (!_reloadSoundEvent.empty())	
				EngineAudio.playEvent(_reloadSoundEvent);

			//_activeReloadTimer.start();
			return true;
		}

		return false;
	}

	return false;
}