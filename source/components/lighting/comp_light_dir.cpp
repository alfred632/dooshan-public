#include "mcv_platform.h"
#include "comp_light_dir.h"

#include "components/camera/comp_camera.h"
#include "components/common/comp_culling.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_aabb.h"

#include "render/textures/texture.h"
#include "render/textures/render_to_texture_array.h" 
#include "render/render_manager.h" 

#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("light_dir", TCompLightDir);

#define CSM_DISTANCE_TO_DIR_LIGHT 10000

DXGI_FORMAT readFormat(const json & j, const std::string& label);

TCompLightDir::~TCompLightDir() {
	if(shadows_rt)
		shadows_rt->destroy();
}

void TCompLightDir::registerMsgs() {
	DECL_MSG(TCompLightDir, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompLightDir, TMsgToogleComponent, onToggleComponent);
}

void TCompLightDir::declareInLua() {
	EngineScripting.declareComponent<TCompLightDir>
	(
		"TCompLightDir",
		"GetLightIntensity", &TCompLightDir::getIntensity,
		"GetLightColor", &TCompLightDir::getColor,
		"SetLightIntensity", &TCompLightDir::setIntensity,
		"SetLightColor", &TCompLightDir::setColor
	);
}

void TCompLightDir::onEntityCreated(const TMsgEntityCreated & msg) {
	transform_h = getComponent<TCompTransform>();
	cameraCompH = getComponent<TCompCamera>();
	cullingCompH = getComponent<TCompCulling>();

	assert(cameraCompH.isValid() && "Attention light directions require a camera component!");
	TCompCamera * camera = cameraCompH;
	TCompCulling * cullingComp = cullingCompH;
	TEntityParseContext ctx;

	if(camera)
		camera->load(lightProjectionParameters, ctx);
	if (cullingComp)
		cullingComp->setActive(false);
}

void TCompLightDir::load(const json & j, TEntityParseContext & ctx) {
	// Store any parameters we may need to pass to the camera component.
	lightProjectionParameters = j;
	lightProjectionParameters["visible_in_debug"] = false; // we don't want to use them in the camera manager.

	// Load light color and intensity.
	TCompBaseLight::load(j, ctx);

	// Check if we need to allocate a shadow map
	casts_shadows = j.value("casts_shadows", false);
	if (casts_shadows) {
		shadows_step = j.value("shadows_step", shadows_step);
		shadows_resolution = j.value("shadows_resolution", shadows_resolution);
		auto shadowmap_fmt = readFormat(j, "shadows_fmt");
		assert(shadows_resolution > 0);

		char my_name[64];
		sprintf(my_name, "shadow_map_%08x", CHandle(this).asUnsigned());
		shadows_rt = new CRenderToTextureArray();
		bool is_ok = shadows_rt->create(my_name, NUM_CASCADES_SHADOW_MAP, shadows_resolution, shadows_resolution, DXGI_FORMAT_R8G8B8A8_UNORM, shadowmap_fmt);
		assert(is_ok);
	}
	shadows_enabled = casts_shadows;

	return;
}

void TCompLightDir::debugInMenu() {
	TCompCamera * camera = cameraCompH;
	if (!camera) return;

	TCompBaseLight::debugInMenu();
	ImGui::DragFloat("Step Size", &shadows_step, 0.01f, 0.f, 5.f);
	ImGui::DragFloat("Sphere offsets", centerOffsets, 0.01, -1000.0f, 1000.0f);

	ImGui::Spacing(0, 5);
	camera->debugInMenu();
	ImGui::Spacing(0, 5);

	shadows_rt->renderInMenu();
}

void TCompLightDir::update(float dt) {}

void TCompLightDir::renderDebug() {
	TCompCamera * camera = cameraCompH;
	if (!camera) return;

	TCompBaseLight::renderDebug();

	MAT44 inv_view_proj = camera->getViewProjection().Invert();
	auto mesh = EngineResources.getResource("unit_frustum.mesh")->as<CMesh>();

	// Sample several times to 'view' the z distribution along the 3d space
	const int nsamples = 10;
	for (int i = 1; i < nsamples; ++i) {
		float f = (float)i / (float)(nsamples - 1);
		MAT44 world = MAT44::CreateScale(1.f, 1.f, f) * inv_view_proj;
		drawMesh(mesh, world, VEC4(1, 1, 1, 1));
	}

	// Show projections in frustums cascade.
	for (int i = 0; i < NUM_CASCADES_SHADOW_MAP; i++) {
			MAT44 world = MAT44::CreateScale(1.f, 1.f, 1.f) * cascade_volume[i];
			drawMesh(mesh, world, VEC4(1, 1, 0, 1));
	}

	// Show culling volumes in frustums cascade.
	for (int i = 0; i < NUM_CASCADES_SHADOW_MAP; i++) {
		MAT44 world = MAT44::CreateScale(1.f, 1.f, 1.f) * culling_volume[i];
		drawMesh(mesh, world, VEC4(1, 0, 1, 1));
	}

	// Show culling volumes in frustums cascade.
	for (int i = 0; i < NUM_CASCADES_SHADOW_MAP; i++) {
		drawAxis(shadowCenter[i]);
		drawWiredSphere(shadowCenter[i], radiusDebug[i]);
		drawAxis(view_pos[i].Invert());
	}
}

// Updates the Shader Cte Light with MY information
void TCompLightDir::activate() {
	TCompCamera * camera = cameraCompH;
	if (!camera) return;
	
	// To avoid converting the range -1..1 to 0..1 in the shader
	// we concatenate the view_proj with a matrix to apply this offset
	MAT44 mtx_offset = MAT44::CreateScale(VEC3(0.5f, -0.5f, 1.0f))
		* MAT44::CreateTranslation(VEC3(0.5f, 0.5f, 0.0f));
	
	ctes_light.LightColor = color;
	ctes_light.LightIntensity = intensity;
	ctes_light.LightPosition = camera->getEye();
	ctes_light.LightFront = camera->getFront();
  
	ctes_light.LightViewProjOffset = globalShadows;
	for (int i = 0; i < NUM_CASCADES_SHADOW_MAP; ++i) {
		//ctes_light.LightViewProjOffset[i] = cascade_view_projections[i] * mtx_offset;
		ctes_light.CascadeEndClipSpace[i] = Vector4(0, 0, clipSpaceCascadeEnds[i], 0);
		ctes_light.CascadeOffsets[i] = cascadeOffsets[i];
		ctes_light.CascadeScales[i] = cascadeScales[i];
	}
  
	// If we have a ZTexture, it's the time to activate it
	if (shadows_rt) {
		ctes_light.LightHasShadows = true;
		ctes_light.LightShadowInverseResolution = 1.0f / (float)shadows_rt->getWidth();
		ctes_light.LightShadowStep = shadows_step;
		ctes_light.LightShadowStepDivResolution = shadows_step / (float)shadows_rt->getWidth();
		assert(shadows_rt->getZTextureArray());
		shadows_rt->getZTextureArray()->activate(TS_LIGHT_SHADOW_MAP);
	}
	else
		ctes_light.LightHasShadows = false;
	ctes_light.updateGPU();
}

void TCompLightDir::generateShadowMap(CCamera & renderCamera) {
	PROFILE_FUNCTION("ShadowMap");
	if (!active || !shadows_rt || !shadows_enabled) return;

	// For testing.
	//TCompCamera * cam = TCompCameraManager::getCamera(TCompCameraManager::Cameras::Final_Camera);

	// Get the orthographic projection of the different frustas of the
	// camera, this will be used for CSM shadows.
	doCSM(renderCamera);
}

void TCompLightDir::doCSM(CCamera & renderCamera) {
	TCompCamera * cameraComp = cameraCompH;
	TCompCulling * cullingComp = cullingCompH;
	assert(cameraComp && "Camera component missing from light.");
	if (!cameraComp) return;

	// Before starting our rendering, we copy the base value of the light camera.
	// We will return to it once all shadows and their projections are rendered.
	CCamera & cameraLight = *cameraComp;
	CCamera lightCameraCopy = *cameraComp;

	// Set the camera of the light as the rendering camera. As we will render from it.
	CRenderManager::get().setEntityCamera(cameraCompH.getOwner());

	// Activate the culling component. We will need to also calculate the culling view matrix we will use during the culling process.
	// This view will be in the position of the light and extend from it until the zfar of the projection.
	if(cullingComp)
		cullingComp->setActive(true);

	/* Start of the CSM algorithm. */

	// Fetch all corners from the frustas of the camera rendering the scene. Also get the radius.
	const Vector3 * renderCamFrustumCorners = renderCamera.calculateCSMFrustasCorners();
	const float * frustaRadiusBS = renderCamera.calculateCSMFrustaBSRadius();

	// Copy the z clips of the camera.
	std::copy(renderCamera.getClipSpaceCascadeEnds(), renderCamera.getClipSpaceCascadeEnds() + NUM_CASCADES_SHADOW_MAP, clipSpaceCascadeEnds);

	// Get the direction of the light.
	Vector3 lightDir = cameraLight.getFront();

	// Set a global shadow matrix we will use later to fetch the cascade offsets and scale for better shadow quality.
	globalShadows = MakeGlobalShadowMatrix(renderCamera, lightDir);

	// Now, for each cascade, we get the camera frustum and calculate the projection for our light.
	for (unsigned int cascadeIdx = 0; cascadeIdx < NUM_CASCADES_SHADOW_MAP; cascadeIdx++) {
		
		// Calculate the centroid of the view frustum slice
		Vector3 frustumCenter;
		for (unsigned int j = 0; j < 8; ++j)
			frustumCenter = frustumCenter + renderCamFrustumCorners[cascadeIdx * 8 + j];
		frustumCenter *= 1.0f / 8.0f;
		
		if (cascadeIdx == 0) {
			float cascadeExtraDistance = centerOffsets[cascadeIdx];
			clipSpaceCascadeEnds[cascadeIdx] += cascadeExtraDistance;
			frustumCenter += renderCamera.getFront() * cascadeExtraDistance;
		}
		else {
			float cascadeExtraDistance = frustaRadiusBS[cascadeIdx] * centerOffsets[cascadeIdx]; // Offset based on radius.
			clipSpaceCascadeEnds[cascadeIdx] += cascadeExtraDistance;
			frustumCenter += renderCamera.getFront() * cascadeExtraDistance;
		}

		// Get radius.
		float sphereRadius = frustaRadiusBS[cascadeIdx];
		Vector3 maxExtents = Vector3(sphereRadius, sphereRadius, sphereRadius);
		Vector3 minExtents = -maxExtents;
		Vector3 cascadeExtents = maxExtents - minExtents;

		// Debug.
		shadowCenter[cascadeIdx] = Matrix::CreateTranslation(frustumCenter);
		radiusDebug[cascadeIdx] = sphereRadius;

		// Get position of the shadow camera behind the frustum center.
		Vector3 shadowCameraPos = frustumCenter + lightDir * minExtents.z;

		// Come up with a new orthographic camera for the shadow caster
		CCamera shadowCamera;
		shadowCamera.lookAt(shadowCameraPos, frustumCenter, Vector3::Up);
		shadowCamera.setOrthoParams(false, minExtents.x, maxExtents.x, maxExtents.y, minExtents.y, 0.0f, cascadeExtents.z);

		view_pos[cascadeIdx] = shadowCamera.getView();

		// Create the rounding matrix, by projecting the world-space origin and determining
		// the fractional offset in texel space.
		createShadowRoundedMatrix(shadowCamera);

		// Gets the cascade offset and scale which will allow us to fetch pixels on cascades based on the bounding spheres and not a simple split.
		GetCascadeOffsetAndScale(cascadeIdx, globalShadows, shadowCamera);

		// This will be used by the CSM shader.
		cascade_view_projections[cascadeIdx] = shadowCamera.getViewProjection();

		// Copy the shadow camera values to the camera component
		// of the light so the scene gets culled with it.
		cameraLight = shadowCamera;

		/* Update the culling of this view cascade so it can be used when rendering shadows.. */
		updateCulling(cameraLight, cullingComp, cascadeIdx, maxExtents, minExtents, cascadeExtents);

		// Used for debugging culling volumes.
		culling_volume[cascadeIdx] = cameraLight.getViewProjection().Invert();

		// we copy the values again so we use the correct projection for the shadows as it was changed for the culling volume.
		cameraLight = shadowCamera;

		// Used for debugging shadow volumes.
		cascade_volume[cascadeIdx] = shadowCamera.getViewProjection().Invert();

		// In this slot is where we activate the render targets that we are going
		// to update now. You can't be active as texture and render target at the
		// same time
		CTexture::setNullTexture(TS_LIGHT_SHADOW_MAP);

		// Now, render the scene.
		shadows_rt->activateRT(cascadeIdx);
		{
			PROFILE_FUNCTION("Clear&SetCommonCtes");
			shadows_rt->clearDepthBuffer(cascadeIdx);
			activateCamera(cameraLight, shadows_rt->getWidth(), shadows_rt->getHeight());
		}

		{
			PROFILE_FUNCTION("Render_Shadow_Maps");
			CRenderManager::get().render(CATEGORY_SHADOWS);
		}
	}

	// Return the light camera to its default value.
	cameraLight = lightCameraCopy;

	if(cullingComp)
		cullingComp->setActive(false);
}

Matrix TCompLightDir::MakeGlobalShadowMatrix(CCamera & renderCamera, const Vector3 & lightDir) {
	// Get the center from the complete frustum of the render camera.
	const Vector3 * frustumCorners;
	frustumCorners = renderCamera.calculateCSMFrustumCorners();
	Vector3 frustumCenter;
	for (unsigned int i = 0; i < 8; ++i)
		frustumCenter += frustumCorners[i];
	frustumCenter /= 8.0f;

	// Get position of the shadow camera
	//Vector3 shadowCameraPos = frustumCenter - lightDir * -0.5f;
	Vector3 shadowCameraPos = frustumCenter + lightDir * -0.5f;

	// Come up with a new orthographic camera for the shadow caster
	CCamera shadowCamera;
	shadowCamera.lookAt(shadowCameraPos, frustumCenter, Vector3::Up);
	shadowCamera.setOrthoParams(false, -0.5f, 0.5f, 0.5f, -0.5f, 0.0f, 1.0f);

	Matrix texScaleBias = Matrix::CreateScale(Vector3(0.5f, -0.5f, 1.0f))
		* Matrix::CreateTranslation(0.5f, 0.5f, 0.0f);

	return shadowCamera.getViewProjection() * texScaleBias;
}

void TCompLightDir::GetCascadeOffsetAndScale(int cascadeIdx, const Matrix & globalShadowMatrix, CCamera & shadowCamera) {
	// Apply the scale/offset matrix, which transforms from [-1,1]
	// post-projection space to [0,1] UV space
	Matrix texScaleBias = Matrix::CreateScale(Vector3(0.5f, -0.5f, 1.0f)) * Matrix::CreateTranslation(0.5f, 0.5f, 0.0f);
	
	Matrix shadowMatrix = shadowCamera.getViewProjection();
	shadowMatrix = shadowMatrix * texScaleBias;

	// Calculate the position of the lower corner of the cascade partition, in the UV space
	// of the first cascade partition
	Matrix invCascadeMat = shadowMatrix.Invert();
	Vector3 cascadeCorner = Vector3::Transform(Vector3(0.0f, 0.0f, 0.0f), invCascadeMat);
	cascadeCorner = Vector3::Transform(cascadeCorner, globalShadowMatrix);

	// Do the same for the upper corner
	Vector3 otherCorner = Vector3::Transform(Vector3(1.0f, 1.0f, 1.0f), invCascadeMat);
	otherCorner = Vector3::Transform(otherCorner, globalShadowMatrix);

	// Calculate the scale and offset
	Vector3 cascadeScale = Vector3(1.0f, 1.0f, 1.0f) / (otherCorner - cascadeCorner);
	cascadeOffsets[cascadeIdx] = Vector4(-cascadeCorner.x, -cascadeCorner.y, -cascadeCorner.z, 0.0f);
	cascadeScales[cascadeIdx] = Vector4(cascadeScale.x, cascadeScale.y, cascadeScale.z, 1.0f);
}

void TCompLightDir::createShadowRoundedMatrix(CCamera & shadowCamera) {
	Matrix shadowMatrix = shadowCamera.getViewProjection();
	Vector4 shadowOrigin = Vector4(0.0f, 0.0f, 0.0f, 1.0f); // Origin in the world
	shadowOrigin = Vector4::Transform(shadowOrigin, shadowMatrix);
	shadowOrigin = shadowOrigin * (shadows_resolution / 2.0f);

	Vector4 roundedOrigin = XMVectorRound(shadowOrigin);
	Vector4 roundOffset = XMVectorSubtract(roundedOrigin, shadowOrigin);
	roundOffset = XMVectorScale(roundOffset, 2.0f / shadows_resolution);
	roundOffset = XMVectorSetZ(roundOffset, 0.0f);
	roundOffset = XMVectorSetW(roundOffset, 0.0f);

	Matrix shadowProj = shadowCamera.getProjection();
	shadowProj._41 += roundOffset.x;
	shadowProj._42 += roundOffset.y;
	shadowCamera.setProjectionMatrix(shadowProj);
	shadowCamera.setViewProjectionMatrix(shadowCamera.getView() * shadowCamera.getProjection());
}

void TCompLightDir::updateCulling(CCamera & lightCameraWithShadowValues, TCompCulling * cullingComp,
	int cascadeIdx, const Vector3 & maxExtents, const Vector3 & minExtents, const Vector3 & cascadeExtents) {
	// Set the culling projection for culling when rendering the cascade.
	lightCameraWithShadowValues.setOrthoParams(false, minExtents.x, maxExtents.x, minExtents.y, maxExtents.y, -CSM_DISTANCE_TO_DIR_LIGHT, cascadeExtents.z);
	
	if (cullingComp)
		cullingComp->update(0.0f); // Also update the culling so we cull from the new perspective.
}