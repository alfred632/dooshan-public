#pragma once

#include "comp_base_light.h"

struct TMsgEntityCreated;

class TCompLightPoint : public TCompBaseLight {

	CHandle transformCompH;

	// Stores the values of the light parameters that are related
	// to the camera. Will be loaded on entity create.
	json lightParameters;

	// Light params
	float           radius = 1.0f;

public:
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	static void registerMsgs();

	DECL_SIBLING_ACCESS();
  
	void activate();
	MAT44 getWorld();
	void setRange(float nRange) override { radius = nRange; }

private:
	void onEntityCreated(const TMsgEntityCreated & msg);
};