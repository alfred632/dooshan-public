#include "mcv_platform.h"
#include "comp_fade_point_light.h"
#include "comp_light_point.h"
#include "geometry/interpolators.h"

DECL_OBJ_MANAGER("fade_point_light", TCompFadePointLight)

void TCompFadePointLight::registerMsgs() {
	DECL_MSG(TCompFadePointLight, TMsgEntityCreated, onEntityCreated);
}

void TCompFadePointLight::load(const json & j, TEntityParseContext & ctx) {
	setTimeToFade(j.value("time", _timeToFade));
	setDestroyEntityWhenFinished(j.value("destroy_when_finished", _destroyEntityWhenFinished));
	setInterpolator(j.value("interpolator", _interpolatorName));
}

void TCompFadePointLight::update(float dt) {
	float time = _fadeTimer.elapsed();
	TCompLightPoint* light = _hCompLight;
	if (time < _timeToFade) {
		float newIntensity = _interpolator(_baseIntensity, 0.0f, time / _timeToFade);
		light->setIntensity(newIntensity);
	}
	else {
		light->setIntensity(0.0f);
		if (_destroyEntityWhenFinished)
			getEntity().destroy();
		else
			setActive(false);
	}
}

void TCompFadePointLight::setInterpolator(const std::string & interpolator) {
	_interpolatorName = interpolator;
	auto& interpolatorManager = Interpolator::InterpolatorManager::Instance();
	_interpolator = interpolatorManager.getInterpolatorAsLambda(_interpolatorName);
}

void TCompFadePointLight::reset() {
	TCompLightPoint* light = _hCompLight;
	_baseIntensity = light->getIntensity();
	_fadeTimer.setTime(_timeToFade);
	_fadeTimer.start();
	setActive(true);
}

void TCompFadePointLight::onEntityCreated(const TMsgEntityCreated & msg) {
	_hCompLight = getComponent<TCompLightPoint>();
	if (_hCompLight.isValid())
		reset();
	else
		setActive(false);
}
