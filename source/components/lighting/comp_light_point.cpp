#include "mcv_platform.h"
#include "comp_light_point.h"

#include "../common/comp_transform.h"
#include "../camera/comp_camera.h"
#include "../common/comp_culling.h"

#include "render/render_manager.h" 
#include "entity/entity_parser.h"

#include "engine.h"

DECL_OBJ_MANAGER("light_point", TCompLightPoint);

// Use nvidia photoshop plugin (dds) to convert the RGBTable16x1.dds recolored to a dds volume texture 
// Volume Texture
// No Mipmaps
// 8.8.8.8 ARGB 32 bpp

DXGI_FORMAT readFormat(const json& j, const std::string& label);

void TCompLightPoint::registerMsgs() {
	DECL_MSG(TCompLightPoint, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompLightPoint, TMsgToogleComponent, onToggleComponent);
}

void TCompLightPoint::onEntityCreated(const TMsgEntityCreated & msg) {
	transform_h = getComponent<TCompTransform>();
}

// -------------------------------------------------
void TCompLightPoint::load(const json& j, TEntityParseContext& ctx) {
	// Store any parameters we may need to pass to the camera component.
	lightParameters = j;
	lightParameters["visible_in_debug"] = false; // we don't want to use them in the camera manager.

	TCompBaseLight::load(j, ctx);
	radius = j.value("radius", radius);
}

void TCompLightPoint::debugInMenu() {
	TCompBaseLight::debugInMenu();
	ImGui::DragFloat("Radius", &radius, 0.01f, 0.f, 100.f);
}

MAT44 TCompLightPoint::getWorld() {
  TCompTransform* c = getComponent<TCompTransform>();
  if (!c)
    return MAT44::Identity;
  return MAT44::CreateScale(radius * 1.05f) * c->asMatrix();
}

void TCompLightPoint::renderDebug() {
	// Render a wire sphere
	auto mesh = EngineResources.getResource("data/meshes/UnitSphere.mesh")->as<CMesh>();
	drawMesh(mesh, getWorld(), VEC4(1, 1, 1, 1));
}

// -------------------------------------------------
// Updates the Shader Cte Light with MY information
void TCompLightPoint::activate() {
	TCompTransform* c = getComponent<TCompTransform>();
	if (!c) return;

	ctes_light.LightColor = color;
	ctes_light.LightIntensity = intensity;
	ctes_light.LightPosition = c->getPosition();
	ctes_light.LightRadius = radius * c->getScale().x;
	ctes_light.LightViewProjOffset = MAT44::Identity;
	ctes_light.LightHasShadows = false;
	ctes_light.updateGPU();
}



