#include "mcv_platform.h"
#include "comp_ranged_projectile_shooter.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_hierarchy.h"
#include "components/common/comp_render.h"
#include "components/render/comp_trail_render.h"
#include "comp_enemy_ranged_projectile.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("enemy_ranged_projectile_shooter", TCompRangedProjectileShooter);

void TCompRangedProjectileShooter::registerMsgs() {
	DECL_MSG(TCompRangedProjectileShooter, TMsgEntityCreated, onEntityCreated);
}

void TCompRangedProjectileShooter::onEntityCreated(const TMsgEntityCreated & msg) {
	_transform = getComponent<TCompTransform>();
	_hierarchy = getComponent<TCompHierarchy>();
}

void TCompRangedProjectileShooter::init(CHandle parent, const Vector3& offset) {
	TCompHierarchy* hierarchy = _hierarchy;

	hierarchy->setParentEntity(parent);
	hierarchy->setPosition(offset);
}

CHandle TCompRangedProjectileShooter::shoot(std::string & prefab, bool accurate) {
	TCompTransform* transform = _transform;
	TCompTransform* playerTransf = getPlayerTransform();
	TEntityParseContext ctx;
	TMsgAssignBulletOwner msg;

	// instantiate projectile
	CTransform tempTransform = *transform;
	tempTransform.setPosition(tempTransform.getPosition());
	ctx.root_transform = tempTransform;
	parseScene(prefab, ctx);
	//msg.destination = (accurate ? predictPlayerPosition() : playerTransf->getPosition());
	msg.h_owner = getEntity();
	msg.h_targetTransform = playerTransf;
	msg.source = tempTransform.getPosition();
	msg.damageBuff = 1.0f; //TODO: change for _damageBoost
	ctx.findEntityByName("RangedEnemyProjectile").sendMsg(msg);

	return ctx.entities_loaded[0];
}

Vector3 TCompRangedProjectileShooter::predictPlayerPosition() {
	TCompTransform* transform = _transform;
	TCompTransform* playerTransf = getPlayerTransform();
	Vector3 playerPos = playerTransf->getPosition();
	TCompPlayerCharacterController* controller = getPlayerCharacterController();
	if (!controller) return playerPos;
	//float dt = Time.delta;
	float distance = Vector3::Distance(transform->getPosition(), playerPos);
	Vector3 playerVelocity = controller->getCurrentVelocity();

	return playerPos + playerVelocity * 0.585f;
}
