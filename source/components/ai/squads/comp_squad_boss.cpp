#include "mcv_platform.h"
#include "comp_squad_boss.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_tags.h"

DECL_OBJ_MANAGER("squad_boss", TCompSquadBoss);

void TCompSquadBoss::load(const json & j, TEntityParseContext & ctx)
{

	for (auto i = 0; i < _numSlots; i++) {
		EntitySlot slot;
		slot.idx = i;
		_playerSlots.push_back(slot);
		_ownSlots.push_back(slot);
	}

}

void TCompSquadBoss::update(float dt)
{
	
	// Update how many minions are
	updateTeams();
	// Update player slots
	updatePlayerSlots();
	// Temp behaviour: Team 1 fills the slots

	
}

void TCompSquadBoss::debugInMenu()
{
	ImGui::DragFloat("Slot Distance: ", &_slotDistance, 0.05f, 0.3f, 5.0f);
	for (EntitySlot s : _playerSlots) {

		ImGui::Text("Player slots: ");
		ImGui::Text("Front : %f %f %f", s.position.x, s.position.y, s.position.z);
		ImGui::Text("Minion: 		%s", getMinionName(s.minion).c_str());
	}
	
	ImGui::Separator();
	ImGui::Text("Teams: ");
	for (auto i = 0; i < _teams.size(); i++) {
		ImGui::Text("	Team %d: ", i);
		for (auto j = 0; j < _teams[i].members.size(); j++) {
			CEntity* e = _teams[i].members[j];
			std::string name = e->getName();
			ImGui::Text("		%s", name.c_str());
		}
	}
}

void TCompSquadBoss::renderDebug()
{
	// Slots
	for (auto i = 0; i < _playerSlots.size(); i++) {
		drawWiredSphere(_playerSlots.at(i).position, 0.5, VEC4(1, 0, 1, 1));
	}
		
}

void TCompSquadBoss::registerMsgs()
{
}

EntitySlot TCompSquadBoss::getAvailableSlot(VEC3 pos)
{
	EntitySlot closest;

	for (EntitySlot s : _playerSlots) {
		if (!s.filled) {
			if (closest.position.y == -100 || VEC3::Distance(s.position, pos) < VEC3::Distance(pos, closest.position))
				closest = s;
		}
	}

	return closest;
}

void TCompSquadBoss::updatePlayerSlots()
{
	CEntity* player = getEntityByName("Player");
	if (!player) return;
	TCompTransform* playerTrans = player->getComponent<TCompTransform>();
	if (!playerTrans) return;

	// Update order: front, right, back, left
	_playerSlots.at(0).position = playerTrans->getPosition() + playerTrans->getFront() * _slotDistance;
	_playerSlots.at(1).position = playerTrans->getPosition() + playerTrans->getLeft()  * _slotDistance * -1.0f;
	_playerSlots.at(2).position = playerTrans->getPosition() + playerTrans->getFront() * _slotDistance * -1.0f;
	_playerSlots.at(3).position = playerTrans->getPosition() + playerTrans->getLeft()  * _slotDistance;

}

void TCompSquadBoss::updateTeams() {

	std::vector<CHandle> minions = CTagsManager::get().getAllEntitiesByTag(Utils::getID("enemy"));
	int numTeams = (int)(minions.size() / _numSlots) + 1;

	if (_currentMinions == minions.size()) return;

	auto teamIdx = 0;
	auto enemyIdx = 0;
	Team t;
	for (auto i = 0; i < minions.size(); i++) {

		if (enemyIdx == _teamSize) {
			teamIdx++;
			enemyIdx = 0;
			_teams.push_back(t);
			t.members.clear();
		}

		t.members.push_back(minions.at(i));
		enemyIdx++;

		_currentMinions++;
	}
	// Push back the last incomplete team
	_teams.push_back(t);

}

std::string TCompSquadBoss::getMinionName(CHandle h)
{
	CEntity* e = h;
	return e == nullptr ? "" : e->getName();
}


void TCompSquadBoss::updateStatusInfo(std::string key, bool value)
{
	if (_statusInfo.at(key))
		_statusInfo.at(key) = value;
}

void TCompSquadBoss::updateSlotInfo(int slot, bool filled, CHandle minion)
{
	_playerSlots.at(slot).filled = filled;
	_playerSlots.at(slot).minion = minion;
}

EntitySlot TCompSquadBoss::getSlot(int idx)
{
	return _playerSlots.at(idx);
}
