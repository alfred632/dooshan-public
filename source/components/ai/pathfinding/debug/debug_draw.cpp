#include "mcv_platform.h"
#include "debug_draw.h"
#include "render/primitives.h"

NavmeshDebugDraw::NavmeshDebugDraw(int cap) :
	m_verts(0),
	m_ind(0),
	m_size(0),
	m_cap(0), 
	isActive(true) {
	if (cap < 8)
		cap = 8;
	resize(cap);
}

NavmeshDebugDraw::~NavmeshDebugDraw() {
	delete[] m_verts;
	delete[] m_ind;
}

void NavmeshDebugDraw::resize(int cap) {
	SimpleVertex* newVerts = new SimpleVertex[cap];
	unsigned int * newInd = new unsigned int[cap * 3];
	if (m_size) {
		memcpy(newVerts, m_verts, sizeof(SimpleVertex) * m_size);
		memcpy(newInd, m_ind, sizeof(int) * m_size * 3);
	}
	delete[] m_verts;
	delete[] m_ind;
	m_verts = newVerts;
	m_ind = newInd;

	m_cap = cap;
}

void NavmeshDebugDraw::createNavmeshPoly(const dtNavMesh & mesh, dtPolyRef ref, const Vector4 col) {
	const dtMeshTile* tile = 0;
	const dtPoly* poly = 0;
	if (dtStatusFailed(mesh.getTileAndPolyByRef(ref, &tile, &poly)))
		return;

	const unsigned int ip = (unsigned int)(poly - tile->polys);

	const dtPolyDetail* pd = &tile->detailMeshes[ip];

	for (int i = 0; i < pd->triCount; ++i) {
		const unsigned char* t = &tile->detailTris[(pd->triBase + i) * 4];
		for (int j = 0; j < 3; ++j) {
			if (t[j] < poly->vertCount)
				vertex(&tile->verts[poly->verts[t[j]] * 3]);
			else
				vertex(&tile->detailVerts[(pd->vertBase + t[j] - poly->vertCount) * 3]);
		}
	}
}

void NavmeshDebugDraw::clear() {
	m_size = 0;
	m_mesh = CMesh();
}

void NavmeshDebugDraw::end() {
	m_mesh.create(m_verts, m_size, sizeof(SimpleVertex), "PosColor", m_ind, m_size * 3, sizeof(int), CMesh::TRIANGLE_LIST);
}

void NavmeshDebugDraw::createFromNavmesh(const dtNavMesh & mesh, const Vector4 & col) {
	for (int i = 0; i < mesh.getMaxTiles(); ++i) {
		const dtMeshTile* tile = mesh.getTile(i);
		if (!tile->header) continue;
		dtPolyRef base = mesh.getPolyRefBase(tile);

		for (int j = 0; j < tile->header->polyCount; ++j) {
			const dtPoly* p = &tile->polys[j];
			createNavmeshPoly(mesh, base | (dtPolyRef)j, col);
		}
	}
	end();
}

void NavmeshDebugDraw::vertex(const float * pos, const Vector4 & color) {
	vertex(Vector3(pos[0], pos[1], pos[2]), color);
}

void NavmeshDebugDraw::vertex(const Vector3 & pos, const Vector4 & color) {
	if (m_size + 1 >= m_cap)
		resize(m_cap * 2);
	SimpleVertex * v = &m_verts[m_size];
	v->Pos = pos;
	v->Color = color;
	unsigned int * ind = &m_ind[m_size * 3];
	ind[2] = m_size * 3;
	ind[1] = m_size * 3 + 1;
	ind[0] = m_size * 3 + 2;

	m_size++;
}

void NavmeshDebugDraw::draw(const Vector3 & pos, const Vector4 & color) {
	if (isActive)
		drawMesh(&m_mesh, Matrix::CreateTranslation(pos) , color);
}