#include "mcv_platform.h"
#include "comp_navmesh.h"
#include "comp_pathfinding_agent.h"
#include "components/common/comp_name.h"
#include "components/common/comp_transform.h"

DECL_OBJ_MANAGER("navmesh", TCompNavmesh)

TCompNavmesh::TCompNavmesh() {
	_navmeshData = new NavmeshData;
	_debugDraw = new NavmeshDebugDraw;
}

TCompNavmesh::~TCompNavmesh() {
	delete _navmeshData;
	delete _debugDraw;
}

void TCompNavmesh::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::Separator();
	ImGui::Text("Press Build to apply any changes");
	if (ImGui::SmallButton("Build")) {
		build();
	}
	ImGui::SameLine();
	if (ImGui::SmallButton("Save")) {
		TCompName * cName = getComponent<TCompName>();
		const std::string path = "data/meshes/navmesh/";
		_navmeshData->saveNavmeshToFile((path + cName->getName() + ".bin").c_str());
	}
	int activeAgents = 0;
	if (ImGui::TreeNode("Active agents")) {
		dtCrowd* crowd = _navmeshData->getCrowd();
		for (size_t i = 0; i < _navmeshData->getMaxAgents(); i++) {
			const dtCrowdAgent* agent = crowd->getAgent(i);
				if (crowd->getAgent(i)->active) {
					CEntity* entity = _agents[i].getOwner();
					TCompName* name = entity->getComponent<TCompName>();
					TCompTransform* transform = entity->getComponent<TCompTransform>();
					Vector3 pos = transform->getPosition();
					ImGui::Text("[%d]: %s at (%f, %f, %f)", i, name->getName(), pos.x, pos.y, pos.z);
					activeAgents++;
				}
		}
		ImGui::TreePop();
	}
	ImGui::LabelText("Active agents", "%d", activeAgents);
	ImGui::Checkbox("Render Debug Active", &_debugDraw->isActive);
	ImGui::Separator();
	ImGui::Text("Global position");
	ImGui::DragFloat3("", &_position.x, 0.01f, -10000.0f, 10000.0f);
	ImGui::Separator();
	_navmeshData->debugInMenu();
}

void TCompNavmesh::renderDebug() {
	if (_navmeshLoaded)
		_debugDraw->draw(_position, Vector4(1, 1, 0, 0.5f));
}

void TCompNavmesh::load(const json & j, TEntityParseContext & ctx) {
	if (j.count("pos"))
		_position = loadVector3(j, "pos");
	else _position = Vector3::Zero;

	/* Load pathfinding geometry path */
	std::string inputMesh = j.value("input_mesh", "");
	_navmeshData->loadInputGeometry(inputMesh);

	std::string navmeshPath = j.value("navmesh", "");
	if (navmeshPath != "") {
		_navmeshData->loadNavmeshFromFile(navmeshPath.c_str());
		_navmeshLoaded = true;
	}
	assert(inputMesh != "" || navmeshPath != "" && "Navmesh component has no way to initialize navmesh");

	if (j.count("navmesh_settings")) {
		json jNavSettings = j["navmesh_settings"];
		_navmeshData->load(jNavSettings, ctx);
	}

}

void TCompNavmesh::update(float dt) {
	_navmeshData->update(dt);
}

void TCompNavmesh::registerMsgs() {
	DECL_MSG(TCompNavmesh, TMsgEntityCreated, onEntityCreated);
}

void TCompNavmesh::onEntityCreated(const TMsgEntityCreated & msg) {
	/* Create navmesh */
	if (!_navmeshData->isNavmeshInitialized()) {
		_navmeshData->build();
	}
	_debugDraw->createFromNavmesh(*_navmeshData->getNavMesh());
	_navmeshData->setupCrowd();

	_agents.resize(_navmeshData->getMaxAgents());

	//If it wasn't loaded, save it now when we have access to the name component
	if (!_navmeshLoaded) {
		TCompName* cName = getComponent<TCompName>();
		const std::string path = "data/meshes/navmesh/";
		_navmeshData->saveNavmeshToFile((path + cName->getName() + ".bin").c_str());
		_navmeshLoaded = true;
	}
}

void TCompNavmesh::build() {
	_navmeshData->build();
	_debugDraw->clear();
	_debugDraw->createFromNavmesh(*_navmeshData->getNavMesh());
	_navmeshData->setupCrowd();

	for (TCompPathfindingAgent * agent : _agents) {
		if (agent && agent->isAgentActive())
			agent->create();
	}
}

const Vector3 TCompNavmesh::getAgentPosition(dtAgentID id) {
	const float * rdPos = _navmeshData->getCrowd()->getAgent(id)->npos;
	return Vector3(rdPos[0], rdPos[1] - _navmeshData->getCellHeight(), rdPos[2]) + _position;
}

int TCompNavmesh::setAgentPosition(dtAgentID id, const Vector3 & pos) {
	const dtCrowdAgentParams& agentParams = _navmeshData->getCrowd()->getAgent(id)->params;
	CHandle hnd = _agents[id];

	releaseAgent(id);
	return registerAgent(pos, agentParams.height, agentParams.radius, agentParams.maxSpeed, hnd);
}

int TCompNavmesh::registerAgent(const Vector3 pos, float height, float radius, float speed, CHandle comp) {
	float rdPos[] = { pos.x - _position.x, pos.y - _position.y, pos.z - _position.z };
	int nAgent = _navmeshData->addAgent(rdPos, height, radius, speed);
	if (nAgent >= 0)
		_agents[nAgent] = comp;
	return nAgent;
}

void TCompNavmesh::requestMoveTarget(dtAgentID id, const Vector3 & pos) {
	float rdPos[] = { pos.x - _position.x, pos.y - _position.y, pos.z - _position.z };
	dtCrowd * crowd = _navmeshData->getCrowd();
	dtPolyRef poly = 0;
	float closestPos[3];
	_navmeshData->getNavMeshQuery()->findNearestPoly(rdPos, crowd->getQueryExtents(), crowd->getFilter(0), &poly, closestPos);

	crowd->requestMoveTarget(id, poly, closestPos);
}

void TCompNavmesh::stopAgent(dtAgentID id) {
	dtCrowd * crowd = _navmeshData->getCrowd();
	crowd->resetMoveTarget(id);
}

void TCompNavmesh::releaseAgent(dtAgentID id) {
	if (agentExists(id)) {
		dtCrowd * crowd = _navmeshData->getCrowd();
		crowd->removeAgent(id);
		_agents[id] = CHandle();
	}
}

bool TCompNavmesh::agentExists(dtAgentID id) {
	dtCrowd * crowd = _navmeshData->getCrowd();
	return id >= 0 && id < crowd->getAgentCount() && crowd->getAgent(id)->active;
}
