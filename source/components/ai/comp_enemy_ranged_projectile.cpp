#include "mcv_platform.h"
#include "comp_enemy_ranged_projectile.h"
#include "components/common/comp_transform.h"
#include "components\common\comp_name.h"
#include "components/common/comp_tags.h"
#include "components/common/comp_health.h"
#include "components/common/comp_render.h"
#include "components/common/comp_particles.h"
#include "components/render/comp_trail_render.h"
#include "components/lighting/comp_light_point.h"
#include "components/game/status_effects/comp_status_effects.h"
#include "entity/entity_parser.h"
#include "engine.h"

DECL_OBJ_MANAGER("enemy_ranged_projectile", TCompEnemyRangedProjectile);

void TCompEnemyRangedProjectile::load(const json& j, TEntityParseContext& ctx) {
	_speed = j.value("speed", _speed);
	_damage = j.value("damage", _damage);
	_timeToDestroy = j.value("time_to_destroy", _timeToDestroy);

	_yawSpeed = j.value("yaw_speed", _yawSpeed);
	_pitchSpeed = j.value("pitch_speed", _pitchSpeed);

	_targetOffset = loadVector3(j, "target_offset");
	_distanceToFail = j.value("distance_to_fail", _distanceToFail);

	_shootParticles = j.value("shoot_particles", _shootParticles);
	_collisionParticles = j.value("collision_particles", _collisionParticles);
	_trailEntityName = j.value("trail_entity", _trailEntityName);

	_shootSoundEvent = j.value("shoot_sound_event", _shootSoundEvent);
	_collisionSoundEvent = j.value("collision_sound_event", _collisionSoundEvent);

	if (j.count("status_effects"))
		_statusEffects = j["status_effects"];
}

void TCompEnemyRangedProjectile::debugInMenu() {
	TCompBase::debugInMenu();
	ImGui::DragFloat("Speed", &_speed, 0.1f, 0.f, 10000.f);
	ImGui::DragFloat("Damage", &_damage, 0.1f, 0.f, 10000.f);
}

void TCompEnemyRangedProjectile::registerMsgs() {
	DECL_MSG(TCompEnemyRangedProjectile, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompEnemyRangedProjectile, TMsgEntitiesGroupCreated, onGroupCreated);
	DECL_MSG(TCompEnemyRangedProjectile, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompEnemyRangedProjectile, TMsgAssignBulletOwner, onProjectileInfoMsg);
	DECL_MSG(TCompEnemyRangedProjectile, TMsgEntityTriggerEnter, onTriggerEnter);
}

void TCompEnemyRangedProjectile::onEntityCreated(const TMsgEntityCreated & msg) {
	_transformH = getComponent<TCompTransform>();
	_colliderH = getComponent<TCompCollider>();
	_lightPointH = getComponent<TCompLightPoint>();
	TCompLightPoint* lightPoint = _lightPointH;
	_lightPointIntensity = (lightPoint ? lightPoint->getIntensity() : 0.0f);
}

void TCompEnemyRangedProjectile::onGroupCreated(const TMsgEntitiesGroupCreated & msg) {
	if (!_trailEntityName.empty()) {
		CEntity* trailEntity = msg.ctx.findEntityByName(_trailEntityName);
		if (trailEntity)
			_traiRenderH = trailEntity->getComponent<TCompTrailRender>();
	}
}

void TCompEnemyRangedProjectile::onProjectileInfoMsg(const TMsgAssignBulletOwner & msg) {
	_source = msg.source;
	_owner = msg.h_owner; // save who generated me.
	_damageBuff = msg.damageBuff;
	_targetTransformH = msg.h_targetTransform;
	TCompTransform* c_trans = _transformH;

	// Spawn particles at start if needed
	if (!_shootParticles.empty())
		LogicManager::Instance().SpawnParticle(_shootParticles, c_trans->getPosition(), c_trans->getFront());

	if (!_collisionSoundEvent.empty())
		EngineAudio.playEvent(_shootSoundEvent, _source);
}

void TCompEnemyRangedProjectile::onTriggerEnter(const TMsgEntityTriggerEnter & msg) {
	if (!_hasCollided) {
		if (msg.h_entity == _owner)
			return;

		TCompTransform* transform = _transformH;
		CEntity * detectedEntity = msg.h_entity;
		TCompTags * tagsEntity = detectedEntity->getComponent<TCompTags>();

		if (_destroyEvent.isStopped()) {
			_destroyEvent.setTime(_timeToDestroy);
			_destroyEvent.start();
		}

		TCompRender* render = getComponent<TCompRender>();
		if (render) {
			render->setCurrentState(-1);
			render->setActive(false);
		}
		TCompParticles* particles = getComponent<TCompParticles>();
		if (particles) particles->setSystemActive(false);

		if (_traiRenderH.isValid()) {
			TCompTrailRender* trailRender = _traiRenderH;
			trailRender->lockPosistion();
			trailRender->fadeOut();
		}

		if (tagsEntity) {
			// If it is the player.
			if (tagsEntity->hasTag(Utils::getID("player"))) {
				TMsgHitDamage damageMsg;
				damageMsg.damage = _damage * _damageBuff;
				damageMsg.damageDirection = -transform->getFront();
				detectedEntity->sendMsg(damageMsg);
				TCompStatusEffects* statusComp = detectedEntity->getComponent<TCompStatusEffects>();
				if (statusComp)
					statusComp->parseStatusEffects(_statusEffects);
			}
		}

		// Move projectile upper so that the light doesn't render weirdly (in the same position as floor)
		TCompCollider * collider = _colliderH;
		Vector3 pos = transform->getPosition() + Vector3::Up * 0.05f;
		transform->setPosition(pos);
		collider->setActive(false);

		if (!_collisionParticles.empty())
			LogicManager::Instance().SpawnParticle(_collisionParticles, pos);

		_timeToLive = 0.0f;

		if (!_collisionSoundEvent.empty())
			EngineAudio.playEvent(_collisionSoundEvent, pos);

		_hasCollided = true;
	}
}

void TCompEnemyRangedProjectile::deleteMe() {
	setActive(false);
	getEntity().destroy();
}

void TCompEnemyRangedProjectile::update(float dt) {
	// check if projectile is too old and needs to be destroyed
	_timeToLive -= dt;
	if (_timeToLive > 0) {
		TCompTransform * transform = _transformH;
		TCompCollider * collider = _colliderH;
		if (!collider) return;
		physx::PxRigidDynamic * actor = static_cast<physx::PxRigidDynamic *>(collider->actor);
		if (!actor) return;

		float projectileToSourceDistance = Vector3::Distance(_source, transform->getPosition());

		Vector3 projectilePos = transform->getPosition();
		CTransform nextTransform;
		if (!_failedProjectile && _targetTransformH.isValid()) {
			// Rotate projectile towards target
			TCompTransform* target = _targetTransformH;
			Vector3 targetPos = target->getPosition() + _targetOffset;
			float projectileToTargetDistance = Vector3::Distance(projectilePos, targetPos);
			float yaw = 0.0f, pitch = 0.0f;
			float deltaYaw = 0.0f, deltaPitch = 0.0f;
			float deltaYawToAimTo = 0.0f, deltaPitchToAimTo = 0.0f;

			transform->getAngles(&yaw, &pitch);
			deltaYawToAimTo = transform->getDeltaYawToAimTo(targetPos);
			deltaPitchToAimTo = transform->getDeltaPitchToAimTo(targetPos);
			if (abs(deltaYawToAimTo) > 0) {
				deltaYaw = (deltaYawToAimTo / abs(deltaYawToAimTo)) * _yawSpeed * M_PI * dt;
				if (abs(deltaYaw) > abs(deltaYawToAimTo))
					deltaYaw = deltaYawToAimTo;
				yaw += deltaYaw;
			}
			if (abs(deltaPitchToAimTo) > 0) {
				deltaPitch = (deltaPitchToAimTo / abs(deltaPitchToAimTo)) * (_pitchSpeed / projectileToTargetDistance) * M_PI * dt;	// make pitch speed proportional to distance so it doesn't fail close shots
				if (abs(deltaPitch) > abs(deltaPitchToAimTo))
					deltaPitch = deltaPitchToAimTo;
				pitch += deltaPitch;
			}
			nextTransform.setAngles(yaw, pitch);

			_failedProjectile = (abs(projectilePos.y - targetPos.y) < _speed * 0.02f);
			_failedProjectile |= projectileToSourceDistance > projectileToTargetDistance && projectileToTargetDistance < _distanceToFail;
		}
		else
			nextTransform.setRotation(transform->getRotation());
		
		// Move forward the physx kinematic body
		nextTransform.setPosition(projectilePos + nextTransform.getFront() * _speed * dt);
		actor->setKinematicTarget(toPxTransform(nextTransform));
	}
	else {
		if (_destroyEvent.isStopped()) {
			_destroyEvent.setTime(_timeToDestroy);
			_destroyEvent.start();
		}

		_destroyEvent.doMethodEvent(this, &TCompEnemyRangedProjectile::deleteMe);
		// Fade out light point over time
		if (_lightPointH.isValid()) {
			TCompLightPoint* lightPoint = _lightPointH;
			const CClock& clock = _destroyEvent.getClock();
			lightPoint->setIntensity((1 / _timeToDestroy) * clock.remaining() * _lightPointIntensity);
		}
	}
}