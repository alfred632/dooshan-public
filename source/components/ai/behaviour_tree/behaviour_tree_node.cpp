#include "mcv_platform.h"
#include "behaviour_tree_node.h"
#include "comp_behaviour_tree.h"

BehaviourTreeNode::BehaviourTreeNode(string s)
{
	_name = s;
}


void BehaviourTreeNode::create(string s)
{
	_name = s;
}


bool BehaviourTreeNode::isRoot()
{
	return (_parent == nullptr);
}


void BehaviourTreeNode::setParent(BehaviourTreeNode *p)
{
	_parent = p;
}


void BehaviourTreeNode::setRight(BehaviourTreeNode *p)
{
	_right = p;
}


void BehaviourTreeNode::setType(Type t)
{
	_type = t;
}


string BehaviourTreeNode::getName()
{
	return _name;
}

void BehaviourTreeNode::setRatios(std::vector<float> & ratios) {
	assert(_type == RANDOM && "Node must be random in order to set ratios");
	assert(_children.size() == ratios.size() && "Number of ratios not corresponding with number of current children");
	_ratios = ratios;
	_sumOfRatios = std::accumulate(_ratios.begin(), _ratios.end(), 0);
}


void BehaviourTreeNode::addChild(BehaviourTreeNode * child)
{
	if (!_children.empty()) { // if this node already had children, connect the last one to this
		assert(_type != DECORATOR && "Decorator nodes only allow one child");
		_children.back()->setRight(child);  // new one so the new one is to the RIGHT of the last one
	}
	_children.push_back(child); // in any case, insert it
	child->_right = nullptr; // as we're adding from the right make sure right points to NULL
}


void BehaviourTreeNode::recalc(TCompBehaviourTree * tree)
{
	// activate the next line to debug
	//printf("recalcing node %s\n", _name.c_str());
	switch (_type)
	{
		case ACTION:
		{
			assert(_children.empty() && "An action node does not allow child nodes");
			// run the controller of this node
			int res = tree->execAction(_name);

			// now, the next lines compute what's the NEXT node to use in the next frame...
			//if (res == STAY) { tree->setCurrent(this); return; }// looping vs. on-shot actions
			// climb tree iteratively, look for the next unfinished sequence to complete
			BehaviourTreeNode *cand = this;
			if (res == ABORT) {
				tree->setCurrent(nullptr);
				return;
			}
			else if (res == INTERRUPT_AND_STAY) {
				tree->_contextStack.push(cand);
				tree->setCurrent(nullptr);
				return;
			}
			else if (res == STAY) {
				tree->setCurrent(cand);
				return;
			}
			else {
				while (cand->_parent != nullptr)
				{
					BehaviourTreeNode *daddy = cand->_parent;
					if (daddy->_type == SEQUENCE)
						// oh we were doing a sequence. make sure we finished it!!!
					{
						if (cand->_right != nullptr)
						{
							cand = cand->_right;
							break;
						}
						// sequence was finished (there is nobody on right). Go up to daddy.
						else cand = daddy;
					}
					// i'm not on a sequence, so keep moving up to the root of the BT
					else cand = daddy;
				}

				if (res == INTERRUPT_AND_LEAVE) {
					tree->_contextStack.push(cand);
					tree->setCurrent(nullptr);
					return;
				}
			}
			// if we've reached the root, means we can reset the traversal for next frame.
			if (cand->_parent == nullptr) cand = nullptr;

			tree->setCurrent(cand);
			break;
		}
		case RANDOM:
		{
			assert(!_children.empty() && "A random node needs at least one child");
			// If there is no ratio info execute default behaviour for random (homogeneous distribution)
			if (_ratios.empty()) {
				int r = RNG.i((int) _children.size()); //r = rand() % _children.size();
				_children[r]->recalc(tree);
			}
			else {
				float r = RNG.f((float) _sumOfRatios);
				float pond = 0.0f;
				for (int i = 0; i < _ratios.size(); i++) {
					if (r >= pond && r < _ratios[i] + pond) {
						_children[i]->recalc(tree);
						break;
					}
					pond += _ratios[i];
				}
			}
			break;
		}
		case PRIORITY:
		{
			assert(!_children.empty() && "A priority node needs at least one child");
			for (int i = 0;i < _children.size();i++)
			{
				if (tree->testCondition(_children[i]->getName()))
				{
					_children[i]->recalc(tree);
					break;
				}
			}
			break;
		}
		case SEQUENCE:
		{
			assert(!_children.empty() && "A sequence node needs at least one child");
			// begin the sequence...the inner node (action) will take care of the sequence
			// via the "setCurrent" mechanism
			_children[0]->recalc(tree);
			break;
		}
		case DECORATOR:
		{
			assert(_children.size() == 1 && "A decorator node needs exactly one child");
			// decorator executes only when condition is true or if there isn't one
			if (tree->testCondition(_name))
				tree->execAction(_name);

			_children[0]->recalc(tree);

			break;
		}
	}
}
