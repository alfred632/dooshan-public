#pragma once

#include "resources/resource.h"
#include "utils/json.hpp"

class CBT : public IResource {
	json jdata;

public:
	CBT(const std::string & filename);

	void renderInMenu() override;

	void onFileChanged(const std::string & filename) override;

	const json & getJson() const { return jdata; }
};
