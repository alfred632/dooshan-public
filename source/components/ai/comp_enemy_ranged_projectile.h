#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/common/comp_collider.h"
#include "geometry/curve.h"
#include "components/game/status_effects/status_effect.h"

class TCompEnemyRangedProjectile : public TCompBase {

public:

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();

	void setDamage(float dmg) { _damage = dmg; }

	DECL_SIBLING_ACCESS();

private:
	CHandle _owner;
	CHandle _targetTransformH;
	CHandle _transformH;
	CHandle _colliderH;
	CHandle _lightPointH;
	CHandle _traiRenderH;

	VEC3  _source;
	float _damageBuff = 1.0; // Projectile damage buff.
	std::string _trailEntityName = "";

	float _speed = 1.0f; // Projectile speed per second.
	float _damage = 20.0f; // Projectile damage on hit.
	float _pitchSpeed = 1.0f, _yawSpeed = 1.0f;
	float _lightPointIntensity;
	float _timeToLive = 10.f; // Time before the entity component containing the bullet is destroyed.  
	float _timeToDestroy = 0.75f;
	float _distanceToFail = 0.5f;
	VEC3	_targetOffset;

	std::string _shootParticles = "";
	std::string _collisionParticles = "";

	std::string _shootSoundEvent = "";
	std::string _collisionSoundEvent = "";

	bool _hasCollided = false;
	bool _failedProjectile = false;
	CTimedEvent _destroyEvent;

	void onEntityCreated(const TMsgEntityCreated & msg);
	void onGroupCreated(const TMsgEntitiesGroupCreated & msg);
	void onProjectileInfoMsg(const TMsgAssignBulletOwner& msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter & msg);

	void deleteMe();

	json _statusEffects;

};