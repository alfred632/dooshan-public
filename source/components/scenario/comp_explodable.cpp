#include "mcv_platform.h"
#include "comp_explodable.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_render.h"
#include "components/common/comp_name.h"
#include "components/common/comp_tags.h"
#include "components/common/comp_material_buffers.h"
#include "components/controllers/comp_rigid_body.h"
#include "engine.h"

DECL_OBJ_MANAGER("explodable", TCompExplodable);


void TCompExplodable::load(const json& j, TEntityParseContext& ctx) 
{
	_innerRadius = j.value("inner_radius", _innerRadius);
	_outerRadius = j.value("outer_radius", _outerRadius);
	_power = j.value("power", _power);
	_debrisPath = j.value("debris_path", _debrisPath);
	_blastPath = j.value("blast_path", _blastPath);
	_explosionSoundEvent = j.value("sound_event", _explosionSoundEvent);

	const json& particles = j["particles"];
	for (auto& particle : particles) {
		_particles.push_back(std::pair("data/particles/" + std::string(particle["name"]) + ".particles", particle.value("priority", 0)));
	}
}

void TCompExplodable::explode() {
	// 1. Generate logic explosion on transform
	TCompTransform* trans = getComponent<TCompTransform>();
	if (!trans) return;
	Vector3 position = trans->getPosition();

	EngineLogicManager.SpawnExplosion(trans, _innerRadius, _outerRadius, _power);
	if (!_blastPath.empty()) {
		CHandle blastHandle = EngineLogicManager.CreateEntity(_blastPath, position);
		EngineEntities.setCHandleTimeToLive(blastHandle, 0.5f);
	}

	// 2. Spawn VFX
	if (!_particles.empty()) {
		for (auto &particle : _particles) {
			particles::CSystem* system = EngineLogicManager.SpawnParticle(particle.first, position);
			system->setPriority(particle.second);
		}
	}
	if (_materialBuffer.isValid()) {
		TCompMaterialBuffers* mb = _materialBuffer;
		mb->activate();
		mb->setActive(true);
	}

	if (!_explosionSoundEvent.empty())
		EngineAudio.playEvent(_explosionSoundEvent, trans->getPosition());
	// 3. Swap with broken mesh
	if (TCompRender* render = getComponent<TCompRender>())
		if (render->hasState(1))
			render->showMeshesWithState(1);
	// 4. Generate debris
	if (!_debrisPath.empty())
		generateDebris();

	EngineLogicManager.smartRemoveEntity(getEntity());
}

void TCompExplodable::onExplode(const TMsgExplode &msg) {
	explode();
}

/* Spawn meshes around as debris */
void TCompExplodable::generateDebris() {
	TEntityParseContext _ctx;
	TCompTransform* transform = getComponent<TCompTransform>();
	_ctx.root_transform = *transform;
	parseScene(_debrisPath, _ctx);
	for (CEntity* c : _ctx.entities_loaded) {
		TCompRigidBody* rb = c->getComponent<TCompRigidBody>();
		if (rb != nullptr) {
			VEC3 throwDir = VEC3(RNG.f(-1.0f, 1.0f), RNG.f(), RNG.f(-1.0f, 1.0f));
			throwDir.Normalize();
			float force = RNG.f(8.0f, 16.0f);
			rb->addVelocityChange(throwDir * force);
			rb->addTorqueVelocityChange(throwDir * force);
		}
	}
}

void TCompExplodable::onTriggerEnter(const TMsgEntityTriggerEnter& trigger_enter) {
	// Add a fire status effect while there are fire particles or this entity is alive?
}

void TCompExplodable::onEntityCreated(const TMsgEntityCreated& msg) {
	_materialBuffer = getComponent<TCompMaterialBuffers>();
	if (_materialBuffer.isValid()) {
		TCompMaterialBuffers* mb = _materialBuffer;
		mb->setActive(false);
	}
}

void TCompExplodable::onResetComponent(const TMsgResetComponent & msg) {
	if (_materialBuffer.isValid()) {
		TCompMaterialBuffers* mb = _materialBuffer;
		mb->setActive(false);
		TCompRender* render = getComponent<TCompRender>();
		render->setActive(true);
		render->setCurrentState(0);
	}
}

void TCompExplodable::onBulletDamage(const TMsgBulletDamage & msg) {
	explode();
}


void TCompExplodable::registerMsgs() {
	DECL_MSG(TCompExplodable, TMsgExplode, onExplode);
	DECL_MSG(TCompExplodable, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompExplodable, TMsgResetComponent, onResetComponent);
	DECL_MSG(TCompExplodable, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompExplodable, TMsgBulletDamage, onBulletDamage);
}

void TCompExplodable::debugInMenu()
{
	ImGui::DragFloat("Radius", &_outerRadius, .05f, _innerRadius, 5.f);
	ImGui::DragFloat("Radius", &_innerRadius, .05f, 0.05f, _outerRadius);
	ImGui::DragFloat("Power", &_power, .05f, 0.05f, 20.f);

	if (ImGui::Button("Make it explode!")) {
		TMsgExplode dummy;
		onExplode(dummy);
	}
	
}

void TCompExplodable::renderDebug()
{
	TCompTransform* trans = getComponent<TCompTransform>();
	if (!trans) return;

	drawWiredSphere(trans->getPosition(), _outerRadius, VEC4(1.0f, 1.0f, 0.0f, 1.0f));
	drawWiredSphere(trans->getPosition(), _innerRadius, VEC4(1.0f, 1.0f, 0.0f, 1.0f));
}

