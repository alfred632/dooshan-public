#include "mcv_platform.h"
#include "comp_doors.h"
#include "../controllers/comp_rigid_body.h"
#include "../common/comp_transform.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "../common/comp_group.h"

DECL_OBJ_MANAGER("door", TCompDoor);

void TCompDoor::load(const json & j, TEntityParseContext& ctx) {
	RightDoorName = j.value("RightDoorName", "");
	LeftDoorName = j.value("LeftDoorName", "");
	UsesMeshRotatedForRightDoor = j.value("UsesMeshRotatedForRightDoor", UsesMeshRotatedForRightDoor);
	openDoorSoundEvent = j.value("open_door_sound_event", openDoorSoundEvent);
	closeDoorSoundEvent = j.value("close_door_sound_event", closeDoorSoundEvent);
}

void TCompDoor::registerMsgs() {
	DECL_MSG(TCompDoor, TMsgEntitiesGroupCreated, onGroupCreated);
}

void TCompDoor::declareInLua() {
	EngineScripting.declareComponent<TCompDoor>
	(
		// name in LUA
		"TCompDoor",
		// methods
		"OpenDoors", &TCompDoor::OpenDoors,
		"CloseDoors", &TCompDoor::CloseDoors,
		"StopDoors", &TCompDoor::StopDoors,
		"ResetDoors", &TCompDoor::ResetDoors
	);
}

void TCompDoor::onGroupCreated(const TMsgEntitiesGroupCreated & msg) {
	ownTransform = getComponent<TCompTransform>();
	if (!ownTransform.isValid()) return;

	TCompGroup * Group = getComponent<TCompGroup>();
	if (!Group) return;

	CHandle RightDoor = Group->GetEntityInsideGroupByName(RightDoorName);
	CHandle LeftDoor = Group->GetEntityInsideGroupByName(LeftDoorName);

	CEntity * rightDoorEntity = RightDoor;
	TCompTransform* doorTransform = nullptr;
	if (rightDoorEntity) {
		RightDoorTransformComponent = rightDoorEntity->getComponent<TCompTransform>();
		RightDoorRbComponent = rightDoorEntity->getComponent<TCompRigidBody>();
		doorTransform = RightDoorTransformComponent;
		RightDoorInitialRotation = doorTransform->getRotation();
	}

	CEntity * leftDoorEntity = LeftDoor;
	if (leftDoorEntity) {
		LeftDoorTransformComponent = leftDoorEntity->getComponent<TCompTransform>();
		LeftDoorRbComponent = leftDoorEntity->getComponent<TCompRigidBody>();
		doorTransform = LeftDoorTransformComponent;
		LeftDoorInitialRotation = doorTransform->getRotation();
	}
}

void TCompDoor::debugInMenu() {
	ImGui::Text("Right door name: %s", RightDoorName.c_str());
	ImGui::Text("Left door name: %s", LeftDoorName.c_str());

	switch (CurrentState) {
		case DoorState::STOPPED:
			ImGui::Text("Current State: STOPPED");
			break;
		case DoorState::CLOSING:
			ImGui::Text("Current State: Closing");
			break;
		case DoorState::OPENING:
			ImGui::Text("Current State: Opening");
			break;
		default:
			break;
	}

	ImGui::DragFloat3("Current angular velocity: ", &AngularVelocityPerSecond.x);

	float StopRotationAngleAsDeg = RAD2DEG(StopRotationAngle);
	if (ImGui::DragFloat("Stop rotation angle: ", &StopRotationAngleAsDeg, 0.01f, 0.0f, 360.0))
		StopRotationAngle = DEG2RAD(StopRotationAngleAsDeg);

	if (ImGui::Button("Open Doors"))
		OpenDoors();

	if (ImGui::Button("Close Doors"))
		CloseDoors();
}

void TCompDoor::update(float dt){
	if (CurrentState == DoorState::STOPPED) return;

	TCompTransform * DoorTransform = ownTransform;
	if (!DoorTransform) return;
	Vector3 doorForwardVector = DoorTransform->getFront();

	TCompTransform * rdTransform = RightDoorTransformComponent;
	TCompRigidBody * rdRB = RightDoorRbComponent;
	bool HasRightDoor = rdTransform && rdRB;
	if (HasRightDoor)
		RotateRightDoor(doorForwardVector, rdTransform, rdRB);

	TCompTransform * ldTransform = LeftDoorTransformComponent;
	TCompRigidBody * ldRB = LeftDoorRbComponent;
	bool HasLeftDoor = ldTransform && ldRB;
	if (HasLeftDoor)
		RotateLeftDoor(doorForwardVector, ldTransform, ldRB);

	CheckDoorStop(rdRB, ldRB);
}

void TCompDoor::RotateRightDoor(const Vector3 & doorForwardVector, TCompTransform * transform, TCompRigidBody * rigidBody){
	float DeltaYawRightDoor = transform->getDeltaYawToAimToFromDir(doorForwardVector);

	if (CurrentState == DoorState::OPENING) {
		// Rotate right door. Note that we are converting the yaw for the right door by substracgin 180.0f due to using the same mesh rotate 180.0.
		// This is a bit tricky. You can disable it by setting to false the UsesMeshRotated parameter.
		float RightDeltaYawConverted = DeltaYawRightDoor;
		if (UsesMeshRotatedForRightDoor)
			RightDeltaYawConverted = abs(DEG2RAD(180.0f) - abs(DeltaYawRightDoor));

		if (RightDeltaYawConverted <= StopRotationAngle)
			rigidBody->setAngularVelocity(AngularVelocityPerSecond * (float)RotationSign * -1);
		else
			rigidBody->setAngularVelocity(Vector3::Zero);
	}
	else if (CurrentState == DoorState::CLOSING) {
		// Rotate right door. Here we are not converting the yaw, we want the real delta one.
		// Easier this way. When the wall is closed, the door will have 180.0f delta yaw and go -180.0f negative afterwards.
		if (DeltaYawRightDoor > 0.0f)
			rigidBody->setAngularVelocity(AngularVelocityPerSecond * (float)RotationSign * -1);
		else
			rigidBody->setAngularVelocity(Vector3::Zero);
	}
}

void TCompDoor::RotateLeftDoor(const Vector3 & doorForwardVector, TCompTransform * transform, TCompRigidBody * rigidBody){
	float DeltaYawLeftDoor = transform->getDeltaYawToAimToFromDir(doorForwardVector);

	if (CurrentState == DoorState::OPENING) {
		// Rotate left door.
		if (abs(DeltaYawLeftDoor) <= StopRotationAngle)
			rigidBody->setAngularVelocity(AngularVelocityPerSecond * (float)RotationSign);
		else
			rigidBody->setAngularVelocity(Vector3::Zero);
	}
	else if (CurrentState == DoorState::CLOSING) {
		// Rotate left door.
		if (DeltaYawLeftDoor > 0.0f)
			rigidBody->setAngularVelocity(AngularVelocityPerSecond * (float)RotationSign);
		else
			rigidBody->setAngularVelocity(Vector3::Zero);
	}
}

void TCompDoor::CheckDoorStop(TCompRigidBody * rRigidBody, TCompRigidBody * lRigidBody){
	if (rRigidBody && lRigidBody) {
		if (rRigidBody->getAngularVelocity() == Vector3::Zero && lRigidBody->getAngularVelocity() == Vector3::Zero)
			StopDoors();
	}
	else if (rRigidBody) {
		if (rRigidBody->getAngularVelocity() == Vector3::Zero)
			StopDoors();
	}
	else if (lRigidBody) {
		if (lRigidBody->getAngularVelocity() == Vector3::Zero)
			StopDoors();
	}
}

void TCompDoor::OpenDoors(){
	if (!openDoorSoundEvent.empty()) {
		TCompTransform* transform = ownTransform;
		EngineAudio.playEvent(openDoorSoundEvent, transform->getPosition());
	}

	CurrentState = DoorState::OPENING;
	RotationSign = -1;
}

void TCompDoor::CloseDoors(){
	if (!closeDoorSoundEvent.empty()) {
		TCompTransform* transform = ownTransform;
		EngineAudio.playEvent(closeDoorSoundEvent, transform->getPosition());
	}

	CurrentState = DoorState::CLOSING;
	RotationSign = 1;
}

void TCompDoor::StopDoors() {
	CurrentState = DoorState::STOPPED;

	TCompRigidBody * lRb = LeftDoorRbComponent;
	if (lRb)
		lRb->setAngularVelocity(Vector3::Zero);

	TCompRigidBody * rRb = RightDoorRbComponent;
	if (rRb)
		rRb->setAngularVelocity(Vector3::Zero);
}

void TCompDoor::ResetDoors() {
	StopDoors();

	TCompRigidBody * rdRB = RightDoorRbComponent;
	if (rdRB)
		rdRB->setRotation(RightDoorInitialRotation);

	TCompRigidBody * ldRB = LeftDoorRbComponent;
	if (ldRB)
		ldRB->setRotation(LeftDoorInitialRotation);
}
