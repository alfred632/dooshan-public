#pragma once

#include <vector>
#include "skeleton/comp_skeleton.h"

/* Blenders calculate the weight of a group of animations based on variables. 
Here we have a one and two dimensional blenders used for blending between animations.
Use the blender to calculate the weights and then set the new weights to the animation with
the corresponding animator passed as parameter. */

/* Abstract class for animation blenders. */
class AnimationBlender {};

/* A one dimensional blender similar to the one found in Unity. */
class OneDimensionalBlender : AnimationBlender{

public:

	struct Motion {
		AnimationID animation;
		float current_weight; // The weight the animation currently has.
		float threshold; // Value where the animation has full weight 1.0f.
		float leftSideDist; // Distance to the previous left motion.
		float rightSideDist; // Distance to the next right motion
		bool isDummy = false; // If true, the animation only works as a way of changing the blender between animations, no animation is played.
	};

private:

	// The blend parameter calculates what weight correspond to each animation.
	float blendParam = 0.0f;
	float minBlendParamVal = FLT_MAX, maxBlendParamVal = -FLT_MAX;

public:
	std::vector<Motion> sorted_motion_thresholds;

	void addMotion(AnimationID animation, float threshold, bool isDummy = false);
	void addDummyMotion(float threshold);

	void removeMotion(int indexInVector);
	void updateWeights(TCompSkeleton * animator, float delay);
	void endMotions(TCompSkeleton * animator, float delay);

	void setValueBlendParameter(float nBlendParam) {
		blendParam = Maths::clamp(nBlendParam, minBlendParamVal, maxBlendParamVal);
	}

private:
	void updateWeight(Motion & motion);
	void sortMotions();
	void recalculateWeights();
};


/* A two dimensional blender based on the use of one dimensional blenders.
This blender works with vectors that are ordered by their horizontal weights.
Animations are blended in the following way:
	- First, horizontal value is used to decide the weight of the horizontal vectors.
*/
class TwoDimensionalBlender : AnimationBlender {
	struct Motion {
		AnimationID animation;
		float current_weight; // The weight the animation currently has.
		float threshold; // Value where the animation has full weight 1.0f.
		float leftSideDist; // Distance to the previous left motion.
		float rightSideDist; // Distance to the next right motion
		bool isDummy = false; // If true, the animation only works as a way of changing the blender between animations, no animation is played.
	};

	struct MotionVector {
		std::vector<Motion> motion_vector_thresholds;
		float current_weight; // The weight the animation currently has.
		float horizontal_threshold; // Value where the animation has full weight 1.0f.
		float leftSideDist; // Distance to the previous left motion.
		float rightSideDist; // Distance to the next right motion
		bool isDummy = false; // If true, the animation only works as a way of changing the blender between animations, no animation is played.
	};

	// The blend parameter calculates what weight correspond to each animation.
	Vector2 blendParams;
	Vector2 minBlendParams = Vector2(FLT_MAX, FLT_MAX);
	Vector2 maxBlendParams = Vector2(-FLT_MAX, -FLT_MAX);

public:
	std::vector<MotionVector> sorted_motion_thresholds;

	void addMotion(AnimationID animation, float vertical_threshold, float horizontal_threshold, bool isDummy = false);
	void addDummyMotion(float vertical_threshold, float horizontal_threshold);

	void removeMotion(float horizontal_threshold, int indexInVector);
	void updateWeights(TCompSkeleton * animator, float delay);
	void endMotions(TCompSkeleton * animator, float delay);

	void setValueBlendParameter(const Vector2 & nBlendParam) {
		blendParams = nBlendParam;
	}

private:
	void updateWeightVector(MotionVector & motionVector);
	void updateWeight(Motion & motion, float vectorWeight);

	void sortVerticalVector(std::vector<Motion> & motions);
	void sortHorizontalVector();

	void recalculateHorizontalWeights();
	void recalculateVerticalWeights(std::vector<Motion> & motions);
};