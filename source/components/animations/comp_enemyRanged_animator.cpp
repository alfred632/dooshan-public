#include "mcv_platform.h"
#include "comp_enemyRanged_animator.h"
#include "skeleton/comp_skeleton.h"
#include "components/controllers/comp_character_controller.h"
#include "fsm/module_fsm.h"
#include "entity/common_msgs.h"
#include "engine.h"

DECL_OBJ_MANAGER("enemyRanged_animator", TCompEnemyRangedAnimator);

void TCompEnemyRangedAnimator::load(const json& j, TEntityParseContext& ctx) {
	_fsm_path = j["src"];
}

void TCompEnemyRangedAnimator::registerMsgs() {
	DECL_MSG(TCompEnemyRangedAnimator, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompEnemyRangedAnimator, TMsgEntityDead, onEntityDead);
	DECL_MSG(TCompEnemyRangedAnimator, TMsgResetComponent, onResetComponent);
	DECL_MSG(TCompEnemyRangedAnimator, TMsgLogicStatus, onLogicStatus);
}

void TCompEnemyRangedAnimator::declareInLua() {
	EngineScripting.declareComponent<TCompEnemyRangedAnimator>
		(
			"TCompEnemyRangedAnimator",
			"setIsAttacking", &TCompEnemyRangedAnimator::setIsAttacking,
			"setSpeed", &TCompEnemyRangedAnimator::setSpeed,
			"setInAttackPose", &TCompEnemyRangedAnimator::setInAttackPose
		);
}

void TCompEnemyRangedAnimator::debugInMenu() {
	TCompBase::debugInMenu();

	bool attacking = std::get<bool>(getVariable("attacking")->_value);
	bool isInjured = std::get<bool>(getVariable("isInjured")->_value);
	bool inAttackPose = std::get<bool>(getVariable("inAttackPose")->_value);
	float speed = std::get<float>(getVariable("speed")->_value);
	IState* currentState = _context.getCurrentState();
	
	ImGui::Text("Current anim: %s", currentState->_name.c_str());
	ImGui::Text("Time in State: %f", _context.getTimeInState());
	ImGui::Separator();
	ImGui::Text("Variables:");
	if (ImGui::Checkbox("Attacking: ", &attacking))
		_context.setVariable(TVariable("attacking", attacking));
	if (ImGui::Checkbox("In attack pose: ", &inAttackPose))
		_context.setVariable(TVariable("inAttackPose", inAttackPose));
	if (ImGui::Checkbox("Injured: ", &isInjured))
		_context.setVariable(TVariable("isInjured", isInjured));
	// Use ImGui magic to not to modify another entities
	if(ImGui::DragFloat("Speed: ", &speed, 0.1f, 0.0f, 30.0f))
		_context.setVariable(TVariable("speed", speed));
	if (currentState->_blenders.size() > 0) {

		ImGui::Separator();
		ImGui::Text("Blendings:");
		int i = 0;
		for (auto blender : currentState->_blenders) {
			ImGui::Text("Blender %d", i++);
			std::vector<OneDimensionalBlender::Motion> motions = blender.sorted_motion_thresholds;
			ImGui::Text("	Motions :   name - threshold");
			for (auto motion : motions) {
				std::string text = _context.getSkeletonComp()->getAnimationName(motion.animation) + "  " + std::to_string(motion.threshold);
				ImGui::Text(text.c_str());
			}
		}
	}

}

void TCompEnemyRangedAnimator::onEntityCreated(const TMsgEntityCreated & msg) {
	_fsm = EngineResources.getResource(_fsm_path)->as<CFSM>();

	/* Register animations to Cal3D */
	int animId = 0;
	for (auto state : _fsm->getStates()) {
		
		TCompSkeleton* skel = getComponent<TCompSkeleton>();
		// Register animations to Cal3D
		for (TAnimation anim : state->_animations) {
			state->animationID = skel->getAnimationID(anim.filename);
		}
	}
	_context.init(const_cast<CFSM*>(_fsm), CHandle(this).getOwner());
}

void TCompEnemyRangedAnimator::onEntityDead(const TMsgEntityDead & msg) {
	reset();
}

void TCompEnemyRangedAnimator::onResetComponent(const TMsgResetComponent & msg) {
	reset();
}

// Used for disabling the animator if the logic must be stopped.
void TCompEnemyRangedAnimator::onLogicStatus(const TMsgLogicStatus & msg) {
	active = msg.activate;
}
	
void TCompEnemyRangedAnimator::update(float dt) {
	if (!active) return;
	PROFILE_FUNCTION("EM FSM Context::Update");

	TCompCharacterController* controller = getComponent<TCompCharacterController>();
	setSpeed(controller ? controller->getCurrentMovementSpeed() : 0.0f);

	_context.update(dt);
}


const TVariable * TCompEnemyRangedAnimator::getVariable(const std::string& name) const
{
	return _context.getVariable(name);
}

void TCompEnemyRangedAnimator::setIsAttacking(bool value)
{
	TVariable var;
	TVariableValue val = value;

	var._name = _attackingVarName;
	var._value = val;

	_context.setVariable(var);
}

void TCompEnemyRangedAnimator::setSpeed(float speed)
{
	TVariable var;
	TVariableValue val = speed;

	var._name = _speedVarName;
	var._value = val;

	_context.setVariable(var);
}

void TCompEnemyRangedAnimator::setInAttackPose(bool value) {
	TVariable var;
	TVariableValue val = value;

	var._name = _inAttackPoseVarName;
	var._value = val;

	_context.setVariable(var);
}

void TCompEnemyRangedAnimator::setIsInjured(bool value) {
	TVariable var;
	TVariableValue val = value;

	var._name = _isInjuredVarName;
	var._value = val;

	_context.setVariable(var);
}

void TCompEnemyRangedAnimator::reset() {
	setIsAttacking(false);
	setSpeed(0.0f);
	setInAttackPose(false);
	setIsInjured(false);
}


