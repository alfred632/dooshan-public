#pragma once
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "fsm/context.h"
#include "fsm/fsm.h"
#include "fsm/state.h"


/* Player animator class. */

enum PlayerFSM {
	FULLBODY,
	LOWER_BODY,
	UPPER_BODDY
};

class TCompPlayerAnimator : public TCompBase {
	DECL_SIBLING_ACCESS();

	bool _start;

	// FSM 
	std::string _fsm_fullbody_path;
	std::string _fsm_lowerbody_path;
	std::string _fsm_upperbody_path;

	const CFSM* _fsm[3];
	CFSMContext _context[3];
	std::map<std::string, TVariable> _blackboard;

	// Definitions
	/*std::string _attackingVarName = "attacking";
	std::string _speedVarName = "speed";*/
	
	std::string _reloadNormalVarName = "isReloadingNormal";
	std::string _reloadFastVarName = "isReloadingFast";
	std::string _reloadFailVarName = "isReloadingFail";
	std::string _reloadNormalAnimNames[3] = { "ub_reload_smg_normal2", "ub_reload_shotgun_normal", "ub_reload_rifle_normal" };
	int _currentWeaponReloading = WeaponAnim::WEAPON_NONE;
	
	std::string _changeWeaponVarName = "isChangingWeapon";
	
	std::string _hipShootingVarName = "isHipShooting";
	std::string _aimShootingVarName = "isAimShooting";
	std::string _aimShootingAnimNames[3] = { "ub_shoot_aiming_smg", "ub_shoot_aiming_shotgun", "ub_shoot_aiming_rifle" };
	int _currentWeaponShooting = WeaponAnim::WEAPON_NONE;

	// Vars to check if player has the weaponUp and ready to shoot
	float _timeIndicator = 0.0f; //0.0f / 0.3f = not / ready to shoot (the time it takes to fadeIn and have the weaponUp)
	std::string _nonWeaponUpStatesVarNames[5] = { "WeaponDown", "Reload", "ReloadFast", "ReloadFail", "ChangeWeapon" }; //We don't use these but it's good to know
	std::string _weaponUpStatesVarNames[4] = { "WeaponUp", "Aiming", "AimShoot", "HipShoot" };


public:
	void load(const json& j, TEntityParseContext& ctx);
	static void registerMsgs();
	void debugInMenu();
	void update(float dt);

	TVariable* getVariable(std::string name, PlayerFSM fsmId);
	void setVariable(std::string name, bool value);
	void setVariable(std::string name, float value);
	void setVariable(std::string name, int value);

	void setReload(int weapon);
	void clearReload();
	float getReloadElapsedTime();
	void setReloadFast(float time);
	void setReloadFail(float time);
	
	void setChangeWeapon(int weapon);
	void setHipShootingWeapon(int weapon);
	void setAimShootingWeapon(int weapon);
	void clearShooting();
	bool isWeaponUpReadyToShoot() { return _timeIndicator >= 0.3f; };

private:
	// Once the entity is created, register animations if they were not registered before and start the two fsms.
	// Also get the skeleton component.
	void onEntityCreated(const TMsgEntityCreated & msg);
	// Used for disabling the animator if the logic must be stopped.
	void onLogicStatus(const TMsgLogicStatus & msg);

	// TO REMOVE: temporary knockback generator
	void onHitDamage(const TMsgHitDamage & msg);

	void onResurrectionMsg(const TMsgFullRestore & msg);

	void updateWeaponReadyToShoot(float dt);
};

