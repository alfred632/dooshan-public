#pragma once
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "fsm/context.h"
#include "fsm/fsm.h"
#include "fsm/state.h"


/* Player menu animator class. */
class TCompPlayerMenuAnimator : public TCompBase {
	DECL_SIBLING_ACCESS();

	bool _start;

	// FSM 
	std::string _fsm_path;
	const CFSM* _fsm = nullptr;
	CFSMContext _context;

	// Definitions	

public:
	void load(const json& j, TEntityParseContext& ctx);
	static void registerMsgs();
	void debugInMenu();
	void update(float dt);

	TVariable* getVariable(std::string name);
	void setMenuState(int state);

private:
	// Once the entity is created, register animations if they were not registered before and start the two fsms.
	// Also get the skeleton component.
	void onEntityCreated(const TMsgEntityCreated & msg);
	// Used for disabling the animator if the logic must be stopped.
	void onLogicStatus(const TMsgLogicStatus & msg);
};

