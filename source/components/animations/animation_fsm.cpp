#include "mcv_platform.h"
#include "animation_fsm.h"

AnimationFSM::AnimationFSM() {}

AnimationFSM::~AnimationFSM() {
	for (auto & state : statemap)
		delete state.second;
}

void AnimationFSM::update(float dt)
{
  if (_currentState)
  {
    _timeInState += dt;
    _currentState->update(*this, dt);
  }
}

void AnimationFSM::addState(const std::string & name, IAnimState * newState, const json & j) {
	// If there is a state already registered with the same name, we throw an exception.
	if (statemap.find(name) != statemap.end())
		Utils::fatal("Invalid AddState(%s). Already defined.\n", name.c_str());
	statemap[name] = newState;
	newState->load(name, this, j);
}

void AnimationFSM::changeToState(const std::string & stateName)
{
	IAnimState * newState = statemap[stateName];
  if (_currentState)
    _currentState->onExit(*this);

  _currentState = newState;
  _timeInState = 0.f;

  if (_currentState)
    _currentState->onEnter(*this);
}

void AnimationFSM::start()
{
  _currentState = nullptr;
	changeToState(_startState);
}

void AnimationFSM::stop()
{
  changeToState(nullptr);
}

void AnimationFSM::debugInMenu()
{
	ImGui::Spacing(0, 5);
  ImGui::Text("Current state: %s", _currentState ? _currentState->getStateName().c_str() : "...");
  ImGui::Text("Time in state: %.2f", _timeInState);
  ImGui::Spacing(0, 5);
}

void IAnimState::debugInMenu() const {}