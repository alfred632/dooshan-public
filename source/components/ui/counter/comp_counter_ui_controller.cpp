#include "mcv_platform.h"
#include "comp_counter_ui_controller.h"
#include "entity/entity_parser.h"
#include "render/render.h"

void TCompCounterUIController::update(float dt) {
	bool showWindow = true;
	auto imGuiFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoBackground;
	//ImFont * font; //Warning unreferenced local varaible


	ImGui::SetNextWindowPos(ImVec2(_position.x * Render.getWidth(), _position.y * Render.getHeight()), 0);
	if (!ImGui::Begin(("GUI_window_" + _attribute).c_str(), &showWindow, imGuiFlags)) {
		ImGui::End();
		return;
	}

	ImGui::Text(_attribute.c_str());
	ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(_fontColor.x, _fontColor.y, _fontColor.z, _fontColor.w));
	if (_storedValue < 0)
		ImGui::Text("%d/%d", _curValue, _maxValue);
	else
		ImGui::Text("%d/%d [%d]", _curValue, _maxValue, _storedValue);
	ImGui::PopStyleColor();

	ImGui::End();
}

void TCompCounterUIController::load(const json& j, TEntityParseContext& ctx) {
	if (j.count("font_color"))
		_fontColor = loadVector4(j, "font_color");
	_position.x = j.value("x_coord", _position.x);
	_position.y = j.value("y_coord", _position.y);
}

void TCompCounterUIController::debugInMenu() {}