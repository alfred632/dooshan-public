#pragma once

#include "entity/entity.h"
#include "components/common/comp_base.h"
#include "entity/common_msgs.h"

struct TMsgLoseMessageUI {
	bool activate = true;
	DECL_MSG_ID();
};

class TCompLoseMessageUI : public TCompBase {
	DECL_SIBLING_ACCESS();

	bool active = false;

	std::string message = "You have died.\n"; // Message at the end of the game.
	std::string endText = "Press E to resurrect (ONLY FOR DEBUG).\n"; // Text in the button.
	VEC2 _position = VEC2(0.5f, 0.5f);

public:
	void update(float dt);
	void load(const json & j, TEntityParseContext& ctx);
	void activateOnMsg(const TMsgLoseMessageUI & msg);
	static void registerMsgs();
};