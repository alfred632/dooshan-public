#include "mcv_platform.h"
#include "comp_crosshair_ui_controller.h"
#include "entity/entity_parser.h"
#include "render/render.h"
#include "logic/logic_manager.h"
#include "ui/widgets/ui_image.h"
#include "engine.h"
#include "ui/ui_widget.h"

DECL_OBJ_MANAGER("crosshair_ui_controller", TCompCrosshairUIController);

TCompCrosshairUIController::~TCompCrosshairUIController() {
	if (bars.empty() || !EngineUI.findWidgetByName(crosshairWidget, "crosshair_smg"))
		return;

	// Restore the original position on the hud
	bars[0]->getParams()->position.x = initialBarPosition.at(0).x;
	bars[1]->getParams()->position.x = initialBarPosition.at(1).x;
	bars[2]->getParams()->position.y = initialBarPosition.at(2).y;
	bars[3]->getParams()->position.y = initialBarPosition.at(3).y;
}

void TCompCrosshairUIController::debugInMenu()
{
	TCompBase::debugInMenu();
}

void TCompCrosshairUIController::onEntityCreated(const TMsgEntityCreated& msg)
{
	getCrosshairBars();
}

void TCompCrosshairUIController::registerMsgs()
{
	DECL_MSG(TCompCrosshairUIController, TMsgEntityCreated, onEntityCreated);
}

void TCompCrosshairUIController::draw(float outerRadius, float innerRadius, float size) {
	if (bars.empty() && !getCrosshairBars()) 
		return;

	if (active) {
		bars[0]->getParams()->position.x = barPosition[0].x - innerRadius * Render.getHeight();
		bars[1]->getParams()->position.x = barPosition[1].x + innerRadius * Render.getHeight();
		bars[2]->getParams()->position.y = barPosition[2].y - innerRadius * Render.getHeight();
		bars[3]->getParams()->position.y = barPosition[3].y + innerRadius * Render.getHeight();

		/*Utils::dbg("innerRadius * Render.getWidth(): %f\n", innerRadius * Render.getHeight());
		Utils::dbg("innerRadius* Render.getHeight(): %f\n", innerRadius * Render.getHeight());*/
	}

}

bool TCompCrosshairUIController::getCrosshairBars() {
	if (!EngineUI.findWidgetByName(crosshairWidget, "crosshair_smg"))
		return false;

	for (auto child : crosshairWidget.getChildren()) {
		if (UI::CImage* imgChild = dynamic_cast<UI::CImage*>(child)) {
			bars.push_back(imgChild);
			barPosition.push_back(imgChild->getParams()->position);
			initialBarPosition.push_back(imgChild->getParams()->position);
		}
	}

	return true;
}
