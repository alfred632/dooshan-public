#include "mcv_platform.h"
#include "comp_floating_text.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "ui/module_ui.h"
#include "modules/module_camera_mixer.h"
#include "components/camera/comp_camera.h"
#include "components/common/comp_aabb.h"


using namespace ImGui;

DECL_OBJ_MANAGER("floating_damage", TCompFloatingDamage);

void TCompFloatingDamage::load(const json& j, TEntityParseContext& ctx) {
	
	_fontName = j.value("font_name", _fontName);
	_textColor = loadVector4(j, "text_color");
	_outlineColor = loadVector4(j, "outline_color");
	_textColorCritical = loadVector4(j, "text_color_critical");
	_outlineColorCritical = loadVector4(j, "outline_color_critical");
	_textTargetScale = j.value("target_scale", _textTargetScale);
	_aliveTime = j.value("alive_time", _aliveTime);
	_timeToStartVanish = j.value("start_time_vanish", _timeToStartVanish);
	_offset = loadVector2(j, "offset");
	
}


void TCompFloatingDamage::debugInMenu()
{
	Text("Text color: ");
	SameLine();
	ColorEdit4("##textClr", (float*)& _textColor, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoLabel);
	Text("");
	Text("Outline color: ");
	SameLine();
	ColorEdit4("##outClr", (float*)& _outlineColor, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoLabel);
	Text("");
	Text("Offset: ");
	SameLine();
	DragFloat2("##offsetdrg", &_offset.x, 0.01f, 0.0f, 1000.0f);
}

void TCompFloatingDamage::registerMsgs() {
	DECL_MSG(TCompFloatingDamage, TMsgFloatingDamageText, onHitDamage);
}


void TCompFloatingDamage::onHitDamage(const TMsgFloatingDamageText& msg)
{
	UI::TFloatDamage settings;
	std::string textDamage = std::to_string((int)msg.damage);

	settings.textDamage = textDamage;
	settings.fontName = _fontName;
	settings.textColor = msg.isCritical ? _textColorCritical : _textColor;
	settings.outlineColor = msg.isCritical ? _outlineColorCritical : _outlineColor;
	settings.isCritical = msg.isCritical;
	settings.targetWorldPosition = msg.targetLocation;
	settings.aliveTime = _aliveTime;
	if (msg.isCritical)
		_scaleMultiplier = 1.5f;

	settings.scale = _scaleMultiplier;
	int rnd = RNG.i(100); // Randomly spawn text on left, center or right
	if (rnd >= 0 && rnd < 30) {
		if(_offset.x > 0.0f)	_offset.x *= -1.0f;
		settings.animationName = "FloatingDamageLeft";
	}
	else if (rnd >= 30 && rnd < 60) {
		if(_offset.x < 0.0f)	_offset.x *= -1.0f;
		settings.animationName = "FloatingDamageRight";
	}
	else {
		_offset.x = 0.0f;
		if (_lastPlayedAnim == "FloatingDamageCenter") {
			settings.animationName = RNG.b() ? "FloatingDamageLeft" : "FloatingDamageRight";
		}
		else
			settings.animationName = "FloatingDamageCenter";
	}
	_lastPlayedAnim = settings.animationName;
	if (settings.animationName == "FloatingDamageCenter")
		_offset.y = -40;
	settings.offset = _offset;
	UI::CTextFont* txtFnt = EngineUI.createFloatingDamageWidget(settings);
	_widgets.push_back(txtFnt);
}

void TCompFloatingDamage::update(float dt) {
	PROFILE_FUNCTION("Floating Damage - Update");
	if (!_widgets.empty()) {
		if (TCompTransform * t = getComponent<TCompTransform>()) {
			CTransform* cameraT = EngineCameraMixer.getMixerOutputCameraTransform();
			TCompLocalAABB* eAABB = getComponent<TCompLocalAABB>();
			VEC3 targetPos = t->getPosition();
			targetPos.y += eAABB->Extents.y; // get height from local aabb

			float distance = VEC3::Distance(targetPos, cameraT->getPosition());
			VEC3* screenSpace = nullptr; // Get screen coords from enemy head
			if (CEntity * eCamera = EngineLogicManager.getOutputCamera()) {
				if (TCompCamera * cameraComp = eCamera->getComponent<TCompCamera>()) {
					screenSpace = new VEC3;
					cameraComp->getScreenCoordsOfWorldCoord(targetPos, screenSpace);
				}
			}
			// for every widget alive, update it's scale depending on ene
			auto it = _widgets.begin();
			while (it != _widgets.end()) {
				if (*it != nullptr) {
					CClock* widgetClock = (*it)->getCLock();
					// We look into enemie's y to check if is not dead and position reset by pooling
					if (!widgetClock->isFinished() && distance > 0.0f && t->getPosition().y > -20.0f) {
						// Calculate the scale depending on distance
						(*it)->getTextParams()->scale.x = (_textTargetScale / distance * _scaleMultiplier);
						// Reduce the scale proportionally until 0 (custom vanish "animation")
						if (widgetClock->remaining() <= _timeToStartVanish) {
							(*it)->getTextParams()->scale.x *= (widgetClock->remaining() / widgetClock->initialTime());
						}
						(*it)->getTextParams()->scale.y = (*it)->getTextParams()->scale.x;
						// if the screencoords are correct, we update the position and the offset of the animation
						if (screenSpace) {
							float yAccum = widgetClock->elapsed() / widgetClock->initialTime() * _leaveDistance;
							(*it)->getTextParams()->position.x = screenSpace->x + _offset.x;
							(*it)->getTextParams()->position.y = screenSpace->y + _offset.y + yAccum;
						}
						it++;
					}
					else {
						EngineUI.deleteWidget((*it));
						it = _widgets.erase(it);
						if (it == _widgets.end())
							break;
					}
				}
			}
		}
	}
}