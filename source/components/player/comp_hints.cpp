#include "mcv_platform.h"
#include "comp_hints.h"
#include "../scenario/comp_sanctuary.h"

DECL_OBJ_MANAGER("hints", TCompHints)

void TCompHints::registerMsgs() {
	DECL_MSG(TCompHints, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompHints, TMsgEntityTriggerEnter, onTriggerEnter);
}

void TCompHints::declareInLua() {
	EngineScripting.declareComponent<TCompHints>
	(
		"TCompHints",
		"GetCurrentHint", &TCompHints::GetCurrentHint,
		"SetEnabled", &TCompHints::SetEnabled
	);
}

void TCompHints::update(float dt) {
	if (_needToGetWidgets) getWidgets();
	if (!_enabled || _currentHint.empty()) return;
	
	assert(_hints.find(_currentHint) != _hints.end());
	assert(_hints[_currentHint].function && "The function pointer is empty!");
	(this->*_hints[_currentHint].function)(dt);
}

void TCompHints::load(const json& j, TEntityParseContext& ctx) {
	_enabled = j.value<bool>("enabled", _enabled);
	_hints["Movement"] = hint{ "Movement", "movement_hint_start", "movement_hint_end", "hintMovement", &TCompHints::checkMovementHint };
	_hints["Shoot"] = hint{ "Shoot", "shoot_hint_start", "shoot_hint_end", "hintShoot", &TCompHints::checkShootHint};
	_hints["Reload"] = hint{ "Reload", "reload_hint_start", "reload_hint_end", "hintReload", &TCompHints::checkReloadHint};
	_hints["Special"] = hint{ "Special", "special_hint_start", "special_hint_end", "hintSpecial", &TCompHints::checkSpecialHint};
	_hints["Scythe"] = hint{ "Scythe", "scythe_hint_start", "scythe_hint_end", "hintScythe", &TCompHints::checkScytheHint };
	_hints["Perception"] = hint{ "Perception", "perception_hint_start", "perception_hint_end", "hintPerception", &TCompHints::checkPerceptionHint };
	_hints["Sanctuary"] = hint{ "Sanctuary", "sanctuary_hint_start", "sanctuary_hint_end", "hintSanctuary", &TCompHints::checkSanctuaryHint };

	_currentHint = "Movement";
	_hints["Special"].duration = 8.0f;
	_hints["Movement"].enableEntityTriggered = true;
	_soundEvent = j.value("sound_event", _soundEvent);
}

void TCompHints::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::Text("Current Hint: %s", _currentHint.c_str());
	if (_currentHint.empty())
		return;

	ImGui::Text("enableEntityTriggered: %s", _hints[_currentHint].enableEntityTriggered ? "true" : "false");
	ImGui::Text("disableEntityTriggered: %s", _hints[_currentHint].disableEntityTriggered ? "true" : "false");
	ImGui::Text("Hint duration: %f", _hints[_currentHint].duration);
	ImGui::Text("Hint active time: %f", _hints[_currentHint].totalTimeActive);
}

void TCompHints::onTriggerEnter(const TMsgEntityTriggerEnter& trigger_enter) {
	CEntity* entity = trigger_enter.h_entity;
	if (!entity) return;

	TCompTags* c_tags = entity->getComponent<TCompTags>();
	if (!c_tags) return;

	if (_enabled)
		checkEntityTags(c_tags);
}

void TCompHints::getWidgets() {
	if (EngineModules.getCurrentGS() != "gs_gametesting") {
		_enabled = false;
		_needToGetWidgets = false;
		return;
	}

	for (auto& hint : _hints) {
		hint.second.widget = EngineUI.getImageWidgetByName("hintWindow", hint.second.widgetName);
		assert(hint.second.widget && "We couldn't find the widget by name");
	}

	_needToGetWidgets = false;
}

void TCompHints::checkEntityTags(TCompTags* c_tags) {
	for (auto& hint : _hints) {
		if (c_tags->hasTag(hint.second.enableEntityTag.c_str()))
			hint.second.enableEntityTriggered = true;
		if (c_tags->hasTag(hint.second.disableEntityTag.c_str()))
			hint.second.disableEntityTriggered = true;
	}
}

void TCompHints::checkMovementHint(float dt) {
	// Check movement completition
	if (EngineInput["forward"].isPressed())
		_movementCompletition |= 1 << 0;
	if (EngineInput["backwards"].isPressed())
		_movementCompletition |= 1 << 1;
	if (EngineInput["left"].isPressed())
		_movementCompletition |= 1 << 2;
	if (EngineInput["right"].isPressed())
		_movementCompletition |= 1 << 3;

	// If the player has moved in all four directions
	if (_movementCompletition == 15)
		_hints[_currentHint].conditionCompleted = true;

	if (checkAndUpdateHintStatus(dt))
		_currentHint = "Shoot";
}

void TCompHints::checkShootHint(float dt) {
	// Check shoot completition
	if (EngineInput["shoot"].isPressed())
		_shootCompletition |= 1 << 0;
	if (EngineInput["aim"].isPressed())
		_shootCompletition |= 1 << 1;

	// If the player has shot and aimed
	if (_shootCompletition == 3)
		_hints[_currentHint].conditionCompleted = true;

	if (checkAndUpdateHintStatus(dt))
		_currentHint = "Reload";
}

void TCompHints::checkReloadHint(float dt) {
	// Check reload completition
	if (EngineInput["reload"].isPressed())
		_hints[_currentHint].conditionCompleted = true;

	if (checkAndUpdateHintStatus(dt))
		_currentHint = "Special";
}

void TCompHints::checkSpecialHint(float dt) {
	// Check special completition
	if (EngineInput["altfire"].isPressed())
		_specialCompletition |= 1 << 0;
	if (_specialCompletition == 1 && EngineInput["shoot"].isPressed())
		_specialCompletition |= 1 << 1;

	// If the player has toggled the special and shot
	if (_specialCompletition == 3)
		_hints[_currentHint].conditionCompleted = true;

	if (checkAndUpdateHintStatus(dt))
		_currentHint = "Scythe";
}

void TCompHints::checkScytheHint(float dt) {
	// Check scythe completition
	if (EngineInput["scythePower"].isPressed())
		_hints[_currentHint].conditionCompleted = true;

	if (checkAndUpdateHintStatus(dt))
		_currentHint = "Perception";
}

void TCompHints::checkPerceptionHint(float dt) {
	// Check perception completition
	if (EngineInput["perceptionPower"].isPressed())
		_hints[_currentHint].conditionCompleted = true;

	if (checkAndUpdateHintStatus(dt))
		_currentHint = "Sanctuary";
}

void TCompHints::checkSanctuaryHint(float dt) {
	CHandle SanctuaryH = getEntityByName("santu018");
	if (!SanctuaryH.isValid()) return;

	CEntity * SanctuaryEntity = SanctuaryH;
	if (!SanctuaryEntity) return;

	TCompSanctuary * Sanctuary = SanctuaryEntity->getComponent<TCompSanctuary>();
	if (!Sanctuary) return;

	// Check perception completion
	if (Sanctuary->HasBeenActivated())
		_hints[_currentHint].conditionCompleted = true;

	if (checkAndUpdateHintStatus(dt)) {
		_currentHint = "";
		_enabled = false;
	}
}

bool TCompHints::checkAndUpdateHintStatus(float dt) {
	hint* cH = &(_hints[_currentHint]);

	// If not showing and not ended already, but has entered the trigger, enable it and play the fadeIn animation
	if (!cH->isShowing && !cH->disableEntityTriggered && cH->enableEntityTriggered ) {
		cH->widget->getParams()->visible = true;
		cH->widget->playAnimation("HintFadeIn", false);
		cH->isShowing = true;
		EngineAudio.playEvent(_soundEvent);
		return false;
	}

	// If it is showing but it is playing the fadeIn animation we return so we will not count this as alive time.
	if (cH->isShowing && cH->widget->getCurrentAnimationPercentage() != 0.0f)
		return false;

	// If showing and needs to be disabled, disable it and play the fadeOut animation (It needs to have lived at least 2.5 seconds)
	if (cH->isShowing && (cH->disableEntityTriggered || cH->conditionCompleted) && cH->totalTimeActive > 2.5f) {
		// If there is any other animation (fadeIn in this case playing) we need to wait for it to finish
		if (cH->widget->getCurrentAnimationPercentage() != 0.0f)
			return false;
		cH->isShowing = false;
		cH->widget->playAnimation("HintFadeOut", false);
		cH->disableEntityTriggered = true; // Just in case if the hint was completed by the condition and not by the end trigger.
		return false;
	}

	// If 'not' showing and completed, check if the fadeout animation has ended
	if (!cH->isShowing && (cH->disableEntityTriggered || cH->conditionCompleted)) {
		if (cH->widget->getCurrentAnimationPercentage() > 0.99f) {
			cH->widget->getParams()->visible = false;
			return true;
		}
		else
			return false;
	}

	// Finally if it is showing and it doesn't have an animation playing, increment the lifetime
	if (cH->isShowing && cH->widget->getCurrentAnimationPercentage() == 0.0f) {
		// If just running we add the dt
		cH->totalTimeActive += dt;
		if (cH->totalTimeActive > cH->duration)
			cH->conditionCompleted = true;
	}

	return false;
}

void TCompHints::SetEnabled(bool nEnabled) {
	_enabled = nEnabled;
}