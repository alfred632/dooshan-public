#pragma once
#include "components/common/comp_base.h"
#include "components/common/comp_collider.h"
#include "components/common/comp_tags.h"
#include "entity/entity.h"
#include "engine.h"
#include "entity/common_msgs.h"
#include "ui/widgets/ui_image.h"

class TCompHints : public TCompBase {

public:

	DECL_SIBLING_ACCESS()

	static void registerMsgs();
	static void declareInLua();

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();

	std::string GetCurrentHint() { return _currentHint; }
private:
	// The hints system is active or not
	bool _enabled = true;
	std::string _soundEvent = "";
	bool _needToGetWidgets = true;

	// In order to execute functions by accessing them in the map
	typedef void (TCompHints::*pfunc)(float dt);
	// Hint information struct
	struct hint {
		std::string hintName = "";
		std::string enableEntityTag = "";
		std::string disableEntityTag = "";
		std::string widgetName = "";
		pfunc function = nullptr;
		UI::CImage* widget = nullptr;
		bool enableEntityTriggered = false;
		bool disableEntityTriggered = false;
		float totalTimeActive = 0.0f;
		float duration = 5.0f;
		bool isShowing = false;
		bool conditionCompleted = false;
	};
	// Hint map with all the relevant information and mapped functions
	std::unordered_map<std::string, hint> _hints;
	// Current hint
	std::string _currentHint;

	// Additional variables for hints
	unsigned int _movementCompletition = 0;
	unsigned int _shootCompletition = 0;
	unsigned int _specialCompletition = 0;

	void onTriggerEnter(const TMsgEntityTriggerEnter& trigger_enter);

	void getWidgets();

	void checkMovementHint(float dt);
	void checkShootHint(float dt);
	void checkReloadHint(float dt);
	void checkSpecialHint(float dt);
	void checkScytheHint(float dt);
	void checkPerceptionHint(float dt);
	void checkSanctuaryHint(float dt);

	// Check the entity tags in order to see if any hint needs updating
	void checkEntityTags(TCompTags* c_tags);
	// Check if the hint needs to be enabled / disabled. Returns true if finished showing
	bool checkAndUpdateHintStatus(float dt);

	void SetEnabled(bool nEnabled);
};