#include "mcv_platform.h"
#include "comp_input_player.h"
#include "entity/entity_parser.h"
#include "components/common/comp_transform.h"
#include "components/camera/comp_player_camera_controller.h"
#include "components/camera/comp_camera.h"
#include "components/player/comp_holy_water.h"
#include "components/weapons/comp_weapons_manager.h"
#include "components/powers/comp_vortex_player.h"
#include "components/powers/comp_teleport_player.h"
#include "components/powers/comp_scythe_player.h"
#include "components/powers/comp_healing.h"
#include "components/powers/comp_perception.h"
#include "components\ui\comp_lose_ui.h"
#include "components\common\comp_render.h"

DECL_OBJ_MANAGER("input_player", TCompInputPlayer);

void TCompInputPlayer::load(const json & j, TEntityParseContext& ctx) {
	playerCamera = j.value("cameraFollowing", playerCamera);
	facingCameraThreshold = DEG2RAD(j.value("facingCamerathreshold", facingCameraThreshold));
	Init();
}

void TCompInputPlayer::debugInMenu() {
	TCompBase::debugInMenu();

	ImGui::Spacing();
	ImGui::Text("Current state: ");
	ImGui::Text(state.c_str());
	ImGui::Spacing();

	float thresholdCamShoot = RAD2DEG(facingCameraThreshold);
	ImGui::DragFloat("Shoot threshold", &thresholdCamShoot, 0.001f, 0.f, 360.f);
	if (thresholdCamShoot != RAD2DEG(facingCameraThreshold))
		facingCameraThreshold = DEG2RAD(thresholdCamShoot);
}

void TCompInputPlayer::update(float dt)
{
	if (aliveColour == VEC4(0,0,0,0)) {
		TCompRender * render = getComponent<TCompRender>();
		if (render) {
			VEC4 renderColour = render->getColor();
			aliveColour = renderColour;
		}
	}	
	playerController = getComponent<TCompPlayerCharacterController>();
	if (playerController == nullptr)
		return;

	// checkPlayerCamera
	if (!cameraTransformHandle.isValid()) {
		CEntity * ent = getEntityByName(playerCamera.c_str());
		cameraTransformHandle = ent->getComponent<TCompTransform>();
		cameraControllerHandle = ent->getComponent<TCompPlayerCameraController>();
		cameraHandle = ent->getComponent<TCompCamera>();
	}

	// TEMP_ADDED - updating the change between states
	assert(!state.empty());
	assert(statemap.find(state) != statemap.end());
	// this is a trusted jump as we've tested for coherence in ChangeState
	(this->*statemap[state])(dt);

	//!** Esto lo pondr�a en otro lado, un Input Menu Controller.
	// Other 
	if (EngineInput["pause"]) {
		// TODO: pauseMenu
		//Engine.getModuleManager().changeToGamestate("pause");
		CApplication::instance().sendQuitMsg(); //Hack MS1 close the app
	}
}

void TCompInputPlayer::registerMsgs() {
	DECL_MSG(TCompInputPlayer, TMsgToogleComponent, onToggleComponent);
	DECL_MSG(TCompInputPlayer, TMsgFullRestore, OnResurrection);  // Used during this milestone to resurrect player on dead. Remove once finished.
	DECL_MSG(TCompInputPlayer, TMsgEntityDead, ChangeToDeadState);
	DECL_MSG(TCompInputPlayer, TMsgInputControllerStatus, OnInputStatus);
}

void TCompInputPlayer::OnInputStatus(const TMsgInputControllerStatus & msg) {
	active = msg.activate;
}

void TCompInputPlayer::Init() {
	AddState("Normal", (statehandler)&TCompInputPlayer::OnNormalGrounded);
	AddState("WeaponUp", (statehandler)&TCompInputPlayer::onWeaponUpGrounded);
	AddState("Aim", (statehandler)&TCompInputPlayer::onAimingGrounded);
	AddState("Sprint", (statehandler)&TCompInputPlayer::OnSprintGrounded);
	AddState("Dead", (statehandler)&TCompInputPlayer::OnDead);
	ChangeState("Normal");
	powerState = PowerState::NONE;
}

TMsgEntityDead msg;

bool TCompInputPlayer::isFacingCamera(TCompTransform * playerTransform, TCompTransform * cameraTransform, float threshold) {
	float yawObj = playerTransform->getDeltaYawToAimTo(playerTransform->getPosition() + cameraTransform->getFront());
	if (abs(yawObj) <= threshold)
		return true;
	return false;
}

void TCompInputPlayer::OnNormalGrounded(float dt) {

	TCompTransform * playerTransform = getComponent<TCompTransform>();
	TCompPlayerCameraController * playerCamController = cameraControllerHandle; // Get the camera that follows the player.
	TCompTransform * cameraTransform = cameraTransformHandle; // Get the camera's transform.
	TCompCamera * camera = cameraHandle;
	if (!playerTransform || !cameraTransform) return;

	TCompWeaponManager * weaponManager = getComponent<TCompWeaponManager>(); // Get the weapon manager.
	TCompWeaponsController* wp_contr = weaponManager->getCurrentWeapon(); // Also get the current weapon.
	TCompVortexPlayer* vortexPlayer = getComponent<TCompVortexPlayer>();
	TCompTeleportPlayer * teleportPlayer = getComponent<TCompTeleportPlayer>();
	TCompScythePlayer* scythePlayer = getComponent<TCompScythePlayer>();

	VEC3 playerPosition = playerTransform->getPosition();
	VEC3 playerFront = playerTransform->getFront();

	// Movement
	bool moving = false, wantToSprint = false;
	VEC3 movementDelta;
	if (EngineInput["forward"]) {
		// Only sprint if moving forward.
		if (EngineInput["sprint"])
			wantToSprint = true;

		movementDelta += VEC3::Forward;
		moving = true;
	}
	if (EngineInput["backwards"]) {
		movementDelta -= VEC3::Forward;
		moving = true;
	}
	if (EngineInput["left"]) {
		movementDelta += VEC3::Left;
		moving = true;
	}
	if (EngineInput["right"]) {
		movementDelta -= VEC3::Left;
		moving = true;
	}

	movementDelta.Normalize();

	if (moving)
		playerController->rotateAndMoveFreely(cameraTransform, movementDelta * playerController->getPlayerSpeedNormal(), playerController->getRotationSpeedNormal(), dt);

	bool facingCamera = isFacingCamera(playerTransform, cameraTransform, facingCameraThreshold);

	/* Weapons */
	if (EngineInput["shoot"].justPressed() && powerState == PowerState::NONE) {
		// Once we are facing, shoot.
		if (facingCamera)
			wp_contr->shoot();

		ChangeState("WeaponUp");
	}

	// Small hack to simulate time before we can sprint again. This should consider animation blending.
	if (currentTimeToStartSprintingAgain >= 0.0f)
		currentTimeToStartSprintingAgain -= dt;

	// Here we check if we must sprint.
	if (wantToSprint) {
		if (facingCamera && currentTimeToStartSprintingAgain <= 0.0f) { // If we are already facing the sprinting direction, sprint.
			ChangeState("Sprint");
			playerCamController->ChangeState("Sprint");
			return;
		}
	}

	if (EngineInput["aim"].justPressed() && powerState == PowerState::NONE) {
		ChangeState("Aim");
		playerCamController->ChangeState("Aim");
		return;
	}

	if (EngineInput["reload"].justPressed()) {
		wp_contr->reload();
	}
	if (EngineInput["weapon1"].justPressed()) {
		weaponManager->setActiveWeaponSlot(0);
	}
	if (EngineInput["weapon2"].justPressed()) {
		weaponManager->setActiveWeaponSlot(1);
	}
	if (EngineInput["weapon3"].justPressed()) {
		weaponManager->setActiveWeaponSlot(2);
	}
	if (EngineInput["weapon4"].justPressed()) {
		TCompHolyWater * holy_water = getComponent<TCompHolyWater>();
		holy_water->use();
	}

	if (EngineInput.mouse()._wheelDelta > 0.f) {
		weaponManager->changeNextWeapon();
	}

	if (EngineInput.mouse()._wheelDelta < 0.f) {
		weaponManager->changePrevWeapon();
	}

	// Powers
	if (EngineInput["scythePower"].justPressed()) {
		// If we are not already moving in the direction of the camera. i.e: We are idle.
		if (moving == false) {
			// If not facing the camera, rotate.
			if (!facingCamera)
				playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedHipShooting(), dt);
		}

		// Once we are facing, use power.
		if (facingCamera && powerState == PowerState::NONE) {
			scythePlayer->activatePower();
		}
	}
	if (EngineInput["healPower"]) {
		TCompHealing* healingPower = getComponent<TCompHealing>();
		assert(healingPower);
		healingPower->heal(); 
	}
	if (EngineInput["interact"].justPressed()) {
		playerController->interact(dt);
	}
	if (EngineInput["vortexPower"].justPressed()) {
		// If we are not already moving in the direction of the camera. i.e: We are idle.
		if (moving == false) {
			// If not facing the camera, rotate.
			if (!facingCamera)
				playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedHipShooting(), dt);
		}
		// Once we are facing, use power if not already using one.
		if (facingCamera && powerState == PowerState::NONE) {
			powerState = vortexPlayer->startPower(cameraTransform) ? PowerState::VORTEX : PowerState::NONE;
		}
	}
	if (EngineInput["teleportPower"].justPressed()) {
		// If we are not already moving in the direction of the camera. i.e: We are idle.
		if (moving == false) {
			// If not facing the camera, rotate.
			if (!facingCamera)
				playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedHipShooting(), dt);
		}
		// Once we are facing, use power if not already using one.
		if (facingCamera && powerState == PowerState::NONE) {
			powerState = teleportPlayer->startPower(cameraTransform) ? PowerState::TELEPORT : PowerState::NONE;
		}
	}
	if (powerState != PowerState::NONE && EngineInput["shoot"].justPressed()) {
		switch (powerState) {
		case PowerState::VORTEX:
			powerState = PowerState::NONE;
			vortexPlayer->activatePower();
			break;
		case PowerState::TELEPORT:
			powerState = PowerState::NONE;
			teleportPlayer->activatePower();
			break;
		default:
			break;
		}
	}
	if (powerState != PowerState::NONE && EngineInput["aim"].justPressed()) {
		switch (powerState) {
		case PowerState::VORTEX:
			powerState = PowerState::NONE;
			vortexPlayer->discardPower();
			break;
		case PowerState::TELEPORT:
			powerState = PowerState::NONE;
			teleportPlayer->discardPower();
			break;
		default:
			break;
		}
	}
	
	if (EngineInput["perceptionPower"].justPressed()) {
		if (powerState == PowerState::NONE) {
			TCompPerception * perception = getComponent<TCompPerception>();
			if(perception)
				perception->Use();
		}
	}
}

void TCompInputPlayer::onWeaponUpGrounded(float dt) {

	TCompTransform * playerTransform = getComponent<TCompTransform>();
	TCompPlayerCameraController * playerCamController = cameraControllerHandle; // Get the camera that follows the player.
	TCompTransform * cameraTransform = cameraTransformHandle; // Get the camera's transform.
	TCompCamera * camera = cameraHandle;
	if (!playerTransform || !cameraTransform) return;

	TCompWeaponManager * weaponManager = getComponent<TCompWeaponManager>(); // Get the weapon manager.
	TCompWeaponsController* wp_contr = weaponManager->getCurrentWeapon(); // Also get the current weapon.
	TCompVortexPlayer* vortexPlayer = getComponent<TCompVortexPlayer>();
	TCompTeleportPlayer * teleportPlayer = getComponent<TCompTeleportPlayer>();
	TCompScythePlayer* scythePlayer = getComponent<TCompScythePlayer>();

	VEC3 playerPosition = playerTransform->getPosition();
	VEC3 playerFront = playerTransform->getFront();

	currentTimeToMoveFreely -= dt;
	if (currentTimeToMoveFreely <= 0) {
		ChangeState("Normal");
		currentTimeToMoveFreely = timeToMoveFreely;
	}

	// Movement
	bool moving = false, wantToSprint = false;
	VEC3 movementDelta;
	if (EngineInput["forward"]) {
		// Only sprint if moving forward.
		if (EngineInput["sprint"])
			wantToSprint = true;

		movementDelta += VEC3::Forward;
		moving = true;
	}
	if (EngineInput["backwards"]) {
		movementDelta -= VEC3::Forward;
		moving = true;
	}
	if (EngineInput["left"]) {
		movementDelta += VEC3::Left;
		moving = true;
	}
	if (EngineInput["right"]) {
		movementDelta -= VEC3::Left;
		moving = true;
	}

	movementDelta.Normalize();

	if (moving)
		playerController->rotateAndMoveInDir(cameraTransform, movementDelta * playerController->getPlayerSpeedNormal(), playerController->getRotationSpeedNormal(), dt);
	else
		playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedAiming(), dt);

	bool facingCamera = isFacingCamera(playerTransform, cameraTransform, facingCameraThreshold);

	/* Weapons */
	if (EngineInput["shoot"] && powerState == PowerState::NONE) {
		playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedHipShooting(), dt);
		currentTimeToMoveFreely = timeToMoveFreely;

		// Once we are facing, shoot.
		if (facingCamera)
			wp_contr->shoot();
	}

	// Small hack to simulate time before we can sprint again. This should consider animation blending.
	if (currentTimeToStartSprintingAgain >= 0.0f)
		currentTimeToStartSprintingAgain -= dt;

	// Here we check if we must sprint.
	if (wantToSprint) {
		if (facingCamera && currentTimeToStartSprintingAgain <= 0.0f) { // If we are already facing the sprinting direction, sprint.
			ChangeState("Sprint");
			playerCamController->ChangeState("Sprint");
			return;
		}
	}

	if (EngineInput["aim"].justPressed() && powerState == PowerState::NONE) {
		ChangeState("Aim");
		playerCamController->ChangeState("Aim");
		currentTimeToMoveFreely = timeToMoveFreely;
		return;
	}

	if (EngineInput["reload"].justPressed()) {
		wp_contr->reload();
	}
	if (EngineInput["weapon1"].justPressed()) {
		weaponManager->setActiveWeaponSlot(0);
	}
	if (EngineInput["weapon2"].justPressed()) {
		weaponManager->setActiveWeaponSlot(1);
	}
	if (EngineInput["weapon3"].justPressed()) {
		weaponManager->setActiveWeaponSlot(2);
	}
	if (EngineInput["weapon4"].justPressed()) {
		TCompHolyWater * holy_water = getComponent<TCompHolyWater>();
		holy_water->use();
	}

	//TODO the mouse device always have a 0 wheel delta, windows mouse input doesn't send the correct values for the mouse wheel...?
	if (EngineInput.mouse()._wheelDelta > 0.f) {
		weaponManager->changeNextWeapon();
	}

	//TODO the mouse device always have a 0 wheel delta, windows mouse input doesn't send the correct values for the mouse wheel...?
	if (EngineInput.mouse()._wheelDelta < 0.f) {
		weaponManager->changePrevWeapon();
	}

	// Powers
	if (EngineInput["scythePower"].justPressed()) {
		// Once we are facing, use power.
		if (facingCamera && powerState == PowerState::NONE) {
			scythePlayer->activatePower();
		}
	}
	if (EngineInput["healPower"].justPressed()) {
		TCompHealing* healingPower = getComponent<TCompHealing>();
		assert(healingPower);
		healingPower->heal();
	}
	if (EngineInput["interact"].justPressed()) {
		playerController->interact(dt);
	}
	if (EngineInput["vortexPower"].justPressed()) {
		// If we are not already moving in the direction of the camera. i.e: We are idle.
		if (moving == false) {
			// If not facing the camera, rotate.
			if (!facingCamera)
				playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedHipShooting(), dt);
		}
		// Once we are facing, use power.
		if (facingCamera && powerState == PowerState::NONE) {
			powerState = vortexPlayer->startPower(cameraTransform) ? PowerState::VORTEX : PowerState::NONE;
		}
	}
	if (EngineInput["teleportPower"].justPressed()) {
		// Once we are facing, use power.
		if (facingCamera && powerState == PowerState::NONE) {
			powerState = teleportPlayer->startPower(cameraTransform) ? PowerState::TELEPORT : PowerState::NONE;
		}
	}
	if (powerState != PowerState::NONE && EngineInput["shoot"].justPressed()) {
		switch (powerState) {
		case PowerState::VORTEX:
			powerState = PowerState::NONE;
			vortexPlayer->activatePower();
			break;
		case PowerState::TELEPORT:
			powerState = PowerState::NONE;
			teleportPlayer->activatePower();
			break;
		default:
			break;
		}
	}
	if (powerState != PowerState::NONE && EngineInput["aim"].justPressed()) {
		switch (powerState) {
		case PowerState::VORTEX:
			powerState = PowerState::NONE;
			vortexPlayer->discardPower();
			break;
		case PowerState::TELEPORT:
			powerState = PowerState::NONE;
			teleportPlayer->discardPower();
			break;
		default:
			break;
		}
	}
	if (EngineInput["perceptionPower"].justPressed()) {
		if (powerState == PowerState::NONE) {
			TCompPerception * perception = getComponent<TCompPerception>();
			if (perception)
				perception->Use();
		}
	}

	//!** Esto lo pondr�a en otro lado, un Input Menu Controller.
	// Other 
	if (EngineInput["pause"]) {
		// TODO: pauseMenu
		//Engine.getModuleManager().changeToGamestate("pause");
	}
}

void TCompInputPlayer::onAimingGrounded(float dt) {

	TCompTransform * playerTransform = getComponent<TCompTransform>();
	TCompPlayerCameraController * playerCamController = cameraControllerHandle; // Get the camera that follows the player.
	TCompTransform * cameraTransform = cameraTransformHandle; // Get the camera's transform.
	TCompCamera * camera = cameraHandle;
	if (!playerTransform || !cameraTransform) return;

	TCompWeaponManager * weaponManager = getComponent<TCompWeaponManager>(); // Get the weapon manager.
	TCompWeaponsController* wp_contr = weaponManager->getCurrentWeapon(); // Also get the current weapon.
	TCompVortexPlayer* vortexPlayer = getComponent<TCompVortexPlayer>();
	TCompTeleportPlayer * teleportPlayer = getComponent<TCompTeleportPlayer>();
	TCompScythePlayer* scythePlayer = getComponent<TCompScythePlayer>();

	VEC3 playerPosition = playerTransform->getPosition();
	VEC3 playerFront = playerTransform->getFront();

	// Movement
	bool moving = false, wantToSprint = false;
	VEC3 movementDelta;
	if (EngineInput["forward"]) {
		// Only sprint if moving forward.
		if (EngineInput["sprint"])
			wantToSprint = true;

		movementDelta += VEC3::Forward;
		moving = true;
	}
	if (EngineInput["backwards"]) {
		movementDelta -= VEC3::Forward;
		moving = true;
	}
	if (EngineInput["left"]) {
		movementDelta += VEC3::Left;
		moving = true;
	}
	if (EngineInput["right"]) {
		movementDelta -= VEC3::Left;
		moving = true;
	}

	movementDelta.Normalize();

	if (moving)
		playerController->rotateAndMoveInDir(cameraTransform, movementDelta * playerController->getPlayerSpeedAiming(), playerController->getRotationSpeedAiming(), dt);
	else
		playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedAiming(), dt);

	/* Weapons */
	if (EngineInput["shoot"] && powerState == PowerState::NONE) {
		wp_contr->shoot();
	}

	// Small hack to simulate sprinting animation.
	if (currentTimeToStartSprintingAgain >= 0.0f)
		currentTimeToStartSprintingAgain -= dt;

	bool facingCamera = isFacingCamera(playerTransform, cameraTransform, facingCameraThreshold);
	// Here we check if we must sprint.
	if (wantToSprint && !EngineInput["aim"].isPressed()) {
		if (facingCamera && currentTimeToStartSprintingAgain < 0.0f) { // If we are already facing the sprinting direction, sprint.
			ChangeState("Sprint");
			playerCamController->ChangeState("Sprint");
			return;
		}
	}

	if (powerState != PowerState::NONE && EngineInput["shoot"].justPressed()) {
		switch (powerState) {
		case PowerState::VORTEX:
			powerState = PowerState::NONE;
			vortexPlayer->activatePower();
			break;
		case PowerState::TELEPORT:
			powerState = PowerState::NONE;
			teleportPlayer->activatePower();
			break;
		default:
			break;
		}
	}
	if (powerState != PowerState::NONE && EngineInput["aim"].justPressed()) {
		switch (powerState) {
		case PowerState::VORTEX:
			powerState = PowerState::NONE;
			vortexPlayer->discardPower();
			break;
		case PowerState::TELEPORT:
			powerState = PowerState::NONE;
			teleportPlayer->discardPower();
			break;
		default:
			break;
		}
	}
	if (EngineInput["aim"].justReleased()) {
		ChangeState("WeaponUp");
		playerCamController->ChangeState("Normal");
		return;
	}

	if (EngineInput["reload"].justPressed()) {
		wp_contr->reload();
	}
	if (EngineInput["weapon1"].justPressed()) {
		weaponManager->setActiveWeaponSlot(0);
	}
	if (EngineInput["weapon2"].justPressed()) {
		weaponManager->setActiveWeaponSlot(1);
	}
	if (EngineInput["weapon3"].justPressed()) {
		weaponManager->setActiveWeaponSlot(2);
	}
	if (EngineInput["weapon4"].justPressed()) {
		TCompHolyWater * holy_water = getComponent<TCompHolyWater>();
		holy_water->use();
	}

	// Powers
	if (EngineInput["scythePower"].justPressed()) {
		// If we are not already moving in the direction of the camera. i.e: We are idle.
		if (moving == false) {
			// If not facing the camera, rotate.
			if (!facingCamera)
				playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedHipShooting(), dt);
		}

		// Once we are facing, use power.
		if (facingCamera && powerState == PowerState::NONE) {
			scythePlayer->activatePower();
		}
	}
	if (EngineInput["healPower"].justPressed()) {
		TCompHealing* healingPower = getComponent<TCompHealing>();
		assert(healingPower);
		healingPower->heal();
	}
	if (EngineInput["interact"].justPressed()) {
		playerController->interact(dt);
	}
	if (EngineInput["vortexPower"].justPressed()) {
		// If we are not already moving in the direction of the camera. i.e: We are idle.
		if (moving == false) {
			// If not facing the camera, rotate.
			if (!facingCamera)
				playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedHipShooting(), dt);
		}
		// Once we are facing, use power.
		if (facingCamera && powerState == PowerState::NONE) {
			powerState = vortexPlayer->startPower(cameraTransform) ? PowerState::VORTEX : PowerState::NONE;
		}
	}
	if (EngineInput["teleportPower"].justPressed()) {
		// If we are not already moving in the direction of the camera. i.e: We are idle.
		if (moving == false) {
			// If not facing the camera, rotate.
			if (!facingCamera)
				playerController->rotateInDir(cameraTransform, playerController->getRotationSpeedHipShooting(), dt);
		}
		// Once we are facing, use power.
		if (facingCamera && powerState == PowerState::NONE) {
			powerState = teleportPlayer->startPower(cameraTransform) ? PowerState::TELEPORT : PowerState::NONE;
		}
	}
	if (EngineInput["perceptionPower"].justPressed()) {
		if (powerState == PowerState::NONE) {
			TCompPerception * perception = getComponent<TCompPerception>();
			perception->Use();
		}
	}
}

void TCompInputPlayer::OnSprintGrounded(float dt) {

	TCompTransform * cameraTransform = cameraTransformHandle;
	TCompTransform * playerTransform = getComponent<TCompTransform>();
	TCompCamera * camera = cameraHandle;
	TCompPlayerCameraController * playerCamController = cameraControllerHandle;
	if (!playerTransform || !cameraTransform) return;

	VEC3 playerPosition = playerTransform->getPosition();
	VEC3 playerFront = playerTransform->getFront();

	// Movement
	bool moving = false, wantToSprint = false;
	VEC3 movementDelta;
	if (EngineInput["forward"] || currentTimeBeforeSprintingEnds >= 0.0f) {
		// Only sprint if moving forward. Once animations are done,
		// we should check if the value is true at the end of the sprinting.
		if (EngineInput["sprint"]) {
			wantToSprint = true;
			currentTimeBeforeSprintingEnds = timeBeforeSprintingEnds;
		}

		movementDelta += VEC3::Forward;
		moving = true;
	}


	// Right now, to stop sprinting, one must stop pressing the key, in the future we may want to stop it
	// by pressing any of the other keys.
	/*
	if (EngineInput["shoot"]) {
		currentTimeToStartSprintingAgain = timeToStartSprintingAgain;
		ChangeState("Normal");
		playerCamController->ChangeState("Normal");
		return;
	}

	if (EngineInput["aim"].justPressed()) {
		currentTimeToStartSprintingAgain = timeToStartSprintingAgain;
		ChangeState("Aim");
		playerCamController->ChangeState("Aim");
		return;
	}

	// Powers
	if (EngineInput["scythePower"] || EngineInput["healPower"] || EngineInput["interact"]
			|| EngineInput["vortexPower"] || EngineInput["teleportPower"]) {
		currentTimeToStartSprintingAgain = timeToStartSprintingAgain;
		ChangeState("Normal");
		playerCamController->ChangeState("Normal");
	}

	if (EngineInput["perceptionPower"]) {
		// TODO: get perceptionPower controller and execute
	}
	*/

	currentTimeBeforeSprintingEnds -= dt;
	if (!wantToSprint && currentTimeBeforeSprintingEnds < 0.0f) { // This controls for both, not moving and not sprinting but moving.
		currentTimeToStartSprintingAgain = timeToStartSprintingAgain;
		ChangeState(lastState);
		playerCamController->ChangeState("Normal");
		return;
	}
	playerController->rotateAndMoveInDir(cameraTransform, movementDelta * playerController->getPlayerSpeedSprinting(), playerController->getRotationSpeedSprinting() , dt);
}

void TCompInputPlayer::ChangeToDeadState(const TMsgEntityDead & msg) {
	ChangeState("Dead");

	// TODO: Make this dead state well done once animations and all the graphic assets are ready.	
	// Send dead message to player.
	CEntity * ent = getEntityByName("HUD");
	if (ent) {
		TMsgLoseMessageUI msgToUI = { true };
		ent->sendMsg(msgToUI); // Debe mostrarse el mensaje de muerte del player.
	}

	// Set normal camera.
	TCompPlayerCameraController * playerCamController = cameraControllerHandle;
	if (playerCamController)
		playerCamController->ChangeState("Dead");

	// Make player stay on the ground.
	TCompTransform * trans = getComponent<TCompTransform>();
	if(trans)
		trans->setRotation(Quaternion::CreateFromAxisAngle(Vector3(1, 0, 0), DEG2RAD(-90.f)));

	// Paint player red.
	TCompRender * render = getComponent<TCompRender>();
	if (render) {
		VEC4 renderColour = render->getColor();
		if (renderColour != Vector4(1, 0, 0, 1))
			aliveColour = renderColour;
		render->setColor(Vector4(1, 0, 0, 1));
	}
}

void TCompInputPlayer::OnDead(float dt) {}

// Used during this milestone to resurrect player on dead. Remove once finished.
void TCompInputPlayer::OnResurrection(const TMsgFullRestore & msg) {
	if (state == "Dead")
		ChangeState("Normal");

	// Set normal camera.
	TCompPlayerCameraController * playerCamController = cameraControllerHandle;
	if (playerCamController)
		playerCamController->ChangeState("Normal");

	// Make player stay normal
	TCompTransform * trans = getComponent<TCompTransform>();
	trans->setRotation(Quaternion::CreateFromAxisAngle(Vector3(1, 0, 0), DEG2RAD(0.f)));

	// Paint player normal.
	TCompRender * render = getComponent<TCompRender>();
	render->setColor(aliveColour);
}