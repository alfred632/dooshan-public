#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "modules/module_physics.h"
#include "components/arena/comp_spawn.h"

typedef std::string EnemyType;

class TCompArena : public TCompBase {
	DECL_SIBLING_ACCESS();

	struct EnemiesToSpawnOfGivenType {
		EnemyType TypeOfEnemy; // Type of enemy for easy identification.
		int NumberOfEnemiesToSpawn;

		void load(const json & j);
		void debugInMenu();
	};

	struct ArenaWave {
		int MinNumberOfEnemiesAliveDuringThisWave = 0;
		int MaxNumberOfEnemiesAliveDuringThisWave = 0;
		float TimeBetweenSpawns = 0.0f;
		std::vector<EnemiesToSpawnOfGivenType> EnemiesToSpawn;

		bool StartNextWaveAfterSpawningAllEnemies = false; // If true, the next wave starts after this one has spawned all enemies. Otherwise, all enemies in the arena need to die.
		float TimeForNextWave = 0.0f; // Time before the next wave starts.

		void load(const json & j);
		void debugInMenu();
		bool SpawnEnemy(std::map< EnemyType, std::vector<CHandle>> & freeSpawnsForEnemyType, float & TimeBeforeSpawningNextEnemy); // True if could spawn an enemy, false if it wasn't able.
		bool SpawnEnemyAtRandomSpawn(EnemyType & enemyType, std::map< EnemyType, std::vector<CHandle>> & freeSpawnsForEnemyType); // True if spawned, false if not a single spawn was found.
		bool CheckIfWaveEnded(int NumberOfEnemies) {
			return EnemiesToSpawn.size() == 0 && (StartNextWaveAfterSpawningAllEnemies || NumberOfEnemies == 0);
		}
	};

	json JArenaConfig;

	bool ActivateFromScript = false;

	/* Waves. */
	int CurrentWave = 0;
	std::vector<ArenaWave> ArenaWaves;

	/* Spawn variables. */
	std::vector<std::string> spawnNames;
	std::vector<CHandle> spawnHandles;
	std::map< EnemyType, std::vector<CHandle>> freeSpawnsForEnemyType;

	// Doors.
	std::vector<std::string> doorsToOpenNames;
	std::vector<CHandle> doorsToOpen;
	std::vector<std::string> doorsToCloseNames;
	std::vector<CHandle> doorsToClose;

	// Script on arena end.
	std::string ScriptToCallOnArenaEnd; // Call this script.

	// Enemies alive.
	std::vector<CHandle> Enemies;
	float timeBeforeNextSpawn = 0.0f;
	float TimeBeforeNextWave = 0.0f;

	// Says if the arena is active. If so, starts spawning
	// enemies. By default the player activates the arena on enters.
	// and deactivates it once all enemies are dead.
	bool active = false;

	/* Callbacks. */
	void onSceneCreated(const TMsgSceneCreated & msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter & msg);
	void onSpawnStatus(const TMsgSpawnStatus & msg);
	void onSpawnCreatedEntity(const TMsgSpawnEntityCreated & msg);

public:
	void update(float dt);
	void load(const json & j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();
	static void declareInLua();

	void StartArena();
	void EndArena();
	void ResetArena();

private:
	/* Start the arena. */
	void FetchDoors();
	void FetchSpawns();
	void KillAllSpawns();

	/* Ends the arena. Removes doors and spawns. */
	bool CheckIfEndArena();



	void CheckIfAnyEnemiesAreDead();
	/* Used for removing enemies when they die. */
	// We need to check each update because certain types of entities
	// don't get removed when they die. So we can't check with onTriggerExit or similar.
	bool CheckIfEnemyDead(CEntity * e);

	/* Spawn functions. */ 
	void SpawnEnemy();
	void StartNextWave(float nTimeBeforeNextWave);
	bool CheckIfWaveIsReady();

	/* Wave. */
	int GetCurrentNumberOfEnemies() { return Enemies.size(); }
	int GetCurrentWave() { return CurrentWave + 1; }
	int GetMaximumNumberOfWaves() { return ArenaWaves.size(); }
};