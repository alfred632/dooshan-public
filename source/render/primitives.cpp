#include "mcv_platform.h"
#include "primitives.h"
#include "render/textures/texture.h"
#include "engine.h"

struct SimpleVertex
{
	VEC3 Pos;
	Vector4 Color;
	SimpleVertex() = default;
  SimpleVertex(VEC3 newPos, Vector4 newColor) : Pos(newPos), Color(newColor) {}
};

struct VertexUVColor
{
  VEC3 Pos;
  VEC2 UV;
  VEC4 Color;
  VertexUVColor() = default;
  VertexUVColor(VEC3 newPos, VEC2 newUV, VEC4 newColor) : Pos(newPos), UV(newUV), Color(newColor) {}
};

// cached pointers
const CTechnique*  tech_debug_color = nullptr;
const CTechnique*  tech_debug_pos = nullptr;
const CTechnique*  tech_debug_pos_skin = nullptr;
const CTechnique*  tech_debug_pos_instanced = nullptr;
const CTechnique*  tech_draw_line = nullptr;
const CVertexDeclaration* vdecl_pos_color = nullptr;

// 
CCteBuffer<TCtesCamera> 		ctes_camera(CTE_BUFFER_SLOT_CAMERAS);
CCteBuffer<TCtesShared> 		ctes_shared(CTE_BUFFER_SLOT_SHARED);
CCteBuffer<TCtesObject> 		ctes_object(CTE_BUFFER_SLOT_OBJECT);
CCteBuffer<TCtesDebugLine> 	ctes_debug_line(CTE_BUFFER_SLOT_DEBUG_LINE);
CCteBuffer<TCtesLight>  		ctes_light(CTE_BUFFER_SLOT_LIGHT);
CCteBuffer<TCtesBlur>   		ctes_blur(CTE_BUFFER_SLOT_BLUR);
CCteBuffer<TCtesUI>     		ctes_ui(CTE_BUFFER_SLOT_UI);

bool createAxis(CMesh& mesh) {
	SimpleVertex axis_data[] =
	{
		{ VEC3(0.0f,  0.0f, 0.0f), Vector4(1,0,0,1)},
		{ VEC3(1.0f,  0.0f, 0.0f), Vector4(1,0,0,1)},
		{ VEC3(0.0f,  0.0f, 0.0f), Vector4(0,1,0,1)},
		{ VEC3(0.0f,  2.0f, 0.0f), Vector4(0,1,0,1)},
		{ VEC3(0.0f,  0.0f, 0.0f), Vector4(0,0,1,1)},
		{ VEC3(0.0f,  0.0f, 3.0f), Vector4(0,0,1,1)},
	};
	
	return mesh.create(axis_data, 6, sizeof(SimpleVertex), "PosColor", nullptr, 0, 0, CMesh::LINE_LIST);
}

bool createGrid(CMesh& mesh, int samples) {
	std::vector< SimpleVertex > vtxs;
	Vector4 color1(0.2f, 0.2f, 0.2f, 1.0f);
	Vector4 color2(0.3f, 0.3f, 0.3f, 1.0f);
	for (int i = -samples; i <= samples; ++i) {
		Vector4 color = (i % 5) ? color1 : color2;
		SimpleVertex v1 = { VEC3((float)i, 0.0f, (float)-samples), color };
		SimpleVertex v2 = { VEC3((float)i, 0.0f, (float)samples), color };
		vtxs.push_back(v1);
		vtxs.push_back(v2);
		SimpleVertex v3 = { VEC3((float)-samples, 0.0f, (float)i), color };
		SimpleVertex v4 = { VEC3((float)samples, 0.0f, (float)i), color };
		vtxs.push_back(v3);
		vtxs.push_back(v4);
	}
	return mesh.create(vtxs.data(), (uint32_t)vtxs.size(), sizeof(SimpleVertex), "PosColor", nullptr, 0, 0, CMesh::LINE_LIST);
}

bool createCube(CMesh & mesh, const Vector4 & baseCubeColor = Vector4(1, 0, 0, 1)) {
	SimpleVertex cube_data[] =
	{
		{ VEC3(-0.5f, -0.5f, -0.5f), baseCubeColor },
		{ VEC3(0.5f, -0.5f, -0.5f),	baseCubeColor },
		{ VEC3(0.5f,  0.5f, -0.5f),  baseCubeColor },
		{ VEC3(0.5f,  0.5f, -0.5f),  baseCubeColor },
		{ VEC3(-0.5f,  0.5f, -0.5f),  baseCubeColor },
		{ VEC3(-0.5f,  -0.5f, -0.5f),  baseCubeColor },

		{ VEC3(-0.5f, -0.5f,  0.5f), baseCubeColor },
		{ VEC3(0.5f,  0.5f,  0.5f),  baseCubeColor },
		{ VEC3(0.5f, -0.5f,  0.5f),  baseCubeColor },
		{ VEC3(-0.5f,  0.5f,  0.5f), baseCubeColor },
		{ VEC3(0.5f,  0.5f,  0.5f),  baseCubeColor },
		{ VEC3(-0.5f, -0.5f,  0.5f), baseCubeColor },

		{ VEC3(-0.5f,  0.5f, -0.5f), baseCubeColor },
		{ VEC3(-0.5f,  0.5f,  0.5f), baseCubeColor },
		{ VEC3(-0.5f, -0.5f, -0.5f), baseCubeColor },
		{ VEC3(-0.5f, -0.5f,  0.5f), baseCubeColor },
		{ VEC3(-0.5f, -0.5f, -0.5f), baseCubeColor },
		{ VEC3(-0.5f,  0.5f,  0.5f), baseCubeColor },

		{ VEC3(0.5f,  0.5f,  0.5f), baseCubeColor },
		{ VEC3(0.5f,  0.5f, -0.5f), baseCubeColor },
		{ VEC3(0.5f, -0.5f, -0.5f), baseCubeColor },
		{ VEC3(0.5f, -0.5f, -0.5f), baseCubeColor },
		{ VEC3(0.5f, -0.5f,  0.5f), baseCubeColor },
		{ VEC3(0.5f,  0.5f,  0.5f), baseCubeColor },

		{ VEC3(-0.5f, -0.5f, -0.5f), baseCubeColor },
		{ VEC3(0.5f, -0.5f,  0.5f),  baseCubeColor },
		{ VEC3(0.5f, -0.5f, -0.5f),  baseCubeColor },
		{ VEC3(0.5f, -0.5f,  0.5f),  baseCubeColor },
		{ VEC3(-0.5f, -0.5f, -0.5f), baseCubeColor },
		{ VEC3(-0.5f, -0.5f,  0.5f), baseCubeColor },

		{ VEC3(-0.5f,  0.5f, -0.5f),  baseCubeColor },
		{ VEC3(0.5f,  0.5f, -0.5f),  baseCubeColor },
		{ VEC3(0.5f,  0.5f,  0.5f),  baseCubeColor },
		{ VEC3(0.5f,  0.5f,  0.5f),  baseCubeColor },
		{ VEC3(-0.5f,  0.5f,  0.5f),  baseCubeColor },
		{ VEC3(-0.5f,  0.5f, -0.5f),  baseCubeColor },
	};
	return mesh.create(cube_data, 36, sizeof(SimpleVertex), "PosColor", nullptr, 0, 0, CMesh::LINE_LIST);
}

bool createCircleXZ(CMesh& mesh, int samples ) {
  std::vector< SimpleVertex > vtxs;
  vtxs.reserve(samples);
  for (int i = 0; i <= samples; ++i) {
    float angle = 2 * (float) M_PI * (float) i / (float) samples;
    SimpleVertex v1 = { yawToVector(angle), Vector4(1,1,1,1) };
    vtxs.push_back(v1);
  }
  return mesh.create(vtxs.data(), (uint32_t)vtxs.size(), sizeof(SimpleVertex), "PosColor", nullptr, 0, 0, CMesh::LINE_STRIP);
}

void addFace(std::vector<uint16_t>& idxs, int a, int b, int c, int d) {
  idxs.push_back(a);
  idxs.push_back(c);
  idxs.push_back(b);
  idxs.push_back(a);
  idxs.push_back(d);
  idxs.push_back(c);
}

bool createCameraFrustumSolid(CMesh& mesh) {
  std::vector<SimpleVertex> vtxs;
  VEC4 clr(1, 1, 1, 1);
  vtxs.emplace_back(VEC3(-1, -1, 0), clr);
  vtxs.emplace_back(VEC3(1, -1, 0), clr);
  vtxs.emplace_back(VEC3(-1, -1, 1), clr);
  vtxs.emplace_back(VEC3(1, -1, 1), clr);
  vtxs.emplace_back(VEC3(-1, 1, 0), clr);
  vtxs.emplace_back(VEC3(1, 1, 0), clr);
  vtxs.emplace_back(VEC3(-1, 1, 1), clr);
  vtxs.emplace_back(VEC3(1, 1, 1), clr);

  std::vector<uint16_t> idxs;
  addFace(idxs, 5, 1, 3, 7);
  addFace(idxs, 1, 0, 2, 3);
  addFace(idxs, 0, 4, 6, 2);
  addFace(idxs, 4, 5, 7, 6);
  addFace(idxs, 0, 1, 5, 4);
  addFace(idxs, 7, 3, 2, 6);

  return mesh.create(vtxs.data(), (uint32_t)vtxs.size(), sizeof(SimpleVertex), "PosColor"
    , idxs.data(), (uint32_t)idxs.size(), sizeof(uint16_t), CMesh::TRIANGLE_LIST);
}

  bool createCameraFrustum(CMesh& mesh) {
  std::vector<SimpleVertex> vtxs;
  Vector4 clr(1, 1, 1, 1);
  vtxs.emplace_back(VEC3(-1, -1, 0), clr);
  vtxs.emplace_back(VEC3(1, -1, 0), clr);
  vtxs.emplace_back(VEC3(-1, -1, 1), clr);
  vtxs.emplace_back(VEC3(1, -1, 1), clr);
  vtxs.emplace_back(VEC3(-1, 1, 0), clr);
  vtxs.emplace_back(VEC3(1, 1, 0), clr);
  vtxs.emplace_back(VEC3(-1, 1, 1), clr);
  vtxs.emplace_back(VEC3(1, 1, 1), clr);

  std::vector<uint16_t> idxs;
  for (int i = 0; i < 4; ++i) {
    // Lines along +x
    idxs.push_back(i * 2);
    idxs.push_back(i * 2 + 1);
    // Vertical lines
    idxs.push_back(i);
    idxs.push_back(i + 4);
  }
  idxs.push_back(0);
  idxs.push_back(2);
  idxs.push_back(1);
  idxs.push_back(3);
  idxs.push_back(4);
  idxs.push_back(6);
  idxs.push_back(5);
  idxs.push_back(7);
  return mesh.create(vtxs.data(), (uint32_t)vtxs.size(), sizeof(SimpleVertex), "PosColor"
    , idxs.data(), (uint32_t) idxs.size(), sizeof(uint16_t), CMesh::LINE_LIST);
}

// ----------------------------------
// To render wired AABB's
bool createWiredUnitCube(CMesh& mesh) {
  std::vector<SimpleVertex> vtxs =
  {
    { VEC3(-0.5f, -0.5f, -0.5f),  Vector4(1, 1, 1, 1) },    // 
    { VEC3(0.5f, -0.5f, -0.5f),   Vector4(1, 1, 1, 1) },
    { VEC3(-0.5f, 0.5f, -0.5f),  Vector4(1, 1, 1, 1) },
    { VEC3(0.5f, 0.5f, -0.5f),   Vector4(1, 1, 1, 1) },    // 
    { VEC3(-0.5f, -0.5f, 0.5f),   Vector4(1, 1, 1, 1) },    // 
    { VEC3(0.5f, -0.5f, 0.5f),    Vector4(1, 1, 1, 1) },
    { VEC3(-0.5f, 0.5f, 0.5f),   Vector4(1, 1, 1, 1) },
    { VEC3(0.5f, 0.5f, 0.5f),    Vector4(1, 1, 1, 1) },    // 
  };
  const std::vector<uint16_t> idxs = {
    0, 1, 2, 3, 4, 5, 6, 7
    , 0, 2, 1, 3, 4, 6, 5, 7
    , 0, 4, 1, 5, 2, 6, 3, 7
  };
  const int nindices = 8 * 3;
  return mesh.create(vtxs.data(), (uint32_t)vtxs.size(), sizeof(SimpleVertex), "PosColor"
    , idxs.data(), nindices, sizeof(uint16_t), CMesh::LINE_LIST);
}

bool createHorizontalCone(CMesh & mesh, float angle, float depth, int samples) {
	std::vector< SimpleVertex > vtxs;
	Vector4 baseColor = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	VEC3 samplePoint;
	VEC3 origin(0.0f, 0.0f, 0.0f);

	vtxs.reserve(samples + 2);

	SimpleVertex vert = { origin, baseColor };
	vtxs.push_back(vert);
	for (int curSample = 0; curSample < samples; curSample++) {
		samplePoint = VEC3(
			cosf(2 * angle * (((samples - 1) / 2.0f) - curSample) / (samples - 1)),
			0.0f,
			sinf(2 * angle * (((samples - 1) / 2.0f) - curSample) / (samples - 1)))
			* depth;
		vert = { samplePoint, baseColor };
		vtxs.push_back(vert);
	}
	vert = { origin, baseColor };
	vtxs.push_back(vert);

	return mesh.create(vtxs.data(), (uint32_t)vtxs.size(), sizeof(SimpleVertex), "PosColor", nullptr, 0, 0, CMesh::LINE_STRIP);
}

bool createSphere(CMesh & mesh, int numVerticalCircles, int numHorizontalCircles, int samplesCircle) {
	samplesCircle = samplesCircle / 2;
	std::vector< SimpleVertex > vtxs;
	vtxs.reserve((numVerticalCircles + numHorizontalCircles) * samplesCircle);

	// Horizontal circle
	// Prevents circles where all points are in the same place (top or bot of the sphere).
	float startingPi = (float)M_PI/2.0f + (float)M_PI/8.0f;
	for (int x = 0; x < numHorizontalCircles; ++x) {
		float pitch = (float)M_PI * (float)(x)/(float)numHorizontalCircles + startingPi;
		for (int z = 0; z < samplesCircle; ++z) {
			float yaw = 2 * (float)M_PI * (float)z / (float)samplesCircle;
			float nextYaw = 2 * (float)M_PI * (float)(z + 1) / (float)samplesCircle;
			SimpleVertex v1 = { yawPitchToVector(yaw, pitch), Vector4(1,1,1,1) };
			SimpleVertex v2 = { yawPitchToVector(nextYaw, pitch), Vector4(1,1,1,1) };
			vtxs.push_back(v1);
			vtxs.push_back(v2);
		}
	}

	for (int x = 0; x < numVerticalCircles; ++x) {
		float yaw = (float)M_PI * (float)x / (float)numVerticalCircles;
		for (int z = 0; z < samplesCircle; ++z) {
			float pitch = 2 * (float)M_PI * (float)z / (float)samplesCircle;
			float nextPitch = 2 * (float)M_PI * (float)(z+1) / (float)samplesCircle;
			SimpleVertex v1 = { yawPitchToVector(yaw, pitch), Vector4(1,1,1,1) };
			SimpleVertex v2 = { yawPitchToVector(yaw, nextPitch), Vector4(1,1,1,1) };
			vtxs.push_back(v1);
			vtxs.push_back(v2);
		}
	}
	return mesh.create(vtxs.data(), (uint32_t)vtxs.size(), sizeof(SimpleVertex), "PosColor", nullptr, 0, 0, CMesh::LINE_LIST);
}


// ----------------------------------
bool createPlaneXYTextured(CMesh& mesh) {
  std::vector<VertexUVColor> vtxs =
  {
    { VEC3(0.0f, 0.0f, 0.0f),  VEC2(0, 0),  VEC4(1, 1, 1, 1) },    // 
    { VEC3(1.0f, 0.0f, 0.0f),  VEC2(1, 0),  VEC4(1, 1, 1, 1) },    // 
    { VEC3(0.0f, 1.0f, 0.0f),  VEC2(0, 1),  VEC4(1, 1, 1, 1) },    // 
    { VEC3(1.0f, 1.0f, 0.0f),  VEC2(1, 1),  VEC4(1, 1, 1, 1) },    // 
  };
  const std::vector<uint16_t> idxs = {
    0, 1, 2, 3
  };
  return mesh.create(vtxs.data(), (uint32_t)vtxs.size(), sizeof(VertexUVColor), 
    "PosUvColor"
    , idxs.data(), (uint32_t)idxs.size(), sizeof(uint16_t), CMesh::TRIANGLE_STRIP);
}

// Full screen quad to dump textures in screen
bool createScreen(CMesh& mesh) {
	const VEC4 white(1, 1, 1, 1);
	const std::vector<SimpleVertex> vtxs = {
		{ VEC3(0, 0, 0), white }
	, { VEC3(1, 0, 0), white }
	, { VEC3(0, 1, 0), white }
	, { VEC3(1, 1, 0), white }
	};
	return mesh.create(vtxs.data(), (uint32_t)vtxs.size(), sizeof(SimpleVertex),
										 "PosColor"
										 , nullptr, 0, 0, CMesh::TRIANGLE_STRIP);
}

// ---------------------------------------------
void drawFullScreenQuad(const std::string& tech_name, const CTexture* texture) {
	auto* tech = EngineResources.getResource(tech_name)->as<CTechnique>();
	assert(tech);
	tech->activate();
	if (texture)
		texture->activate(TS_ALBEDO);
	auto* mesh = EngineResources.getResource("unit_quad_xy.mesh")->as<CMesh>();
	mesh->activateAndRender();

	texture->setNullTexture(TS_ALBEDO);
}

void drawFullScreenQuad(const CTechnique * technique, CTexture* texture) {
	assert(technique);
	technique->activate();
	if (texture)
		texture->activate(TS_ALBEDO);
	auto* mesh = EngineResources.getResource("unit_quad_xy.mesh")->as<CMesh>();
	mesh->activateAndRender();

	texture->setNullTexture(TS_ALBEDO);
}

bool createUnitQuadXYCentered(CMesh& mesh) {
  const float min = -0.5f;
  const float max = 0.5f;
  std::vector<VertexUVColor> vtxs =
  {
    { VEC3(min, min, 0.0f),  VEC2(0, 0),  VEC4(1, 1, 1, 1) },    // 
    { VEC3(max, min, 0.0f),  VEC2(1, 0),  VEC4(1, 1, 1, 1) },    // 
    { VEC3(min, max, 0.0f),  VEC2(0, 1),  VEC4(1, 1, 1, 1) },    // 
    { VEC3(max, max, 0.0f),  VEC2(1, 1),  VEC4(1, 1, 1, 1) },    // 
  };
  const std::vector<uint16_t> idxs = {
    0, 1, 2, 3
  };
  return mesh.create(vtxs.data(), (uint32_t)vtxs.size(), sizeof(VertexUVColor),
    "PosUvColor"
    , idxs.data(), (uint32_t)idxs.size(), sizeof(uint16_t), CMesh::TRIANGLE_STRIP);
}

// The template wants a name, a funcion, and optional args
// The first arg of the function is a mesh, then the args.
template< typename fn, typename ...Args>
void registerMesh(const char* new_name, fn f, Args... args) {
	std::string profile_text = "RegisterMesh : " + std::string(new_name);
	PROFILE_FUNCTION(profile_text.c_str());
	CMesh* mesh = new CMesh();
	// Call the fn, first arg is the mesh
	bool is_ok = f(*mesh, args...);
	// Assign the .mesh to the name
	std::string name = std::string(new_name) + ".mesh";
	// Assign the name and class
	mesh->setNameAndType(name, getResourceTypeFor<CMesh>());
	// Save it in the Resources container
	EngineResources.registerResource(mesh);
}

bool createRenderPrimitives() {
	bool is_ok = true;

  registerMesh("axis", createAxis);
  registerMesh("grid", createGrid, 10);
  registerMesh("unit_plane_xy", createPlaneXYTextured);
  registerMesh("unit_circle_xz", createCircleXZ, 32);
  registerMesh("unit_frustum", createCameraFrustum);
  registerMesh("unit_frustum_solid", createCameraFrustumSolid);
  registerMesh("unit_wired_cube", createWiredUnitCube);
	registerMesh("unit_quad_xy", createScreen);
	registerMesh("unit_quad_xy_centered", createUnitQuadXYCentered);

  tech_debug_color = EngineResources.getResource("debug_color.tech")->as<CTechnique>();
  tech_debug_pos = EngineResources.getResource("debug_pos.tech")->as<CTechnique>();
  tech_debug_pos_skin = EngineResources.getResource("debug_pos_skin.tech")->as<CTechnique>();
  tech_debug_pos_instanced = EngineResources.getResource("debug_pos_instanced.tech")->as<CTechnique>();
  tech_draw_line = EngineResources.getResource("debug_line.tech")->as<CTechnique>();
  vdecl_pos_color = CVertexDeclarationManager::instance().getVertexDeclByName("PosColor");
  is_ok &= ctes_camera.create("Camera");
  is_ok &= ctes_shared.create("Shared");
  is_ok &= ctes_object.create("Object");
  is_ok &= ctes_light.create("Light");
  is_ok &= ctes_ui.create("UI");
  is_ok &= ctes_debug_line.create("DebugLine");
  is_ok &= ctes_blur.create("Blur");
	assert(is_ok);

  ctes_debug_line.activate();     // This could be done once per runtime
  ctes_object.activate();
  ctes_camera.activate();
  ctes_shared.activate();
  ctes_light.activate();
  ctes_blur.activate();

	return is_ok;
}

void destroyRenderPrimitives() {
  ctes_debug_line.destroy();     // This could be done once per runtime
  ctes_object.destroy();
  ctes_camera.destroy();
  ctes_shared.destroy();
  ctes_light.destroy();
  ctes_blur.destroy();
  ctes_ui.destroy();
}

void activateObject(const Matrix & world, const Vector4 color) {
	ctes_object.ObjColor = color;
	ctes_object.World = world;
	ctes_object.updateGPU();
}

void activateCamera(const CCamera& camera, int width, int height) {
	ctes_camera.Projection = camera.getProjection();
	ctes_camera.View = camera.getView();
	ctes_camera.ViewProjection = camera.getViewProjection();
	ctes_camera.InverseViewProjection = ctes_camera.ViewProjection.Invert();
	ctes_camera.CameraFront = camera.getFront();
	ctes_camera.CameraZFar = camera.getFar();
	ctes_camera.CameraPosition = camera.getPosition();
	ctes_camera.CameraZNear = camera.getNear();
	ctes_camera.CameraTanHalfFov = tanf(camera.getFov() * 0.5f);
	ctes_camera.CameraInvResolution = VEC2(1.0f / (float)width, 1.0f / (float)height);
	ctes_camera.CameraAspectRatio = camera.getAspectRatio();

	ctes_camera.CameraLeft = camera.getLeft();
	ctes_camera.CameraUp = camera.getUp();
	
	// Simplify conversion from screen coords to world coords 
	MAT44 m = MAT44::CreateScale(-ctes_camera.CameraInvResolution.x * 2.f, -ctes_camera.CameraInvResolution.y * 2.f, 1.f)
		* MAT44::CreateTranslation(1, 1, 0)
		* MAT44::CreateScale(ctes_camera.CameraTanHalfFov * ctes_camera.CameraAspectRatio, ctes_camera.CameraTanHalfFov, 1.f)
		* MAT44::CreateScale(ctes_camera.CameraZFar);

	// To avoid converting the range -1..1 to 0..1 in the shader
	// we concatenate the view_proj with a matrix to apply this offset
	MAT44 mtx_offset = MAT44::CreateScale(VEC3(0.5f, -0.5f, 1.0f)) * MAT44::CreateTranslation(VEC3(0.5f, 0.5f, 0.0f));
	ctes_camera.CameraProjWithOffset = camera.getProjection() * mtx_offset;

	// Now the transform local to world coords part
	// float3 wPos =
	//     CameraFront.xyz * view_dir.z
	//   + CameraLeft.xyz  * view_dir.x
	//   + CameraUp.xyz    * view_dir.y
	MAT44 mtx_axis = MAT44::Identity;
	mtx_axis.Forward(-camera.getFront());      // -getFront() because MAT44.Forward negates our input
	mtx_axis.Left(-camera.getLeft());
	mtx_axis.Up(camera.getUp());

	ctes_camera.ScreenResolutionVertical = height;
	ctes_camera.ScreenResolutionHorizontal = width;

	ctes_camera.CameraScreenToWorld = m * mtx_axis;
	ctes_camera.updateGPU();
	ctes_camera.activate();
}

void drawLine(const VEC3 & src, const VEC3 & dst, const Vector4 & color) {
	ctes_debug_line.DebugSrc = Vector4(src.x, src.y, src.z, 1.0f);
	ctes_debug_line.DebugDst = Vector4(dst.x, dst.y, dst.z, 1.0f);
	ctes_debug_line.DebugColor = color;
	ctes_debug_line.updateGPU();
	tech_draw_line->activate();
	Render.ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	Render.ctx->Draw(2, 0);
}

void activateDebugTech(const CMesh * mesh) {
	if (mesh->getVertexDecl() == vdecl_pos_color)
		tech_debug_color->activate();
	else if (mesh->getVertexDecl()->name == "PosNUvTanSkin")
		tech_debug_pos_skin->activate();
	else if (strncmp(mesh->getVertexDecl()->name.c_str(), "Instance", 8) == 0)
		tech_debug_pos_instanced->activate();
	else
		tech_debug_pos->activate();
}

void activateUICts()
{
	ctes_ui.activate();
}

void drawMesh(const CMesh* mesh, Matrix world, Vector4 color) {
  activateObject(world, color);
  activateDebugTech(mesh);
  mesh->activateAndRender();
}

void drawCircle(const VEC3 & center, float radius, const Vector4 & color) {
  Matrix world = Matrix::CreateScale(radius) * Matrix::CreateTranslation(center);
	const CMesh* mesh = EngineResources.getResource("unit_circle_xz.mesh")->as<CMesh>();
  drawMesh(mesh, world, color);
}

void drawCircle(const VEC3& center, float radius, Matrix world, const Vector4& color) {
	Matrix world_final = Matrix::CreateScale(radius)
		* Matrix::CreateTranslation(center)
		* world;
	const CMesh* mesh = EngineResources.getResource("unit_circle_xz.mesh")->as<CMesh>();
	drawMesh(mesh, world_final, color);
}

// ---------------------------------------------
void drawWiredAABB(VEC3 center, VEC3 half, Matrix world, Vector4 color) {
  // Accede a una mesh que esta centrada en el origen y
  // tiene 0.5 de half size
  Matrix unit_cube_to_aabb = Matrix::CreateScale(VEC3(half) * 2.f)
    * Matrix::CreateTranslation(center)
    * world;
  const CMesh* mesh = EngineResources.getResource("unit_wired_cube.mesh")->as<CMesh>();
  drawMesh(mesh, unit_cube_to_aabb, color);
}

void drawWiredSphere(Matrix user_world, float radius, Vector4 color) {
  Matrix world = Matrix::CreateScale(radius) * user_world;
	const CMesh* mesh = EngineResources.getResource("unit_circle_xz.mesh")->as<CMesh>();

  //Draw 3 circleXZ 
  drawMesh(mesh, world, color);
  drawMesh(mesh, Matrix::CreateRotationX((float)M_PI_2) * world, color);
  drawMesh(mesh, Matrix::CreateRotationZ((float)M_PI_2) * world, color);
}

void drawWiredSphere(VEC3 center, float radius, Vector4 color) {
  drawWiredSphere(Matrix::CreateTranslation(center), radius, color);
}

void drawHorizontalCone(VEC3 & origin, float originRotation, float angle, float depth, int samples, const Vector4 & color) {
	CMesh cone;
	if (createHorizontalCone(cone, angle, depth, samples)) {
		activateObject(Matrix::CreateRotationY(originRotation - (float)M_PI_2) * Matrix::CreateTranslation(origin), color);
		tech_debug_color->activate();
		cone.activateAndRender();
	}
	cone.destroy();
}

void drawAxis(const Matrix & world) {
  const CMesh* mesh = EngineResources.getResource("axis.mesh")->as<CMesh>();
  drawMesh(mesh, world, Vector4(1,1,1,1));
}
