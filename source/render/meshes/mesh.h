#pragma once

#include "resources/resource.h"
#include "mesh_group.h"

struct CVertexDeclaration;

class CMesh : public IResource {

public:

  enum eTopology {
    UNDEFINED = D3D_PRIMITIVE_TOPOLOGY_UNDEFINED,
    TRIANGLE_LIST = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
    LINE_LIST = D3D11_PRIMITIVE_TOPOLOGY_LINELIST,
    LINE_STRIP = D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,
    TRIANGLE_STRIP = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,
    POINT_LIST = D3D10_PRIMITIVE_TOPOLOGY_POINTLIST
  };

protected:

  ID3D11Buffer* vb = nullptr; // Pointer to vertex buffer in GPU where the vertices are allocated.
  ID3D11Buffer* ib = nullptr; // Pointer to index buffer in GPU where the indices are allocated.
  uint32_t      num_vertices = 0; // Number of vertices the mesh has.
  uint32_t      num_indices = 0; // Number of indices the mesh has.
  uint32_t      bytes_per_vertice = 0; // Number of bytes per vertice.
  DXGI_FORMAT   index_fmt = DXGI_FORMAT_UNKNOWN; // Mesh index format.
  eTopology     topology = UNDEFINED; // Mesh topology
  const CVertexDeclaration* vertex_decl = nullptr; // Vertex declaration.
  bool          is_dynamic = false;
  VMeshGroups   groups;
  AABB          aabb;

  // Use to update VB and IB
  void updateResourceFromCPU(ID3D11Resource* res, const void *new_cpu_data, size_t num_bytes_to_update, size_t max_bytes_allowed);
  void activateIndexBuffer() const;

  friend class CMeshInstanced;

	bool createVertexBuffer(const void * vertices);
	bool createIndexBuffer(const void * indices, uint32_t new_bytes_per_index);

public:

  bool create(
      const void* vertices
    , uint32_t new_num_vertices 
    , uint32_t new_bytes_per_vertice
    , const std::string& vertex_decl_name
    , const void* indices = nullptr
    , uint32_t new_num_indices = 0
    , uint32_t new_bytes_per_indice = 0
    , eTopology = TRIANGLE_LIST
    , bool new_is_dynamic = false
	, const AABB* new_aabb = nullptr
  );
  void setGroups(const VMeshGroups& new_groups);
  const VMeshGroups& getGroups() const {
    return groups;
  }
  VMeshGroups& getGroups() {    // Non-const version
    return groups;
  }
  void destroy();
  void activate() const; // Activates the IA vertex buffer for this mesh and sets the IA primitive topology.
  void render() const; // Renders the mesh, call activate before rendering.
  virtual void renderGroup(uint16_t group_idx, uint16_t instanced_group_idx) const;
  void renderRange(uint32_t count, uint32_t base) const;
  void renderGroupInstanced(uint16_t group_idx, uint32_t num_instances) const;
  void activateAndRender() const;
  void renderInMenu() override;
  void updateVertsFromCPU(const void *new_cpu_data, size_t num_bytes_to_update);
  void updateIndicesFromCPU(const void *new_cpu_data, size_t num_bytes_to_update);
  bool isValid() const { return vb != nullptr && !groups.empty(); }
  const CVertexDeclaration* getVertexDecl() const {
    return vertex_decl;
  }
  const AABB& getAABB() const { return aabb; }
};
