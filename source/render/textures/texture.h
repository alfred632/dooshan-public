#pragma once

/* A class representing a texture. */
class CTexture : public IResource {
protected:
	// A pointer to the memory location where the texture is at.
	ID3D11Resource *           texture = nullptr;
	// This interface specifies the subresources a shader can
	// access during rendering. Examples of shader resources
	// include a constant buffer, a texture buffer, and a texture.
	ID3D11ShaderResourceView * shader_resource_view = nullptr;

public:
	// Create a new texture from params
	enum eCreateOptions {
		CREATE_STATIC,
		CREATE_DYNAMIC,
		CREATE_RENDER_TARGET
	};
	
	~CTexture() { destroy(); }
	
	// Creates a DDS texture from a file whose filename is sent as argument.
	bool create(const std::string & filename);
	// Creates a texture with the given resolution, color format and type of texture.
	bool create(int new_xres, int new_yres, DXGI_FORMAT new_color_format, eCreateOptions create_options = CREATE_STATIC);

	void destroy();
	
	void activate(int slot, int num = 1) const;
	
	void renderInMenu() override;

	const ID3D11ShaderResourceView* getShaderResourceView() { return shader_resource_view; }

  static void setNullTexture(int slot);

  void setDXParams(int new_xres, int new_yres, ID3D11Texture2D* new_texture, ID3D11ShaderResourceView* new_srv);
	ID3D11Resource * getTextureResource() { return texture; }
	ID3D11ShaderResourceView* getShaderResourceView() const { return shader_resource_view; }
};
