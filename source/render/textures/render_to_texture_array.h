#pragma once

#include "render/textures/texture_array.h"

/* This class represents a render to texture array that can be used for rendering data to it. 
The CTexture it inherits from serves as the array resource buffer. */
class CRenderToTextureArray : public CTextureArray {
	// Width and height of the render to texture, texture.
	int width, height;

	// Format of both the color and depth buffer.
	DXGI_FORMAT              color_format = DXGI_FORMAT_UNKNOWN;
	DXGI_FORMAT              depth_format = DXGI_FORMAT_UNKNOWN;
	
	// Render Target View and Depth Stencil View pointers.
	std::vector<ID3D11RenderTargetView *> render_target_views;
	std::vector<ID3D11DepthStencilView *> depth_stencil_views;

	// Pointer to the depth texture in the GPU.
	CTextureArray * z_textures_array;

	static CRenderToTextureArray * current_rt;

public:
	/* Create and destroy RT. */
	bool create(const char * new_name, int array_size, int tex_width, int tex_height, DXGI_FORMAT new_color_format, DXGI_FORMAT new_depth_format, bool uses_depth_of_backbuffer = false);
	bool create(const std::string & filename) = delete; 	// Methods of creation of a normal texture can't be called from RenderToTexture by the user.
	bool create(int new_xres, int new_yres, DXGI_FORMAT new_color_format, eCreateOptions create_options = CREATE_STATIC) = delete;
	void destroy();

	/* Activate render to texture to be rendered to. */
	// Activates render texture target and depth view to be rendered to. No clear is done.
	CRenderToTextureArray * activateRT(int slot, const Vector4 & clear_color = Vector4(0, 0, 0, 1));
	// Activates render to texture target and depth view and clears both views.
	CRenderToTextureArray * activateRTAndClear(int slot, const Vector4 & clear_color = Vector4(0, 0, 0, 1));
	// Activates render to texture target and depth view but doesn't clear depth buffer.
	CRenderToTextureArray * activateRTNoZClear(int slot, const Vector4 & clear_color = Vector4(0, 0, 0, 1));
	// Clears the render target view.
	void clearRenderTargetView(int slot, const Vector4 & clear_color = Vector4(0, 0, 0, 1));
	// Clears the depth buffer.
	void clearDepthBuffer(int slot, const Vector4 & clear_color = Vector4(0, 0, 0, 1));
	
	// Activate a shader that modifies the texture.
	// Activate the texture in the shader.
	// Then call this function to apply the shader in all of the screen.
	void renderToScreen() const;

	void renderInMenu() override;

	/* Getters and setters. */
	int getWidth() const { return width; }
	int getHeight() const { return height; }
	CTextureArray * getZTextureArray() { return z_textures_array; }
	static CTextureArray * getCurrentRT() { return current_rt; }

private:
	void activateViewport();
	bool setRenderTargetViews();
	bool setDepthStencils(const std::string & aname, int width, int height, DXGI_FORMAT format);
};
