#include "mcv_platform.h"
#include "deferred_renderer.h"

#include "render_manager.h"
#include "render_utils.h"
#include "resources/resource.h"
#include "render/primitives.h"
#include "components/lighting/comp_light_dir.h"
#include "components/lighting/comp_light_point.h"
#include "components/lighting/comp_light_spot.h"

#include "components/postFX/comp_postFX_AO.h"
#include "components/postFX/comp_postFX_black_bars.h"
#include "render/intel/ASSAO.h"
#include "render/module_render.h"

#include "utils/data_provider.h"

#include "particles/module_particles.h"

#include "engine.h"

CDeferredRenderer::CDeferredRenderer() {
	Utils::dbg("Creating CDeferredRenderer\n");
}

// Creates the deferred renderer.
bool CDeferredRenderer::create(int new_xres, int new_yres) {
	PROFILE_FUNCTION("CDeferredRenderer::create");
	xres = new_xres;
	yres = new_yres;

	Utils::dbg("Deferred set to %dx%d\n", xres, yres);

	destroy();

	if (!rt_albedos) {
		rt_albedos = new CRenderToTexture();
		rt_normals = new CRenderToTexture();
		rt_depth = new CRenderToTexture();
		rt_self_illumination = new CRenderToTexture();
		rt_acc_depth = new CRenderToTexture(); // Has the depth of all categories rendered in it.
		rt_perception_mask = new CRenderToTexture();
		rt_acc_light = new CRenderToTexture();
		rt_auxiliar = new CRenderToTexture();
		rt_distortions = new CRenderToTexture();
	}

	// Main render to textures for the deferred renderer.
	if (!rt_albedos->create("g_albedos.dds", xres, yres, DXGI_FORMAT_R16G16B16A16_FLOAT)) return false;
	if (!rt_normals->create("g_normals.dds", xres, yres, DXGI_FORMAT_R16G16B16A16_UNORM)) return false;
	if (!rt_depth->create("g_depths.dds", xres, yres, DXGI_FORMAT_R32_FLOAT)) return false;
	if (!rt_self_illumination->create("g_self_illumination.dds", xres, yres, DXGI_FORMAT_R8G8B8A8_UNORM)) return false;
	if (!rt_acc_depth->create("g_acc_depth.dds", xres, yres, DXGI_FORMAT_R32_FLOAT)) return false;
	if (!rt_perception_mask->create("g_perception_mask.dds", xres, yres, DXGI_FORMAT_R32G32_FLOAT)) return false;
	if (!rt_acc_light->create("acc_light.dds", xres, yres, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_UNKNOWN, true)) return false;
	if (!rt_distortions->create("distorsions.dds", xres, yres, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_UNKNOWN, true)) return false;

	// These two textures are used for ping pong copy of screen quads.
	if (!rt_auxiliar->create("AuxiliarRT", xres, yres, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_UNKNOWN, true)) return false;

	if (assao_fx) {
		ASSAO_Effect::DestroyInstance(assao_fx); 
		assao_fx = nullptr;
	}
	
	CMemoryDataProvider mdp("data/shaders/ASSAO.hlsl");
	ASSAO_CreateDescDX11 desc(Render.device, mdp.data(), mdp.size());
	assao_fx = ASSAO_Effect::CreateInstance(&desc);
	
	return true;
}

void CDeferredRenderer::renderGBuffer(eRenderCategory categoryToRender, bool clearDepth) {
	CGpuScope gpu_scope("Deferred.GBuffer");
	
	// Disable the gbuffer textures as we are going to update them
	// Can't render to those textures and have them active in some slot...
	CTexture::setNullTexture(TS_DEFERRED_ALBEDOS);
	CTexture::setNullTexture(TS_DEFERRED_NORMALS);
	CTexture::setNullTexture(TS_DEFERRED_LINEAR_DEPTH);
	CTexture::setNullTexture(TS_DEFERRED_SELF_ILLUMINATION);
	CTexture::setNullTexture(TS_DEFERRED_FULL_DEPTH);

	// Activate el multi-render-target MRT
	const int nrender_targets = 5;
	ID3D11RenderTargetView * rts[nrender_targets] = {
		rt_albedos->getRenderTargetView(),
		rt_normals->getRenderTargetView(),
		rt_depth->getRenderTargetView(),
		rt_self_illumination->getRenderTargetView(),
		rt_acc_depth->getRenderTargetView()
	};
	
	// We use our 5 rt's and the Zbuffer of the backbuffer
	Render.ctx->OMSetRenderTargets(nrender_targets, rts, Render.getDepthStencilView());
	rt_albedos->activateViewport();   // Any rt will do...
	
	// Clear output buffers.
	rt_albedos->clearRenderTargetView(VEC4(0, 0, 0, 0));
	rt_normals->clearRenderTargetView(VEC4(0, 0, 0, 0));
	rt_depth->clearRenderTargetView(VEC4(1, 1, 1, 1));
	if (clearDepth) {
		rt_acc_depth->clearRenderTargetView(VEC4(1, 1, 1, 1));
		Render.ctx->ClearDepthStencilView(Render.getDepthStencilView(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	}
	rt_self_illumination->clearRenderTargetView(VEC4(0, 0, 0, 0));

	// Render the solid objects that output to the G-Buffer
	CRenderManager::get().render(categoryToRender);
	
	// Disable rendering to all render targets.
	ID3D11RenderTargetView* rt_nulls[nrender_targets];
	for (int i = 0; i < nrender_targets; ++i) rt_nulls[i] = nullptr;
	Render.ctx->OMSetRenderTargets(nrender_targets, rt_nulls, nullptr);

	// Activate the gbuffer textures to other shaders
	rt_albedos->activate(TS_DEFERRED_ALBEDOS);
	rt_normals->activate(TS_DEFERRED_NORMALS);
	rt_depth->activate(TS_DEFERRED_LINEAR_DEPTH);
	rt_self_illumination->activate(TS_DEFERRED_SELF_ILLUMINATION);
	rt_acc_depth->activate(TS_DEFERRED_FULL_DEPTH);
}

void CDeferredRenderer::renderGBufferPerception(eRenderCategory categoryToRender, bool clearDepth) {
	CGpuScope gpu_scope("Deferred.GBuffer");

	// Disable the gbuffer textures as we are going to update them
	// Can't render to those textures and have them active in some slot...
	CTexture::setNullTexture(TS_DEFERRED_ALBEDOS);
	CTexture::setNullTexture(TS_DEFERRED_NORMALS);
	CTexture::setNullTexture(TS_DEFERRED_LINEAR_DEPTH);
	CTexture::setNullTexture(TS_DEFERRED_SELF_ILLUMINATION);
	CTexture::setNullTexture(TS_DEFERRED_FULL_DEPTH);
	CTexture::setNullTexture(TS_DEFERRED_PERCEPTION_MASK);

	// Activate el multi-render-target MRT
	const int nrender_targets = 6;
	ID3D11RenderTargetView * rts[nrender_targets] = {
		rt_albedos->getRenderTargetView(),
		rt_normals->getRenderTargetView(),
		rt_depth->getRenderTargetView(),
		rt_self_illumination->getRenderTargetView(),
		rt_acc_depth->getRenderTargetView(),
		rt_perception_mask->getRenderTargetView()
	};

	// We use our 6 rt's and the Zbuffer of the backbuffer
	Render.ctx->OMSetRenderTargets(nrender_targets, rts, Render.getDepthStencilView());
	rt_albedos->activateViewport();   // Any rt will do...

	// Clear output buffers.
	rt_albedos->clearRenderTargetView(VEC4(0, 0, 0, 0));
	rt_normals->clearRenderTargetView(VEC4(0, 0, 0, 0));
	rt_depth->clearRenderTargetView(VEC4(1, 1, 1, 1));
	if (clearDepth) {
		rt_perception_mask->clearRenderTargetView(VEC4(1.0, 0.0, 0.0, 0.0));
	}
	rt_self_illumination->clearRenderTargetView(VEC4(0, 0, 0, 0));

	// Render the objects that output to the G-Buffer
	CRenderManager::get().render(categoryToRender);

	// Disable rendering to all render targets.
	ID3D11RenderTargetView* rt_nulls[nrender_targets];
	for (int i = 0; i < nrender_targets; ++i) rt_nulls[i] = nullptr;
	Render.ctx->OMSetRenderTargets(nrender_targets, rt_nulls, nullptr);

	// Activate the gbuffer textures to other shaders
	rt_albedos->activate(TS_DEFERRED_ALBEDOS);
	rt_normals->activate(TS_DEFERRED_NORMALS);
	rt_depth->activate(TS_DEFERRED_LINEAR_DEPTH);
	rt_self_illumination->activate(TS_DEFERRED_SELF_ILLUMINATION);
	rt_acc_depth->activate(TS_DEFERRED_FULL_DEPTH);
	rt_perception_mask->activate(TS_DEFERRED_PERCEPTION_MASK);
}

void CDeferredRenderer::renderGBufferDecals(eRenderCategory decalCategoryToRender, bool clearRts) {
	CGpuScope gpu_scope("Deferred.GBuffer.Decals");
	
	// Disable the gbuffer textures as we are going to update them
	// Can't render to those textures and have them active in some slot...
	CTexture::setNullTexture(TS_DEFERRED_ALBEDOS);
	CTexture::setNullTexture(TS_DEFERRED_NORMALS);
	CTexture::setNullTexture(TS_DEFERRED_SELF_ILLUMINATION);

	// Activate el multi-render-target MRT
	const int nrender_targets = 3;
	ID3D11RenderTargetView* rts[nrender_targets] = {
		rt_albedos->getRenderTargetView(),
		rt_normals->getRenderTargetView(),
		rt_self_illumination->getRenderTargetView()
		// No Z as we need to read to reconstruct the position
	};
	
	// We use our 3 rt's and the Zbuffer of the backbuffer
	Render.ctx->OMSetRenderTargets(nrender_targets, rts, Render.getDepthStencilView());
	rt_albedos->activateViewport();   // Any rt will do...

	if (clearRts) {
		rt_albedos->clearRenderTargetView(VEC4(0, 0, 0, 0));
		rt_normals->clearRenderTargetView(VEC4(0, 0, 0, 0));
		rt_self_illumination->clearRenderTargetView(VEC4(0, 0, 0, 0));
	}
	// Render blending layer on top of gbuffer before adding lights
	CRenderManager::get().render(decalCategoryToRender);
	
	// Disable rendering to all render targets.
	ID3D11RenderTargetView* rt_nulls[nrender_targets];
	for (int i = 0; i < nrender_targets; ++i) rt_nulls[i] = nullptr;
	Render.ctx->OMSetRenderTargets(nrender_targets, rt_nulls, nullptr);
	
	// Activate the gbuffer textures to other shaders
	rt_albedos->activate(TS_DEFERRED_ALBEDOS);
	rt_normals->activate(TS_DEFERRED_NORMALS);
	rt_self_illumination->activate(TS_DEFERRED_SELF_ILLUMINATION);
}

void CDeferredRenderer::destroy() {
	if (rt_albedos) {
		rt_albedos->destroy();
		rt_normals->destroy();
		rt_depth->destroy();
		rt_acc_light->destroy();
		rt_self_illumination->destroy();
		rt_acc_depth->destroy();
		rt_auxiliar->destroy();
	}

	if (assao_fx) {
		ASSAO_Effect::DestroyInstance(assao_fx);
		assao_fx = nullptr;
	}
}

void CDeferredRenderer::renderAccLight() {
	CGpuScope gpu_scope("Deferred.AccLight");

	CTexture::setNullTexture(TS_ALBEDO);

	rt_acc_light->activateRT();
	rt_acc_light->clearRenderTargetView(VEC4(0, 0, 0, 0));
	renderAmbientPass();
	renderPointLights();
	renderSpotLights();
	renderDirectionalLights();
	renderSkyBox();
}

void CDeferredRenderer::renderAccLightMinusSkyBox() {
	CGpuScope gpu_scope("Deferred.AccLight");

	CTexture::setNullTexture(TS_ALBEDO);

	rt_acc_light->activateRT();
	rt_acc_light->clearRenderTargetView(VEC4(0, 0, 0, 0));
	renderAmbientPass();
	renderPointLights();
	renderSpotLights();
	renderDirectionalLights();
}

void CDeferredRenderer::renderAmbientPass() {
	CGpuScope gpu_scope("renderAmbientPass");
	drawFullScreenQuad("pbr_ambient.tech", nullptr);
}

void CDeferredRenderer::renderPointLights() {
	CGpuScope gpu_scope("renderPointLights");

	// Activate tech for the point lights
	auto* tech = EngineResources.getResource("pbr_point_lights.tech")->as<CTechnique>();
	tech->activate();

	// All point lights use the same mesh
	auto* mesh = EngineResources.getResource("data/meshes/UnitSphere.mesh")->as<CMesh>();
	mesh->activate();

	// Para todas las luces... pintala
	getObjectManager<TCompLightPoint>()->forEach([mesh](TCompLightPoint* c) {
		if (c->isActive()) {
			// subir las contantes de la posicion/dir
			// activar el shadow map...
			c->activate();

			activateObject(c->getWorld());

			// mandar a pintar una geometria que refleje los pixeles que potencialmente
			// puede iluminar esta luz.... El Frustum solido
			mesh->render();
		}
	});
}

void CDeferredRenderer::renderSpotLights() {
	CGpuScope gpu_scope("renderSpotLights");

	// Activate tech for the light dir
	auto* tech = EngineResources.getResource("pbr_spot_lights.tech")->as<CTechnique>();
	tech->activate();

	// All light directional use the same mesh
	auto* mesh = EngineResources.getResource("unit_frustum_solid.mesh")->as<CMesh>();
	mesh->activate();

	// Para todas las luces... pintala
	getObjectManager<TCompLightSpot>()->forEach([mesh](TCompLightSpot* c) {
		if (c->isActive()) {
			// subir las contantes de la posicion/dir
			// activar el shadow map...
			c->activate();

			activateObject(c->getViewProjection().Invert());

			// mandar a pintar una geometria que refleje los pixeles que potencialmente
			// puede iluminar esta luz.... El Frustum solido
			mesh->render();
		}
  });
}

void CDeferredRenderer::renderDirectionalLights() {
	CGpuScope gpu_scope("renderDirectionalLights");

	// Activate tech for the light dir
	auto* tech = EngineResources.getResource("pbr_dir_lights.tech")->as<CTechnique>();
	tech->activate();

	// All light directional use the same mesh
	auto* mesh = EngineResources.getResource("unit_quad_xy.mesh")->as<CMesh>();
	mesh->activate();

	// Para todas las luces... pintala
	getObjectManager<TCompLightDir>()->forEach([mesh](TCompLightDir* c) {
		if (c->isActive()) {
			// subir las contantes de la posicion/dir
			// activar el shadow map...
			c->activate();

			// mandar a pintar una geometria que refleje los pixeles que potencialmente
			// puede iluminar esta luz.... Un quad entero.
			mesh->render();
		}
	});
}

void CDeferredRenderer::renderSkyBox() const {
	CGpuScope gpu_scope("renderSkyBox");
	drawFullScreenQuad("pbr_skybox.tech", nullptr);
}

void CDeferredRenderer::renderAO(CHandle h_camera, bool deactivate) const {
	CGpuScope gpu_scope("Deferred.AO");

	CEntity* e_camera = h_camera;
	assert(e_camera);

	TCompPostFXAO * comp_ao = e_camera->getComponent<TCompPostFXAO>();
	if (!comp_ao || deactivate) {
		// As there is no comp AO, use a white texture as substitute
		const CTexture * white_texture = EngineResources.getResource("data/textures/white.dds")->as<CTexture>();
		white_texture->activate(TS_DEFERRED_AO);
		return;
	}

	// As we are going to update the RenderTarget AO
	// it can NOT be active as a texture while updating it.
	CTexture::setNullTexture(TS_DEFERRED_AO);
	auto ao = comp_ao->apply(rt_depth, rt_normals, assao_fx);
	// Activate the updated AO texture so everybody else can use it
	// Like the AccLight (Ambient pass or the debugger)
	ao->activate(TS_DEFERRED_AO);
}

void CDeferredRenderer::renderNormalCategory(CRenderToTexture* rt_destination, CHandle h_camera, eRenderCategory categoryToRender, bool perceptionMode, bool clearDepth) {
	{
		CGpuScope gpu_scope("Rendering Normal Category");

		assert(xres);
		assert(rt_destination->getWidth() == xres);
		assert(rt_destination->getHeight() == yres);

		renderGBuffer(categoryToRender, clearDepth);
		renderGBufferDecals(eRenderCategory::CATEGORY_DECALS);
		if(!perceptionMode)
			renderGBufferDecals(eRenderCategory::CATEGORY_DECALS_PERCEPTION);
		renderAO(h_camera, false);

		CTexture::setNullTexture(TS_DEFERRED_ACC_LIGHTS);
		renderAccLight();

		{
			CGpuScope gpu_scope("Deferred.Resolve");
			// Get the deferred output on a texture.
			rt_destination->activateRT();
			rt_acc_light->activate(TS_DEFERRED_ACC_LIGHTS);
			drawFullScreenQuad("gbuffer_resolve.tech", nullptr);
		}
	}
}

void CDeferredRenderer::renderTransparentCategory(CRenderToTexture* rt_destination, CHandle h_camera) {
	{
		CGpuScope gpu_scope("Rendering Transparent Category");
		{
			CGpuScope gpu_scope("Render blending");
			// Render blended elements, don't erase the depth.
			renderCategoryNoSkyBox(rt_auxiliar, h_camera, eRenderCategory::CATEGORY_TRANSPARENTS, false);
		}

		{
			CGpuScope gpu_scope("Combine with the rest.");
			rt_destination->activateRT();
			drawFullScreenQuad("combine_textures_blend.tech", rt_auxiliar);
		}
	}
}

// Used for rendering the transparent category which doesn't need the skybox.
void CDeferredRenderer::renderCategoryNoSkyBox(CRenderToTexture* rt_destination, CHandle h_camera, eRenderCategory categoryToRender, bool clearDepth) {
	renderGBuffer(categoryToRender, clearDepth);

	// Do the same with the acc light
	CTexture::setNullTexture(TS_DEFERRED_ACC_LIGHTS);
	renderAccLightMinusSkyBox();

	// Combine the deferred output and add some basic processing.
	rt_destination->activateRT();
	rt_acc_light->activate(TS_DEFERRED_ACC_LIGHTS);
	drawFullScreenQuad("gbuffer_resolve.tech", nullptr);
}

void CDeferredRenderer::renderPerceptionCategory(CRenderToTexture * rt_destination, CHandle h_camera, eRenderCategory perceptionCategory, CModuleRender * renderModule, bool clearDepth) {
	{
		CGpuScope gpu_scope("Rendering perception category");

		// Apply postprocessing to the perceptive objects.
		{
			CGpuScope gpu_scope("Render perception");
			// Render perception solids.
			renderPerceptionLightedCategory(rt_auxiliar, h_camera, perceptionCategory, clearDepth);
		}

		// Render x-ray.
		if (perceptionCategory == eRenderCategory::CATEGORY_PERCEPTION_ENEMIES) {
			CTexture::setNullTexture(TS_DEFERRED_PERCEPTION_MASK);

			// Activate el multi-render-target MRT
			const int nrender_targets = 2;
			ID3D11RenderTargetView * rts[nrender_targets] = {
				rt_auxiliar->getRenderTargetView(),
				rt_perception_mask->getRenderTargetView()
			};

			// We use our 6 rt's and the Zbuffer of the backbuffer
			Render.ctx->OMSetRenderTargets(nrender_targets, rts, Render.getDepthStencilView());
			rt_auxiliar->activateViewport();   // Any rt will do...
			CRenderManager::get().render(eRenderCategory::CATEGORY_XRAY);

			// Disable rendering to all render targets.
			ID3D11RenderTargetView* rt_nulls[nrender_targets];
			for (int i = 0; i < nrender_targets; ++i) rt_nulls[i] = nullptr;
			Render.ctx->OMSetRenderTargets(nrender_targets, rt_nulls, nullptr);

			rt_perception_mask->activate(TS_DEFERRED_PERCEPTION_MASK);
		}

		// Generate bloom highlights.
		CRenderToTexture * last_rt_postProcess = rt_auxiliar;
		{
			CGpuScope gpu_scope("Rendering PostFX Perception Objects");
			last_rt_postProcess = renderModule->applyPostProcessingPerceptionCategory(rt_auxiliar, perceptionCategory);
		}

		// Combine perceptive with rest of output.
		{
			CGpuScope gpu_scope("Combine with perception");
			rt_destination->activateRT();
			drawFullScreenQuad("combine_textures.tech", last_rt_postProcess);
		}

		// Apply final bloom post processing to output.
		{
			CGpuScope gpu_scope("Render perception bloom");
			std::string bloomFilter = perceptionCategory == eRenderCategory::CATEGORY_PERCEPTION ? "perception_player"
				: perceptionCategory == eRenderCategory::CATEGORY_PERCEPTION_ENEMIES ? "perception_enemies" : "perception_objects";
			renderModule->applyFinalPostProcessingPerceptionCategory(bloomFilter);
		}
	}
}

// Used for rendering perception.
void CDeferredRenderer::renderPerceptionLightedCategory(CRenderToTexture * rt_destination, CHandle h_camera, eRenderCategory categoryToRender, bool clearDepth) {
	renderGBufferPerception(categoryToRender, clearDepth);
	renderAO(h_camera, true); // Deactivate the previous texture.

	// Render only ambient light.
	{
		CTexture::setNullTexture(TS_DEFERRED_ACC_LIGHTS);
		CGpuScope gpu_scope("Deferred.AccLight");
		rt_acc_light->activateRT();
		rt_acc_light->clearRenderTargetView(VEC4(0, 0, 0, 0));
		renderAmbientPass();
	}

	// Combine the deferred output and add some basic processing.
	rt_destination->activateRT();
	rt_acc_light->activate(TS_DEFERRED_ACC_LIGHTS);
	drawFullScreenQuad("gbuffer_resolve.tech", nullptr);
}

void CDeferredRenderer::renderPerceptionParticles(CRenderToTexture * rt_destination, CModuleRender * renderModule) {
	{
		CGpuScope gpu_scope("Rendering perception category");

		// Apply postprocessing to the perceptive objects.
		{
			CGpuScope gpu_scope("Render perception particles");
			// Render particles.
			rt_auxiliar->activateRTWithDepth(Render.getDepthStencilView());
			// Upload linear depth for spherical billboards
			rt_acc_depth->activate(TS_DEFERRED_LINEAR_DEPTH);
			CRenderManager::get().render(eRenderCategory::CATEGORY_PARTICLES);
			CRenderManager::get().render(eRenderCategory::CATEGORY_PARTICLES_PERCEPTION);
			CTexture::setNullTexture(TS_DEFERRED_LINEAR_DEPTH);
		}

		// Generate bloom highlights.
		CRenderToTexture * last_rt_postProcess = rt_auxiliar;
		{
			CGpuScope gpu_scope("Rendering PostFX Perception Particles");
			last_rt_postProcess = renderModule->applyPostProcessingPerceptionParticles(last_rt_postProcess);
		}

		// Combine particles with rest of output.
		{
			CGpuScope gpu_scope("Combine particles with perception");
			rt_destination->activateRT();
			drawFullScreenQuad("combine_textures.tech", last_rt_postProcess);
		}

		// Apply final bloom post processing to output.
		{
			CGpuScope gpu_scope("Render perception particles bloom");
			rt_destination = renderModule->applyFinalPostProcessingPerceptionParticles(rt_destination, "perception_base");
		}
	}
}

void CDeferredRenderer::renderDecalsPerception(CRenderToTexture * rt_destination) {
	CGpuScope gpu_scope("DecalPerception");

	renderGBufferDecals(eRenderCategory::CATEGORY_DECALS_PERCEPTION, true);
	rt_destination->activateRT();
	drawFullScreenQuad("pbr_ambient_combinative.tech", nullptr);
}

void CDeferredRenderer::renderDefault(CModuleRender * renderModule, CRenderToTexture* rt_destination, CHandle h_camera) {
	PROFILE_FUNCTION("Deferred Default");

	assert(rt_destination);

	CGpuScope gpu_scope("Render default");
	if (rt_destination->getWidth() != xres || rt_destination->getHeight() != yres) {
		bool is_ok = create(rt_destination->getWidth(), rt_destination->getHeight());
		assert(is_ok);
	}
	CRenderToTexture * current_rt = rt_destination;

	// Render default category. This renders all objects that don't have any special treatment.
	renderNormalCategory(current_rt, h_camera, eRenderCategory::CATEGORY_NORMAL_SOLID, false);

	CRenderManager::get().render(eRenderCategory::CATEGORY_NO_LIGHTING);

	// Render distortions.
	rt_distortions->activateRT();
	drawFullScreenQuad("texture_to_screen.tech", current_rt);
	
	current_rt->activateRT();
	CRenderManager::get().render(eRenderCategory::CATEGORY_DISTORSIONS);

	// Render transparent objects (includes objects blended by camera if too close to it).
	// It's not a real transparency actually but it fakes it good enough for now.
	renderTransparentCategory(current_rt, h_camera);
	
	// Use the full depth with all the categories.
	rt_acc_depth->activate(TS_DEFERRED_LINEAR_DEPTH);

	Vector4 clearColorBackBuffer;
	{
		CGpuScope gpu_scope("Rendering PostFX Blur");

		// Apply any postprocessing to the rendered image.
		clearColorBackBuffer = renderModule->getBackBufferClearColor();
		current_rt = renderModule->applyPostProcessing(current_rt, clearColorBackBuffer);
	}

	// Render particles.
	current_rt->activateRTWithDepth(Render.getDepthStencilView());
	CRenderManager::get().render(eRenderCategory::CATEGORY_PARTICLES);

	// Use the full depth with all the categories.
	rt_acc_depth->activate(TS_DEFERRED_LINEAR_DEPTH);

	{
		CGpuScope gpu_scope("Rendering PostFX Final");

		// Apply any postprocessing to the rendered image.
		clearColorBackBuffer = renderModule->getBackBufferClearColor();
		current_rt = renderModule->applyFinalPostProcessing(this, current_rt, rt_acc_depth, clearColorBackBuffer);
	}

	{
		PROFILE_FUNCTION("Tonemapping and Gamma Correction");
		CGpuScope gpu_scope("Tonemapping and Gamma Correction rendering");

		CRenderToTexture * rt_to_render_to = rt_auxiliar;
		if (rt_to_render_to == current_rt)
			rt_to_render_to = rt_acc_light;

		// Apply the presentation technique to the image.
		rt_to_render_to->activateRT();
		drawFullScreenQuad("presentation.tech", current_rt);
		current_rt = rt_to_render_to;
	}

	current_rt = renderModule->applyAntialiasing(current_rt);

	{
		PROFILE_FUNCTION("Presentation");
		CGpuScope gpu_scope("Presentation rendering");

		// Apply the presentation technique to the image.
		renderModule->activateRenderToBackBuffer(false, clearColorBackBuffer);
		drawFullScreenQuad("texture_to_screen.tech", current_rt);
	}
}

void CDeferredRenderer::renderPerception(CModuleRender * renderModule, CRenderToTexture* rt_destination, CHandle h_camera) {
	PROFILE_FUNCTION("Deferred Perception");

	assert(rt_destination);

	CGpuScope gpu_scope("Deferred Perception");

	if (!xres || rt_destination->getWidth() != xres || rt_destination->getHeight() != yres) {
		bool is_ok = create(rt_destination->getWidth(), rt_destination->getHeight());
		assert(is_ok);
	}
	CRenderToTexture * current_rt = rt_destination;

	// Render default category. This renders all objects that don't have any special treatment.
	renderNormalCategory(current_rt, h_camera, eRenderCategory::CATEGORY_NORMAL_SOLID, true);

	// Render distortions.
	rt_distortions->activateRT();
	drawFullScreenQuad("texture_to_screen.tech", current_rt);
	current_rt->activateRT();
	CRenderManager::get().render(eRenderCategory::CATEGORY_DISTORSIONS);

	{
		CGpuScope gpu_scope("Rendering PostFX Normal Objects");
		rt_acc_depth->activate(TS_DEFERRED_LINEAR_DEPTH);

		// Apply any postprocessing to the rendered image.
		current_rt = renderModule->applyPostProcessingPerceptionNormalObjects(current_rt);
	}

	renderDecalsPerception(current_rt);

	// Render transparent objects (includes objects blended by camera if too close to it).
	// It's not a real transparency actually but it fakes it good enough for now.
	renderTransparentCategory(current_rt, h_camera);

	renderModule->applyFinalPostProcessingPerceptionNormalObjects(current_rt, "perception_base");

	CRenderManager::get().render(eRenderCategory::CATEGORY_NO_LIGHTING);

	// Render perceptive player.
	renderPerceptionCategory(current_rt, h_camera, eRenderCategory::CATEGORY_PERCEPTION, renderModule, true);

	// Render perceptive enemies.
	renderPerceptionCategory(current_rt, h_camera, eRenderCategory::CATEGORY_PERCEPTION_ENEMIES, renderModule);

	// Render perceptive objects.
	renderPerceptionCategory(current_rt, h_camera, eRenderCategory::CATEGORY_PERCEPTION_OBJECTS, renderModule);

	// Render particles.
	renderPerceptionParticles(current_rt, renderModule);

	Vector4 clearColorBackBuffer;
	{
		CGpuScope gpu_scope("Rendering PostFX Perception Objections Outlines");
		clearColorBackBuffer = renderModule->getBackBufferClearColor();
		current_rt = renderModule->applyPostProcessingPerceptionOutlines(current_rt, rt_acc_depth, rt_perception_mask, clearColorBackBuffer);
	}

	{
		CGpuScope gpu_scope("Rendering PostFX Perception Objections Final");
		clearColorBackBuffer = renderModule->getBackBufferClearColor();
		current_rt = renderModule->applyFinalPostProcessingPerception(this, current_rt, rt_acc_depth, rt_perception_mask, clearColorBackBuffer);
	}

	{
		PROFILE_FUNCTION("Tonemapping and Gamma Correction");
		CGpuScope gpu_scope("Tonemapping and Gamma Correction rendering");
		
		CRenderToTexture * rt_to_render_to = rt_auxiliar;
		if (rt_to_render_to == current_rt)
			rt_to_render_to = rt_acc_light;

		// Apply the presentation technique to the image.
		rt_to_render_to->activateRT();
		drawFullScreenQuad("presentation.tech", current_rt);
		current_rt = rt_to_render_to;
	}

	current_rt = renderModule->applyAntialiasing(current_rt);

	{
		PROFILE_FUNCTION("Presentation");
		CGpuScope gpu_scope("Presentation rendering");

		// Apply the presentation technique to the image.
		renderModule->activateRenderToBackBuffer(false, clearColorBackBuffer);
		drawFullScreenQuad("texture_to_screen.tech", current_rt);
	}
}