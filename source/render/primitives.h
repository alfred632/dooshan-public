#pragma once

#include "meshes/mesh.h"
#include "render/shaders/technique.h"
#include "render/shaders/cte_buffer.h"
#include "../bin/data/shaders/constants.h"
#include "render/meshes/vertex_declarations.h"

bool createRenderPrimitives();
void destroyRenderPrimitives();

void drawMesh(const CMesh* mesh, Matrix world, Vector4 color);
void drawLine(const VEC3 & src, const VEC3 & dst, const Vector4 & color = Vector4(0, 1, 0, 1));
void drawCircle(const VEC3& center, float radius, const Vector4& color = Vector4(0, 1, 0, 1));
void drawCircle(const VEC3 & center, float radius, Matrix world, const Vector4 & color = Vector4(0, 1, 0, 1));
//void drawSphere(const VEC3 & center, float radius, const Vector4 & color = Vector4(0, 1, 0, 1));
void drawAxis(const Matrix & world);
void drawHorizontalCone(VEC3 & origin, float originRotation, float angle, float depth, int samples = 16, const Vector4 & color = Vector4(1.0f, 1.0f, 0.0f, 1.0f));
void drawWiredSphere(VEC3 center, float radius, Vector4 color = Vector4(0, 1, 0, 1));
void drawWiredSphere(Matrix world, float radius, Vector4 color = Vector4(0, 1, 0, 1));
void drawWiredAABB(VEC3 center, VEC3 half, Matrix world, Vector4 color);
void drawFullScreenQuad(const std::string& tech_name, const CTexture* texture);
void drawFullScreenQuad(const CTechnique * technique, CTexture* texture);



extern CCteBuffer<TCtesCamera> ctes_camera;
extern CCteBuffer<TCtesObject> ctes_object;
extern CCteBuffer<TCtesShared> ctes_shared;
extern CCteBuffer<TCtesLight>  ctes_light;
extern CCteBuffer<TCtesBlur>   ctes_blur;
extern CCteBuffer<TCtesUI>     ctes_ui;

void activateObject(const Matrix & world, const Vector4 color = Vector4(1, 1, 1, 1));
void activateCamera(const CCamera& camera, int width, int height);
void activateDebugTech(const CMesh * mesh);
void activateUICts();