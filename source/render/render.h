#pragma once

#include "render/gpu_trace.h"

/* Our render class suporting rendering through the DirectX 11 API. */
class CRenderD11 {
	// Series of frame buffers used for rendering.
	IDXGISwapChain *         swap_chain = nullptr;
	// The render target view holds a location in video memory where the render is stored. It will be the back buffer in our case.
	ID3D11RenderTargetView * render_target_view = nullptr;

	// Pointer to the space in memory that we allocated for using as zbuffer.
	ID3D11Texture2D*          depth_texture = nullptr;
	// Pointer to the depth stencil view that will point to the memory
	// allocated upwards and use it as zbuffer.
	ID3D11DepthStencilView *   depth_stencil_view = nullptr;
	// Shader resource view necessary to access the depth view. Used by intel AO.
	ID3D11ShaderResourceView * depth_srv = nullptr;
	
	ID3D11DepthStencilState * depthStencilNormal;
	ID3D11DepthStencilState * depthStencilNoDepth;

	/* Setup private functions for creation of back buffers, devices, and render target view.*/
	void setupSwapChainStruct(HWND hWnd, DXGI_SWAP_CHAIN_DESC & sd);
	bool setupDevice(const DXGI_SWAP_CHAIN_DESC & sd); // Set device and ctx for video adapter communication, instantiate swap chain in memory.
	bool setupRenderTargetView();
	bool setupZBuffer();
	// Sets the depth stencil view, identifies the memory allocated as a D3D11_TEXTURE_2D
	// as the memory to use for the ZBuffer.
	bool setupDepthStencilView();
	// Set Resource Shader View.
	bool setupShaderResourceView();
	// Sets the render target view to be used by the render context. Call it before starting rendering.
	// New rendered objects will be rendered in the pointer pointer by the render target view.
	void setRenderTargetView(ID3D11RenderTargetView ** render_target_view, ID3D11DepthStencilView * depth_stencil_view);
	void destroyRenderTarget();

public:
	// Direct3D11 pointer to device representing a virtual representation of the video adapter.
	ID3D11Device *           device = nullptr;
	// Context manages the GPU and rendering pipeline while device manages mostly video memory.
	ID3D11DeviceContext *    ctx = nullptr;

	// CRender resolution values.
	int width = 0, height = 0;

	/* Initiation and closing methods. */

	// Initiates Direct3D11 rendering.
	bool init(HWND hWnd, int renderWidth, int renderHeight);
	// Frees all the requested resources. Call before shutting down the engine for whatever reason.
	void close();
	
	/* Rendering methods back buffer. */
	
	// Activates the back buffer setting the render target view and viewport.  
	void startRenderingBackBuffer();
	// Clears the render targer view memory setting it all to the requested background color.
	void clearBackBufferRenderTargetView(const Vector4 & backgroundColor = Vector4(0, 0, 0, 1));
	// Clears the depth buffer setting it all to the requested background color.
	void clearBackBufferDepthStencil(const Vector4 & backgroundColor = Vector4(0, 0, 0, 1));
	// Sets the viewport for rendering.
	void setViewport();
	// Call after rendering scene to send it to front buffer.
	void swapChain();
	// Resizes the back buffer.
	bool resizeBackBuffer(int width, int height);

	/* Rendering methods for any target view. */

	// Activates the render target view and viewport.  
	void activateRenderTarget(ID3D11RenderTargetView ** render_target_view, ID3D11DepthStencilView * depth_stencil_view);
	// Clears the render target view memory setting it all to the requested background color.
	void clearRenderingTargetView(ID3D11RenderTargetView * render_target_view, const Vector4 & backgroundColor = Vector4(0, 0, 0, 1));
	// Clears the depth buffer memory setting it all to the requested background color.
	void clearDepthStencilView(ID3D11DepthStencilView * depth_stencil_view, const Vector4 & backgroundColor = Vector4(0, 0, 0, 1),
		UINT depthStencilFlags = D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL);
	// Sets a viewport given as parameter for rendering.
	void setViewport(D3D11_VIEWPORT & vp);

	/* Getters and setters. */

	int getWidth() { return width; }
	int getHeight() { return height; }
	float getAspectRatio() { return (float)width / (float)height; }
	ID3D11RenderTargetView * getBackBufferRenderTargetView() { return render_target_view; }
	ID3D11DepthStencilView * getDepthStencilView() { return depth_stencil_view; }
	ID3D11ShaderResourceView * getShaderResourceView() { return depth_srv; }
};

#define SAFE_RELEASE(x) if(x) { (x)->Release(); x = nullptr; }

extern CRenderD11 Render;

// To assign a name to each DX object
#define setDXName(dx_obj,new_name) \
        (dx_obj)->SetPrivateData(WKPDID_D3DDebugObjectName, (UINT)strlen(new_name), new_name);

enum eRenderCategory {
	CATEGORY_NORMAL_SOLID, // Previously called solids.
	CATEGORY_DISTORSIONS,
	CATEGORY_PERCEPTION,
	CATEGORY_PERCEPTION_ENEMIES,
	CATEGORY_PERCEPTION_OBJECTS,
	CATEGORY_TRANSPARENTS,
	CATEGORY_XRAY,
	CATEGORY_DECALS,
	CATEGORY_DECALS_PERCEPTION,
	CATEGORY_UI,
	CATEGORY_SHADOWS,
	CATEGORY_SKYBOX_LIGHTS,
	CATEGORY_PARTICLES,
	CATEGORY_PARTICLES_PERCEPTION,
	CATEGORY_NO_LIGHTING,
};

#include "render/render_utils.h"
#include "render/primitives.h"
#include "render/textures/render_to_texture.h"

