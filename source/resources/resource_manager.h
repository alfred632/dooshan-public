#pragma once

#include <unordered_map>
#include <vector>

class IResource;
class CResourceType;



/*  The resources manager class holds all resources in memory, being responsible
for their management, giveaway and freeing from memory. */
class CResourcesManager {
	std::unordered_map< std::string, IResource * > _all_resources; // Holds all the loaded resources indexed by name.
	std::vector< const CResourceType* > _resource_types; // Holds all the resource types in a vector.
	
	// Finds a resource type by the name of the filename = data/meshes/car.mesh
	const CResourceType * findResourceTypeByFileExtension(const std::string & filepath);
	// Finds a resource type by their extension (i.e .mesh).
	const CResourceType * findResourceTypeByExtension(const std::string & ext);
	

public:
	// Registers a new resource type.
	void registerResourceType(const CResourceType * res_type);
	
	// Checks if a given resource exists.
	bool existsResource(const std::string & name) const;

	// Registers a new resource adding it to the resource manager.
	void registerResource(IResource * new_resource);

	// Registers a new resource adding it to the resource manager.
	void registerOrOverwriteResource(IResource * new_resource);
	
	// Returns a resource given its name.
	const IResource * getResource(const std::string & filepath);
	IResource * getEditableResource(const std::string & filepath);
	
	void destroyResource(const std::string & filepath);
	
	// Called when a filename gets changed during execution.
	void onFileChanged(const std::string & strfilename);
	
	// Renders resources in menu.
	void renderInMenu();

	// Destroys all resources freeing them from memory.
	void destroyAll();


	// Renders a resource type in render in menu.
	template<typename ResourceType> void
	renderResourceTypeInMenu() {
		const CResourceType * rt = getResourceTypeFor<ResourceType>();
		for (auto it : _all_resources) {
			IResource * res = it.second;
			// Is of the current resource type I'm showing...
			if (res->getResourceType() == rt) {
				// Resource name.
				if (ImGui::TreeNode(it.first.c_str())) {
					res->renderInMenu();
					ImGui::TreePop();
				}
			}
		}
	}

	// Renders a resource type in render debug.
	template<typename ResourceType> void
		renderResourceTypeInRenderDebug() {
		const CResourceType * rt = getResourceTypeFor<ResourceType>();
		for (auto it : _all_resources) {
			IResource * res = it.second;
			// Is of the current resource type I'm showing...
			if (res->getResourceType() == rt) {
				ResourceType * resource = (ResourceType *)res;
				resource->renderDebug();
			}
		}
	}
};