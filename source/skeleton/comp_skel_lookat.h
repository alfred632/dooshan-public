#ifndef INC_COMP_SKEL_LOOKAT_H_
#define INC_COMP_SKEL_LOOKAT_H_

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "bone_correction.h"

#include "entity/common_msgs.h"

#include "utils/WeightController.h"

class CalModel;
class TCompTransform;
struct TCompSkeleton;

/* This component modifies a bone setting rotating it's position to point at the given target. */
class TCompSkelLookAt : public TCompBase {
	DECL_SIBLING_ACCESS();

	struct TTrackingTargetAndBones {
		std::string lookAt_name;
		std::vector<BoneStatus> bonesStatus; // Current yaw and rotation of each bone, also has the speed of rotation for each one. For now, same speed for all bones.

	private:

		// Whether the look at is affecting the skeleton or not.
		bool isActive = false;

		/* Targetting variables. */

		// Entity targetting variables.
		bool isTargettingEntity; // Are we targetting an entity or a position.
		std::string target_entity_name; // Nameof the target entity to track.
		CHandle     h_target_entity; // Handle to the target entity being tracked.

		// Absolute position to move c to. Used if no entity is being targetted.
		Vector3   target;

		// Speed at which the bones will rotate. In radians.
		float speed_rotation = 0.0f;
		
		/* Amount variables. */

		// Controls the amount of the ik. Starts working when the ik gets activated.
		// Create your own class inheriting from this to control the IK value.
		// Otherwise, leave it to null and modify the values of this IK directly.
		WeightController::IWeightController * weightController = nullptr;
		float	amount = 1.f; // When 1.0f, we are pointing to the position, when 0.0f, we are not affecting the bone.
	
		/* Target functions. */
		
		// Gets the handle of the entity if we are following one.
		void fetchEntityTargetting();
		
		// Update the target position if we are following an entity.
		void updateEntityTargetPos(float dt);

	public:
		TTrackingTargetAndBones() {}
		TTrackingTargetAndBones(const std::string & nLookAtName);
		
		void load(const json& j);
		void update(const TCompTransform * transform, TCompSkeleton * skeleton, float dt);
		void renderDebug() {}
		void debugInMenu();
		
		void setLookAtStatus(bool active);
		void setTargetPosition(const Vector3 & nTarget);
		void setTargetEntity(CHandle nEntity);
		void setTargetEntityByName(const std::string & entity_name);
		void setLookAtAmount(float nAmount) { amount = nAmount; }
		void setSpeedTargetFollowing(float speedTarget) { speed_rotation = speedTarget; }

		float getSpeedRotation() { return speed_rotation; }
	};

	CHandle     h_skeleton; // Handle of the skeleton component of the entity that has the component.
	CHandle		h_transform; // Transform handle.
	std::map<std::string, TTrackingTargetAndBones> tracking_target_and_bones;

	// For debugging.
	//Vector3 a = Vector3(2.0f, 2.20f, 0.0f);

public:
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void debugInMenu();
	void renderDebug();
	static void registerMsgs();
	void onEntityCreated(const TMsgEntityCreated & msg);

	// Activates a look at, which will start affecting the bones it was marked to affect.
	void activateLookAt(const std::string & lookAt);
	void activateLookAt(const std::string & lookAt, const Vector3 & nTarget);
	void activateLookAt(const std::string & lookAt, CHandle nEntity);

	// Deactivates a look at, which will stop affecting the bones it was marked to affect.
	void deactivateLookAt(const std::string & lookAt);

	// Returns the look at info if you want to modify it's values.
	TTrackingTargetAndBones & getLookAtInfo(const std::string & lookAt);
};

#endif
