#pragma once

#include "PxPhysicsAPI.h"
struct TJsonSkeletonShapes;
struct TJsonSkeletonBoneShape;

struct TSkeletonShapes {
  struct TSkeletonBoneShape {
    TJsonSkeletonBoneShape * core;

	physx::PxRigidDynamic* actor;
	physx::PxArticulationLink* link;
	physx::PxArticulationJoint* parent_joint;

    static const int MAX_RAGDOLL_BONE_CHILDREN = 128;
    int children_idxs[MAX_RAGDOLL_BONE_CHILDREN];

    int num_children;
    int idx;
    int parent_idx;
		bool has_parent = true;
		bool custom_height = true;

		bool isRoot = false;

    TSkeletonBoneShape() : num_children(0) {}

  };

  static const int MAX_RAGDOLL_BONES= 128;
  int num_bones;
  //Physx shapes information per bone
  TSkeletonBoneShape bones[MAX_RAGDOLL_BONES];

  bool created;

  TSkeletonShapes() :created(false), num_bones(0){};

};