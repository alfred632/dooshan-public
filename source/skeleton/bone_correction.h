#pragma once

class TCompTransform;
class CalSkeleton;
class CalVector;

struct BoneStatus {
	float		currentDeltaYaw, currentDeltaPitch; // Current yaw and pitch of the bone. Used for smooth lerping.
	float		boneRotationSpeed; // Speed at which the bone changes angles.
};

/* This struct applies bone correction to a bone making it point in a given direction. */
struct TBoneCorrection {
	std::string bone_name; // Name of the bone to correct.
	int         bone_id = -1; // Id of the bone being corrected.
	VEC3        local_axis_to_correct; // 0,1,-0.2
	float       amount;                // Amout to correct. In case we want to rotate not the 100%
	float		maxYaw = 60.0f, maxPitch = 20.0f; // Max yaw and max pitch the bone can rotate. If -1, no check is done.

	void load(const json& j);
	void debugInMenu();

	// This method applies an instant correction to a bone making it point in the given direction instantly. Doesn't consider max rotation of the bone.
	void apply(CalSkeleton* skel, VEC3 world_target, float external_unit_amount) const;
	// This method applies a correction to the bone considering the maximum rotation of the bone and the rotation speed and current angle it is in, both of them passed in the bone status struct.
	void applyBounded(CalSkeleton* skel, VEC3 world_target, float external_unit_amount, const TCompTransform * modelTransform, BoneStatus & boneState) const;
	// This method applies a correction to the bone instantly but considering the maximum rotation of the bone. The bone state structure is updated to keep with the current yaw and pitch in case it would be necessary.
	void applyBoundedInstantly(CalSkeleton* skel, VEC3 world_target, float external_unit_amount, const TCompTransform * modelTransform, BoneStatus & boneState) const;

private:
	void getBoneFinalDirection(const TCompTransform * modelTransform, CalVector & abs_dir, BoneStatus & boneState) const;
	void getBoneFinalDirectionInstantly(const TCompTransform * modelTransform, CalVector & abs_dir, BoneStatus & boneState) const;
};

