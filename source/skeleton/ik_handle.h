#ifndef INC_IK_HANDLER_H_
#define INC_IK_HANDLER_H_

#include "mcv_platform.h"

struct TIKHandle {
  float    AB;        // Given from Bone
  float    BC;        // Given from Bone
  VEC3     A, B, C;   // A & C are given, B is found
  VEC3     normal;
  float    h;         // Perp amount with respect to AC
  float    a1, a2;

  enum eState {
    NORMAL
  , TOO_FAR
  , TOO_SHORT
  , UNKNOWN
  };
  eState   state;

  bool solveB() {

    VEC3 dir = C - A;
    float AC = dir.Length();
    dir.Normalize();

    // C is too far, 
    if (AC > AB + BC) {
      state = TOO_FAR;
      B = A + dir * AB;
      return false;
    }

    float num = AB*AB - BC*BC - AC*AC;
    float den = -2 * AC;

    if (den == 0) {
      state = UNKNOWN;
      return false;
    }

    a2 = num / den;
    a1 = AC - a2;

    // h^2 + a1^2 = AB^2
    float h2 = AB*AB - a1 * a1;
    if (h2 < 0.f) {
      state = TOO_SHORT;
      B = A - dir * AB;
      return false;
    }
    h = sqrtf(h2);

    VEC3 perp_dir = normal.Cross(dir);
    perp_dir.Normalize();
    B = A + dir * a1 + h * perp_dir;
    state = NORMAL;
    return true;
  }

};



#endif

