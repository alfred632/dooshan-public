#pragma once

#include "base_ik_resolver.h"
#include "FABRIKSolver.h"

// Struct for arithmetic bones resolution with the FABRIK method. Supports more than three bones.

struct TIKFABRIKBonesResolution : public TIKBaseResolver {
	/* Bones of the IK. */
	std::vector<int> boneIds; // Ids of the bones, first one is start bone, last one is target.

	// Stores the positions of the bones before iks, used for lerping between iks and normal pos.
	// We store the vector as a variable to avoid creating it each time.
	// Not used by the ik.
	std::vector<Vector3> bonesNormalPositions;

public:
	/* Public functions */

	void load(const json & j, CalModel * model) override;
	void update(const TCompTransform * transform, CalModel * model, float dt) override;
	void renderInMenu() override;
	void renderInDebug() override;

private:
	/* IK Handle. */

	// This struct resolves the IK by using the FABRIk solver.
	FABRIKSolver fabrik;

	/* Load functions. */

	// Get the bones ids from the config file.
	void loadBones(const json & j, CalModel * model);
	// Loads the target for the IKs.
	void loadTarget(const json & j);
};