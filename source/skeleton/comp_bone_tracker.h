#ifndef INC_COMP_BONE_TRACKER_H_
#define INC_COMP_BONE_TRACKER_H_

#include "components/common/comp_base.h"
#include "entity/entity.h"

class CalModel;
struct TMsgEntitiesGroupCreated;

/* This component tracks a bone setting the position of the entity
to the one of the bone in each update call. */
class TCompBoneTracker : public TCompBase {
	DECL_SIBLING_ACCESS();
	
	CHandle     h_skeleton;        // Handle to comp_skeleton of the entity being tracked
	int         bone_id = -1;      // Id of the bone of the skeleton to track
	std::string bone_name;
	std::string parent_name;

	Vector3 offset_from_bone; // Offset to apply based on the bone.
	Vector3 yawPitchRollOffset = Vector3::Zero;
	Quaternion rotation_offset_from_bone;

	bool fetchHandles();
	void fetchHandlesOnLoad(TEntityParseContext & ctx);
public:
	void load(const json& j, TEntityParseContext & ctx);
	void update(float dt);
	void debugInMenu();
	
	static void registerMsgs();
	void onGroupCreated(const TMsgEntitiesGroupCreated & msg);

	void setNewBoneAndParentToTrack(const std::string & parentName, const std::string & boneName);
	void setNewBoneToTrack(const std::string & boneName);

	/* Getters y setters. */
	const std::string & getBoneNameTracked() const { return bone_name; }
	const Vector3 & getBoneOffset() const { return offset_from_bone; }
	void setBoneOffset(Vector3 & boneOffset) { offset_from_bone = boneOffset; }
	const Quaternion & getRotationOffset() const { return rotation_offset_from_bone; }
	void setBoneRotationOffset(Quaternion & boneRotationOffset) { rotation_offset_from_bone = boneRotationOffset; }
};

#endif