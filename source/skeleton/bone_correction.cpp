#include "mcv_platform.h"
#include "bone_correction.h"
#include "cal3d/cal3d.h"
#include "cal3d2engine.h"

#include "components/common/comp_transform.h"

/* 
	Each bone correction which apply a local transform to a bone
	such as that one local direction aims to a world direction

	We have to choose which local direction we want to aim to that 
	target direction, as the artist can set any axis to any direction

	We normally want the 'front' local direction to aim to use as
	direction to modify, but sometimes it's the green axis, sometimes another
	Use that bone_ids_to_debug or the imgui to 'view' those directions.
  
	The last correction should have the amount = 1 to force a proper lookat
*/

void TBoneCorrection::load(const json& j) {
	// Name and ID of the bone to affect.
	bone_name = j.value("bone", "");
	bone_id = -1;
	// Axis to correct in the bone.
	local_axis_to_correct = loadVector3(j["local_axis_to_correct"]);
	// Amount of correction to perform.
	amount = j.value("amount", 0.f);
	maxYaw = DEG2RAD(j.value("maxYaw", maxYaw));
	maxPitch = DEG2RAD(j.value("maxPitch", maxPitch));
}

void TBoneCorrection::debugInMenu() {
	ImGui::LabelText("Bone", "%s (Id:%d)", bone_name.c_str(), bone_id);
	ImGui::DragFloat("Amount", &amount, 0.01f, 0.0f, 1.0f);
	ImGui::InputFloat3("Local Axis", &local_axis_to_correct.x);
}

// This function returns the rotation to perform between two points. This is
// used to apply one degree IKs.
QUAT getRotationFromAToB(VEC3 A, VEC3 B, float unit_amount) {
	// Returns te axis of rotation between the vectors.
	VEC3 rotation_axis = A.Cross(B);
	rotation_axis.Normalize();

	// Get the angle to rotate to get from one point to the other. Multiplies it
	// by the amount of rotation to do. Returns the final angle to move.
	VEC3 angle_between_a_b = DirectX::XMVector3AngleBetweenVectors(A, B);
	float angle = angle_between_a_b.x * unit_amount;

	// Nothing to correct. Return no rotation.
	if (fabsf(angle) < 1e-2f)
	return QUAT::Identity;

	// Return the rotation based on the rotation axis and the angle to move.
	return QUAT::CreateFromAxisAngle(rotation_axis, angle);
}

void TBoneCorrection::apply(CalSkeleton * skel, VEC3 dx_world_target, float external_unit_amount) const {
	CalVector world_target = DX2Cal(dx_world_target);

	// We want to correct a bone by name, but we need a bone_id to access that bone
	if (bone_id == -1) return;  // The bone_name does not exists in the skel...

	CalBone * bone = skel->getBone(bone_id);
	assert(bone);

	// Where is the bone now in abs coords.
	CalVector bone_abs_pos = bone->getTranslationAbsolute();

	// In world coordinate system... Get the direction the bone needs to move to.
	CalVector abs_dir = world_target - bone_abs_pos;

	// getRotationAbsolute goes from local to abs, so I need the inverse
	CalQuaternion rot_abs_to_local = bone->getRotationAbsolute();
	rot_abs_to_local.invert();

	// Convert from absolute coords to local bone coords
	CalVector local_dir = abs_dir;
	local_dir *= rot_abs_to_local;

	// Find a quaternion to rotate 'local_axis_to_correct' to 'local_dir'
	CalQuaternion q_rot = DX2Cal(getRotationFromAToB(local_axis_to_correct, Cal2DX(local_dir), amount * external_unit_amount));

	// Add the correction before the current bone
	CalQuaternion new_rot = q_rot;
	new_rot *= bone->getRotation();

	bone->setRotation(new_rot);
	bone->calculateState();
}

void TBoneCorrection::applyBounded(CalSkeleton * skel, VEC3 dx_world_target, float external_unit_amount, const TCompTransform * modelTransform, BoneStatus & boneState) const {
	CalVector world_target = DX2Cal(dx_world_target);

	// We want to correct a bone by name, but we need a bone_id to access that bone
	if (bone_id == -1) return;  // The bone_name does not exists in the skel...

	CalBone * bone = skel->getBone(bone_id);
	assert(bone);

	// Where is the bone now in abs coords.
	CalVector bone_abs_pos = bone->getTranslationAbsolute();

	// In world coordinate system... Get the direction the bone needs to move to.
	CalVector abs_dir = world_target - bone_abs_pos;

	getBoneFinalDirection(modelTransform, abs_dir, boneState);

	// getRotationAbsolute goes from local to abs, so I need the inverse
	CalQuaternion rot_abs_to_local = bone->getRotationAbsolute();
	rot_abs_to_local.invert();

	// Convert from absolute coords to local bone coords
	CalVector local_dir = abs_dir;
	local_dir *= rot_abs_to_local;

	// Find a quaternion to rotate 'local_axis_to_correct' to 'local_dir'
	CalQuaternion q_rot = DX2Cal(getRotationFromAToB(local_axis_to_correct, Cal2DX(local_dir), amount * external_unit_amount));

	// Add the correction before the current bone
	CalQuaternion new_rot = q_rot;
	new_rot *= bone->getRotation();

	bone->setRotation(new_rot);
	bone->calculateState();
}

void TBoneCorrection::applyBoundedInstantly(CalSkeleton * skel, VEC3 dx_world_target, float external_unit_amount, const TCompTransform * modelTransform, BoneStatus & boneState) const {
	CalVector world_target = DX2Cal(dx_world_target);

	// We want to correct a bone by name, but we need a bone_id to access that bone
	if (bone_id == -1) return;  // The bone_name does not exists in the skel...

	CalBone * bone = skel->getBone(bone_id);
	assert(bone);

	// Where is the bone now in abs coords.
	CalVector bone_abs_pos = bone->getTranslationAbsolute();

	// In world coordinate system... Get the direction the bone needs to move to.
	CalVector abs_dir = world_target - bone_abs_pos;

	getBoneFinalDirectionInstantly(modelTransform, abs_dir, boneState);

	// getRotationAbsolute goes from local to abs, so I need the inverse
	CalQuaternion rot_abs_to_local = bone->getRotationAbsolute();
	rot_abs_to_local.invert();

	// Convert from absolute coords to local bone coords
	CalVector local_dir = abs_dir;
	local_dir *= rot_abs_to_local;

	// Find a quaternion to rotate 'local_axis_to_correct' to 'local_dir'
	CalQuaternion q_rot = DX2Cal(getRotationFromAToB(local_axis_to_correct, Cal2DX(local_dir), amount * external_unit_amount));

	// Add the correction before the current bone
	CalQuaternion new_rot = q_rot;
	new_rot *= bone->getRotation();

	bone->setRotation(new_rot);
	bone->calculateState();
}

void TBoneCorrection::getBoneFinalDirection(const TCompTransform * modelTransform, CalVector & abs_dir, BoneStatus & boneState) const {
	// Ideally, we should use the transform of the bone in it's core position and rotated according
	// to the skeleton current rotation. But, due to the fact our actual bones transform are
	// terribly broken and don't follow a logical setup (atleast when I have been coding this) , we use the player transform instead.
	// If we would like to do it right, we should store the base core transform from core skeleton
	// and rotate it according to the skeleton current transform.
	float boneYaw, bonePitch;
	modelTransform->getAngles(&boneYaw, &bonePitch);

	// Get the delta yaw and pitch.
	float deltaYaw = modelTransform->getDeltaYawToAimToFromDir(Cal2DX(abs_dir));
	float deltaPitch = modelTransform->getDeltaPitchToAimToFromDir(Cal2DX(abs_dir));

	// Check whether the delta is too big for the bone for yaw.
	if (deltaYaw > maxYaw)
		deltaYaw = maxYaw;
	else if (deltaYaw < -maxYaw)
		deltaYaw = -maxYaw;

	// Check whether the delta is too big for the bone for pitch.
	if (deltaPitch > maxPitch)
		deltaPitch = maxPitch;
	else if (deltaPitch < -maxPitch)
		deltaPitch = -maxPitch;

	// Yaw correction.

	// Now calculate the actual yaw of the bone, we use this to avoid hard changes.
	// We are considering the current yaw as the delta yaw in this type of movement.
	if (deltaYaw >= boneState.currentDeltaYaw)
		boneState.currentDeltaYaw = Maths::clamp(boneState.currentDeltaYaw + Time.delta * boneState.boneRotationSpeed, boneState.currentDeltaYaw, deltaYaw);
	else
		boneState.currentDeltaYaw = Maths::clamp(boneState.currentDeltaYaw - Time.delta * boneState.boneRotationSpeed, deltaYaw, boneState.currentDeltaYaw);

	// Pitch correction.

	// Now calculate the actual pitch of the bone, we use this to avoid hard changes.
	// We are considering the current pitch as the delta pitch in this type of movement.
	if (deltaPitch >= boneState.currentDeltaPitch)
		boneState.currentDeltaPitch = Maths::clamp(boneState.currentDeltaPitch + Time.delta * boneState.boneRotationSpeed, boneState.currentDeltaPitch, deltaPitch);
	else
		boneState.currentDeltaPitch = Maths::clamp(boneState.currentDeltaPitch - Time.delta * boneState.boneRotationSpeed, deltaPitch, boneState.currentDeltaPitch);

	abs_dir = DX2Cal(yawPitchToVector(boneYaw + boneState.currentDeltaYaw, bonePitch + boneState.currentDeltaPitch));
}

void TBoneCorrection::getBoneFinalDirectionInstantly(const TCompTransform * modelTransform, CalVector & abs_dir, BoneStatus & boneState) const {
	// Ideally, we should use the transform of the bone in it's core position and rotated according
	// to the skeleton current rotation. But, due to the fact our actual bones transform are
	// terribly broken and don't follow a logical setup (atleast when I have been coding this), we use the player transform instead.
	// If we would like to do it right, we should store the base core transform from core skeleton
	// and rotate it according to the skeleton current transform.
	float baseYaw, basePitch;
	modelTransform->getAngles(&baseYaw, &basePitch);

	// Get the delta yaw and pitch.
	float deltaYaw = modelTransform->getDeltaYawToAimToFromDir(Cal2DX(abs_dir));
	float deltaPitch = modelTransform->getDeltaPitchToAimToFromDir(Cal2DX(abs_dir));

	// Check whether the delta is too big for the bone for yaw.
	if (deltaYaw > maxYaw)
		deltaYaw = maxYaw;
	else if (deltaYaw < -maxYaw)
		deltaYaw = -maxYaw;

	// Check whether the delta is too big for the bone for pitch.
	if (deltaPitch > maxPitch)
		deltaPitch = maxPitch;
	else if (deltaPitch < -maxPitch)
		deltaPitch = -maxPitch;

	// Yaw correction.
	boneState.currentDeltaYaw = deltaYaw;

	// Pitch correction.
	boneState.currentDeltaPitch = deltaPitch;

	abs_dir = DX2Cal(yawPitchToVector(baseYaw + boneState.currentDeltaYaw, basePitch + boneState.currentDeltaPitch));
}