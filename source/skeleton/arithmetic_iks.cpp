#include "mcv_platform.h"
#include "arithmetic_iks.h"

#include "bone_correction.h"
#include "game_core_skeleton.h"
#include "cal3d2engine.h"

/* Arithmetic resolution. */

void TIKArithmeticBonesResolution::load(const json & j, CalModel * model) {
	isActive = j.value("active", false);

	// Get the three bones that will be used by the IK.
	// C is the bone that must be moved to the position.
	// A is the bone that acts as first motor.
	// B is the bone that must be found it's new position and acts as second motor.
	loadBones(j, model);

	// Get the target or entity that must be followed.
	loadTarget(j);

	// Get a weight controller if there is one.
	weightController = WeightController::WeightControllerManager::Instance().createState(j.value("weightControllerType", ""));
	if (weightController)
		weightController->load(j);

	// Get targetting speed, by default -1.0f, this equals, no change.
	speed_target_following = j.value("targetFollowingSpeed", -1.0f);
}

void TIKArithmeticBonesResolution::loadBones(const json & j, CalModel * model) {
	std::string bone_c_name = j.value("bone_name_c", "");
	std::string bone_b_name = j.value("bone_name_b", "");
	std::string bone_a_name = j.value("bone_name_a", "");

	// Fetch the ids for the given bone.
	bone_id_c = model->getSkeleton()->getCoreSkeleton()->getCoreBoneId(bone_c_name);
	bone_id_b = model->getSkeleton()->getCoreSkeleton()->getCoreBoneId(bone_b_name);
	bone_id_a = model->getSkeleton()->getCoreSkeleton()->getCoreBoneId(bone_a_name);

	// Check bone C is correct.
	assert(bone_id_c != -1 && "Bone C ID for this lookat was not found. Make sure you set a correct bone name and the bone exists in the skeleton.");

	// If b was not defined, then, select c's parent by default.
	if (bone_id_b == -1) {
		CalBone * bone_C = model->getSkeleton()->getBone(bone_id_c);
		assert(bone_C);
		bone_id_b = bone_C->getCoreBone()->getParentId();
	}

	// Check bone B is correct.
	assert(bone_id_b != -1 && "Bone B ID for this lookat was not found. Make sure you set a correct bone name and the bone exists in the skeleton.");

	// If A was not defined, then, select b's parent by default.
	if (bone_id_a == -1) {
		CalBone * bone_B = model->getSkeleton()->getBone(bone_id_b);
		assert(bone_B);
		bone_id_a = bone_B->getCoreBone()->getParentId();
	}

	// Check bone A is correct.
	assert(bone_id_a != -1 && "Bone A ID for this lookat was not found. Make sure you set a correct bone name and the bone exists in the skeleton.");

	// Check bones are in the given skeleton.
	CalBone * cal_bone_c = model->getSkeleton()->getBone(bone_id_c);
	CalBone * cal_bone_b = model->getSkeleton()->getBone(bone_id_b);
	CalBone * cal_bone_a = model->getSkeleton()->getBone(bone_id_a);
	assert(cal_bone_c != nullptr && "Bone C was not found in the skeleton. Make sure the skeleton is correct.");
	assert(cal_bone_b != nullptr && "Bone B was not found in the skeleton. Make sure the skeleton is correct.");
	assert(cal_bone_a != nullptr && "Bone A was not found in the skeleton. Make sure the skeleton is correct.");

	// Get distance from A to B looking at the skeleton in the bind pose. This will return the length of the bone that goes from x to y.
	ABLength = (cal_bone_b->getCoreBone()->getTranslationAbsolute() - cal_bone_a->getCoreBone()->getTranslationAbsolute()).length();
	BCLength = (cal_bone_c->getCoreBone()->getTranslationAbsolute() - cal_bone_b->getCoreBone()->getTranslationAbsolute()).length();
}


void TIKArithmeticBonesResolution::loadTarget(const json & j) {
	isTargettingEntity = j.find("target_entity_name") != j.end();
	std::string targetEntityName = j.value("target_entity_name", "");
	Vector3 targetPos = loadVector3(j, "target_pos");
}

void TIKArithmeticBonesResolution::update(const TCompTransform * transform, CalModel * model, float dt) {
	// If we have a weight controller, we let the weight controller decide if to keep active or not.
	// The weight controller should have recieved the setWeightControllerStatus call and start closing
	// if necessary, once it closes, it will set the is active to false.
	if (weightController && !weightController->getWeightControllerStatus()) return;
	else if (!isActive) return;

	// If we are targetting an entity, fetch it.
	fetchEntityTargetting();

	// If we have a target entity, use it to assign a target position
	updateTargetPos(dt);

	// Calculate the amount for the given animation.
	if (weightController)
		weightController->update(dt, amount);

	// Get the bones of the skeleton that is being animated. (Not the core one).
	CalBone * bone_C = model->getSkeleton()->getBone(bone_id_c);
	CalBone * bone_B = model->getSkeleton()->getBone(bone_id_b);
	CalBone * bone_A = model->getSkeleton()->getBone(bone_id_a);

	// Feed the IK Handle struct.
	ik.A = Cal2DX(bone_A->getTranslationAbsolute());
	ik.B = Cal2DX(bone_B->getTranslationAbsolute());
	ik.C = Cal2DX(bone_C->getTranslationAbsolute());
	ik.AB = ABLength;
	ik.BC = BCLength;
	ik.normal = (ik.C - ik.A).Cross(ik.B - ik.A); // Normal vector (allows the solver to discriminate between all the possible solutions, telling it how it must rotate).
	ik.normal.Normalize();

	// Set C to the position it must be at.
	ik.C = current_target_pos;

	// Find B.
	ik.solveB();

	// Now, that we have the point b, we rotate both A and B to get to the desired state.
	CalSkeleton * skeleton = model->getSkeleton();

	TBoneCorrection bcA;
	bcA.bone_id = bone_id_a;
	bcA.amount = 1.f;
	bcA.local_axis_to_correct = Vector3(1, 0, 0);
	bcA.apply(skeleton, ik.B, amount); // A point to B.

	TBoneCorrection bcB;
	bcB.bone_id = bone_id_b;
	bcB.amount = 1.f;
	bcB.local_axis_to_correct = Vector3(1, 0, 0);
	bcB.apply(skeleton, ik.C, amount); // B point to C.
}

void TIKArithmeticBonesResolution::renderInMenu() {
	ImGui::DragFloat3("Target", &target.x, 0.01f, -1.f, 1.f);
	if (isTargettingEntity)
		ImGui::Text("Target Name", "%s", target_entity_name);

	ImGui::DragFloat3("Current Target Position", &current_target_pos.x, 0.01f, -1.f, 1.f);
	ImGui::DragFloat("Amount", &amount, 0.01f, 0.f, 1.f);

	ImGui::Text("Amount control variables");
	ImGui::Spacing();
	if (weightController)
		weightController->renderInMenu();
	ImGui::Spacing();

	ImGui::Text("Speed Targetting Following %f", speed_target_following);

	ImGui::Spacing(0, 5);

	ImGui::Text("IK Bones");
	ImGui::Spacing(0, 5);
	ImGui::DragFloat3("IK A", &ik.A.x, 0.05f, -10.f, 10.f);
	ImGui::DragFloat3("IK B", &ik.B.x, 0.05f, -10.f, 10.f);
	ImGui::DragFloat3("IK C", &ik.C.x, 0.05f, -10.f, 10.f);
	ImGui::DragFloat3("IK Normal", &ik.normal.x, 0.05f, -1.f, 1.f);
	ImGui::LabelText("h", "%f", ik.h);
	ImGui::DragFloat("AB", &ik.AB, 0.02f, 0.1f, 10.f);
	ImGui::DragFloat("BC", &ik.BC, 0.02f, 0.1f, 10.f);
	ImGui::LabelText("IK State", "%d", ik.state);
}

void TIKArithmeticBonesResolution::renderInDebug() {
	// Green if IK is correct, red otherwise.
	Vector4 ik_color(0, 1, 0, 1);
	if (ik.state != TIKHandle::NORMAL)
		ik_color = Vector4(1, 0, 0, 1);

	drawLine(ik.A, ik.B, ik_color);
	drawLine(ik.B, ik.C, ik_color);

	Vector3 dir_ac = (ik.C - ik.A);
	dir_ac.Normalize();
	Vector3 perp_dir = ik.normal.Cross(dir_ac);
	perp_dir.Normalize();

	Vector3 A_a1 = ik.A + dir_ac * ik.a1;
	drawLine(A_a1, A_a1 + dir_ac, Vector4(0, 1, 1, 1));
	drawLine(A_a1, A_a1 + ik.normal, Vector4(1, 0, 1, 1));
	drawLine(A_a1, A_a1 + perp_dir, Vector4(1, 1, 0, 1));
}