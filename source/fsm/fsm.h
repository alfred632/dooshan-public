#pragma once

#include "resources/resource.h"
#include "fsm/variable.h"
#include "fsm/common/transitions/onFunction.h"

class IState;
class ITransition;

class CFSM : public IResource
{
	~CFSM();

public:
	/* Loads the fsm from a file creating all states and transitions. */
	bool load(const std::string & file);
  
	// Registers a function for the given transition.
	void registerTransitionFunction(const std::string & functionName, transitionFunc nFunc);

	void renderInMenu() override;

	/* Getters. */
	IState* getState(const std::string & name) const;
	IState* getStartingState() const { return _startState; }
	std::vector<IState*> getStates() const { return _states; }
	std::vector<TVariable> & getVariables() { return _variables; }

private:
	// Starting state.
	IState* _startState = nullptr;


	// Vectors with pointers to the states, transitions and variables of the machine.
	std::vector<IState*> _states;
	std::vector<ITransition *> _transitions;
	std::map<std::string, std::vector<COnFunctionAnimation *>> _functionTransitions; // Stores transitions that use function pointers and must be declared from outside.

	std::vector<TVariable> _variables;
};
