#pragma once

#include "mcv_platform.h"
#include "fsm/transition.h"

/* On function transition for fsm managing animations. Can set the fade in parameter. */

typedef bool (* transitionFunc)();

class COnFunctionAnimation : public ITransition
{
	transitionFunc function;
	int condModif = 1; // 1 or -1. Allows negation.

	std::string stateToTransitionTo;
	
	float fadeInNextState;

public:
  void load(const json& jData) override;
  bool check(const CFSMContext& ctx) const override;

	void setFunction(transitionFunc nFunc) { function = nFunc; }
};
