#include "mcv_platform.h"

#include "movement.h"


void CEMMovement::update(CFSMContext& ctx, float dt) {
	
	float speed = std::get<float>(ctx.getVariable("speed")->_value);
	_blenders[_rand_movement].setValueBlendParameter(speed);
	
	float fade = std::max(_fadeIn -  ctx.getTimeInState(), _weight);
	_blenders[_rand_movement].updateWeights(ctx.getSkeletonComp(), fade);
	
}

void CEMMovement::onEnter(CFSMContext& ctx) {

	_rand_movement = RNG.i(2);
	_blenders = ctx.getStateBlenders(_name);

	float speed = std::get<float>(ctx.getVariable("speed")->_value);

	
	_blenders[_rand_movement].setValueBlendParameter(speed);
	_blenders[_rand_movement].updateWeights(ctx.getSkeletonComp(), _weight);

}

void CEMMovement::onExit(CFSMContext& ctx) {
	_blenders[_rand_movement].endMotions(ctx.getSkeletonComp(), _fadeOut);
}
