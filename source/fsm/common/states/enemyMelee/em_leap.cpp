#include "mcv_platform.h"
#include "engine.h"
#include "fsm/module_fsm.h"
#include "em_leap.h"

void CEMLeap::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx))
		ctx.setVariable(TVariable("leaping", false));
}

void CEMLeap::onEnter(CFSMContext& ctx) {
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn, 0.0f, true);
}

void CEMLeap::onExit(CFSMContext& ctx) {
	if(isAnimationOver(ctx))
		ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}

bool CEMLeap::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	return c_skel->getAnimationElapsedTime(animationID) >= (c_skel->getAnimationDuration(animationID) - _fadeOut) || c_skel->isAnimationOver(animationID);
}
