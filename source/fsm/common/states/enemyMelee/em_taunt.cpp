#include "mcv_platform.h"
#include "engine.h"
#include "fsm/module_fsm.h"
#include "em_taunt.h"

void CEMTaunt::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx))
		ctx.setVariable(TVariable("taunting", false));
}

void CEMTaunt::onEnter(CFSMContext& ctx) {
	
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn);
}

void CEMTaunt::onExit(CFSMContext& ctx) {
	if(isAnimationOver(ctx))
		ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}

bool CEMTaunt::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	return c_skel->getAnimationElapsedTime(animationID) >= (c_skel->getAnimationDuration(animationID) - _fadeOut) || c_skel->isAnimationOver(animationID);
}
