#include "mcv_platform.h"
#include "engine.h"
#include "fsm/module_fsm.h"
#include "ei_attack.h"

void CEIAttack::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx))
		ctx.setVariable(TVariable("attacking", false));
}

void CEIAttack::onEnter(CFSMContext& ctx) {
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn);
}

void CEIAttack::onExit(CFSMContext& ctx) {
	if(isAnimationOver(ctx))
		ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}

bool CEIAttack::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	return c_skel->getAnimationElapsedTime(animationID) >= (c_skel->getAnimationDuration(animationID) - _fadeOut) || c_skel->isAnimationOver(animationID);
}