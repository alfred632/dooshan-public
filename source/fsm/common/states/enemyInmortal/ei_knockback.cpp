#include "mcv_platform.h"
#include "engine.h"
#include "fsm/module_fsm.h"
#include "ei_knockback.h"

void CEIKnockback::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx))
		ctx.setVariable(TVariable("knockback", -1));
}

void CEIKnockback::onEnter(CFSMContext& ctx) {
	_knockbackId = KnockbackAnim(std::get<int>(ctx.getVariable("knockback")->_value));

	ctx.getSkeletonComp()->PlayAnimation(_animations[_knockbackId].filename, _weight, _fadeIn, _fadeOut);
}

void CEIKnockback::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(_animations[_knockbackId].filename, _fadeOut);
}

bool CEIKnockback::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	int knockBackID = c_skel->getAnimationID(_animations[_knockbackId].filename);
	return c_skel->getAnimationElapsedTime(knockBackID) >= (c_skel->getAnimationDuration(knockBackID) - _fadeOut) || c_skel->isAnimationOver(knockBackID);
}