#include "mcv_platform.h"
#include "engine.h"
#include "fsm/module_fsm.h"
#include "er_idle_war.h"

void CERIdleWar::update(CFSMContext& ctx, float dt) {
	float speed = std::get<float>(ctx.getVariable("speed")->_value);
	_blenders[0].setValueBlendParameter(speed);

	float fade = std::max(_fadeIn - ctx.getTimeInState(), _weight);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), fade);
}

void CERIdleWar::onEnter(CFSMContext& ctx) {
	_blenders = ctx.getStateBlenders(_name);

	float speed = std::get<float>(ctx.getVariable("speed")->_value);


	_blenders[0].setValueBlendParameter(speed);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), _weight);
}

void CERIdleWar::onExit(CFSMContext& ctx) {
	_blenders.at(0).endMotions(ctx.getSkeletonComp(), _fadeOut);
}
