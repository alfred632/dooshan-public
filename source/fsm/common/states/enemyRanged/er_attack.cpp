#include "mcv_platform.h"
#include "engine.h"
#include "fsm/module_fsm.h"
#include "er_attack.h"

void CERAttack::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx))
		ctx.setVariable(TVariable("attacking", false));
}

void CERAttack::onEnter(CFSMContext& ctx) {
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn, _fadeOut);
}

void CERAttack::onExit(CFSMContext& ctx) {
	if (isAnimationOver(ctx))
		ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}

bool CERAttack::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	return c_skel->getAnimationElapsedTime(animationID) >= (c_skel->getAnimationDuration(animationID) - _fadeOut) || c_skel->isAnimationOver(animationID);
}
