#include "mcv_platform.h"
#include "pub_aim.h"


void CPUBAim::update(CFSMContext& ctx, float dt) {
	
	float aimAngle = std::get<float>(ctx.getVariable("aimAngle")->_value);
	_blenders[0].setValueBlendParameter(aimAngle);
	
	float fade = std::max(_fadeIn -  ctx.getTimeInState(), _weight);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), fade);
	
}

void CPUBAim::onEnter(CFSMContext& ctx) {


	_blenders = ctx.getStateBlenders(_name);

	float aimAngle = std::get<float>(ctx.getVariable("aimAngle")->_value);

	
	_blenders[0].setValueBlendParameter(aimAngle);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), _weight);

}

void CPUBAim::onExit(CFSMContext& ctx) {
	_blenders[0].endMotions(ctx.getSkeletonComp(), _fadeOut);
}
