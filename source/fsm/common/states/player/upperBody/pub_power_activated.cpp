#include "mcv_platform.h"
#include "pub_power_activated.h"
#include "components/animations/comp_player_animator.h"


void CPUBPowerActivated::update(CFSMContext& ctx, float dt) {
	if (ctx.getSkeletonComp()->isAnimationOver(animationID)) {
		TCompPlayerAnimator* a = _animator;
		a->setVariable("isPowerActivated", false);
	}
}

void CPUBPowerActivated::onEnter(CFSMContext& ctx) {
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn);
	CEntity* e = ctx.getOwner();
	_animator = e->getComponent<TCompPlayerAnimator>();
}

void CPUBPowerActivated::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}
