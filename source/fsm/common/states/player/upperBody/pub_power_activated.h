#pragma once

#include "fsm/context.h"
#include "fsm/state.h"

class CFSMContext;
// Class Player UpperBody Aim
class CPUBPowerActivated: public IState
{
	
	CHandle _animator;
public:

  void update(CFSMContext& ctx, float dt) override;
  void onEnter(CFSMContext& ctx)  override;
  void onExit(CFSMContext& ctx)  override;

};
