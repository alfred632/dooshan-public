#include "mcv_platform.h"
#include "pfb_menu_idle.h"
#include "components/animations/comp_player_menu_animator.h"



void CPFBMenuIdle::load(const json & jData)
{
}

void CPFBMenuIdle::update(CFSMContext& ctx, float dt) {}

void CPFBMenuIdle::onEnter(CFSMContext& ctx) {
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn, _fadeOut);

	CEntity* e = ctx.getOwner();
	_animator = e->getComponent<TCompPlayerMenuAnimator>();
}

void CPFBMenuIdle::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}
