#include "mcv_platform.h"
#include "pfb_menu_start.h"
#include "components/animations/comp_player_menu_animator.h"



void CPFBMenuStart::load(const json & jData)
{
}

void CPFBMenuStart::update(CFSMContext& ctx, float dt) {
	if (isAnimationOver(ctx)) {
		TCompPlayerMenuAnimator* a = _animator;
		a->setMenuState(2);
	}
}

void CPFBMenuStart::onEnter(CFSMContext& ctx) {
	ctx.getSkeletonComp()->PlayAnimation(animationID, _weight, _fadeIn, _fadeOut);

	CEntity* e = ctx.getOwner();
	_animator = e->getComponent<TCompPlayerMenuAnimator>();
}

void CPFBMenuStart::onExit(CFSMContext& ctx) {
	ctx.getSkeletonComp()->RemoveAnimation(animationID, _fadeOut);
}

bool CPFBMenuStart::isAnimationOver(CFSMContext& ctx) {
	TCompSkeleton* c_skel = ctx.getSkeletonComp();
	return c_skel->getAnimationElapsedTime(animationID) >= (c_skel->getAnimationDuration(animationID) - _fadeOut) || c_skel->isAnimationOver(animationID);
}
