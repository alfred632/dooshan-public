#include "mcv_platform.h"
#include "plb_strafing.h"


void CPLBStrafing::update(CFSMContext& ctx, float dt) {
	
	float fade = std::max(_fadeIn -  ctx.getTimeInState(), _weight);
	
	float xSpeed = std::get<float>(ctx.getVariable("xSpeed")->_value);
	float zSpeed = std::get<float>(ctx.getVariable("zSpeed")->_value); 

	// We check if the player is moving horizontally only, because as we need two blenders and one need an idle animation
	// and thus if the player is moving horizontally, the x blender we set it to apply a dummy animation, and not the idle one
	bool isMovingHorizontalOnly = (xSpeed > 0.05f || xSpeed < -0.05f) && (zSpeed < 0.05f || zSpeed > -0.05f);
	if (isMovingHorizontalOnly)
		_blenders[0].setValueBlendParameter(20.0f);
	else
		_blenders[0].setValueBlendParameter(zSpeed);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), fade);
		
	_blenders[1].setValueBlendParameter(xSpeed);
	_blenders[1].updateWeights(ctx.getSkeletonComp(), fade);

	
}

void CPLBStrafing::onEnter(CFSMContext& ctx) {


	_blenders = ctx.getStateBlenders(_name);

	float zSpeed = std::get<float>(ctx.getVariable("zSpeed")->_value);
	float xSpeed = std::get<float>(ctx.getVariable("xSpeed")->_value);

	
	_blenders[0].setValueBlendParameter(zSpeed);
	_blenders[0].updateWeights(ctx.getSkeletonComp(), _weight);

	_blenders[1].setValueBlendParameter(xSpeed);
	_blenders[1].updateWeights(ctx.getSkeletonComp(), _weight);

}

void CPLBStrafing::onExit(CFSMContext& ctx) {
	_blenders[0].endMotions(ctx.getSkeletonComp(), _fadeOut);
	_blenders[1].endMotions(ctx.getSkeletonComp(), _fadeOut);
}
