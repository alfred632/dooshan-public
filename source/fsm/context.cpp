#include "mcv_platform.h"
#include "fsm/context.h"
#include "fsm/fsm.h"
#include "fsm/state.h"
#include "fsm/transition.h"
#include "fsm/variable.h"
#include "entity/entity.h"
#include "skeleton/comp_skeleton.h"

void CFSMContext::init(CFSM * fsm, CHandle owner)
{
  stop();

  _fsm = fsm;
  for (auto& var : _fsm->getVariables())
    setVariable(var);

  _owner = owner;

  // Init skeleton
  CEntity* owner_entity = owner;
  _skeleton = owner_entity->getComponent<TCompSkeleton>();
  assert(_skeleton.isValid() && "Owner of this context fsm does not have a valid skeleton");


  // Blenders
  TCompSkeleton* skel = _skeleton;
  const json& j = Utils::loadJson(_fsm->getName());
  const json& blenders = j["blenders"];
  for (const auto& b : blenders) {
	  const std::string fsm_state = b["state"];
	  //IAnimationState* state = (IAnimationState*) _fsm->getState(fsm_state);
	  const json& tresholds = b["thresholds"];

	  /* Right now we re only using one dimensional blenders */
	  OneDimensionalBlender blender;
	  for (const auto& t : tresholds) {
		  const std::string aName = t["animation_name"];
		  const AnimationID aId = skel->getAnimationID(aName);
		  const float tValue = t["threshold"];
		  const bool isDummy =   t.value("isDummy", false);

          blender.addMotion(aId, tValue, isDummy);
	  }

	  _blenderInfo[fsm_state].push_back(blender);

}


  start();
}

void CFSMContext::update(float dt)
{
  if (_currentState)
  {
    _timeInState += dt;
    _currentState->update(*this, dt);

    for (const auto& tr : _currentState->_transitions)
    {
      if (tr->check(*this))
      {
        changeToState(tr->_target);
        break;
      }
    }
  }
}

void CFSMContext::changeToState(IState* newState)
{
  if (_currentState)
		_currentState->onExit(*this);
	
	_currentState = newState;
	_timeInState = 0.f;
	
	if (_currentState)
		_currentState->onEnter(*this);
}

void CFSMContext::start()
{
  _currentState = nullptr;
  if (_fsm)
  {
    changeToState(_fsm->getStartingState());
  }
}

void CFSMContext::stop()
{
  changeToState(nullptr);
}

TVariable* CFSMContext::getVariable(const std::string& name)
{
  const TVariable* var = const_cast<const CFSMContext*>(this)->getVariable(name);
  return const_cast<TVariable*>(var);
}

const TVariable* CFSMContext::getVariable(const std::string& name) const
{
  auto it = _variables.find(name);
  return it != _variables.end() ? &it->second : nullptr;
}

void CFSMContext::setVariable(const TVariable& var)
{
  _variables[var._name] = var;
}

IState * CFSMContext::getState(const std::string & name) const
{
	return _fsm->getState(name);
}

TCompSkeleton * CFSMContext::getSkeletonComp() const
{
	return (TCompSkeleton*)_skeleton;
}

IState * CFSMContext::getCurrentState() const
{
	return _currentState;
}

std::vector<OneDimensionalBlender> CFSMContext::getStateBlenders(std::string state)
{
	return _blenderInfo[state];
}

void CFSMContext::debugInMenu()
{
  ImGui::Text("Current state: %s", _currentState ? _currentState->_name.c_str() : "...");
  ImGui::Text("Time in state: %.2f", _timeInState);
  ImGui::Separator();

  if (ImGui::TreeNode("Variables"))
  {
    for (auto& var : _variables)
    {
      ImGui::Text("%s [%s] : %s", var.first.c_str(), var.second.getType().c_str(), var.second.toString().c_str());
    }

    ImGui::TreePop();
  }
}
